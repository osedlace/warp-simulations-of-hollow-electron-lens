"""
This file contains the configure parameters for BField preparation script.
This scripts takes a B-Field file from a CST preferably with mesh size 1mm.
The final B-Field file name remapped and ready to be imported into simulation is in variable: g_fName_BfieldFinal
Log of this script is saved after final BField file created (the one with name in g_fName_BfieldFinal)
The CST coordinates are called CAD in this and all other scripts.

Make sure:
1) If new B-Field from CST is used to check that variables containing first and last coordinates as well as number of header lines
2) That mesh_cellsPercm, channel_txy_mm, g_channel_len_mm, g_channel_zstart_mm correspond to the simulation settings (In configure file)
3) If you change the g_path or g_fName_BfieldFinal make sure that the final BField file is correctly imported into the simulation (In configure file)
"""

# Parameters of bfield file and box in the file in mm (in CAD coordinates)
g_num_headerLines =     2 # Number of header lines (to skip)
g_zfirst_file     = -2988 # The lowest z in the file
g_zlast_file      = -1400 # The highest z in the file
g_xfirst_file     =   -30 # The lowest x in the file
g_xlast_file      =    30 # The highest x in the file
g_yfirst_file     =   -30 # The lowest y in the file
g_ylast_file      =   663 # The highest y in the file

g_CathodeInCADcoord_z = -2816.87 # in mm
g_CathodeInCADcoord_x = 506.525 # in mm
# bfield_box_xs_CAD = zlower_box
# bfield_box_xs_lab = zlower_box - g_CathodeInCADcoord_z

mm = 1.e-3

g_mesh_cellsPercm   = 8 # Number of mesh cells per cm (of the Warp mesh)
g_channel_txy_mm    = 100. # The transverse size in mm (of the Warp mesh)
g_channel_len_mm    = 1570. # The length of the simulating box in mm (of the Warp mesh)
g_channel_zstart_mm = -10. # Starting point in mm (of the Warp mesh)

g_fName_BfieldOrigin_txt = "B-Field.txt" # Name of the CST B-Field file
g_fName_BfieldCutted = "BFieldCutted_Zfull.h5py" # File with Cropped (if needed), saved into h5py format to reduce the size
g_fName_BfieldReshaped = "BFieldCutted_Zfull_reshaped.h5py" # File after x and y is swapped (since y is horizontal in Warp and vertical coordinate in CST) 

g_path = "./txy{}/{}cpcm".format(g_channel_txy_mm,g_mesh_cellsPercm) # Path where to store the Mesh and final BField file.
g_fName_WarpedMeshInCAD = "{}/WarpedMeshInCAD.h5py".format(g_path) # File with Warped mesh transformed into CAD (CST) coordinates
g_fName_BfieldFinal = "{}/BFieldFinalWarped_Zfull.h5py".format(g_path) # File with final BField ready to be imported into main simulation script

g_channel_zend_mm   = g_channel_zstart_mm + g_channel_len_mm
g_channel_ri_mm     = g_channel_txy_mm/2. # The transverse size (or diameter if RZ geometry)
g_channel_ri_m      = g_channel_ri_mm/1000. # The transverse size (or diameter if RZ geometry)
g_channel_txy_m     = g_channel_txy_mm/1000.
g_channel_len_m     = g_channel_len_mm/1000.
g_channel_zstart_m  = g_channel_zstart_mm/1000.
g_channel_zend_m    = g_channel_zend_mm/1000.

#       Mesh size
g_ny = int(g_channel_txy_mm/10.)*g_mesh_cellsPercm
g_nx = int(g_channel_txy_mm/10.)*g_mesh_cellsPercm
g_nz = int(g_channel_len_mm/10.)*g_mesh_cellsPercm

#----------------------
#       Bend parameters
#----------------------
# Set bend parameters
g_bend_lab_len       = 0.22482
g_bend_angle_deg     = 30.
g_bend_start = 0.889
