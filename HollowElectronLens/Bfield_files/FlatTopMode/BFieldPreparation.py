import timeit
import numpy as np
import h5py 
import sys
import os
sys.path.append("../../../Libs")
from MagneticFieldTransformationHeader import *

import BFieldPreparation_Config as config
from optparse import OptionParser
parser = OptionParser(usage="Usage: %prog [options]")
parser.add_option("-d", "--do", action="store", type="str", dest="do", \
                   default="0", help="do: 0 - Do everything except 5, \
                   1 - Cutting, 2 - Reshaping, 3 - Warped mesh to CAD, \
                   4 - Aproximating, 5 - Additional analysis (can be added eg. 1,3,4) [default: %default]" )
(options, args) = parser.parse_args()

do = map(int, options.do.split(","))
BfileCut_flag = 1 in do or 0 in do
BfileReshape_flag = 2 in do or 0 in do
PreCalculateWarpedMeshInCADcoord_flag = 3 in do or 0 in do
BfileApproximating_flag = 4 in do or 0 in do
BfileAnalysis_flag = 5 in do 


# Currently only cutting in z dimension is possible... If needed cutting in other dimensions can be added quite easily
zlower_box = config.g_zfirst_file # The lower z of the box
zupper_box = config.g_zlast_file # The lower z of the box
xlower_box = config.g_xfirst_file  # The lowest x in the box
xupper_box = config.g_xlast_file  # The highest x in the box
ylower_box = config.g_yfirst_file  # The lowest y in the box
yupper_box = config.g_ylast_file  # The highest y in the box


# pbar = tqdm(total = 0) # Just for timing


#Cathode end is zero:
# lab - lab coordinates where cathode end is z = 0
# CAD - coordinates where 0 is at center of the system and cathode at -2816.87mm
# bfield_box_xs_CAD = zlower_box
# bfield_box_xs_lab = zlower_box - config.g_CathodeInCADcoord_z

# Adding the directory path
directoryPathToBfieldFiles = str(os.getcwd()) + "/"
config.g_fName_BfieldOrigin_txt = directoryPathToBfieldFiles+config.g_fName_BfieldOrigin_txt
config.g_fName_BfieldCutted = directoryPathToBfieldFiles+config.g_fName_BfieldCutted
config.g_fName_BfieldReshaped = directoryPathToBfieldFiles+config.g_fName_BfieldReshaped
config.g_fName_WarpedMeshInCAD = directoryPathToBfieldFiles+config.g_fName_WarpedMeshInCAD
config.g_fName_BfieldFinal = directoryPathToBfieldFiles+config.g_fName_BfieldFinal

if BfileCut_flag:
    # The loading of the Bfield from the .txt file, cutting it in z direction and then saving results into .h5py file for better compresion

    # Num of lines to Jump at the start of the file to jump over: 1) header lines, 2) config.g_zfirst_file->zlower_box = z lower than box
    numOfLinesToJumpAtStart = config.g_num_headerLines + (zlower_box-config.g_zfirst_file)*(config.g_xlast_file-config.g_xfirst_file + 1)*(config.g_ylast_file-config.g_yfirst_file + 1)
    # Calculating the num of lines to read -> this cuts the z higher than box which are therefore unnecesarry lines to read
    num_linestToRead = (zupper_box-zlower_box+1)*(config.g_xlast_file-config.g_xfirst_file+1)*(config.g_ylast_file-config.g_yfirst_file+1)
    # print("num_linestToRead = {}".format(num_linestToRead))

    x_r = np.zeros(num_linestToRead)
    y_r = np.zeros(num_linestToRead)
    z_r = np.zeros(num_linestToRead)
    bx_r = np.zeros(num_linestToRead)
    by_r = np.zeros(num_linestToRead)
    bz_r = np.zeros(num_linestToRead)

    # Calculate the range of the x,y,z dimension -> is currently used to replace x_r,... and therefore cuts the size of the file.
    
    print("\nStarting the loading and cutting of the file: {}\n".format(config.g_fName_BfieldOrigin_txt)) 
    pbar = tqdm(total = num_linestToRead + numOfLinesToJumpAtStart) # Just for timing
    with open(config.g_fName_BfieldOrigin_txt,"r") as Bfile_in:
        for line_i, line in enumerate(Bfile_in):
            if line_i==num_linestToRead + numOfLinesToJumpAtStart :
                break
            if line_i>=numOfLinesToJumpAtStart:
                x_r[line_i-numOfLinesToJumpAtStart],y_r[line_i-numOfLinesToJumpAtStart],z_r[line_i-numOfLinesToJumpAtStart],\
                bx_r[line_i-numOfLinesToJumpAtStart],by_r[line_i-numOfLinesToJumpAtStart],bz_r[line_i-numOfLinesToJumpAtStart] = map(lambda x: float(x),line.split())[:6]
            pbar.update(1)
    print("\nCompleted the loading and cutting of the file: {}\n".format(config.g_fName_BfieldOrigin_txt)) 
                
    x_small = np.array([config.g_xfirst_file + x_i for x_i in range(-config.g_xfirst_file+config.g_xlast_file+1)])
    y_small = np.array([config.g_yfirst_file + y_i for y_i in range(-config.g_yfirst_file+config.g_ylast_file+1)])
    z_small = np.array([zlower_box + z_i for z_i in range(-zlower_box+zupper_box+1)])

    with h5py.File(config.g_fName_BfieldCutted,"w") as Bfile_out:
        x_small_ds = Bfile_out.create_dataset("x_small", data = x_small)
        y_small_ds = Bfile_out.create_dataset("y_small", data = y_small)
        z_small_ds = Bfile_out.create_dataset("z_small", data = z_small)
        # x_r_ds = Bfile_out.create_dataset("x_r", data = x_r) # not needed as of now, because x_small can raplace it, in case of need just uncomment
        # y_r_ds = Bfile_out.create_dataset("y_r", data = y_r) # not needed as of now, because y_small can raplace it, in case of need just uncomment
        # z_r_ds = Bfile_out.create_dataset("z_r", data = z_r) # not needed as of now, because z_small can raplace it, in case of need just uncomment
        bx_r_ds = Bfile_out.create_dataset("bx_r", data = bx_r)
        by_r_ds = Bfile_out.create_dataset("by_r", data = by_r)
        bz_r_ds = Bfile_out.create_dataset("bz_r", data = bz_r)
        print("\nThe cutted arrays: {} ".format(Bfile_out.keys())) 
        print("were saved into file: {}\n".format(config.g_fName_BfieldCutted)) 

    # with open("./BFieldPreparation_Config.py","r") as config_in:
    #     with open("./Logs/LogConfig_{}.py".format("BFieldCutted_Zfull"),"w") as config_log:
    #         config_log.writelines(config_in.readlines())


if BfileReshape_flag:
    # The reshaping of bx,by,bz to 3d array and swapping the x and y
    with h5py.File(config.g_fName_BfieldCutted,"r") as Bfile_in:
        bx_r = np.array(Bfile_in["bx_r"])
        by_r = np.array(Bfile_in["by_r"])
        bz_r = np.array(Bfile_in["bz_r"])
        x_small = np.array(Bfile_in["x_small"])
        y_small = np.array(Bfile_in["y_small"])
        z_small = np.array(Bfile_in["z_small"])

    bx_3d_CAD = np.reshape(bx_r,((zupper_box-zlower_box+1),(config.g_ylast_file-config.g_yfirst_file+1),(config.g_xlast_file-config.g_xfirst_file+1)))
    by_3d_CAD = np.reshape(by_r,((zupper_box-zlower_box+1),(config.g_ylast_file-config.g_yfirst_file+1),(config.g_xlast_file-config.g_xfirst_file+1)))
    bz_3d_CAD = np.reshape(bz_r,((zupper_box-zlower_box+1),(config.g_ylast_file-config.g_yfirst_file+1),(config.g_xlast_file-config.g_xfirst_file+1)))

    print("\nStarting the swapping of the x and y\n") 
    pbar = tqdm(total = (config.g_xlast_file-config.g_xfirst_file+1)*(config.g_ylast_file-config.g_yfirst_file+1)*(zupper_box-zlower_box+1)) # Just for timing

    # The y in CAD corrseponds to x dimension in LAB and Warped
    x_CAD, y_CAD, z_CAD = y_small, x_small, z_small
    #From mm to m
    x_CAD, y_CAD, z_CAD = x_CAD/1000., y_CAD/1000., z_CAD/1000.
    bx_CAD = np.zeros(((config.g_ylast_file-config.g_yfirst_file+1),(config.g_xlast_file-config.g_xfirst_file+1),(zupper_box-zlower_box+1)))
    by_CAD = np.zeros(((config.g_ylast_file-config.g_yfirst_file+1),(config.g_xlast_file-config.g_xfirst_file+1),(zupper_box-zlower_box+1)))
    bz_CAD = np.zeros(((config.g_ylast_file-config.g_yfirst_file+1),(config.g_xlast_file-config.g_xfirst_file+1),(zupper_box-zlower_box+1)))

    for x_i in range(config.g_xlast_file-config.g_xfirst_file+1):
        for y_i in range(config.g_ylast_file-config.g_yfirst_file+1):
            for z_i in range(zupper_box-zlower_box+1):
                bx_CAD[y_i][x_i][z_i] = by_3d_CAD[z_i][y_i][x_i]
                by_CAD[y_i][x_i][z_i] = bx_3d_CAD[z_i][y_i][x_i]
                bz_CAD[y_i][x_i][z_i] = bz_3d_CAD[z_i][y_i][x_i]
                pbar.update(1)
    print("\nCompleted the swapping of the x and y\n") 

    with h5py.File(config.g_fName_BfieldReshaped,"w") as Bfile_out:
        x_CAD_ds = Bfile_out.create_dataset("x_CAD", data = x_CAD)
        y_CAD_ds = Bfile_out.create_dataset("y_CAD", data = y_CAD)
        z_CAD_ds = Bfile_out.create_dataset("z_CAD", data = z_CAD)
        bx_CAD_ds = Bfile_out.create_dataset("bx_CAD", data = bx_CAD)
        by_CAD_ds = Bfile_out.create_dataset("by_CAD", data = by_CAD)
        bz_CAD_ds = Bfile_out.create_dataset("bz_CAD", data = bz_CAD)
        print("\nThe reshaped arrays: {} ".format(Bfile_out.keys())) 
        print("were saved into file: {}\n".format(config.g_fName_BfieldReshaped)) 

    # with open("./BFieldPreparation_Config.py","r") as config_in:
    #     with open("./Logs/LogConfig_{}.py".format("BFieldCutted_Zfull_reshaped"),"w") as config_log:
    #         config_log.writelines(config_in.readlines())

if PreCalculateWarpedMeshInCADcoord_flag:
    #The Warped mesh
    ny = config.g_ny
    nx = config.g_nx
    nz = config.g_nz
    dz = 0.01/config.g_mesh_cellsPercm
    dx = dz
    dy = dz
    z_w = np.array([ config.g_channel_zstart_m + dz*z_i for z_i in range(nz+1)])
    x_w = np.array([-config.g_channel_txy_m/2. + dx*x_i for x_i in range(nx+1)])
    y_w = np.array([-config.g_channel_txy_m/2. + dy*y_i for y_i in range(ny+1)])
    print(np.shape(z_w))
    #       Bend parameters
    #----------------------
    # Set bend parameters
    # Calcuclated bend parameters
    bend_angle  = config.g_bend_angle_deg/360.*2.*np.pi
    bend_len    = config.g_bend_lab_len*bend_angle/np.sin(bend_angle)
    bend_end    = config.g_bend_start + bend_len
    bend_radius = bend_len/bend_angle
    bend_par    = [config.g_bend_start,bend_end,bend_radius]

    lab0inCAD_z    = config.g_CathodeInCADcoord_z/1000. # in m
    lab0inCAD_x    = config.g_CathodeInCADcoord_x/1000. # in m
    angle_labToCAD = bend_angle

    WarpedToCAD_Mesh1Din_3Dout(z_w, x_w, y_w, lab0inCAD_z, lab0inCAD_x, angle_labToCAD, bend_par, LoadFromFile_flag = False, SaveToFile_name = config.g_fName_WarpedMeshInCAD, timing = True)

    # with open("./BFieldPreparation_Config.py","r") as config_in:
    #     with open("./Logs/LogConfig_{}.py".format("WarpedMeshInCAD"),"w") as config_log:
    #         config_log.writelines(config_in.readlines())


if BfileApproximating_flag:
    # The calculating of the Warped mesh in CAD coordinates, then mapping-approximating the bfield on it and transforming back.

    #Loading the bfile bfield and its mesh
    with h5py.File(config.g_fName_BfieldReshaped,"r") as Bfile_in:
        x_CAD = np.array(Bfile_in["x_CAD"])
        y_CAD = np.array(Bfile_in["y_CAD"])
        z_CAD = np.array(Bfile_in["z_CAD"])
        bx_CAD = np.array(Bfile_in["bx_CAD"])
        by_CAD = np.array(Bfile_in["by_CAD"])
        bz_CAD = np.array(Bfile_in["bz_CAD"])

    #The Warped mesh
    ny = config.g_ny
    nx = config.g_nx
    nz = config.g_nz
    dz = 0.01/config.g_mesh_cellsPercm
    dx = dz
    dy = dz

    z_w = np.array([ config.g_channel_zstart_m + dz*z_i for z_i in range(nz+1)])
    x_w = np.array([-config.g_channel_txy_m/2. + dx*x_i for x_i in range(nx+1)])
    y_w = np.array([-config.g_channel_txy_m/2. + dy*y_i for y_i in range(ny+1)])

    #       Bend parameters
    #----------------------
    # Set bend parameters
    # Calcuclated bend parameters
    bend_angle  = config.g_bend_angle_deg/360.*2.*np.pi
    bend_len    = config.g_bend_lab_len*bend_angle/np.sin(bend_angle)
    bend_end    = config.g_bend_start + bend_len
    bend_radius = bend_len/bend_angle
    bend_par    = [config.g_bend_start,bend_end,bend_radius]

    lab0inCAD_z    = config.g_CathodeInCADcoord_z/1000. # in m
    lab0inCAD_x    = config.g_CathodeInCADcoord_x/1000. # in m
    angle_labToCAD = bend_angle


    # mapping-approximating bfield on the warped mesh in CAD coord. and transforming back to warped coord.
    bz_warped_final_3d, bx_warped_final_3d, by_warped_final_3d = MagneticFieldTransformation_boxMesh_CAD_3D(zmesh_box_CAD_v = z_CAD,\
                                                                                                    xmesh_box_CAD_v = x_CAD,\
                                                                                                    ymesh_box_CAD_v = y_CAD,\
                                                                                                    bz_CAD_vvv = bz_CAD,\
                                                                                                    bx_CAD_vvv = bx_CAD,\
                                                                                                    by_CAD_vvv = by_CAD,\
                                                                                                    zmesh_w_v = z_w,\
                                                                                                    xmesh_w_v = x_w,\
                                                                                                    ymesh_w_v = y_w,\
                                                                                                    lab0inCAD_z = lab0inCAD_z ,\
                                                                                                    lab0inCAD_x = lab0inCAD_x ,\
                                                                                                    angle_labToCAD = angle_labToCAD ,\
                                                                                                    bend_par_v = bend_par,\
                                                                                                    round_flag = True,\
                                                                                                    distanceTreshold = 1e-10,\
                                                                                                    LoadFromFile_flag = True,\
                                                                                                    LoadFromFile_name = config.g_fName_WarpedMeshInCAD,\
                                                                                                    ) # Careful with LoadFromFile_flag if the mesh change, 
                                                                                                      #must be turned off for once to recalculate, this should be automatized in the future
    with h5py.File(config.g_fName_BfieldFinal,"w") as Bfile_out:
        bx_warped_final_3d_ds = Bfile_out.create_dataset("bx_warped_final_3d", data = bx_warped_final_3d)
        by_warped_final_3d_ds = Bfile_out.create_dataset("by_warped_final_3d", data = by_warped_final_3d)
        bz_warped_final_3d_ds = Bfile_out.create_dataset("bz_warped_final_3d", data = bz_warped_final_3d)
        print("\nThe final bfield arrays: {} ".format(Bfile_out.keys())) 
        print("were saved into file: {}\n".format(config.g_fName_BfieldFinal)) 
 

    print(bz_warped_final_3d)
    print(by_warped_final_3d)
    print(bx_warped_final_3d)

    with open("./BFieldPreparation_Config.py","r") as config_in:
        with open("{}/BFieldFinalWarped_Zfull.log".format(config.g_path),"w") as config_log:
            config_log.writelines(config_in.readlines())



if BfileAnalysis_flag:
    # with open(config.g_fName_BfieldFinal_config,"w") as f_config:
    #     f_config.write('zlower_box = {} \n'.format(zlower_box))
    #     f_config.write('zupper_box = {} \n'.format(zupper_box))
    #     f_config.write('xlower_box = {} \n'.format(xlower_box))
    #     f_config.write('xupper_box = {} \n'.format(xupper_box))
    #     f_config.write('ylower_box = {} \n'.format(ylower_box))
    #     f_config.write('yupper_box = {} \n'.format(yupper_box))
    #     f_config.write('config.g_CathodeInCADcoord_z = {} \n'.format(config.g_CathodeInCADcoord_z))
    #     f_config.write('config.g_CathodeInCADcoord_x = {} \n'.format(config.g_CathodeInCADcoord_x))
    #     f_config.write('config.g_fName_BfieldOrigin_txt = "{}" \n'.format(config.g_fName_BfieldOrigin_txt))
    #     f_config.write('config.g_fName_BfieldCutted = "{}" \n'.format(config.g_fName_BfieldCutted))
    #     f_config.write('config.g_fName_BfieldReshaped = "{}" \n'.format(config.g_fName_BfieldReshaped))
    #     f_config.write('config.g_fName_WarpedMeshInCAD = "{}" \n'.format(config.g_fName_WarpedMeshInCAD))
    #     f_config.write('config.g_fName_BfieldFinal = "{}" \n'.format(config.g_fName_BfieldFinal))


    with h5py.File(config.g_fName_BfieldFinal,"r") as Bfile_out:
        bx_warped_final_3d = np.array(Bfile_out["bx_warped_final_3d"])
        by_warped_final_3d = np.array(Bfile_out["by_warped_final_3d"])
        bz_warped_final_3d = np.array(Bfile_out["bz_warped_final_3d"])
    print(len(bx_warped_final_3d[np.where(bx_warped_final_3d!=0)]))
    print(len(by_warped_final_3d[np.where(by_warped_final_3d!=0)]))
    print(len(bz_warped_final_3d[np.where(bz_warped_final_3d!=0)]))

    # with open(config.g_fName_WarpedMeshInCAD):
    #     zz_CAD_ds = file_backup.create_dataset("zz_CAD", data = zz_CAD)
    #     yy_CAD_ds = file_backup.create_dataset("yy_CAD", data = yy_CAD)
    #     xx_CAD_ds = file_backup.create_dataset("xx_CAD", data = xx_CAD)