"""
File name:      Config_WarpSimulation.py
Author:         Ondrej Sedlacek
Last modified:  28.10.2020
Purpose:        Configure file for the Warp simulations of Hollow Electron Beam.
Notes: Please note that because of the magnetic field file size (~300 MB), it cannot be uploaded to GitLab. To obtain the file please email me.
"""

import numpy as np
# constants:
mm = 1.e-3
pi = np.pi
emass = 9.1093837015e-31
echarge = 1.602176634e-19
# Important parameters

g_saveParticleData_flag = True  # Turns on/off the storage of particle data for later diagnostics
g_sim_geometry          = "XYZ" # Simulation geometry - "RZ" or "XYZ" 
g_mesh_cellsPercm       = 8     # [1/cm] Number of mesh cells per cm.
g_numOfSteps            = 35000 # If 0 then calculated automaticly so the beam travels whole geometry
g_numOfStepsPerPlot     = 3500  # Num of steps between displaying plots
g_bend_lab_len_alt = 0.00 # Differece between PipeBend and MeshBend parameters (used in bend effect study, where geometry was kept same, but mesh bending radius was varied)

    # B Field
g_numOfDataSaved_reductionFactor = 10 # Data of every Xth particle will be saved.
g_pline2 = "Egun to Bend simulation" # A comment line in diagnostic window
g_runmaker = "Sedlacek Ondrej"

g_magneticFieldMode = "FlatTopMode" # Specification of magnetic field file directory
g_magneticFieldFile = "txy100.0/{}cpcm/BFieldFinalWarped_Zfull_ext.h5py".format(g_mesh_cellsPercm) # Magnetic field file name and the path to it

#   Parameters
#       Mesh size in mm
g_channel_txy_mm    = 100.  #[mm] The transverse size of the mesh (or diameter if RZ geometry)
g_channel_len_mm    = 1570. # [mm] The length of the mesh
g_channel_zstart_mm = -10.  # [mm] The mesh's starting z coordinate

g_channel_zend_mm   = g_channel_zstart_mm + g_channel_len_mm
g_channel_ri_mm     = g_channel_txy_mm/2.
g_channel_ri_m      = g_channel_ri_mm/1000. 
g_channel_txy_m     = g_channel_txy_mm/1000.
g_channel_len_m     = g_channel_len_mm/1000.
g_channel_zstart_m  = g_channel_zstart_mm/1000.
g_channel_zend_m    = g_channel_zend_mm/1000.

#       Number of mesh cells
g_ny = int(g_channel_txy_mm/10.)*g_mesh_cellsPercm
g_nx = int(g_channel_txy_mm/10.)*g_mesh_cellsPercm
g_nz = int(g_channel_len_mm/10.)*g_mesh_cellsPercm

# If RZ is used use only half the number of mesh points so the density is the same as if in XYZ
# this happens because the xmin is set automaticly to 0
if g_sim_geometry == "RZ": 
    g_nx = g_nx/2


g_Bmain             = 5. # Main solenoid field - should be used only for the calculations of the cyclotron frequency, need to be assesed
g_Bgun              = 1 # Originally = 0.3
g_Cathode_Potential = -15.e3 # Potential on the Cathode
g_Gun_Perveance     = 7.e-6 # Perveance from master thesis of Vince Moens
g_Current           = pow(-g_Cathode_Potential,1.5)*g_Gun_Perveance # Setting up the current according to perveance from master thesis of Vince Moens
g_npart             = 50.e6 # Setting the num of macro-particles
g_compact_factor    = 1 # Multiplies the timestep for faster, but less precise simulations
g_injection_type    = 2 # 1 constant current, 2 space-charge limited

# Beam parameters
g_source_temperature = (900.+273.15)/1.6e4 # Initial temperature of the beam used only if g_vthz_flag=True
g_cyc_freq    = echarge*g_Bmain/emass # Cyclotron frequency used to calculate the timestep


g_timestep = g_compact_factor*pi/(2*g_cyc_freq) # Time step calculation 

#Flags
g_vthz_flag      = False # Turns on/off vthz and vthperp spread of the beam

g_bend_flag      = True  # Turns on/off (True/False) bend
g_dipole_flag    = False # Turns on/off (True/False) the automatic creation of dipole field in bends

g_PipeA_flag     = True # Turns on/off (True/False) an installation of junction pipe
g_PipeB_flag     = True # Turns on/off (True/False) an installation of proton pipe

g_GunToBend_flag = True # Turns on/off (True/False) an installation of gun to bend pipe
g_FieldSave_flag = False # Turns on/off (True/False) the saving the fields into a file for later analysis
g_Bz1_flag       = False # Sets up a constant B field in the z direction (in warped coord.)
g_Bgrd_flag       = True # Turns on/off (True/False) an imported magnetic field

g_Comment = " "

#----------------------
#   Conductors parameters
#----------------------

#-----------
# Electron Gun
# Gun design of Vince Moens scaled from 1-in electron gun to 16mm e-gun
#-----------
g_Gun_scaleFactor = 0.634 # The scaling factor of the gun design from Vince Moens 1-in hollow electron gun design to 16mm hollow electron gun.
# Cathode
g_Cathode_zstart   = -29.25*mm  # [m] The starting z coordinate
g_Cathode_zend     = 0.0*mm     # [m] The ending z coordinate
g_Cathode_radi     = 6.75*mm    # [m] The inner radius
g_Cathode_rado     = 12.7*mm    # [m] The outer radius
g_Cathode_radcurvb = 10*mm      # [m] The curvature radius of the cathode emmiting surface
g_Cathode_voltage  = g_Cathode_Potential # [V] The applied voltage on cathode


# Anode
g_Anode_zstart     = 9.48*mm # [m] The starting z coordinate
g_Anode_z1         = g_Anode_zstart + 1.5*mm # [m] The z coordinate of a point 1
g_Anode_z2         = g_Anode_zstart + 3.5*mm # [m] The z coordinate of a point 2
g_Anode_z3         = g_Anode_z1 + 9*mm # [m] The z coordinate of a point 3
g_Anode_z4         = g_Anode_z3 + 11.25*mm # [m] The z coordinate of a point 4
g_Anode_z5         = g_Anode_z4 + 5.625*mm # [m] The z coordinate of a point 5
g_Anode_zend       = g_Anode_z5 + 58.5*mm # [m] The ending z coordinate
g_Anode_ri         = 14.25*mm # [m] The inner radius
g_Anode_ro         = g_Anode_ri+5.33*mm # [m] The outer radius
g_Anode_r1         = g_Anode_ri # [m] The radius at a point 1
g_Anode_r2         = g_Anode_ro # [m] The radius at a point 2
g_Anode_r3         = g_Anode_ri # [m] The radius at a point 3
g_Anode_r4         = g_Anode_ri + 0.675*mm # [m] The radius at a point 4
g_Anode_radtipi    = g_Anode_ri + 1.5*mm # [m] The inner radius at the start
g_Anode_radtipo    = g_Anode_ro -3.5*mm # [m] The outer radius at the start
g_Anode_r5         = g_Anode_ri + 5.625*mm # [m] The radius at a point 5
g_Anode_rendi      = g_Anode_r5 # [m] The inner radius at the end
g_Anode_rendo      = g_Anode_rendi + 1.35*mm # [m] The outer radius at the end
g_Anode_radcurvb   = 3.5*mm # radcurvb and radcurvs designate the larger and smaller radius of curvatures used to describe the curvature at the end and beginning of the anode.
g_Anode_radcurvs   = -1.5*mm # [m] The curvature radius at the start
g_Anode_voltage    = -5.e3

# - Control electrode F 
g_ElectrodeF_zstart   = g_Cathode_zstart # [m] The starting z coordinate
g_ElectrodeF_zend     = 0.98*mm # [m] The ending z coordinate
g_ElectrodeF_z1       = g_ElectrodeF_zend - 0.5*mm # [m] The z coordinate of a point 1
g_ElectrodeF_z2       = g_ElectrodeF_zend - 1.4*mm # [m] The z coordinate of a point 2
g_ElectrodeF_ri       = 13.1*mm # [m] The inner radius
g_ElectrodeF_ro       = g_ElectrodeF_ri + 1.9*mm # [m] The outer radius
g_ElectrodeF_r1       = g_ElectrodeF_ri + 0.5*mm # [m] The radius at a point 1
g_ElectrodeF_radcurvb = 1.4*mm # [m] The curvature radius at the end
g_ElectrodeF_radcurvs = -0.5*mm # [m] The curvature radius at the start
g_ElectrodeF_voltage  = g_Cathode_Potential # [V] The applied voltage

# - Control electrode C
g_ElectrodeC_zstart  = g_Cathode_zstart # [m] The starting z coordinate
g_ElectrodeC_zend    = 1.97*mm # [m] The ending z coordinate
g_ElectrodeC_ri      = 20.5*mm # [m] The inner radius
g_ElectrodeC_ro      = 22.0*mm # [m] The outer radius
g_ElectrodeC_radcurv = 0.75*mm # [m] The curvature radius
g_ElectrodeC_z1      = g_ElectrodeC_zend - 0.75*mm # [m] The z coordinate of a point 1
g_ElectrodeC_voltage = g_Cathode_Potential # [V] The applied voltage


# - Gun drift pipe
g_Gun_pipe_zstart     = 84.375*mm   # [m] The starting z coordinate
g_Gun_pipe_zend       = 178.875*mm  # [m] The ending z coordinate
g_Gun_pipe_ri         = 36*mm       # [m] The inner radius
g_Gun_pipe_ro         = 33.75*mm    # [m] The outer radius
g_Gun_pipe_voltage    = 0.          # [V] The applied voltage

# The gun solenoid is not used by default
g_gunSolenoid_flag    = False       # Turns on/off (True/False) a solenoid field around gun
g_Gun_solenoid_zstart = -167.1*mm   # [m] The starting z coordinate of the solenoid
g_Gun_solenoid_length = 330*mm      # [m] The length of the solenoid
g_Gun_solenoid_zend   = g_Gun_solenoid_zstart + g_Gun_solenoid_length # [m] The ending z coordinate of the solenoid
g_Gun_solenoid_radi   = 120*mm # [m] The inner radius of the solenoid
g_Gun_solenoid_rado   = 248*mm # [m] The outer radius of the solenoid
g_Gun_solenoid_b      = g_Bgun # [T] The maximum field of the solenoid

#----------------------
# Pipes parameters
#----------------------

# Pipe sequence:
# Gun pipe -> Valve connection1 -> Valve flat -> Valve connection2 -> Gun To Bend -> BendEGun

#Gun To Bend pipe
g_mainGunToBendPipe_ri = 30*mm 
g_gun_pipe_ri          = g_mainGunToBendPipe_ri # [m] The inner radius of the pipe before the valve
g_valve_pipe_ri        = 20.*mm # [m] The inner radius of the valve
g_valveToBend_pipe_ri  = g_mainGunToBendPipe_ri # [m] The inner radius of the pipe after valve before junction
g_gunToBend_ro         = g_mainGunToBendPipe_ri + 50.*mm # [m] The outer radius
# Gun pipe -> Valve connection1 -> Valve flat -> Valve connection2 -> Gun To Bend -> BendEGun
g_gun_pipe_zs           = 0. # [m] The starting z coordinate
g_gun_pipe_len          = g_Anode_zend # [m] The length of the pipe from cathode to end of Anode
g_valve_connection1_len = 55.21*mm # [m] The length of the valve angled region
g_valve_flat_len        = 348.2*mm - g_Anode_zend # [m] The length of the valve flat region
g_valve_connection2_len = g_valve_connection1_len # [m] The length of the valve angled region
g_valveToBend_len       = 430.38*mm # [m] The length of the pipe from valve to junction

# Calcuclated gun to bend pipe parameters
g_gun_pipe_ze           = g_gun_pipe_zs + g_gun_pipe_len # [m] The ending z coordinate of the pipe around gun
g_valve_connection1_ze  = g_gun_pipe_ze + g_valve_connection1_len # [m] The ending z coordinate connection from pipe to valve
g_valve_flat_ze         = g_valve_connection1_ze + g_valve_flat_len # [m] The ending z coordinate of the flat region of the valve
g_valve_connection2_ze  = g_valve_flat_ze + g_valve_connection2_len # [m] The ending z coordinate of the 2nd connection from valve to pipe 
g_gunToPipe_ze          = g_valve_connection2_ze + g_valveToBend_len # [m] The ending z coordinate of the gun to bend pipe
g_gunToBend_voltage     = 0


CathodeEndInCADcoord_z = -2816.87 # [mm] z coordinate of the cathode in CAD frame (where (0,0) is the center of the proton pipe), not used in simulation, only in diagnostics
CathodeEndInCADcoord_x = 506.525# [mm] x coordinate of the cathode in CAD frame not used in simulation, only in diagnostics.

#----------------------
#       Bend parameters
#----------------------
# Set bend parameters
g_bend_angle_deg     = 30. # [deg] The bending angle
g_bend_angle         = g_bend_angle_deg/360.*2.*pi # [rad] The bending angle
g_bend_start         = g_gunToPipe_ze # [m] The starting z coordinate of the bend

# Mesh and Pipe bend angle MUST be equal for current junction design.
g_PipeBend_lab_len       = 0.22482 # [m] The length of the pipe bend in !laboratory! frame
g_PipeBend_angle_deg     = g_bend_angle_deg # [deg] The bending angle of the pipe
g_PipeBend_start         = g_bend_start # [m] The starting z coordinate of the bend

# Calcuclated bend parameters
g_PipeBend_angle         = g_PipeBend_angle_deg/360.*2.*pi # [rad] The bending angle of the pipe
g_PipeBend_len           = g_PipeBend_lab_len*g_PipeBend_angle/np.sin(g_PipeBend_angle) # [m] The length of the pipe bending in Warped frame
g_PipeBend_end           = g_PipeBend_start + g_PipeBend_len # [m] The ending z coordinate (Warped coord.) of the pipe bending
g_PipeBend_radius        = g_PipeBend_len/g_PipeBend_angle # [m] The pipe bending radius
g_PipeBend_par           = [g_PipeBend_start,g_PipeBend_end,g_PipeBend_radius]


# Mesh and Pipe bend angle MUST be equal for current junction design.
g_MeshBend_lab_len       = 0.22482 + g_bend_lab_len_alt # [m] The length of the mesh bend in !laboratory! frame
g_MeshBend_angle_deg     = g_PipeBend_angle_deg # [deg] The mesh bending angle
g_MeshBend_start         = g_PipeBend_start # [m] The starting z coordinate of the bend

# Calcuclated bend parameters
g_MeshBend_angle         = g_MeshBend_angle_deg/360.*2.*pi # [rad] The bending angle of the pipe
g_MeshBend_len           = g_MeshBend_lab_len*g_MeshBend_angle/np.sin(g_MeshBend_angle) # [m] The length of the mesh bending in Warped frame
g_MeshBend_end           = g_MeshBend_start + g_MeshBend_len # [m] The ending z coordinate (Warped coord.) of the mesh bending
g_MeshBend_radius        = g_MeshBend_len/g_MeshBend_angle # [m] The mesh bending radius
g_MeshBend_par           = [g_MeshBend_start,g_MeshBend_end,g_MeshBend_radius]


g_MeshPipeRadiusDifference = g_MeshBend_radius - g_PipeBend_radius # [m] The difference between radii of mesh and pipe bending
g_MeshPipeXDifference      = g_MeshPipeRadiusDifference*(1-np.cos(g_bend_angle)) # [m] X projection of the difference between radii of mesh and pipe bending


g_pipeBendEGun_num_condPoints_line1 = 100 # The number of segments in Junction pipe
g_pipeBendEGun_num_condPoints_line2 = 500 # See lines 330-356 in Simulation script
g_pipeBendEGun_zs      = g_bend_start # [m] The starting z coordinate of the junction pipe
g_pipeBendEGun_ze      = g_bend_start + g_MeshBend_len # [m] The ending z coordinate of the junction pipe
g_pipeBendEGun_xcenter = 0 # [m] The x center coordinate of the junction pipe
g_pipeBendEGun_r       = g_mainGunToBendPipe_ri # [m] The radius of the junction pipe
g_pipeBendEGun_voltage = 0 # [m] The applied voltage on the junction pipe


#Bend-Proton pipe
g_pipeBendProton_num_condPoints_line1 = 100 # The number of segments in Proton pipe
g_pipeBendProton_num_condPoints_line2 = 500 # See lines 422-443 in Simulation script
g_pipeBendProton_len = (g_channel_len_m-g_PipeBend_end) + g_PipeBend_lab_len*2 # [m] The length of the proton pipe in Laboratory frame
g_pipeBendProton_ze  = g_channel_zend_m # [m] The ending z coordinate of the proton pipe
g_pipeBendProton_xcenter   = g_MeshPipeXDifference # [m] The x center coordinate of the proton pipe
g_pipeBendProton_r   = g_mainGunToBendPipe_ri # [m] The radius of the proton pipe


#     Beam inject parameters
g_beamzinject  = 0              # [m] z coordinate of the beam injection
g_l_inj_exact = True            #if true, position and angle of injected particle are computed analytically rather than interpolated
g_l_inj_regular = False         # if true, inject one particle at each grid node (random injection otherwise)
g_l_inj_addtempz_abs = True     # Sets initial velocity spread only in the forward direction


# Boundary conditions
# Neumann = 1
# Dirichlet = 0
g_boundZstart = 1 # Boundary condition at the longitudinal start
g_boundZend   = 1 # Boundary condition at the longitudinal end
g_boundXY     = 0 # Boundary condition at the transverse limits

# ------------
# Drift-Lorentz mover
g_interpdk = 1 # Allowing a timestep which is larger than the gyroperiod. May not be needed

# B grad
g_igradb =1 # Specifies method of calculating grad B Lets (1 - table, 2 - assuming quadrupoles, 3 - lookup in z, quad in xy) try it with and without. May not be needed
