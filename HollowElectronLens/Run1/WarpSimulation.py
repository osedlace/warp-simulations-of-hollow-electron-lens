"""
File name:      WarpSimulation.py
Configure file: Config_WarpSimulation.py
Author:         Ondrej Sedlacek
Last modified:  28.10.2020
Purpose:        3D Warp simulation of hollow electron beam through first part of Hollow Electron Lens.
"""

import os.path
import sys
import matplotlib.pyplot as plt
import numpy as np
import h5py 
import datetime


import Config_WarpSimulation as config # Importing the configure variables from Config_HEGunAndBend file


sys.path.append("../../Libs")
from CoordinationTransformationsHeader import LabToWarped_point, WarpedToLab_point # Importing coordination transformation functions to allow construction of the conductors in LAB instead of Warped

sys.path.append('/home/sedlacekondrej/Programs/Warp/Warp_Release_4.5/warp/scripts/utils/')
from warp import *

# --- Set four-character run id, comment lines, user's name.
top.pline2   = config.g_pline2
top.runmaker = config.g_runmaker

# --- Invoke setup routine for the plotting
setup()

palette("ImageJ_Fire.gp") # Palette for plotting


# --- The simulation geometry
if config.g_sim_geometry == "RZ":
    w3d.solvergeom = w3d.RZgeom # Cylindrical coordiantes - axysimetrical
if config.g_sim_geometry == "XYZ":
    w3d.solvergeom = w3d.XYZgeom # 3D Rectangular coordiantes - 
if config.g_sim_geometry != "XYZ" and config.g_sim_geometry != "RZ":
    print("Wrong Geometry!!! = {}".format(config.g_sim_geometry))


#Number of nsteps
nsteps = config.g_numOfSteps

#---------------
# Injection
#---------------

top.inject = config.g_injection_type # 1 = Constant injection; 2 = space-charge limited injection
top.zinject[0] = config.g_beamzinject # The injection z position

top.npinject = config.g_npart/nsteps
top.ainject[0]    = config.g_Cathode_rado*config.g_Gun_scaleFactor
top.binject[0]    = config.g_Cathode_rado*config.g_Gun_scaleFactor
top.ainjmin[0]    = config.g_Cathode_radi*config.g_Gun_scaleFactor
top.binjmin[0]    = config.g_Cathode_radi*config.g_Gun_scaleFactor

top.lvinject = false # if false, source conductor input by user
top.vinject[0]    = config.g_Cathode_Potential

w3d.l_inj_exact = config.g_l_inj_exact               # if true, position and angle of injected particle are computed analytically rather than interpolated
w3d.l_inj_regular = config.g_l_inj_regular           #  if true, inject one particle at each grid node (random injection otherwise)
w3d.l_inj_addtempz_abs = config.g_l_inj_addtempz_abs # Sets initial velocity spread only in the forward direction

# -------------
# Beam design
# -------------

# --- Setup simulation beam species
beam = Species(type=Electron, name='beam')

# --- Set basic beam parameters
beam.ibeam    = -config.g_Current

if config.g_vthz_flag:
    beam.vthz       = sqrt(config.g_source_temperature*jperev/beam.mass)
    beam.vthperp    = sqrt(config.g_source_temperature*jperev/beam.mass)
derivqty() # To calculate vbeam from ekin



#---------------
# Lattice
#---------------

# Gun Solenoid
if config.g_gunSolenoid_flag: # The gun solenoid is not used by default as Bgrd should be used
    addnewsolenoid(zi = config.g_Gun_solenoid_zstart, zf = config.g_Gun_solenoid_zend, ri = config.g_Gun_solenoid_radi, ro = config.g_Gun_solenoid_rado, maxbz = config.g_Gun_solenoid_b)

#---------------------------
# Set up the bend
#---------------------------
if config.g_dipole_flag == False:
    top.diposet = false # Whether to set dipoles in bends automatically
# Sets up the bend
if config.g_bend_flag:
    addnewbend(zs=config.g_bend_start, ze=config.g_MeshBend_end, rc=-config.g_MeshBend_radius)


#---------------
# Input parameters of the simulations
#---------------

# --- Specify the timestep.
top.dt = config.g_timestep

# --- Specify the number of grid cells in each dimension.
w3d.nx = config.g_nx
w3d.ny = config.g_ny
w3d.nz = config.g_nz
top.prwall = config.g_channel_txy_m # The radius at which particle will be scrapped and lost

# --- Sets the dimensions of the mesh in space
w3d.xmmin = -config.g_channel_ri_m
w3d.xmmax = +config.g_channel_ri_m
w3d.ymmin = -config.g_channel_ri_m
w3d.ymmax = +config.g_channel_ri_m
w3d.zmmin = config.g_channel_zstart_m
w3d.zmmax = config.g_channel_zend_m

dx = (w3d.xmmax-w3d.xmmin) / w3d.nx
dy = (w3d.ymmax-w3d.ymmin) / w3d.ny
dz = (w3d.zmmax-w3d.zmmin) / w3d.nz

# --- Sets boundary conditions
# ---   for field solve
w3d.bound0  = config.g_boundZstart
w3d.boundnz = config.g_boundZend
w3d.boundxy = config.g_boundXY

# ---   for particles
top.pbound0 = absorb
top.pboundnz = absorb
top.pboundxy = absorb



# Setting up the MultiGrid solver
if config.g_sim_geometry == "RZ":
    solver = MultiGridRZ()
if config.g_sim_geometry == "XYZ":
    solver = MultiGrid3D()

registersolver(solver)

# Envelope boundaries
env.dzenv = top.tunelen/config.g_nz
env.zl = w3d.zmmin # Envelope starting z
env.zu = w3d.zmmax # Envelope finishing z
env.dzenv = config.g_channel_len_m/1000 # Envelope step size

# ------------
# Drift-Lorentz mover
w3d.interpdk[1] =  config.g_interpdk # Allowing a timestep which is larger than the gyroperiod.

# B grad
w3d.igradb = config.g_igradb # Specifies method of calculating grad B


# --- Set pline1 
top.pline1 = (config.g_Comment)


#---------------------------
# B field
#---------------------------

if config.g_Bz1_flag:
    top.bz0 = 1. # Sets a constant B field in the z direction (in warped coord.)

    # Adding a magnetic field (currently works only in XYZ)
if config.g_Bgrd_flag:
    with h5py.File("../Bfield_files/{}/{}".format(config.g_magneticFieldMode,config.g_magneticFieldFile),"r") as file_BwarpedOut:
        bx_warped_final_3d = np.array(file_BwarpedOut["bx_warped_final_3d"])
        by_warped_final_3d = np.array(file_BwarpedOut["by_warped_final_3d"])
        bz_warped_final_3d = np.array(file_BwarpedOut["bz_warped_final_3d"])
        nx_field,ny_field,nz_field = np.shape(bx_warped_final_3d)
    mesh_recalculationFactor = 1.
    if w3d.solvergeom == w3d.XYZgeom:
        bfield_1 = addnewbgrddataset(bx=bx_warped_final_3d,by=by_warped_final_3d,bz=bz_warped_final_3d,dx=dx,dy=dy,zlength= config.g_channel_len_m)
        addnewbgrd(id=bfield_1,zs=config.g_channel_zstart_m,ze= config.g_channel_zend_m,xs = -config.g_channel_txy_m/mesh_recalculationFactor/2.,ys = -config.g_channel_txy_m/mesh_recalculationFactor/2.)

#------------
# Generate
#------------

# Generate and run of the Envelope solver
package("env"); generate(); step()

# PIC code
# Generate the PIC code (allocate storage, plots, etc.)
package("w3d"); generate();

#------------------------
# Conductors
#------------------------

conductors_list = [] # List containing all the conductors to install
conductors_scrapper_list = [] # List containing all the conductors to add as a ParticleScrapper
conductorsToPlot_list = [] # List containing all the conductors to Plot

#------------
# Gun
#------------


# Gun drift pipe
gun_driftpipe=ZCylinderOut(radius= config.g_Gun_pipe_ri, zlower= config.g_Gun_pipe_zstart,zupper= config.g_Gun_pipe_zend,xcent=0.0,ycent=0.0, voltage= config.g_Gun_pipe_voltage)

# Cathode
gun_cathode_r = [ config.g_Cathode_radi, config.g_Cathode_radi, config.g_Cathode_rado, config.g_Cathode_rado]
gun_cathode_r = map(lambda x: x*config.g_Gun_scaleFactor, gun_cathode_r) # Scaling to HEL gun
gun_cathode_z = [ config.g_Cathode_zstart,config.g_Cathode_zend,config.g_Cathode_zend,config.g_Cathode_zstart]
gun_cathode_z = map(lambda x: x*config.g_Gun_scaleFactor, gun_cathode_z) # Scaling to HEL gun
gun_cathode_radi = [None, config.g_Cathode_radcurvb*config.g_Gun_scaleFactor,None,None]
gun_cathode = ZSrfrv(rsrf=gun_cathode_r,zsrf=gun_cathode_z,rad=gun_cathode_radi,voltage=config.g_Cathode_voltage,xcent=0.,ycent=0.,zcent=0.)

# - Anode
gun_anode_r = [config.g_Anode_r1,config.g_Anode_r3,config.g_Anode_r4,config.g_Anode_r5,config.g_Anode_rendi,config.g_Anode_rendo,config.g_Anode_rendo,config.g_Anode_r2,config.g_Anode_r2,config.g_Anode_radtipo,config.g_Anode_radtipi]
gun_anode_r = map(lambda x: x*config.g_Gun_scaleFactor,gun_anode_r) # Scaling to HEL gun
gun_anode_z = [ config.g_Anode_z1, config.g_Anode_z3, config.g_Anode_z4, config.g_Anode_z5, config.g_Anode_zend, config.g_Anode_zend, config.g_Anode_z5, config.g_Anode_z4, config.g_Anode_z2, config.g_Anode_zstart, config.g_Anode_zstart]
gun_anode_z = map(lambda x: x*config.g_Gun_scaleFactor,gun_anode_z) # Scaling to HEL gun
gun_anode_radi = [None,None,None,None,None,None,None,None, config.g_Anode_radcurvb*config.g_Gun_scaleFactor,None, config.g_Anode_radcurvs*config.g_Gun_scaleFactor]
gun_anode = ZSrfrv(rsrf=gun_anode_r,zsrf=gun_anode_z,rad=gun_anode_radi, voltage= config.g_Anode_voltage,xcent=.0e0,ycent=.0e0,zcent=.0e0)
# - Electrode F
gun_electrodef_r = [ config.g_ElectrodeF_r1, config.g_ElectrodeF_ro, config.g_ElectrodeF_ro, config.g_ElectrodeF_ri, config.g_ElectrodeF_ri]
gun_electrodef_r = map(lambda x: x*config.g_Gun_scaleFactor,gun_electrodef_r) # Scaling to HEL gun
gun_electrodef_z = [ config.g_ElectrodeF_zend, config.g_ElectrodeF_z2, config.g_ElectrodeF_zstart, config.g_ElectrodeF_zstart, config.g_ElectrodeF_z1]
gun_electrodef_z = map(lambda x: x*config.g_Gun_scaleFactor,gun_electrodef_z) # Scaling to HEL gun
gun_electrodef_radi = [ config.g_ElectrodeF_radcurvb*config.g_Gun_scaleFactor,None,None,None, config.g_ElectrodeF_radcurvs*config.g_Gun_scaleFactor]
gun_electrodef = ZSrfrv(rsrf=gun_electrodef_r,zsrf=gun_electrodef_z,rad=gun_electrodef_radi,voltage= config.g_ElectrodeF_voltage,xcent=0,ycent=0,zcent=.0e0)


# - Electrode C
gun_electrodeC_r = [config.g_ElectrodeC_ro,config.g_ElectrodeC_ro,config.g_ElectrodeC_ri,config.g_ElectrodeC_ri]
gun_electrodeC_r = map(lambda x: x*config.g_Gun_scaleFactor,gun_electrodeC_r) # Scaling to HEL gun
gun_electrodeC_z = [config.g_ElectrodeC_z1,config.g_ElectrodeC_zstart, config.g_ElectrodeC_zstart,config.g_ElectrodeC_z1]
gun_electrodeC_z = map(lambda x: x*config.g_Gun_scaleFactor,gun_electrodeC_z) # Scaling to HEL gun
gun_electrodeC_radi = [None,None,None,config.g_ElectrodeC_radcurv*config.g_Gun_scaleFactor]
gun_electrodeC = ZSrfrv(rsrf=gun_electrodeC_r,zsrf=gun_electrodeC_z,rad=gun_electrodeC_radi,voltage=config.g_ElectrodeC_voltage,xcent=0,ycent=0,zcent=.0e0)


#Installing the gun conductors
conductors_list.append(gun_driftpipe) 
conductors_list.append(gun_cathode) # Must not be a scrapper
conductors_list.append(gun_anode)
conductors_list.append(gun_electrodef)
conductors_list.append(gun_electrodeC)

conductors_scrapper_list.append(gun_driftpipe)
conductors_scrapper_list.append(gun_anode)
conductors_scrapper_list.append(gun_electrodef)
conductors_scrapper_list.append(gun_electrodeC)

# Plotting the gun conductors
conductorsToPlot_list.append(gun_driftpipe)
conductorsToPlot_list.append(gun_cathode)
conductorsToPlot_list.append(gun_anode)
conductorsToPlot_list.append(gun_electrodef)
conductorsToPlot_list.append(gun_electrodeC)



#------------
# Gun To Bend Pipe
#------------

gunToBend_pipe_r = [config.g_gun_pipe_ri,config.g_gun_pipe_ri,config.g_valve_pipe_ri,config.g_valve_pipe_ri,config.g_valveToBend_pipe_ri,config.g_valveToBend_pipe_ri,config.g_gunToBend_ro,config.g_gunToBend_ro]
gunToBend_pipe_z = [config.g_gun_pipe_zs,config.g_gun_pipe_ze,config.g_valve_connection1_ze,config.g_valve_flat_ze,config.g_valve_connection2_ze,config.g_pipeBendEGun_zs,config.g_pipeBendEGun_zs,config.g_gun_pipe_zs]
gunToBend = ZSrfrv(rsrf=gunToBend_pipe_r,zsrf=gunToBend_pipe_z,voltage=config.g_gunToBend_voltage,xcent=0,ycent=0,zcent=.0e0)

if config.g_GunToBend_flag:
    conductors_list.append(gunToBend)
    conductors_scrapper_list.append(gunToBend)
    conductorsToPlot_list.append(gunToBend)


#------------
# The Bend-EGun Pipe
#------------

# """ The points of the pipe bend Egun pipe
# B---A
# -   center
# C---D
# """

#  When g_bend_lab_len_alt != 0 then, line B-A or C-D needs to be longer!
pipeBendEGun_Az, pipeBendEGun_Ax = WarpedToLab_point(config.g_pipeBendEGun_ze, config.g_pipeBendEGun_xcenter + config.g_pipeBendEGun_r,config.g_bend_start, config.g_MeshBend_end, config.g_MeshBend_radius) # warped coordinates: [channel_len,pipeBendEGun_r]
pipeBendEGun_Bz, pipeBendEGun_Bx = WarpedToLab_point(config.g_pipeBendEGun_zs, config.g_pipeBendEGun_xcenter + config.g_pipeBendEGun_r,config.g_bend_start, config.g_MeshBend_end, config.g_MeshBend_radius) # warped coordinates: [channel_len,pipeBendEGun_r]
pipeBendEGun_Dz, pipeBendEGun_Dx = WarpedToLab_point(config.g_pipeBendEGun_ze, config.g_pipeBendEGun_xcenter - config.g_pipeBendEGun_r,config.g_bend_start, config.g_MeshBend_end, config.g_MeshBend_radius) # warped coordinates: [channel_len,-pipeBendEGun_r]
pipeBendEGun_Cz, pipeBendEGun_Cx = WarpedToLab_point(config.g_pipeBendEGun_zs, config.g_pipeBendEGun_xcenter - config.g_pipeBendEGun_r,config.g_bend_start, config.g_MeshBend_end, config.g_MeshBend_radius) # warped coordinates: [channel_len,-pipeBendEGun_r]



pipeBendEGun_par = [pipeBendEGun_Az, pipeBendEGun_Ax, pipeBendEGun_Bz, pipeBendEGun_Bx,pipeBendEGun_Cz + np.cos(config.g_bend_angle)*0.5, pipeBendEGun_Cx + np.sin(config.g_bend_angle)*0.5,pipeBendEGun_Dz, pipeBendEGun_Dx]

pipeBendEGun_line1_z_lab = np.linspace(pipeBendEGun_Bz,pipeBendEGun_Az,config.g_pipeBendEGun_num_condPoints_line1)
pipeBendEGun_line1_x_lab = np.linspace(pipeBendEGun_Bx,pipeBendEGun_Ax,config.g_pipeBendEGun_num_condPoints_line1)

pipeBendEGun_line2_z_lab = np.linspace(pipeBendEGun_Cz,pipeBendEGun_Dz,config.g_pipeBendEGun_num_condPoints_line2)
pipeBendEGun_line2_x_lab = np.linspace(pipeBendEGun_Cx,pipeBendEGun_Dx,config.g_pipeBendEGun_num_condPoints_line2)

pipeBendEGun_line1_z_w = np.zeros(config.g_pipeBendEGun_num_condPoints_line1)
pipeBendEGun_line1_x_w = np.zeros(config.g_pipeBendEGun_num_condPoints_line1)

pipeBendEGun_line2_z_w = np.zeros(config.g_pipeBendEGun_num_condPoints_line2)
pipeBendEGun_line2_x_w = np.zeros(config.g_pipeBendEGun_num_condPoints_line2)

for i in range(config.g_pipeBendEGun_num_condPoints_line1):
    pipeBendEGun_line1_z_w[i], pipeBendEGun_line1_x_w[i] = LabToWarped_point(pipeBendEGun_line1_z_lab[i],pipeBendEGun_line1_x_lab[i], config.g_bend_start, config.g_MeshBend_end, config.g_MeshBend_radius)
for i in range(config.g_pipeBendEGun_num_condPoints_line2):
    pipeBendEGun_line2_z_w[i], pipeBendEGun_line2_x_w[i] = LabToWarped_point(pipeBendEGun_line2_z_lab[i],pipeBendEGun_line2_x_lab[i], config.g_bend_start, config.g_MeshBend_end, config.g_MeshBend_radius)

pipeBendEGun_line1_z_w = pipeBendEGun_line1_z_w[::-1]
pipeBendEGun_line1_x_w = pipeBendEGun_line1_x_w[::-1]
pipeBendEGun_line2_z_w = pipeBendEGun_line2_z_w[::-1]
pipeBendEGun_line2_x_w = pipeBendEGun_line2_x_w[::-1]

sliceAin_a = np.array([])
sliceAout_a = np.array([])
sliceA_r_a = np.array([])
sliceA_zs_a = np.array([])
sliceA_ze_a = np.array([])
sliceA_xcent_a = np.array([])

offset = 0
for i in range(1,config.g_pipeBendEGun_num_condPoints_line1):
    dist_points = 100
    while dist_points > abs(pipeBendEGun_line2_z_w[i-1+offset]-pipeBendEGun_line1_z_w[i-1]):
        dist_points = abs(pipeBendEGun_line2_z_w[i-1+offset]-pipeBendEGun_line1_z_w[i-1])
        offset+=1
        try:
            dummy = abs(pipeBendEGun_line2_z_w[i-1+offset]-pipeBendEGun_line1_z_w[i-1])
        except IndexError:
            break
    offset-=1
    sliceA_r = abs(pipeBendEGun_line1_x_w[i-1] - pipeBendEGun_line2_x_w[i-1 + offset])/2.
    sliceA_zs = pipeBendEGun_line1_z_w[i]
    sliceA_ze = pipeBendEGun_line1_z_w[i-1]
    sliceA_xcent = (pipeBendEGun_line1_x_w[i-1] + pipeBendEGun_line2_x_w[i-1 + offset])/2.

    sliceA_r_a = np.append(sliceA_r_a,sliceA_r)
    sliceA_zs_a = np.append(sliceA_zs_a,sliceA_zs)
    sliceA_ze_a = np.append(sliceA_ze_a,sliceA_ze)
    sliceA_xcent_a = np.append(sliceA_xcent_a,sliceA_xcent)

    if i == 1:
        sliceAin_a  = np.append(sliceAin_a, ZCylinder(radius = sliceA_r, zlower = sliceA_zs, zupper = sliceA_ze, voltage = 0, xcent = sliceA_xcent))
        sliceAout_a  = np.append(sliceAout_a, ZCylinderOut(radius = sliceA_r, zlower = sliceA_zs, zupper = sliceA_ze, voltage = 0, xcent = sliceA_xcent))
    else: 
        sliceAin_a  = np.append(sliceAin_a, ZCylinder(radius = sliceA_r, zlower = sliceA_zs, zupper = sliceA_ze, voltage = 0, xcent = sliceA_xcent, condid = sliceAin_a[0].condid))
        sliceAout_a  = np.append(sliceAout_a, ZCylinderOut(radius = sliceA_r, zlower = sliceA_zs, zupper = sliceA_ze, voltage = 0, xcent = sliceA_xcent, condid = sliceAout_a[0].condid))

sliceAin_a = sliceAin_a[::-1] 
sliceAout_a = sliceAout_a[::-1] 

# Creating one conductor
Ain = sliceAin_a[0]
Aout_c = sliceAout_a[0]
for sliceAin_i in range(1,len(sliceAin_a)):
    Ain += sliceAin_a[sliceAin_i]
    Aout_c += sliceAout_a[sliceAin_i]   




#------------
# The Bend-Proton Pipe
#------------


# """ The points of the pipe bend proton pipe
# B---A
# -   center
# C---D
# """
pipeBendProton_Az, pipeBendProton_Ax = WarpedToLab_point(config.g_pipeBendProton_ze, config.g_pipeBendProton_xcenter + config.g_pipeBendProton_r,config.g_bend_start, config.g_MeshBend_end, config.g_MeshBend_radius) 
pipeBendProton_Dz, pipeBendProton_Dx = WarpedToLab_point(config.g_pipeBendProton_ze, config.g_pipeBendProton_xcenter - config.g_pipeBendProton_r,config.g_bend_start, config.g_MeshBend_end, config.g_MeshBend_radius) 


pipeBendProton_Bz = pipeBendProton_Az - np.cos(config.g_bend_angle)*config.g_pipeBendProton_len 
pipeBendProton_Bx = pipeBendProton_Ax - np.sin(config.g_bend_angle)*config.g_pipeBendProton_len 
pipeBendProton_Cz = pipeBendProton_Dz - np.cos(config.g_bend_angle)*(config.g_pipeBendProton_len+0.5) 
pipeBendProton_Cx = pipeBendProton_Dx - np.sin(config.g_bend_angle)*(config.g_pipeBendProton_len+0.5) 

pipeBendProton_par = [pipeBendProton_Az, pipeBendProton_Ax, pipeBendProton_Bz, pipeBendProton_Bx,pipeBendProton_Cz + np.cos(config.g_bend_angle)*0.5, pipeBendProton_Cx + np.sin(config.g_bend_angle)*0.5,pipeBendProton_Dz, pipeBendProton_Dx]

pipeBendProton_line1_z_lab = np.linspace(pipeBendProton_Bz,pipeBendProton_Az,config.g_pipeBendProton_num_condPoints_line1)
pipeBendProton_line1_x_lab = np.linspace(pipeBendProton_Bx,pipeBendProton_Ax,config.g_pipeBendProton_num_condPoints_line1)

pipeBendProton_line2_z_lab = np.linspace(pipeBendProton_Cz,pipeBendProton_Dz,config.g_pipeBendProton_num_condPoints_line2)
pipeBendProton_line2_x_lab = np.linspace(pipeBendProton_Cx,pipeBendProton_Dx,config.g_pipeBendProton_num_condPoints_line2)

pipeBendProton_line1_z_w = np.zeros(config.g_pipeBendProton_num_condPoints_line1)
pipeBendProton_line1_x_w = np.zeros(config.g_pipeBendProton_num_condPoints_line1)

pipeBendProton_line2_z_w = np.zeros(config.g_pipeBendProton_num_condPoints_line2)
pipeBendProton_line2_x_w = np.zeros(config.g_pipeBendProton_num_condPoints_line2)

for i in range(config.g_pipeBendProton_num_condPoints_line1):
    pipeBendProton_line1_z_w[i], pipeBendProton_line1_x_w[i] = LabToWarped_point(pipeBendProton_line1_z_lab[i],pipeBendProton_line1_x_lab[i], config.g_bend_start, config.g_MeshBend_end, config.g_MeshBend_radius)
for i in range(config.g_pipeBendProton_num_condPoints_line2):
    pipeBendProton_line2_z_w[i], pipeBendProton_line2_x_w[i] = LabToWarped_point(pipeBendProton_line2_z_lab[i],pipeBendProton_line2_x_lab[i], config.g_bend_start, config.g_MeshBend_end, config.g_MeshBend_radius)

pipeBendProton_line1_z_w = pipeBendProton_line1_z_w[::-1]
pipeBendProton_line1_x_w = pipeBendProton_line1_x_w[::-1]
pipeBendProton_line2_z_w = pipeBendProton_line2_z_w[::-1]
pipeBendProton_line2_x_w = pipeBendProton_line2_x_w[::-1]

sliceBin_a = np.array([])
sliceBout_a = np.array([])
sliceB_r_a = np.array([])
sliceB_zs_a = np.array([])
sliceB_ze_a = np.array([])
sliceB_xcent_a = np.array([])

offset = 0
for i in range(1,config.g_pipeBendProton_num_condPoints_line1):
    dist_points = 100
    while dist_points > abs(pipeBendProton_line2_z_w[i-1+offset]-pipeBendProton_line1_z_w[i-1]):
        dist_points = abs(pipeBendProton_line2_z_w[i-1+offset]-pipeBendProton_line1_z_w[i-1])
        offset+=1
        try:
            dummy = abs(pipeBendProton_line2_z_w[i-1+offset]-pipeBendProton_line1_z_w[i-1])
        except IndexError:
            break
    offset-=1
    sliceB_r = abs(pipeBendProton_line1_x_w[i-1] - pipeBendProton_line2_x_w[i-1 + offset])/2.
    sliceB_zs = pipeBendProton_line1_z_w[i]
    sliceB_ze = pipeBendProton_line1_z_w[i-1]
    sliceB_xcent = (pipeBendProton_line1_x_w[i-1] + pipeBendProton_line2_x_w[i-1 + offset])/2.

    sliceB_r_a = np.append(sliceB_r_a,sliceB_r)
    sliceB_zs_a = np.append(sliceB_zs_a,sliceB_zs)
    sliceB_ze_a = np.append(sliceB_ze_a,sliceB_ze)
    sliceB_xcent_a = np.append(sliceB_xcent_a,sliceB_xcent)

    sliceBin_a  = np.append(sliceBin_a, ZCylinder(radius = sliceB_r, zlower = sliceB_zs, zupper = sliceB_ze, voltage = 0, xcent = sliceB_xcent, condid = sliceAout_a[0].condid))
    sliceBout_a  = np.append(sliceBout_a, ZCylinderOut(radius = sliceB_r, zlower = sliceB_zs, zupper = sliceB_ze, voltage = 0, xcent = sliceB_xcent, condid = sliceAin_a[0].condid))

sliceBin_a = sliceBin_a[::-1] 
sliceBout_a = sliceBout_a[::-1] 

# Creating one conductor
Bin_c = sliceBin_a[0]
Bout_c = sliceBout_a[0]
for sliceBin_i in range(1,len(sliceBin_a)):
    Bin_c += sliceBin_a[sliceBin_i]
    Bout_c += sliceBout_a[sliceBin_i]   

#-------------------------------
#     Finall modifications of the conductors
#-------------------------------
PipeBendToAin  = ZCylinder(radius = config.g_pipeBendEGun_r, zlower = config.g_valve_connection2_ze, zupper = 1.5, voltage = 0, xcent = 0,condid = sliceAout_a[0].condid)
Aout = Aout_c - PipeBendToAin
Bout = Bout_c - PipeBendToAin
PipeA = Aout - Bin_c
PipeB = Bout - Ain


if config.g_PipeA_flag:
    conductors_list.append(PipeA)
    conductors_scrapper_list.append(PipeA)
    conductorsToPlot_list.append(Aout)
if config.g_PipeB_flag:
    conductors_list.append(PipeB)
    conductors_scrapper_list.append(PipeB)
    conductorsToPlot_list.append(Bout_c)

# Installing the conductors and adding them as a Particle Scrapper
print("Installing conductors starting")
for conductors in conductors_list:
    installconductor(conductors)
scraper = ParticleScraper(conductors_scrapper_list)
print("Installing conductors completed")


print("Field solve starting")
fieldsolve()
print("Field solve completed")

#Saving mesh for plotting (needed only for RZ simulations)
if w3d.solvergeom == w3d.RZgeom:
    xmesh_w = np.zeros(len(w3d.xmesh)*2 - 1) 
    for i in range(len(xmesh_w)):
        if i < len(w3d.xmesh) - 1:
            xmesh_w[i] = -w3d.xmesh[-i-1]
        if i > len(w3d.xmesh) - 1:
            xmesh_w[i] = w3d.xmesh[i - len(w3d.xmesh) +1]
if w3d.solvergeom == w3d.XYZgeom:
    xmesh_w = w3d.xmesh

# # Saving data about the simulation for better visualisation and diagnostics.
id_number = setup.pname.split(".")[-2] # The number of the run (eg. 000, or 001)
print("Saving data for plotting starting")
f_diagnostics = PWpickle.PW("./Logs/DiagnosticsLog_{}.pkl".format(id_number))
# Saving the mesh
f_diagnostics.xmesh_w_ds = xmesh_w
f_diagnostics.zmesh_w_ds = w3d.zmesh
#Saving the bend parameters
f_diagnostics.bend_par_ds = config.g_MeshBend_par
f_diagnostics.MeshBend_par_ds = config.g_MeshBend_par
f_diagnostics.PipeBend_par_ds = config.g_PipeBend_par
#Saving the conductors in Warped for check
f_diagnostics.sliceA_r_a_ds = sliceA_r_a
f_diagnostics.sliceA_zs_a_ds = sliceA_zs_a
f_diagnostics.sliceA_ze_a_ds = sliceA_ze_a
f_diagnostics.sliceA_xcent_a_ds = sliceA_xcent_a

f_diagnostics.sliceB_r_a_ds = sliceB_r_a
f_diagnostics.sliceB_zs_a_ds = sliceB_zs_a
f_diagnostics.sliceB_ze_a_ds = sliceB_ze_a
f_diagnostics.sliceB_xcent_a_ds = sliceB_xcent_a
f_diagnostics.gunToBend_pipe_z_ds = gunToBend_pipe_z
f_diagnostics.gunToBend_pipe_r_ds = gunToBend_pipe_r
#Saving applied magnetic field in Warped for check
if  config.g_FieldSave_flag:
    f_diagnostics.magFieldWarped_x_ds = getappliedfieldsongrid()[3]
    f_diagnostics.magFieldWarped_y_ds = getappliedfieldsongrid()[4]
    f_diagnostics.magFieldWarped_z_ds = getappliedfieldsongrid()[5]

f_diagnostics.close()
print("Saving data for plotting completed")

print("Copying the config file...")
path_configLog = "./Logs/Config_{}.py".format(id_number)
with open("./Config_{}.py".format(arraytostr(top.runid)),"r") as config_in:
    with open(path_configLog,"w") as config_log:
        config_log.writelines(config_in.readlines())

# The simulation plots
def beamplots():
    window(0)
    plsys(3)
    # Ekin vs z plot
    limits = (0., config.g_channel_len_m, -config.g_Cathode_Potential*0.5, -config.g_Cathode_Potential*1.1)
    ppzke(lframe = True, pplimits = limits, color = "density")


    plsys(4)
    # Transverse plot
    limits = (-config.g_channel_txy_m/4.0, config.g_channel_txy_m/4.0,-config.g_channel_txy_m/4.0, config.g_channel_txy_m/4.0)
    ppxy(lframe = True, pplimits = limits, color = "density")

    plsys(5)
    # Particle and conductors plot
    limits = (0., config.g_channel_len_m, -config.g_channel_txy_m/2., config.g_channel_txy_m/2.)
    ppzx(lframe = True, pplimits = limits,color = "density")
    for conductors in conductorsToPlot_list:
        conductors.drawzx(filled = 250)

    plsys(6)
    #Bx- XZ plot
    limits = (0., config.g_channel_len_m, -config.g_channel_txy_m/2., config.g_channel_txy_m/2.)
    if config.g_Bgrd_flag:
        plotbgrd(lframe = True, pplimits = limits,component = "x", iy = w3d.ny/2, withbends = false) 

    fma()
    refresh()

# --- Open up plotting windows
winon(0) # Should be commented out in case of grid simulations

top.ncolor = 200 #Number of colors used in plots

# Printing out the current time and time difference to see the pace of the progress
t_prev = [datetime.datetime.now().replace(microsecond=0),]
def timeOfSteps(t_prev):
    t_now = datetime.datetime.now().replace(microsecond=0)
    print("time now = {}".format(t_now))
    print("time diff = {}".format(t_now-t_prev[-1]))
    t_prev.append(t_now)

@callfromafterstep
def makebeamplots():
    if top.it%config.g_numOfStepsPerPlot == 0:  #Freq of displaying data
        beamplots()
    if top.it%10 == 0: 
        timeOfSteps(t_prev) #Freq of displaying time step

step(int(nsteps)) #Advance num_steps

# # Saving data for analysis
f_partdata = PWpickle.PW("./ParticleData/ParticleData_{}.pkl".format(id_number))
f_partdata.dataset_x = np.array(getx())[::config.g_numOfDataSaved_reductionFactor]
f_partdata.dataset_y = np.array(gety())[::config.g_numOfDataSaved_reductionFactor]
f_partdata.dataset_z = np.array(getz())[::config.g_numOfDataSaved_reductionFactor]
f_partdata.dataset_vx = np.array(getvx())[::config.g_numOfDataSaved_reductionFactor]  
f_partdata.dataset_vy = np.array(getvy())[::config.g_numOfDataSaved_reductionFactor]
f_partdata.dataset_vz = np.array(getvz())[::config.g_numOfDataSaved_reductionFactor]
f_partdata.dataset_kinEn = np.array(getke())[::config.g_numOfDataSaved_reductionFactor]
f_partdata.dataset_selfEx = np.array(getselfe(comp="x"))
f_partdata.dataset_selfEy = np.array(getselfe(comp="y"))
f_partdata.dataset_selfEz = np.array(getselfe(comp="z"))
f_partdata.close()



# --- Make sure that last plot frames get sent to the cgm file
window(0)
hcp()
window(1)
hcp()