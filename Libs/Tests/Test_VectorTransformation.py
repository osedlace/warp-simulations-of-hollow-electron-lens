"""
Unit testing the vector transformation functions namely LabToWarped_vector nad WarpedToLab_vector 

Results:

"""

import h5py 
import numpy as np
import matplotlib.pyplot as plt
import sys

sys.path.append("..")
from Plot_diagnostics_Header import LabToWarped_vector, WarpedToLab_vector


# Inputs:
in_vector_z = 
in_vector_x = 
in_position_z = 
in_position_x = 
bend_s = 0.1
bend_e = 0.6
bend_r = 0.5*4./np.pi # rotation by pi/4
testNum = 5
bend_start = 0.1
bend_end = 0.6
bend_radius = 0.5*4/np.pi

LabToWarped_vector(m_z_lab,m_x_lab,z_lab,x_lab,bend_s,bend_e,bend_r):

