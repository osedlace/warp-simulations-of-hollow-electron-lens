"""
Unit testing the coordinate transformation functions 

Results:
 The bending over pi does work from Warped to Lab, but not from Lab to Warped
 The bending to other side (negative radius) does work from Warped to Lab, but not from Lab to Warped

"""

import h5py 
import numpy as np
import matplotlib.pyplot as plt
import sys

sys.path.append("..")
from Plot_diagnostics_Header import LabToWarped_point, WarpedToLab_point



def Test_WarpToLabAndLabToWarp(testNum,z_mesh_Lab2D,x_mesh_Lab2D,z_mesh_Warped1D,x_mesh_Warped1D,bend_start,bend_end,bend_radius):
    """
    z_mesh_Lab2D - 2D mesh in lab cooridnates
    x_mesh_Lab2D - 2D mesh in lab cooridnates
    z_mesh_Warped1D - 1D mesh in warped coordinates (is rectangular therefore 1D is enough)
    x_mesh_Warped1D - 1D mesh in warped coordinates (is rectangular therefore 1D is enough)
    bend_start,end,radius - parameters of the bend that were used for creating the mesh in 
    Lab coordinates (used in the simulation from where the mesh was extracted)
    """
    x_len = len(x_mesh_Warped1D)
    z_len = len(z_mesh_Warped1D)

    # Making a 2D mesh
    x_mesh_Warped2D = np.array([[x_mesh_Warped1D[x_i] for z_i in range(z_len)] for x_i in range(x_len)])
    z_mesh_Warped2D = np.array([[z_mesh_Warped1D[z_i] for z_i in range(z_len)] for x_i in range(x_len)])


    # Creating an empty arrays for transformed mesh
    x_mesh_WarpedInLab = np.zeros((x_len,z_len))
    z_mesh_WarpedInLab = np.zeros((x_len,z_len))

    x_mesh_LabInWarped = np.zeros((x_len,z_len))
    z_mesh_LabInWarped = np.zeros((x_len,z_len))




    for x_i in range(x_len):
        for z_i in range(z_len):
            z_mesh_WarpedInLab[x_i][z_i], x_mesh_WarpedInLab[x_i][z_i] = WarpedToLab_point(z_mesh_Warped2D[x_i][z_i],x_mesh_Warped2D[x_i][z_i],bend_start,bend_end,bend_radius)
            z_mesh_LabInWarped[x_i][z_i], x_mesh_LabInWarped[x_i][z_i] = LabToWarped_point(z_mesh_Lab2D[x_i][z_i],x_mesh_Lab2D[x_i][z_i],bend_start,bend_end,bend_radius)

    x_mesh_WarpedInLab_ravel = x_mesh_WarpedInLab.ravel()
    z_mesh_WarpedInLab_ravel = z_mesh_WarpedInLab.ravel()

    x_mesh_LabInWarped_ravel = x_mesh_LabInWarped.ravel()
    z_mesh_LabInWarped_ravel = z_mesh_LabInWarped.ravel()

    x_mesh_LabRavel = x_mesh_Lab2D.ravel()
    z_mesh_LabRavel = z_mesh_Lab2D.ravel()

    x_mesh_WarpedRavel = x_mesh_Warped2D.ravel()
    z_mesh_WarpedRavel = z_mesh_Warped2D.ravel()



    def rounding(x): return round(x,10)
    x_mesh_WarpedInLab_ravel = np.array(map(rounding,x_mesh_WarpedInLab_ravel))
    z_mesh_WarpedInLab_ravel = np.array(map(rounding,z_mesh_WarpedInLab_ravel))

    x_mesh_LabInWarped_ravel = np.array(map(rounding,x_mesh_LabInWarped_ravel))
    z_mesh_LabInWarped_ravel = np.array(map(rounding,z_mesh_LabInWarped_ravel))

    x_mesh_LabRavel = np.array(map(rounding,x_mesh_LabRavel))
    z_mesh_LabRavel = np.array(map(rounding,z_mesh_LabRavel))

    x_mesh_WarpedRavel = np.array(map(rounding,x_mesh_WarpedRavel))
    z_mesh_WarpedRavel = np.array(map(rounding,z_mesh_WarpedRavel))

    print("\n Testing num {}".format(testNum))
    print("   bend_start = {}".format(bend_start))
    print("   bend_end = {}".format(bend_end))
    print("   bend_radius = {}".format(bend_radius))
    if (x_mesh_LabRavel == x_mesh_WarpedInLab_ravel).all():
        print("Warped to Lab x passed")
    else:
        print("Warped to Lab x failed")
        # print(x_mesh_LabRavel == x_mesh_WarpedInLab_ravel)
        # print("Where x_mesh_LabRavel != x_mesh_WarpedInLab_ravel:")
        # for i_point in range(len(x_mesh_WarpedInLab_ravel)):
        #     if x_mesh_LabRavel[i_point] != x_mesh_WarpedInLab_ravel[i_point]:
        #         print("x_mesh_LabRavel[i]={} != {}=x_mesh_WarpedInLab_ravel[i]".format(x_mesh_LabRavel[i_point],x_mesh_WarpedInLab_ravel[i_point]))
        plt.figure("WarpedMeshInLab_failedTest_x")
        plt.scatter(z_mesh_LabRavel,x_mesh_LabRavel, s = 1, label = "Original mesh")
        plt.scatter(z_mesh_WarpedInLab_ravel,x_mesh_WarpedInLab_ravel, s = 1, label = "Warped mesh")

        plt.title("Original vs transformed mesh from warped to lab")
        plt.xlabel("z [m]")
        # plt.ylabel("x [m]")
        # print("bend_start = {}".format(bend_start))
        # print("bend_end = {}".format(bend_end))
        # print("bend_radius = {}".format(bend_radius))
        plt.show()

    if (z_mesh_LabRavel == z_mesh_WarpedInLab_ravel).all():
        print("Warped to Lab z passed")
    else:
        print("Warped to Lab z failed")
        plt.figure("WarpedMeshInLab_failedTest_z")
        plt.scatter(z_mesh_LabRavel,x_mesh_LabRavel, s = 1, label = "Original mesh")
        plt.scatter(z_mesh_WarpedInLab_ravel,x_mesh_WarpedInLab_ravel, s = 1, label = "Warped mesh")

        plt.title("Original vs transformed mesh from warped to lab")
        plt.xlabel("z [m]")
        # plt.ylabel("x [m]")
        # print("bend_start = {}".format(bend_start))
        # print("bend_end = {}".format(bend_end))
        # print("bend_radius = {}".format(bend_radius))
        plt.show()


    if (x_mesh_WarpedRavel == x_mesh_LabInWarped_ravel).all():
        print("Lab to Warped x passed")
    else:
        print("Lab to Warped x failed")
        plt.figure("WarpedMeshInLab_failedTest_x")
        plt.scatter(z_mesh_WarpedRavel,x_mesh_WarpedRavel, s = 1, label = "Original mesh")
        plt.scatter(z_mesh_LabInWarped_ravel,x_mesh_LabInWarped_ravel, s = 1, label = "Warped mesh")

        plt.title("Original vs transformed mesh from warped to lab")
        plt.xlabel("z [m]")
        # plt.ylabel("x [m]")
        # print("bend_start = {}".format(bend_start))
        # print("bend_end = {}".format(bend_end))
        # print("bend_radius = {}".format(bend_radius))
        plt.show()

    if (z_mesh_WarpedRavel == z_mesh_LabInWarped_ravel).all():
        print("Lab to Warped z passed")
    else:
        print("Lab to Warped z failed")
        plt.figure("WarpedMeshInLab_failedTest_z")
        plt.scatter(z_mesh_WarpedRavel,x_mesh_WarpedRavel, s = 1, label = "Original mesh")
        plt.scatter(z_mesh_LabInWarped_ravel,x_mesh_LabInWarped_ravel, s = 1, label = "Warped mesh")

        plt.title("Original vs transformed mesh from warped to lab")
        plt.xlabel("z [m]")
        # plt.ylabel("x [m]")
        # print("bend_start = {}".format(bend_start))
        # print("bend_end = {}".format(bend_end))
        # print("bend_radius = {}".format(bend_radius))
        plt.show()




x_mesh_Lab2D = np.loadtxt("./FilesIn/meshXLab_1.log")
z_mesh_Lab2D = np.loadtxt("./FilesIn/meshZLab_1.log")

# # Loading waped mesh
x_mesh_Warped1D = np.loadtxt("./FilesIn/meshX1DWarped_1.log")
z_mesh_Warped1D = np.loadtxt("./FilesIn/meshZ1DWarped_1.log")


testNum = 1
bend_start = 0.15
bend_end = 0.5
bend_radius = 0.3
Test_WarpToLabAndLabToWarp(testNum,z_mesh_Lab2D,x_mesh_Lab2D,z_mesh_Warped1D,x_mesh_Warped1D,bend_start,bend_end,bend_radius)

x_mesh_Lab2D = np.loadtxt("./FilesIn/meshXLab_2.log")
z_mesh_Lab2D = np.loadtxt("./FilesIn/meshZLab_2.log")

# # Loading waped mesh
x_mesh_Warped1D = np.loadtxt("./FilesIn/meshX1DWarped_2.log")
z_mesh_Warped1D = np.loadtxt("./FilesIn/meshZ1DWarped_2.log")


testNum = 2
bend_start = 0.15
bend_end = 0.2
bend_radius = 0.1

Test_WarpToLabAndLabToWarp(testNum,z_mesh_Lab2D,x_mesh_Lab2D,z_mesh_Warped1D,x_mesh_Warped1D,bend_start,bend_end,bend_radius)


x_mesh_Lab2D = np.loadtxt("./FilesIn/meshXLab_3.log")
z_mesh_Lab2D = np.loadtxt("./FilesIn/meshZLab_3.log")

# # Loading waped mesh
x_mesh_Warped1D = np.loadtxt("./FilesIn/meshX1DWarped_3.log")
z_mesh_Warped1D = np.loadtxt("./FilesIn/meshZ1DWarped_3.log")


testNum = 3
bend_start = 0.15
bend_end = 0.2
bend_radius = -0.1

# The bending to the other side does not work from Warped to Lab, but not from Lab to Warped... 
Test_WarpToLabAndLabToWarp(testNum,z_mesh_Lab2D,x_mesh_Lab2D,z_mesh_Warped1D,x_mesh_Warped1D,bend_start,bend_end,bend_radius)



x_mesh_Lab2D = np.loadtxt("./FilesIn/meshXLab_4.log")
z_mesh_Lab2D = np.loadtxt("./FilesIn/meshZLab_4.log")

# # Loading waped mesh
x_mesh_Warped1D = np.loadtxt("./FilesIn/meshX1DWarped_4.log")
z_mesh_Warped1D = np.loadtxt("./FilesIn/meshZ1DWarped_4.log")


testNum = 4
bend_start = 0.15
bend_end = 0.5
bend_radius = 0.02

Test_WarpToLabAndLabToWarp(testNum,z_mesh_Lab2D,x_mesh_Lab2D,z_mesh_Warped1D,x_mesh_Warped1D,bend_start,bend_end,bend_radius)

x_mesh_Lab2D = np.loadtxt("./FilesIn/meshXLab_5.log")
z_mesh_Lab2D = np.loadtxt("./FilesIn/meshZLab_5.log")

# # Loading waped mesh
x_mesh_Warped1D = np.loadtxt("./FilesIn/meshX1DWarped_5.log")
z_mesh_Warped1D = np.loadtxt("./FilesIn/meshZ1DWarped_5.log")


testNum = 5
bend_start = 0.1
bend_end = 0.6
bend_radius = 0.5*4/np.pi

Test_WarpToLabAndLabToWarp(testNum,z_mesh_Lab2D,x_mesh_Lab2D,z_mesh_Warped1D,x_mesh_Warped1D,bend_start,bend_end,bend_radius)


x_mesh_Lab2D = np.loadtxt("./FilesIn/meshXLab_6.log")
z_mesh_Lab2D = np.loadtxt("./FilesIn/meshZLab_6.log")

# # Loading waped mesh
x_mesh_Warped1D = np.loadtxt("./FilesIn/meshX1DWarped_6.log")
z_mesh_Warped1D = np.loadtxt("./FilesIn/meshZ1DWarped_6.log")


testNum = 6
bend_start = 0.1
bend_end = 0.6
bend_radius = 0.5*3/np.pi

Test_WarpToLabAndLabToWarp(testNum,z_mesh_Lab2D,x_mesh_Lab2D,z_mesh_Warped1D,x_mesh_Warped1D,bend_start,bend_end,bend_radius)

x_mesh_Lab2D = np.loadtxt("./FilesIn/meshXLab_7.log")
z_mesh_Lab2D = np.loadtxt("./FilesIn/meshZLab_7.log")

# # Loading waped mesh
x_mesh_Warped1D = np.loadtxt("./FilesIn/meshX1DWarped_7.log")
z_mesh_Warped1D = np.loadtxt("./FilesIn/meshZ1DWarped_7.log")


testNum = 7
bend_start = 0.1
bend_end = 0.6
bend_radius = 0.5*2./np.pi/3.

Test_WarpToLabAndLabToWarp(testNum,z_mesh_Lab2D,x_mesh_Lab2D,z_mesh_Warped1D,x_mesh_Warped1D,bend_start,bend_end,bend_radius)

# The bending over pi does work from Warped to Lab, but not from Lab to Warped...