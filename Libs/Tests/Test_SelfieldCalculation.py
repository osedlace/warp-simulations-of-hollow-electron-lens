import numpy as np
import h5py 
from tqdm import tqdm
import itertools
import matplotlib.pyplot as plt
import sys
sys.path.append("../")
from SelfieldsCalculation_Header import Set_ESelfield, Set_EAnalytical 
from ParticleAndMesh_ClassHeader import Particle, Mesh_point

# class Mesh_3DGrid:
#     """

#     """
#     def __init__(self, dx,dy,dz,):
#         self.
            # self.particles = []

nx = 10
ny = 1
nz = 1
dx = 0.5
dy = 0.5
dz = 0.5
x_s = -2.5
y_s = 0
z_s = 0

# Axis of the mesh 
mesh_x = np.array([x_i*dx+x_s for x_i in np.arange(nx)])
mesh_y = np.array([y_i*dy+y_s for y_i in np.arange(ny)])
mesh_z = np.array([z_i*dz+z_s for z_i in np.arange(nz)])

# The mesh array of Mesh_points
mesh_aaa_calculate = np.array([[[Mesh_point(x_i,y_i,z_i) for x_i in mesh_x] for y_i in mesh_y] for z_i in mesh_z])
mesh_a_calculate = mesh_aaa_calculate.ravel()
mesh_aaa_ideal = np.array([[[Mesh_point(x_i,y_i,z_i) for x_i in mesh_x] for y_i in mesh_y] for z_i in mesh_z])
mesh_a_ideal = mesh_aaa_ideal.ravel()


# Creating the particles
numOfParticles = 100000

# Constants
# -------------------
rmax = 1 # m
e = 1.602e-19 # C
k = 0.8988e10 # N*m^2*C^{-2}
ek = e*k

# Distribution of particles
particle_a = np.array([Particle() for i in np.arange(numOfParticles)])
for i in np.arange(numOfParticles):
        r = rmax*np.sqrt(np.random.random())
        # r = 1
        theta = 2.0*np.pi*np.random.random()
        phi = np.arccos(2*np.random.random() - 1.)
        particle_a[i].x = r*np.sin(phi)*np.cos(theta) # m
        particle_a[i].y = r*np.sin(phi)*np.sin(theta) # m
        particle_a[i].z = r*np.cos(phi) # m
        # particle_a = np.append(particle_a,Particle(x,y,z))



# print(particle_a)
# for particle in particle_a:
#     print("x,y,z    = ({},{},{})".format(particle.x,particle.y,particle.z))
#     print("vx,vy,vz = ({},{},{})".format(particle.vx,particle.vy,particle.vz))

# print(particle_a)

# pbar = tqdm(total = len(mesh_a)) # Just for timing

Set_EAnalytical(mesh_a = mesh_a_ideal, overallChargeTk = numOfParticles*ek)

parbar = tqdm(total = len(mesh_a_calculate)*len(particle_a)*3) # Just for timing
Set_ESelfield(mesh_a = mesh_a_calculate, particle_a = particle_a,particleChargeTk = ek, parbar = parbar )

for mesh_point_j_calculated,mesh_point_j_ideal in itertools.izip(mesh_a_calculate,mesh_a_ideal):
    # if mesh_point_j_calculated.x**2 + mesh_point_j_calculated.y**2 + mesh_point_j_calculated.z**2 <= 1:
    print("x,y,z               = ({},{},{})".format(mesh_point_j_calculated.x,mesh_point_j_calculated.y,mesh_point_j_calculated.z))
    print("Calculated Ex,Ey,Ez = ({},{},{})".format(mesh_point_j_calculated.Ex,mesh_point_j_calculated.Ey,mesh_point_j_calculated.Ez))
    print("Ideal      Ex,Ey,Ez = ({},{},{}) ".format(mesh_point_j_ideal.Ex,mesh_point_j_ideal.Ey,mesh_point_j_ideal.Ez))
    print("E_size Calculated = {}".format(np.sqrt(mesh_point_j_calculated.Ex**2 + mesh_point_j_calculated.Ey**2 + mesh_point_j_calculated.Ez**2)))
    print("E_size Ideal = {}\n".format(np.sqrt(mesh_point_j_ideal.Ex**2 + mesh_point_j_ideal.Ey**2 + mesh_point_j_ideal.Ez**2)))


# Plot the field mesh_a_calculate



# meshgrid_x,meshgrid_y = np.meshgrid(mesh_x,mesh_y)

z_i_slice = len(mesh_z)/2 # y_i slice
y_i_slice = len(mesh_y)/2 # y_i slice
# print("y_slice = {}".format(y_w[y_i]))

Ex_Calculated_2d = np.array([[mesh_aaa_calculate[z_i_slice][y_i][x_i].Ex for x_i in np.arange(nx)] for y_i in np.arange(ny)])
Ey_Calculated_2d = np.array([[mesh_aaa_calculate[z_i_slice][y_i][x_i].Ey for x_i in np.arange(nx)] for y_i in np.arange(ny)])
Ez_Calculated_2d = np.array([[mesh_aaa_calculate[z_i_slice][y_i][x_i].Ez for x_i in np.arange(nx)] for y_i in np.arange(ny)])

Ex_Ideal_2d = np.array([[mesh_aaa_ideal[z_i_slice][y_i][x_i].Ex for x_i in np.arange(nx)] for y_i in np.arange(ny)])
Ey_Ideal_2d = np.array([[mesh_aaa_ideal[z_i_slice][y_i][x_i].Ey for x_i in np.arange(nx)] for y_i in np.arange(ny)])
Ez_Ideal_2d = np.array([[mesh_aaa_ideal[z_i_slice][y_i][x_i].Ez for x_i in np.arange(nx)] for y_i in np.arange(ny)])

Ex_Calculated_1d = np.array([mesh_aaa_calculate[z_i_slice][y_i_slice][x_i].Ex for x_i in np.arange(nx)])
Ey_Calculated_1d = np.array([mesh_aaa_calculate[z_i_slice][y_i_slice][x_i].Ey for x_i in np.arange(nx)])
Ez_Calculated_1d = np.array([mesh_aaa_calculate[z_i_slice][y_i_slice][x_i].Ez for x_i in np.arange(nx)])

Ex_Ideal_1d = np.array([mesh_aaa_ideal[z_i_slice][y_i_slice][x_i].Ex for x_i in np.arange(nx)])
Ey_Ideal_1d = np.array([mesh_aaa_ideal[z_i_slice][y_i_slice][x_i].Ey for x_i in np.arange(nx)])
Ez_Ideal_1d = np.array([mesh_aaa_ideal[z_i_slice][y_i_slice][x_i].Ez for x_i in np.arange(nx)])



# plt.figure("warped_prop_bz")
# plt.contourf(meshgridWarped_z,meshgridWarped_x,bz_warped_2d,50)
# plt.colorbar()

# plt.figure("Ideal Ex in xy ")
# # plt.streamplot(meshgrid_x,meshgrid_y,Ex_Ideal_2d,Ey_Ideal_2d, color = "red", linewidth=1, cmap=plt.cm.inferno, density=2, arrowstyle='->', arrowsize=1.5)
# plt.contourf(meshgrid_x,meshgrid_y,Ex_Ideal_2d,100)
# plt.axes().set_aspect('equal', 'box')
# plt.colorbar()

# plt.figure("Calculated Ex in xy")
# # plt.streamplot(meshgrid_x,meshgrid_y,Ex_Ideal_2d,Ey_Ideal_2d, color = "red", linewidth=1, cmap=plt.cm.inferno, density=2, arrowstyle='->', arrowsize=1.5)
# plt.contourf(meshgrid_x,meshgrid_y,Ex_Calculated_2d,100)
# plt.axes().set_aspect('equal', 'box')
# plt.colorbar()

plt.figure("Ideal Ex in x")
plt.plot(mesh_x,Ex_Ideal_1d)
plt.xlabel("x [m] (chargeBall, r = 1 m)")
plt.ylabel("Ex[N/C]")
# print(Ex_Ideal_1d)
# plt.axes().set_aspect('equal', 'box')

plt.figure("Calculated Ex in x")
plt.plot(mesh_x,Ex_Calculated_1d)
plt.xlabel("x [m] (chargeBall, r = 1 m)")
plt.ylabel("Ex[N/C]")
# plt.axes().set_aspect('equal', 'box')

plt.show()



    