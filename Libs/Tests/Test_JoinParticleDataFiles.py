"""
Unit testing the JoinParticleDataFiles function 
"""

import h5py 
import numpy as np
import sys

sys.path.append("..")
from Plot_diagnostics_Header import JoinParticleDataFiles

id_number = "JoinFilesTest"
numOfProcesses = 36
numOfData = 10
name_FilesToJoin = ["./Files/ParticleData_id_{}part_{}.h5py".format(id_number, processId) for processId in range(numOfProcesses)]
name_FileToSave = "./Files/ParticleData_id_{}.h5py".format(id_number)


x = np.array([])
y = np.array([])
z = np.array([])
vx = np.array([])
vy = np.array([])
vz = np.array([])
kinEn = np.array([])
x_app = np.array([])
y_app = np.array([])
z_app = np.array([])
vx_app = np.array([])
vy_app = np.array([])
vz_app = np.array([])
kinEn_app = np.array([])

for i,name_FileToJoin in enumerate(name_FilesToJoin):
    with h5py.File(name_FileToJoin,"w") as f_partdata:
        x = np.array([i*numOfData + r for r in range(numOfData)])
        y = np.array([i*numOfData + r for r in range(numOfData)])
        z = np.array([i*numOfData + r for r in range(numOfData)])
        vx = np.array([i*numOfData + r for r in range(numOfData)])
        vy = np.array([i*numOfData + r for r in range(numOfData)])
        vz = np.array([i*numOfData + r for r in range(numOfData)])
        kinEn = np.array([i*numOfData + r for r in range(numOfData)])
        x_app = np.append(x_app,x)
        y_app = np.append(y_app,y)
        z_app = np.append(z_app,z)
        vx_app = np.append(vx_app,vx)
        vy_app = np.append(vy_app,vy)
        vz_app = np.append(vz_app,vz)
        kinEn_app = np.append(kinEn_app,kinEn)
        dx = f_partdata.create_dataset("x", data = x)
        dy = f_partdata.create_dataset("y", data = y)
        dz = f_partdata.create_dataset("z", data = z)
        dvx = f_partdata.create_dataset("vx", data = vx)
        dvy = f_partdata.create_dataset("vy", data = vy)
        dvz = f_partdata.create_dataset("vz", data = vz)
        dkinEn = f_partdata.create_dataset("KinEn", data = kinEn)


JoinParticleDataFiles(  name_FilesToJoin = name_FilesToJoin,\
                        name_FileToSave = name_FileToSave)

with h5py.File(name_FileToSave,"r") as JoinedFile:
    if (np.array(JoinedFile["x"]) == x_app).all():
        print("x joint test passed")
    else:
        print("x joint test failed")
        print("x = {}".format(x))
        print("x_app = {}".format(x_app))

    if (np.array(JoinedFile["y"]) == y_app).all():
        print("y joint test passed")
    else:
        print("y joint test failed")

    if (np.array(JoinedFile["z"]) == z_app).all():
        print("z joint test passed")
    else:
        print("z joint test failed")

    if (np.array(JoinedFile["vx"]) == vx_app).all():
        print("vx joint test passed")
    else:
        print("vx joint test failed")

    if (np.array(JoinedFile["vy"]) == vy_app).all():
        print("vy joint test passed")
    else:
        print("vy joint test failed")

    if (np.array(JoinedFile["vz"]) == vz_app).all():
        print("vz joint test passed")
    else:
        print("vz joint test failed")

    if (np.array(JoinedFile["KinEn"]) == kinEn_app).all():
        print("KinEn joint test passed")
    else:
        print("KinEn joint test failed")
