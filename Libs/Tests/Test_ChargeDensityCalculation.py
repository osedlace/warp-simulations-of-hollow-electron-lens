import numpy as np
# import h5py 
from tqdm import tqdm
# import itertools
import matplotlib.pyplot as plt
import sys
sys.path.append("../")
from ParticleAndMesh_ClassHeader import Particle, Mesh_point
from BeamProperties_Header import CalculateRhoJ

# Create testing particles
numOfParticles = 1000000

# Constants
# -------------------
# Cube dimensions
amin = -1 # m 
amax = 1 # m  
# e = 1.602e-19 # C
e = 1 # C
q = e*1

print("Started Creating particles")

# Distribution of particles

# Uniform distribution from 0 to 1
particlesUniform = np.array([Particle() for i in np.arange(numOfParticles)])
# for i in np.arange(numOfParticles):
#     particlesUniform[i].x = np.random.random() # m
#     particlesUniform[i].y = np.random.random() # m
#     particlesUniform[i].z = np.random.random() # m
#     particlesUniform[i].vx = np.random.random() # m
#     particlesUniform[i].vy = np.random.normal() # m
#     particlesUniform[i].vz = np.random.normal() # m

# Normal distribution
particlesGauss = np.array([Particle() for i in np.arange(numOfParticles)])
for i in np.arange(numOfParticles):
    particlesGauss[i].x = np.random.normal() # m
    particlesGauss[i].y = np.random.normal() # m
    particlesGauss[i].z = np.random.normal() # m
    particlesGauss[i].vx = np.random.random() # m
    particlesGauss[i].vy = np.random.normal() # m
    particlesGauss[i].vz = np.random.normal() # m


# Create testing mesh for chargeDensity calculation
dx=dy=dz=0.2
numOfMeshpoints = int(((amax-amin)/dx + 1)**3)
numOfMeshpoints1D = int((amax-amin)/dx + 1)
mesh3D = np.array([[[Mesh_point(x=x,y=y,z=z) for x in np.arange(amin,amax+dx,dx)]for y in np.arange(amin,amax+dx,dx)]for z in np.arange(amin,amax+dx,dx)])
mesh = mesh3D.ravel()


# Calculate the charge density on mesh


print("Started Calculating Charge density")
parbar = tqdm(total = numOfMeshpoints) # Just for timing
CalculateRhoJ(mesh_a = mesh,particles_a = particlesUniform,dx = dx,q=q,parbar = parbar)




# Plot results

Jx = [mesh_point.Jx for mesh_point in mesh3D[5,5,::]]
Jxy = [mesh_point.Jx for mesh_point in mesh3D[5,::,5]]
Jy = [mesh_point.Jy for mesh_point in mesh3D[5,::,5]]
Jyx = [mesh_point.Jy for mesh_point in mesh3D[5,5,::]]
Jz = [mesh_point.Jz for mesh_point in mesh3D[::,5,5]]
Jzy = [mesh_point.Jz for mesh_point in mesh3D[5,::,5]]
Rho = [mesh_point.Rho for mesh_point in mesh3D[5,5,::]]
x = [mesh_point.x for mesh_point in mesh3D[5,5,::]]
xy = [mesh_point.x for mesh_point in mesh3D[5,::,5]]
y = [mesh_point.y for mesh_point in mesh3D[5,::,5]]
yx = [mesh_point.y for mesh_point in mesh3D[5,5,::]]
z = [mesh_point.z for mesh_point in mesh3D[::,5,5]]
zy = [mesh_point.z for mesh_point in mesh3D[5,::,5]]
plt.figure("Rho vs x")
plt.scatter(x,Rho)

plt.figure("Jx vs x")
plt.scatter(x,Jx)
plt.figure("Jx vs y")
plt.scatter(xy,Jxy)


plt.figure("Jy vs y")
plt.scatter(y,Jy)
plt.figure("Jy vs x")
plt.scatter(yx,Jyx)

plt.figure("Jz vs z")
plt.scatter(z,Jz)
plt.figure("Jz vs y")
plt.scatter(zy,Jzy)


plt.show()