class Particle:
    """
    Properties:
    x,y,z,vx,vy,vz
    """
    def __init__(self,x=0,y=0,z=0,vx=0,vy=0,vz=0):
        # super(Particle, self).__init__()
        self.x = x
        self.y = y
        self.z = z
        self.vx = vx
        self.vy = vy
        self.vz = vz


class Mesh_point:
    """
    Properties:
    x,y,z,Ex,Ey,Ez,Bx,By,Bz,
    """
    def __init__(self,x=0.,y=0.,z=0.,Ex=0.,Ey=0.,Ez=0.,Bx=0.,By=0.,Bz=0.,Rho=0.,Jx=0.,Jy=0.,Jz=0.,Ix=0.,Iy=0.,Iz=0.,I=0.,Tx=0.,Ty=0.,Tz=0.,T=0.,Ttheta=0.,Tr=0.,Tm=0.,Lambda=0.,LambdaApprox=0.,OmegaL=0.,n =0.,mean_x=0.,mean_y=0.,mean_z=0.,mean_Ekin_z=0.,mean_vx=0.,mean_vy=0.,mean_vz=0.,qPm=0.):
        self.x = x
        self.y = y
        self.z = z

        self.Ex = Ex
        self.Ey = Ey
        self.Ez = Ez

        self.Bx = Bx
        self.By = By
        self.Bz = Bz

        self.Rho = Rho
        self.Jx = Jx
        self.Jy = Jy
        self.Jz = Jz

        self.Ix = Ix
        self.Iy = Iy
        self.Iz = Iz
        self.I=I

        self.Tx = Tx
        self.Ty = Ty
        self.Tz = Tz
        self.T = T
        self.Ttheta = Ttheta
        self.Tr = Tr
        self.Tm = Tm

        self.Lambda = Lambda
        self.LambdaApprox = LambdaApprox
        self.OmegaL = OmegaL
        self.n = n

        self.mean_x = mean_x
        self.mean_y = mean_y
        self.mean_z = mean_z
        self.mean_Ekin_z = mean_Ekin_z 

        self.mean_vx = mean_vx
        self.mean_vy = mean_vy
        self.mean_vz = mean_vz

        self.qPm = qPm
# class Mesh1D:
#     """
#     Properties:
#     x,y,z,Ex,Ey,Ez,Bx,By,Bz,
#     """
#     def __init__(self,xmin,xmax,dx):
#         self.points = [Mesh_point(x) for x in xrange(xmin,xmax,dx)]


# class Mesh:
#     """
#     Properties:
#     x,y,z,Ex,Ey,Ez,Bx,By,Bz,
#     """
#     def __init__(self,numOfPoints):
#         self.points = [Mesh_point(x) for x in xrange(numOfPoints)]
#     def __init__(self,xmin,xmax,xmin,xmax,xmin,xmax,dx,dy,dz):
#         self.points = [[[Mesh_point(x,y,z) for x in xrange(xmin,xmax,dx)]for y in xrange(ymin,ymax,dy)]for z in xrange(zmin,zmax,dz)]

