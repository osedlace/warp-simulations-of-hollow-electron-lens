"""
    The transformation of magnetic field functions
"""

import numpy as np
import math
import matplotlib.pyplot as plt
import h5py 
import itertools as it 
from tqdm import tqdm
from CoordinationTransformationsHeader import *

# pbar = tqdm(total = 0)

def Aproximate_RZ(zz_lab_r, xx_lab_r, zmesh_box_lab_v, xmesh_box_lab_v, bz_1_vvv, bx_1_vvv, by_1_vvv, bend_s, bend_e, bend_r, distanceTreshold, round_flag = True):
    """
    zmesh_box_lab_v = array of the z points of the box (the space coordiantes of bz/bx_1_vvv) in laboratory coordinates 
    xmesh_box_lab_v = array of the x points of the box (the space coordiantes of bz/bx_1_vvv) in laboratory coordinates  
    bz_1_vvv = 3D array of the Bz in laboratory coordinates (must correspond and sit on the z/xmesh_box_lab_v)
    bx_1_vvv = 3D array of the Bx in laboratory coordinates (must correspond and sit on the z/xmesh_box_lab_v)
    zz_lab_r = z coordinate of a point where the vector is to be aproximated
    xx_lab_r = x coordinate of a point where the vector is to be aproximated
    bend_s = lowest z of the bend
    bend_e = highest z of the bend
    bend_r = radius of the bend
    round_flag = round_flag Carefull, this can have an effect on the code if the better precision for the coordinates is needed (finner mesh),
                 but removes some rounding effect stemming frin calculation process. If Off, the effect it removes should be negligible...
    distanceTreshold = The distance between points where no approximation is done

    This function takes the point (zz_lab_r,xx_lab_r, must be inside the mag. field mesh), and a magnetic field sitting on a mesh/grid.
    It aproximates (linearly) the vector of the magnetic field at that point (if the point is not identical with mesh-point, 4 vectors surounding the point are used for the aproximation).
    The aproximated vector and its coordinates are then transformed to warped coordinates and returned
    """
    y_i = 0
    bx_1_lab_ap = -100
    bz_1_lab_ap = -100

    lab_z_1_To_zz_lab_ri_distance_a = np.array([abs(z_1 - zz_lab_r) for z_1 in zmesh_box_lab_v])
    lab_x_1_To_xx_lab_ri_distance_a = np.array([abs(x_1 - xx_lab_r) for x_1 in xmesh_box_lab_v])
    pointAtZline_flag = min(lab_z_1_To_zz_lab_ri_distance_a) < distanceTreshold # The point is on the z-line -> Aproximation not needed in this dimension 
    pointAtXline_flag = min(lab_x_1_To_xx_lab_ri_distance_a) < distanceTreshold # The point is on the x-line -> Aproximation not needed in this dimension 

    if  pointAtZline_flag and pointAtXline_flag: # The points overlap -> Aproximation not needed

        if round_flag:
            precise_z = np.where(lab_z_1_To_zz_lab_ri_distance_a<=distanceTreshold)
            precise_x = np.where(lab_x_1_To_xx_lab_ri_distance_a<=distanceTreshold)
        else:
            precise_z = np.where(zmesh_box_lab_v == zz_lab_r)
            precise_x = np.where(xmesh_box_lab_v == xx_lab_r)

        z_i = precise_z[0][0]
        x_i = precise_x[0][0]
        bx_1_lab_ap = bx_1_vvv[x_i][y_i][z_i]
        by_1_lab_ap = by_1_vvv[x_i][y_i][z_i]
        bz_1_lab_ap = bz_1_vvv[x_i][y_i][z_i]


    elif pointAtZline_flag: # The point is on the z-line -> Aproximation not needed in this dimension
        if round_flag:
            precise_z = np.where(lab_z_1_To_zz_lab_ri_distance_a<=distanceTreshold)
        else:
            precise_z = np.where(zmesh_box_lab_v == zz_lab_r)
        first_greater_x = np.min(np.where(xmesh_box_lab_v > xx_lab_r)[0])
        z_i = precise_z[0][0]
        x_i = first_greater_x
        weights = np.zeros(2)
        weights[0] = PPDistance(zz_lab_r,xx_lab_r,zmesh_box_lab_v[z_i],xmesh_box_lab_v[x_i])
        weights[1] = PPDistance(zz_lab_r,xx_lab_r,zmesh_box_lab_v[z_i],xmesh_box_lab_v[x_i-1])
        # Normalization of the weights:
        weights = [weight/sum(weights) for weight in weights]

        bx_1_lab_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i-1][y_i][z_i]
        by_1_lab_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i-1][y_i][z_i]
        bz_1_lab_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i-1][y_i][z_i]


    elif pointAtXline_flag: # The point is on the z-line -> Aproximation not needed in this dimension

        first_greater_z = np.min(np.where(zmesh_box_lab_v > zz_lab_r)[0])
        if round_flag:
            precise_x = np.where(lab_x_1_To_xx_lab_ri_distance_a<=distanceTreshold)
        else:
            precise_x = np.where(xmesh_box_lab_v == xx_lab_r)

        z_i = first_greater_z
        x_i = precise_x[0][0]
        weights = np.zeros(2)
        weights[0] = PPDistance(zz_lab_r,xx_lab_r,zmesh_box_lab_v[z_i],xmesh_box_lab_v[x_i])
        weights[1] = PPDistance(zz_lab_r,xx_lab_r,zmesh_box_lab_v[z_i-1],xmesh_box_lab_v[x_i])
        # Normalization of the weights:
        weights = [weight/sum(weights) for weight in weights]

        bx_1_lab_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i][y_i][z_i-1]
        by_1_lab_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i][y_i][z_i-1]
        bz_1_lab_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i][y_i][z_i-1]


    else:
        first_greater_z = np.min(np.where(zmesh_box_lab_v > zz_lab_r)[0])
        first_greater_x = np.min(np.where(xmesh_box_lab_v > xx_lab_r)[0])
        z_i = first_greater_z
        x_i = first_greater_x
        weights = np.zeros(4)
        weights[0] = PPDistance(zz_lab_r,xx_lab_r,zmesh_box_lab_v[z_i],xmesh_box_lab_v[x_i])
        weights[1] = PPDistance(zz_lab_r,xx_lab_r,zmesh_box_lab_v[z_i],xmesh_box_lab_v[x_i-1])
        weights[2] = PPDistance(zz_lab_r,xx_lab_r,zmesh_box_lab_v[z_i-1],xmesh_box_lab_v[x_i-1])
        weights[3] = PPDistance(zz_lab_r,xx_lab_r,zmesh_box_lab_v[z_i-1],xmesh_box_lab_v[x_i])
        # Normalization of the weights:
        weights = [weight/sum(weights) for weight in weights]


        bx_1_lab_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i-1][y_i][z_i] + weights[2]*bx_1_vvv[x_i-1][y_i][z_i-1] + weights[3]*bx_1_vvv[x_i][y_i][z_i-1]
        by_1_lab_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i-1][y_i][z_i] + weights[2]*by_1_vvv[x_i-1][y_i][z_i-1] + weights[3]*by_1_vvv[x_i][y_i][z_i-1]
        bz_1_lab_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i-1][y_i][z_i] + weights[2]*bz_1_vvv[x_i-1][y_i][z_i-1] + weights[3]*bz_1_vvv[x_i][y_i][z_i-1]

    by_1_warped_final_r = by_1_lab_ap
    bz_1_warped_final_r, bx_1_warped_final_r, z_1_warped_final_r, x_1_warped_final_r = LabToWarped_vector(bz_1_lab_ap,bx_1_lab_ap,zz_lab_r,xx_lab_r,bend_s,bend_e,bend_r)
    return bz_1_warped_final_r, bx_1_warped_final_r, by_1_warped_final_r, z_1_warped_final_r, x_1_warped_final_r

def MagneticFieldTransformation_boxMesh_RZ(zmesh_box_lab_v, xmesh_box_lab_v, bz_lab_vvv, bx_lab_vvv, by_lab_vvv, zmesh_w_v, xmesh_w_v, bend_par_v, round_flag = True, distanceTreshold = 1e-10):
    """
    The box mesh must be rectangular,
    zmesh_box_lab_v = array of the z points of the box (the space coordiantes of bz/bx_1_vvv) in laboratory coordinates 
    xmesh_box_lab_v = array of the x points of the box (the space coordiantes of bz/bx_1_vvv) in laboratory coordinates 
    bz_lab_vvv = 3D array of the Bz in laboratory coordinates (must correspond and sit on the z/xmesh_box_lab_v)
    bx_lab_vvv = 3D array of the Bx in laboratory coordinates (must correspond and sit on the z/xmesh_box_lab_v)
    by_lab_vvv = 3D array of the By in laboratory coordinates (must correspond and sit on the z/xmesh_box_lab_v)
    zmesh_w_v = array of the z points of warped mesh in warped coordinates 
    xmesh_w_v = array of the x points of warped mesh in warped coordinates 
    bend_par_v = parameters of the bend, must be in format:

    bend_start = bend_par_v[0] - lowest z of the bend
    bend_end = bend_par_v[1] - - highest z of the bend
    bend_radius = bend_par_v[2] - radius of the bend

    round_flag = flag if to use distanceTreshold or exact coordinates for points to be delared identical and no aproximation is then done, for more info viz Aproximation function
    distanceTreshold = the distance treshold to declare two points as identical



    The idea is following: Get warped mesh into lab, aproximate the field in the lab on the warped mesh in lab, 
    and then transform this field back into warped coordinates, and the x y z will create a good grid which is esential.
    The aproximation of the field is done in the lab coord. instead in the warped coord.
    because of more clean and safe aproximation from grided data into "non-grid" instead the opposite.

    This function returns takes the bz and bx as 3D but returns bz_final and bx_final as a 2D (z and x dimensions) - the y dimension is ommited as RZ and XYZ could affect this.
    """

    # Parameters

    # Few pre-calculations
    z_len = len(zmesh_w_v)
    x_len = len(xmesh_w_v)
    y_len = 1 # y_len = 1 because the y dimension is not used
    y_i = 0
    mesh_w_numOfPoints = z_len*x_len
    print("mesh_w_numOfPoints: {}".format(mesh_w_numOfPoints))

    bend_start = bend_par_v[0]
    bend_end = bend_par_v[1]
    bend_radius = bend_par_v[2]

    #Declaring empty arrays for filling

        #Final bx and bz aproximated and back in warped coordinates
    by_1_warped_final_r = np.zeros(mesh_w_numOfPoints)
    bx_1_warped_final_r = np.zeros(mesh_w_numOfPoints)
    bz_1_warped_final_r = np.zeros(mesh_w_numOfPoints)

        #Final x an z of the bx and bz back in warped coordinates nonzero only inside the box, --> can be used for the box identification in warped coordinates otherwise not used
    x_1_warped_final_r = np.zeros(mesh_w_numOfPoints)
    z_1_warped_final_r = np.zeros(mesh_w_numOfPoints)

        # The mesh warped mesh in Lab coordinates:
    xx_lab = np.zeros((x_len,y_len,z_len))
    zz_lab = np.zeros((x_len,y_len,z_len))


        # Transformation of the warped mesh to lab coordinates
    for x_i in range(x_len):
        for z_i in range(z_len):
            zz_lab[x_i][y_i][z_i], xx_lab[x_i][y_i][z_i] = WarpedToLab_point(zmesh_w_v[z_i],xmesh_w_v[x_i],bend_start,bend_end,bend_radius)

    xx_lab_r = xx_lab.ravel()
    zz_lab_r = zz_lab.ravel()

    # plt.figure("MeshInLAB")
    # plt.scatter(zz_lab_r,xx_lab_r, s = 1, color = "b", label = "Warped mesh")
    # plt.legend()



    # Box cropping of the transformed warped mesh in lab - Finding the points that will be used for the aproximation (inside the bfield_box) 
    used_coord_0 = np.where(zz_lab_r >= zmesh_box_lab_v[0])
    used_coord = used_coord_0
    used_coord_1 = np.where(zz_lab_r[used_coord] <= zmesh_box_lab_v[-1])
    used_coord = (used_coord[0][used_coord_1],) 
    used_coord_2 = np.where(xx_lab_r[used_coord] <= xmesh_box_lab_v[-1])
    used_coord = (used_coord[0][used_coord_2],) 
    used_coord_3 = np.where(xx_lab_r[used_coord] >= xmesh_box_lab_v[0])
    used_coord = (used_coord[0][used_coord_3],) 


    print("The box of     : {} < z < {}".format(zmesh_box_lab_v[-1],zmesh_box_lab_v[0]))
    print("magnetic field : {} < x < {}".format(xmesh_box_lab_v[-1],xmesh_box_lab_v[0]))

    # print("Original zz_lab_r: {}".format(zz_lab_r))
    # print("Croped zz_lab_r: {}".format(zz_lab_r[used_coord]))
    # print("Original xx_lab_r: {}".format(xx_lab_r))
    # print("Croped xx_lab_r: {}".format(xx_lab_r[used_coord]))

    # Aproximating the B field from box mesh into the warped mesh points in lab coordinates and then transforming them back into warped coordinates
    print(used_coord[0]) 
    for i_warpedMesh in used_coord[0]:
        bz_1_warped_final_r[i_warpedMesh], bx_1_warped_final_r[i_warpedMesh], by_1_warped_final_r[i_warpedMesh],\
        z_1_warped_final_r[i_warpedMesh], x_1_warped_final_r[i_warpedMesh] = \
        Aproximate_RZ(  zmesh_box_lab_v = zmesh_box_lab_v,\
                        xmesh_box_lab_v = xmesh_box_lab_v,\
                        bz_1_vvv = bz_lab_vvv,\
                        bx_1_vvv = bx_lab_vvv,\
                        by_1_vvv = by_lab_vvv,\
                        zz_lab_r = zz_lab_r[i_warpedMesh],\
                        xx_lab_r = xx_lab_r[i_warpedMesh],\
                        bend_s = bend_start,\
                        bend_e = bend_end,\
                        bend_r = bend_radius,\
                        distanceTreshold = distanceTreshold,\
                        round_flag = round_flag)
        
    by_1_warped_final = by_1_warped_final_r.reshape(x_len,y_len,z_len)
    bx_1_warped_final = bx_1_warped_final_r.reshape(x_len,y_len,z_len)
    bz_1_warped_final = bz_1_warped_final_r.reshape(x_len,y_len,z_len)
    # print("bz_1_warped_final {}".format(bz_1_warped_final_r[used_coord]))
    # print("bx_1_warped_final {}".format(bx_1_warped_final_r[used_coord]))


    # Just testting plot of warped mesh and used cooridnates (inside the box in warped coordinates)
    # zz_w = np.array([[zmes_w for zmes_w in zmesh_w_v] for xmesh_w in xmesh_w_v])
    # xx_w = np.array([[xmesh_w for zmes_w in zmesh_w_v] for xmesh_w in xmesh_w_v])

    # xx_w_r = xx_w.ravel()
    # zz_w_r = zz_w.ravel()

    # plt.figure("mesh")
    # plt.scatter(zz_w_r,xx_w_r, s = 1, color = "b", label = "Warped mesh")
    # plt.scatter(z_1_warped_final_r, x_1_warped_final_r, s = 1, color = "r", label = "Warped mesh")
    # plt.legend()

    return bz_1_warped_final, bx_1_warped_final, by_1_warped_final


def MagneticFieldTransformation_boxPar_RZ(bfield_box_par_v, dz_box_lab, dx_box_lab, bz_lab_vvv, bx_lab_vvv, by_lab_vvv, zmesh_w_v, xmesh_w_v, bend_par_v, round_flag = True, distanceTreshold = 1e-10):
    """
    The bfield box must be rectangular and the bfield_box_par_v in format: 

    bfield_box_xcent_lab = bfield_box_par_v[0] - x center at lowest z
    bfield_box_zs_lab = bfield_box_par_v[1] - lowest z
    bfield_box_ze_lab = bfield_box_par_v[2] - higest z
    bfield_box_halfOfwidth_lab = bfield_box_par_v[3] - half of the width of side in x dimension ( so maybe technically hight and not width but wathever)

    dz_box_lab = the increment in z direction of the box mesh in laboratory coordinates 
    dx_box_lab = the increment in x direction of the box mesh in laboratory coordinates 
    bz_lab_vvv = 3D array of the Bz in laboratory coordinates (must correspond and sit on the z/xmesh_box_lab_v)
    bx_lab_vvv = 3D array of the Bx in laboratory coordinates (must correspond and sit on the z/xmesh_box_lab_v)
    by_lab_vvv = 3D array of the By in laboratory coordinates (must correspond and sit on the z/xmesh_box_lab_v)
    zmesh_w_v = array of the z points of warped mesh in warped coordinates 
    xmesh_w_v = array of the x points of warped mesh in warped coordinates 
    bend_par_v = parameters of the bend, must be in format:

    bend_start = bend_par_v[0] - lowest z of the bend
    bend_end = bend_par_v[1] - - highest z of the bend
    bend_radius = bend_par_v[2] - radius of the bend

    round_flag = flag if to use distanceTreshold or exact coordinates for points to be delared identical and no aproximation is then done, for more info viz Aproximation function
    distanceTreshold = the distance treshold to declare two points as identical

    The idea is following: Get warped mesh in warped coordinates, transform it into lab coordinates, aproximate the field in the lab coordinates onto the warped mesh in lab coordinates, 
    and then transform this field back into warped coordinates, and the x y z will create a good grid which is esential for applying the field
    The aproximation of the field is done in the lab coord. instead in the warped coord.
    because of more clean and safe aproximation from grided data into "non-grid" instead the opposite.

    This function returns takes the bz and bx as 3D but returns bz_final and bx_final as a 2D (z and x dimensions) - the y dimension is ommited as RZ and XYZ could affect this.
    """
    bfield_box_xcent_lab = bfield_box_par_v[0]
    bfield_box_zs_lab = bfield_box_par_v[1]
    bfield_box_ze_lab = bfield_box_par_v[2]
    bfield_box_halfOfwidth_lab = bfield_box_par_v[3]
    print("bfield_box_par_v: {}".format(bfield_box_par_v))

    # Defining the bfield mesh in the LAB
    nx_box = int(bfield_box_halfOfwidth_lab*2./dx_box_lab + 1.)
    nz_box = int((bfield_box_ze_lab-bfield_box_zs_lab)/dz_box_lab + 1.)
    
    xmesh_box_lab_v = [bfield_box_xcent_lab - bfield_box_halfOfwidth_lab + x_i*dx_box_lab for x_i in range(nx_box)]
    zmesh_box_lab_v = [bfield_box_zs_lab + z_i*dz_box_lab for z_i in range(nz_box)]

    # print(np.shape(xmesh_box_lab_v))

    bz_1_warped_final, bx_1_warped_final, by_1_warped_final, = MagneticFieldTransformation_boxMesh_RZ( zmesh_box_lab_v = zmesh_box_lab_v,\
                                                                                                    xmesh_box_lab_v = xmesh_box_lab_v,\
                                                                                                    bz_lab_vvv = bz_lab_vvv,\
                                                                                                    bx_lab_vvv = bx_lab_vvv,\
                                                                                                    by_lab_vvv = by_lab_vvv,\
                                                                                                    zmesh_w_v = zmesh_w_v,\
                                                                                                    xmesh_w_v = xmesh_w_v,\
                                                                                                    bend_par_v = bend_par_v,\
                                                                                                    round_flag = round_flag,\
                                                                                                    distanceTreshold = distanceTreshold
                                                                                                    )
    return bz_1_warped_final, bx_1_warped_final, by_1_warped_final


# 3D --
def Aproximate_3D(zz_lab_r, xx_lab_r, yy_lab_r, zmesh_box_lab_v, xmesh_box_lab_v, ymesh_box_lab_v, bz_1_vvv, bx_1_vvv, by_1_vvv, bend_s, bend_e, bend_r, distanceTreshold, round_flag = True):
    """
    zmesh_box_lab_v = array of the z points of the box (the space coordiantes of bz/bx_1_vvv) in laboratory coordinates 
    xmesh_box_lab_v = array of the x points of the box (the space coordiantes of bz/bx_1_vvv) in laboratory coordinates  
    ymesh_box_lab_v = array of the y points of the box (the space coordiantes of bz/bx_1_vvv) in laboratory coordinates  
    bz_1_vvv = 3D array of the Bz in laboratory coordinates (must correspond and sit on the mesh_box_lab_v)
    bx_1_vvv = 3D array of the Bx in laboratory coordinates (must correspond and sit on the mesh_box_lab_v)
    by_1_vvv = 3D array of the By in laboratory coordinates (must correspond and sit on the mesh_box_lab_v)
    zz_lab_r = z coordinate of a point where the vector is to be aproximated
    xx_lab_r = x coordinate of a point where the vector is to be aproximated
    yy_lab_r = y coordinate of a point where the vector is to be aproximated
    bend_s = lowest z of the bend
    bend_e = highest z of the bend
    bend_r = radius of the bend
    round_flag = round_flag Carefull, this can have an effect on the code if the better precision for the coordinates is needed (finner mesh),
                 but removes some rounding effect stemming frin calculation process. If Off, the effect it removes should be negligible...
    distanceTreshold = The distance between points where no approximation is done

    This function takes the point (zz_lab_r,xx_lab_r, must be inside the mag. field mesh), and a magnetic field sitting on a mesh/grid.
    It aproximates (linearly) the vector of the magnetic field at that point (if the point is not identical with mesh-point, 4 vectors surounding the point are used for the aproximation).
    The aproximated vector and its coordinates are then transformed to warped coordinates and returned
    """

    by_1_lab_ap = -100
    bx_1_lab_ap = -100
    bz_1_lab_ap = -100

    lab_y_1_To_yy_lab_ri_distance_a = np.array([abs(y_1 - yy_lab_r) for y_1 in ymesh_box_lab_v]) 
    lab_z_1_To_zz_lab_ri_distance_a = np.array([abs(z_1 - zz_lab_r) for z_1 in zmesh_box_lab_v])
    lab_x_1_To_xx_lab_ri_distance_a = np.array([abs(x_1 - xx_lab_r) for x_1 in xmesh_box_lab_v])
    pointAtZline_flag = min(lab_z_1_To_zz_lab_ri_distance_a) < distanceTreshold # The point is on the z-line -> Aproximation not needed in this direction 
    pointAtXline_flag = min(lab_x_1_To_xx_lab_ri_distance_a) < distanceTreshold # The point is on the x-line -> Aproximation not needed in this direction 
    pointAtYline_flag = min(lab_y_1_To_yy_lab_ri_distance_a) < distanceTreshold # The point is on the y-line -> Aproximation not needed in this direction Should be always true!
    if pointAtYline_flag == False:
        print("pointAtYline_flag == False -> should be True as there must not be any bend in y dimension!, ")
        print("lab_y_1_To_yy_lab_ri_distance_a = {}".format(lab_y_1_To_yy_lab_ri_distance_a))
        print("ymesh_box_lab_v = {}".format(ymesh_box_lab_v))
        print("yy_lab_r = {}".format(yy_lab_r))

    if  pointAtZline_flag and pointAtXline_flag: # The points overlap -> Aproximation not needed

        if round_flag:
            precise_z = np.where(lab_z_1_To_zz_lab_ri_distance_a<=distanceTreshold)
            precise_x = np.where(lab_x_1_To_xx_lab_ri_distance_a<=distanceTreshold)
            precise_y = np.where(lab_y_1_To_yy_lab_ri_distance_a<=distanceTreshold)
        else:
            precise_z = np.where(zmesh_box_lab_v == zz_lab_r)
            precise_x = np.where(xmesh_box_lab_v == xx_lab_r)
            precise_y = np.where(ymesh_box_lab_v == yy_lab_r)

        z_i = precise_z[0][0]
        x_i = precise_x[0][0]
        y_i = precise_y[0][0]
        bx_1_lab_ap = bx_1_vvv[x_i][y_i][z_i]
        by_1_lab_ap = by_1_vvv[x_i][y_i][z_i]
        bz_1_lab_ap = bz_1_vvv[x_i][y_i][z_i]
        


    elif pointAtZline_flag: # The point is on the z-line -> Aproximation not needed in this direction
        if round_flag:
            precise_z = np.where(lab_z_1_To_zz_lab_ri_distance_a<=distanceTreshold)
            precise_y = np.where(lab_y_1_To_yy_lab_ri_distance_a<=distanceTreshold)
        else:
            precise_z = np.where(zmesh_box_lab_v == zz_lab_r)
            precise_y = np.where(ymesh_box_lab_v == yy_lab_r)
        first_greater_x = np.min(np.where(lab_x_1 > xx_lab_r)[0])
        z_i = precise_z[0][0]
        x_i = first_greater_x
        y_i = precise_y[0][0]
        weights = np.zeros(2)
        weights[0] = PPDistance(zz_lab_r,xx_lab_r,zmesh_box_lab_v[z_i],xmesh_box_lab_v[x_i])
        weights[1] = PPDistance(zz_lab_r,xx_lab_r,zmesh_box_lab_v[z_i],xmesh_box_lab_v[x_i-1])
        # Normalization of the weights:
        weights = [weight/sum(weights) for weight in weights]

        bx_1_lab_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i-1][y_i][z_i]
        by_1_lab_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i-1][y_i][z_i]
        bz_1_lab_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i-1][y_i][z_i]

    elif pointAtXline_flag: # The point is on the z-line -> Aproximation not needed in this direction

        first_greater_z = np.min(np.where(zmesh_box_lab_v > zz_lab_r)[0])
        if round_flag:
            precise_x = np.where(lab_x_1_To_xx_lab_ri_distance_a<=distanceTreshold)
            precise_y = np.where(lab_y_1_To_yy_lab_ri_distance_a<=distanceTreshold)
        else:
            precise_x = np.where(xmesh_box_lab_v == xx_lab_r)
            precise_y = np.where(ymesh_box_lab_v == yy_lab_r)


        z_i = first_greater_z
        x_i = precise_x[0][0]
        y_i = precise_y[0][0]

        weights = np.zeros(2)
        weights[0] = PPDistance(zz_lab_r,xx_lab_r,zmesh_box_lab_v[z_i],xmesh_box_lab_v[x_i])
        weights[1] = PPDistance(zz_lab_r,xx_lab_r,zmesh_box_lab_v[z_i-1],xmesh_box_lab_v[x_i])
        # Normalization of the weights:
        weights = [weight/sum(weights) for weight in weights]

        bx_1_lab_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i][y_i][z_i-1]
        by_1_lab_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i][y_i][z_i-1]
        bz_1_lab_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i][y_i][z_i-1]


    else:
        if round_flag:
            precise_y = np.where(lab_y_1_To_yy_lab_ri_distance_a<=distanceTreshold)
        else:
            precise_y = np.where(ymesh_box_lab_v == yy_lab_r)
        first_greater_z = np.min(np.where(zmesh_box_lab_v > zz_lab_r)[0])
        first_greater_x = np.min(np.where(xmesh_box_lab_v > xx_lab_r)[0])
        z_i = first_greater_z
        x_i = first_greater_x
        y_i = precise_y[0][0]
        weights = np.zeros(4)
        weights[0] = PPDistance(zz_lab_r,xx_lab_r,zmesh_box_lab_v[z_i],xmesh_box_lab_v[x_i])
        weights[1] = PPDistance(zz_lab_r,xx_lab_r,zmesh_box_lab_v[z_i],xmesh_box_lab_v[x_i-1])
        weights[2] = PPDistance(zz_lab_r,xx_lab_r,zmesh_box_lab_v[z_i-1],xmesh_box_lab_v[x_i-1])
        weights[3] = PPDistance(zz_lab_r,xx_lab_r,zmesh_box_lab_v[z_i-1],xmesh_box_lab_v[x_i])
        # Normalization of the weights:
        weights = [weight/sum(weights) for weight in weights]


        bx_1_lab_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i-1][y_i][z_i] + weights[2]*bx_1_vvv[x_i-1][y_i][z_i-1] + weights[3]*bx_1_vvv[x_i][y_i][z_i-1]
        by_1_lab_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i-1][y_i][z_i] + weights[2]*by_1_vvv[x_i-1][y_i][z_i-1] + weights[3]*by_1_vvv[x_i][y_i][z_i-1]
        bz_1_lab_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i-1][y_i][z_i] + weights[2]*bz_1_vvv[x_i-1][y_i][z_i-1] + weights[3]*bz_1_vvv[x_i][y_i][z_i-1]
    
    by_1_warped_final_r = by_1_lab_ap
    y_1_warped_final_r = yy_lab_r
    bz_1_warped_final_r, bx_1_warped_final_r, z_1_warped_final_r, x_1_warped_final_r = LabToWarped_vector(bz_1_lab_ap,bx_1_lab_ap,zz_lab_r,xx_lab_r,bend_s,bend_e,bend_r)
    return bz_1_warped_final_r, bx_1_warped_final_r, by_1_warped_final_r, z_1_warped_final_r, x_1_warped_final_r, y_1_warped_final_r


def MagneticFieldTransformation_boxMesh_3D(zmesh_box_lab_v, xmesh_box_lab_v, ymesh_box_lab_v, bz_lab_vvv, bx_lab_vvv, by_lab_vvv, zmesh_w_v, xmesh_w_v, ymesh_w_v, bend_par_v, round_flag = True, distanceTreshold = 1e-10):
    """
    The box mesh must be rectangular,
    zmesh_box_lab_v = array of the z points of the box (the space coordiantes of bz/bx_1_vvv) in laboratory coordinates 
    xmesh_box_lab_v = array of the x points of the box (the space coordiantes of bz/bx_1_vvv) in laboratory coordinates 
    xmesh_box_lab_v = array of the y points of the box (the space coordiantes of bz/bx_1_vvv) in laboratory coordinates 
    bz_lab_vvv = 3D array of the Bz in laboratory coordinates (must correspond and sit on the mesh_box_lab_v)
    bx_lab_vvv = 3D array of the Bx in laboratory coordinates (must correspond and sit on the mesh_box_lab_v)
    by_lab_vvv = 3D array of the By in laboratory coordinates (must correspond and sit on the mesh_box_lab_v)
    zmesh_w_v = array of the z points of warped mesh in warped coordinates 
    xmesh_w_v = array of the x points of warped mesh in warped coordinates 
    ymesh_w_v = array of the y points of warped mesh in warped coordinates 
    bend_par_v = parameters of the bend, must be in format:

    bend_start = bend_par_v[0] - lowest z of the bend
    bend_end = bend_par_v[1] - - highest z of the bend
    bend_radius = bend_par_v[2] - radius of the bend

    round_flag = flag if to use distanceTreshold or exact coordinates for points to be delared identical and no aproximation is then done, for more info viz Aproximation function
    distanceTreshold = the distance treshold to declare two points as identical



    The idea is following: Get warped mesh into lab, aproximate the field in the lab on the warped mesh in lab, 
    and then transform this field back into warped coordinates, and the x y z will create a good grid which is esential.
    The aproximation of the field is done in the lab coord. instead in the warped coord.
    because of more clean and safe aproximation from grided data into "non-grid" instead the opposite.

    This function returns takes the bz and bx as 3D but returns bz_final and bx_final as a 2D (z and x dimensions) - the y dimension is ommited as RZ and XYZ could affect this.

    """

    # Parameters

    # Few pre-calculations
    z_len = len(zmesh_w_v)
    x_len = len(xmesh_w_v)
    y_len = len(ymesh_w_v)
    mesh_w_numOfPoints = z_len*x_len*y_len

    bend_start = bend_par_v[0]
    bend_end = bend_par_v[1]
    bend_radius = bend_par_v[2]

    #Declaring empty arrays for filling

        #Final bx and bz aproximated and back in warped coordinates
    by_1_warped_final_r = np.zeros(mesh_w_numOfPoints)
    bx_1_warped_final_r = np.zeros(mesh_w_numOfPoints)
    bz_1_warped_final_r = np.zeros(mesh_w_numOfPoints)

        #Final x an z of the bx and bz back in warped coordinates nonzero only inside the box, --> can be used for the box identification in warped coordinates otherwise not used
    y_1_warped_final_r = np.zeros(mesh_w_numOfPoints)
    x_1_warped_final_r = np.zeros(mesh_w_numOfPoints)
    z_1_warped_final_r = np.zeros(mesh_w_numOfPoints)

        # The mesh warped mesh in Lab coordinates:
    yy_lab = np.zeros((x_len,y_len,z_len))
    xx_lab = np.zeros((x_len,y_len,z_len))
    zz_lab = np.zeros((x_len,y_len,z_len))


    print("WarpingToLab starting")
        # Transformation of the warped mesh to lab coordinates
    for y_i in range(y_len):
        for x_i in range(x_len):
            for z_i in range(z_len):
                zz_lab[x_i][y_i][z_i], xx_lab[x_i][y_i][z_i] = WarpedToLab_point(zmesh_w_v[z_i],xmesh_w_v[x_i],bend_start,bend_end,bend_radius)
                # yy_lab[x_i][y_i][z_i] = ymesh_w_v[y_i]
    yy_lab = [[[ymesh_w_v[y_i] for z_i in range(z_len)] for y_i in range(y_len)] for x_i in range(x_len)]
    print("WarpingToLab completed")

    xx_lab_r = xx_lab.ravel()
    yy_lab_r = yy_lab.ravel()
    zz_lab_r = zz_lab.ravel()


    # Box cropping of the transformed warped mesh in lab - Finding the points that will be used for the aproximation (inside the bfield_box) 
    used_coord_0 = np.where(zz_lab_r >= zmesh_box_lab_v[0])
    used_coord = used_coord_0
    used_coord_1 = np.where(zz_lab_r[used_coord] <= zmesh_box_lab_v[-1])
    used_coord = (used_coord[0][used_coord_1],) 
    # print("used_coord: z  completed")
    used_coord_2 = np.where(xx_lab_r[used_coord] <= xmesh_box_lab_v[-1])
    used_coord = (used_coord[0][used_coord_2],) 
    used_coord_3 = np.where(xx_lab_r[used_coord] >= xmesh_box_lab_v[0])
    used_coord = (used_coord[0][used_coord_3],) 
    # print("used_coord: x  completed")
    used_coord_4 = np.where(yy_lab_r[used_coord] <= ymesh_box_lab_v[-1])
    used_coord = (used_coord[0][used_coord_4],) 
    used_coord_5 = np.where(yy_lab_r[used_coord] >= ymesh_box_lab_v[0])
    used_coord = (used_coord[0][used_coord_5],) 
    # print("used_coord: y  completed")

    print("the box: {} < z < {}".format(zmesh_box_lab_v[-1],zmesh_box_lab_v[0]))
    print("in LAB : {} < x < {}".format(xmesh_box_lab_v[-1],xmesh_box_lab_v[0]))
    print("coord. : {} < y < {}".format(ymesh_box_lab_v[-1],ymesh_box_lab_v[0]))
    print("used point number: {}".format(len(used_coord[0])))

    # print("Original zz_lab_r: {}".format(zz_lab_r))
    # print("Croped zz_lab_r: {}".format(zz_lab_r[used_coord]))
    # print("Original xx_lab_r: {}".format(xx_lab_r))
    # print("Croped xx_lab_r: {}".format(xx_lab_r[used_coord]))

    # Aproximating the B field from box mesh into the warped mesh points in lab coordinates and then transforming them back into warped coordinates 
    line_i = 0
    for i_warpedMesh in used_coord[0]:
        if line_i % 5000 == 0:
            print("{}/{} thousands points aproximated".format(int(line_i/1000),int(len(used_coord[0])/1000)))
        line_i += 1 
        bz_1_warped_final_r[i_warpedMesh], bx_1_warped_final_r[i_warpedMesh], by_1_warped_final_r[i_warpedMesh], \
        z_1_warped_final_r[i_warpedMesh], x_1_warped_final_r[i_warpedMesh], y_1_warped_final_r[i_warpedMesh] = \
        Aproximate_3D( zmesh_box_lab_v = zmesh_box_lab_v,\
                    xmesh_box_lab_v = xmesh_box_lab_v,\
                    ymesh_box_lab_v = ymesh_box_lab_v,\
                    bz_1_vvv = bz_lab_vvv,\
                    bx_1_vvv = bx_lab_vvv,\
                    by_1_vvv = by_lab_vvv,\
                    zz_lab_r = zz_lab_r[i_warpedMesh],\
                    xx_lab_r = xx_lab_r[i_warpedMesh],\
                    yy_lab_r = yy_lab_r[i_warpedMesh],\
                    bend_s = bend_start,\
                    bend_e = bend_end,\
                    bend_r = bend_radius,\
                    distanceTreshold = distanceTreshold,\
                    round_flag = round_flag)
    

    by_1_warped_final = by_1_warped_final_r.reshape(x_len, y_len, z_len)
    bx_1_warped_final = bx_1_warped_final_r.reshape(x_len, y_len, z_len)
    bz_1_warped_final = bz_1_warped_final_r.reshape(x_len, y_len, z_len)


    # Just testting plot of warped mesh and used cooridnates (inside the box in warped coordinates)
    # zz_w = np.array([[zmes_w for zmes_w in zmesh_w_v] for xmesh_w in xmesh_w_v])
    # xx_w = np.array([[xmesh_w for zmes_w in zmesh_w_v] for xmesh_w in xmesh_w_v])

    # xx_w_r = xx_w.ravel()
    # zz_w_r = zz_w.ravel()

    # plt.scatter(zz_w_r,xx_w_r, s = 1, color = "b", label = "Warped mesh")
    # plt.scatter(z_1_warped_final_r, x_1_warped_final_r, s = 1, color = "r", label = "Warped mesh")

    return bz_1_warped_final, bx_1_warped_final, by_1_warped_final


def MagneticFieldTransformation_boxPar_3D(bfield_box_par_v, dz_box_lab, dx_box_lab, dy_box_lab, bz_lab_vvv, bx_lab_vvv, by_lab_vvv, zmesh_w_v, xmesh_w_v, ymesh_w_v, bend_par_v, round_flag = True, distanceTreshold = 1e-10):
    """
    The bfield box must be rectangular and the bfield_box_par_v in format: 

    bfield_box_xcent_lab = bfield_box_par_v[0] - x center at lowest z
    bfield_box_ycent_lab = bfield_box_par_v[1] - y center at lowest z
    bfield_box_zs_lab = bfield_box_par_v[2] - lowest z
    bfield_box_ze_lab = bfield_box_par_v[3] - higest z
    bfield_box_XhalfOfwidth_lab = bfield_box_par_v[4] - half of the width of side in x dimension ( so maybe technically hight and not width but wathever)
    bfield_box_YhalfOfwidth_lab = bfield_box_par_v[5] - half of the width of side in y dimension ( so maybe technically hight and not width but wathever)

    dz_box_lab = the increment in z direction of the box mesh in laboratory coordinates 
    dx_box_lab = the increment in x direction of the box mesh in laboratory coordinates 
    bz_lab_vvv = 3D array of the Bz in laboratory coordinates (must correspond and sit on the z/xmesh_box_lab_v)
    bx_lab_vvv = 3D array of the Bx in laboratory coordinates (must correspond and sit on the z/xmesh_box_lab_v)
    zmesh_w_v = array of the z points of warped mesh in warped coordinates 
    xmesh_w_v = array of the x points of warped mesh in warped coordinates 
    bend_par_v = parameters of the bend, must be in format:

    bend_start = bend_par_v[0] - lowest z of the bend
    bend_end = bend_par_v[1] - - highest z of the bend
    bend_radius = bend_par_v[2] - radius of the bend

    round_flag = flag if to use distanceTreshold or exact coordinates for points to be delared identical and no aproximation is then done, for more info viz Aproximation function
    distanceTreshold = the distance treshold to declare two points as identical

    The idea is following: Get warped mesh in warped coordinates, transform it into lab coordinates, aproximate the field in the lab coordinates onto the warped mesh in lab coordinates, 
    and then transform this field back into warped coordinates, and the x y z will create a good grid which is esential for applying the field
    The aproximation of the field is done in the lab coord. instead in the warped coord.
    because of more clean and safe aproximation from grided data into "non-grid" instead the opposite.

    This function returns takes the bz and bx as 3D but returns bz_final and bx_final as a 2D (z and x dimensions) - the y dimension is ommited as RZ and XYZ could affect this.
    """
    bfield_box_xcent_lab = bfield_box_par_v[0]
    bfield_box_ycent_lab = bfield_box_par_v[1]
    bfield_box_zs_lab = bfield_box_par_v[2]
    bfield_box_ze_lab = bfield_box_par_v[3]
    bfield_box_XhalfOfwidth_lab = bfield_box_par_v[4]
    bfield_box_YhalfOfwidth_lab = bfield_box_par_v[5]
    print("bfield_box_par_v: {}".format(bfield_box_par_v))

    # Defining the bfield mesh in the LAB
    nz_box = int((bfield_box_ze_lab-bfield_box_zs_lab)/dz_box_lab + 1)
    nx_box = int(bfield_box_XhalfOfwidth_lab*2/dx_box_lab + 1)
    ny_box = int(bfield_box_YhalfOfwidth_lab*2/dy_box_lab + 1)

    zmesh_box_lab_v = [bfield_box_zs_lab + z_i*dz_box_lab for z_i in range(nz_box)]
    xmesh_box_lab_v = [bfield_box_xcent_lab - bfield_box_XhalfOfwidth_lab + x_i*dx_box_lab for x_i in range(nx_box)]
    ymesh_box_lab_v = [bfield_box_ycent_lab - bfield_box_YhalfOfwidth_lab + y_i*dy_box_lab for y_i in range(ny_box)]


    bz_1_warped_final, bx_1_warped_final, by_1_warped_final = MagneticFieldTransformation_boxMesh_3D(   zmesh_box_lab_v = zmesh_box_lab_v,\
                                                                                                        xmesh_box_lab_v = xmesh_box_lab_v,\
                                                                                                        ymesh_box_lab_v = ymesh_box_lab_v,\
                                                                                                        bz_lab_vvv = bz_lab_vvv,\
                                                                                                        bx_lab_vvv = bx_lab_vvv,\
                                                                                                        by_lab_vvv = by_lab_vvv,\
                                                                                                        zmesh_w_v = zmesh_w_v,\
                                                                                                        xmesh_w_v = xmesh_w_v,\
                                                                                                        ymesh_w_v = ymesh_w_v,\
                                                                                                        bend_par_v = bend_par_v,\
                                                                                                        round_flag = round_flag,\
                                                                                                        distanceTreshold = distanceTreshold)
    return bz_1_warped_final, bx_1_warped_final, by_1_warped_final

# CAD
#----------------------------------------------------------------------------
# RZ of approximation using CAD coordinates
def Aproximate_CAD_RZ(zz_CAD_r, xx_CAD_r, zmesh_box_CAD_v, xmesh_box_CAD_v, bz_1_vvv, bx_1_vvv, by_1_vvv, lab0inCAD_z, lab0inCAD_x, angle_labToCAD, bend_s, bend_e, bend_r, distanceTreshold, round_flag = True):
    """
    zmesh_box_CAD_v = array of the z points of the box (the space coordiantes of bz/bx_1_vvv) in CAD coordinates 
    xmesh_box_CAD_v = array of the x points of the box (the space coordiantes of bz/bx_1_vvv) in CAD coordinates  
    bz_1_vvv = 3D array of the Bz in CAD coordinates (must correspond and sit on the mesh_box_CAD_v)
    bx_1_vvv = 3D array of the Bx in CAD coordinates (must correspond and sit on the mesh_box_CAD_v)
    by_1_vvv = 3D array of the By in CAD coordinates (must correspond and sit on the mesh_box_CAD_v)
    zz_CAD_r = z coordinate of a point where the vector is to be aproximated
    xx_CAD_r = x coordinate of a point where the vector is to be aproximated
    bend_s = lowest z of the bend
    bend_e = highest z of the bend
    bend_r = radius of the bend
    round_flag = round_flag Carefull, this can have an effect on the code if the better precision for the coordinates is needed (finner mesh),
                 but removes some rounding effect stemming frin calculation process. If Off, the effect it removes should be negligible...
    distanceTreshold = The distance between points where no approximation is done

    This function takes the point (zz_CAD_r,xx_CAD_r, must be inside the mag. field mesh), and a magnetic field sitting on a mesh/grid.
    It aproximates (linearly) the vector of the magnetic field at that point (if the point is not identical with mesh-point, 4 vectors surounding the point are used for the aproximation).
    The aproximated vector and its coordinates are then transformed to warped coordinates and returned
    """

    y_i = 0
    bx_1_CAD_ap = -100
    bz_1_CAD_ap = -100

    CAD_z_1_To_zz_CAD_ri_distance_a = np.array([abs(z_1 - zz_CAD_r) for z_1 in zmesh_box_CAD_v])
    CAD_x_1_To_xx_CAD_ri_distance_a = np.array([abs(x_1 - xx_CAD_r) for x_1 in xmesh_box_CAD_v])
    pointAtZline_flag = min(CAD_z_1_To_zz_CAD_ri_distance_a) < distanceTreshold # The point is on the z-line -> Aproximation not needed in this direction 
    pointAtXline_flag = min(CAD_x_1_To_xx_CAD_ri_distance_a) < distanceTreshold # The point is on the x-line -> Aproximation not needed in this direction 

    if  pointAtZline_flag and pointAtXline_flag: # The points overlap -> Aproximation not needed

        if round_flag:
            precise_z = np.where(CAD_z_1_To_zz_CAD_ri_distance_a<=distanceTreshold)
            precise_x = np.where(CAD_x_1_To_xx_CAD_ri_distance_a<=distanceTreshold)
        else:
            precise_z = np.where(zmesh_box_CAD_v == zz_CAD_r)
            precise_x = np.where(xmesh_box_CAD_v == xx_CAD_r)

        z_i = precise_z[0][0]
        x_i = precise_x[0][0]
        bx_1_CAD_ap = bx_1_vvv[x_i][y_i][z_i]
        by_1_CAD_ap = by_1_vvv[x_i][y_i][z_i]
        bz_1_CAD_ap = bz_1_vvv[x_i][y_i][z_i]
        


    elif pointAtZline_flag: # The point is on the z-line -> Aproximation not needed in this direction
        if round_flag:
            precise_z = np.where(CAD_z_1_To_zz_CAD_ri_distance_a<=distanceTreshold)
        else:
            precise_z = np.where(zmesh_box_CAD_v == zz_CAD_r)
        first_greater_x = np.min(np.where(xmesh_box_CAD_v > xx_CAD_r)[0])
        z_i = precise_z[0][0]
        x_i = first_greater_x
        weights = np.zeros(2)
        weights[0] = PPDistance(zz_CAD_r,xx_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i])
        weights[1] = PPDistance(zz_CAD_r,xx_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i-1])
        # Normalization of the weights:
        weights = [weight/sum(weights) for weight in weights]

        bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i-1][y_i][z_i]
        by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i-1][y_i][z_i]
        bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i-1][y_i][z_i]

    elif pointAtXline_flag: # The point is on the z-line -> Aproximation not needed in this direction

        first_greater_z = np.min(np.where(zmesh_box_CAD_v > zz_CAD_r)[0])
        if round_flag:
            precise_x = np.where(CAD_x_1_To_xx_CAD_ri_distance_a<=distanceTreshold)
        else:
            precise_x = np.where(xmesh_box_CAD_v == xx_CAD_r)


        z_i = first_greater_z
        x_i = precise_x[0][0]

        weights = np.zeros(2)
        weights[0] = PPDistance(zz_CAD_r,xx_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i])
        weights[1] = PPDistance(zz_CAD_r,xx_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i])
        # Normalization of the weights:
        weights = [weight/sum(weights) for weight in weights]

        bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i][y_i][z_i-1]
        by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i][y_i][z_i-1]
        bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i][y_i][z_i-1]


    else:
        first_greater_z = np.min(np.where(zmesh_box_CAD_v > zz_CAD_r)[0])
        first_greater_x = np.min(np.where(xmesh_box_CAD_v > xx_CAD_r)[0])
        z_i = first_greater_z
        x_i = first_greater_x
        weights = np.zeros(4)
        weights[0] = PPDistance(zz_CAD_r,xx_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i])
        weights[1] = PPDistance(zz_CAD_r,xx_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i-1])
        weights[2] = PPDistance(zz_CAD_r,xx_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i-1])
        weights[3] = PPDistance(zz_CAD_r,xx_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i])
        # Normalization of the weights:
        weights = [weight/sum(weights) for weight in weights]


        bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i-1][y_i][z_i] + weights[2]*bx_1_vvv[x_i-1][y_i][z_i-1] + weights[3]*bx_1_vvv[x_i][y_i][z_i-1]
        by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i-1][y_i][z_i] + weights[2]*by_1_vvv[x_i-1][y_i][z_i-1] + weights[3]*by_1_vvv[x_i][y_i][z_i-1]
        bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i-1][y_i][z_i] + weights[2]*bz_1_vvv[x_i-1][y_i][z_i-1] + weights[3]*bz_1_vvv[x_i][y_i][z_i-1]
    
    by_1_warped_final_r = by_1_CAD_ap
    bz_1_warped_final_r, bx_1_warped_final_r, z_1_warped_final_r, x_1_warped_final_r = CADToWarped_vector(bz_1_CAD_ap,bx_1_CAD_ap,zz_CAD_r,xx_CAD_r,lab0inCAD_z, lab0inCAD_x, angle_labToCAD, bend_s,bend_e,bend_r)
    return bz_1_warped_final_r, bx_1_warped_final_r, by_1_warped_final_r, z_1_warped_final_r, x_1_warped_final_r


def MagneticFieldTransformation_boxMesh_CAD_RZ(zmesh_box_CAD_v, xmesh_box_CAD_v, bz_CAD_vvv, bx_CAD_vvv, by_CAD_vvv, zmesh_w_v, xmesh_w_v, lab0inCAD_z, lab0inCAD_x, angle_labToCAD, bend_par_v, round_flag = True, distanceTreshold = 1e-10):
    """
    The box mesh must be rectangular,
    zmesh_box_CAD_v = array of the z points of the box (the space coordiantes of bz/bx_1_vvv) in CADoratory coordinates 
    xmesh_box_CAD_v = array of the x points of the box (the space coordiantes of bz/bx_1_vvv) in CADoratory coordinates 
    bz_CAD_vvv = 3D array of the Bz in CADoratory coordinates (must correspond and sit on the mesh_box_CAD_v)
    bx_CAD_vvv = 3D array of the Bx in CADoratory coordinates (must correspond and sit on the mesh_box_CAD_v)
    by_CAD_vvv = 3D array of the By in CADoratory coordinates (must correspond and sit on the mesh_box_CAD_v)
    zmesh_w_v = array of the z points of warped mesh in warped coordinates 
    xmesh_w_v = array of the x points of warped mesh in warped coordinates 
    bend_par_v = parameters of the bend, must be in format:

    bend_start = bend_par_v[0] - lowest z of the bend
    bend_end = bend_par_v[1] - - highest z of the bend
    bend_radius = bend_par_v[2] - radius of the bend

    round_flag = flag if to use distanceTreshold or exact coordinates for points to be delared identical and no aproximation is then done, for more info viz Aproximation function
    distanceTreshold = the distance treshold to declare two points as identical



    The idea is following: Get warped mesh into CAD, aproximate the field in the CAD on the warped mesh in CAD, 
    and then transform this field back into warped coordinates, and the x y z will create a good grid which is esential.
    The aproximation of the field is done in the CAD coord. instead in the warped coord.
    because of more clean and safe aproximation from grided data into "non-grid" instead the opposite.

    This function returns takes the bz and bx as 3D but returns bz_final and bx_final as a 2D (z and x dimensions) - the y dimension is ommited as RZ and XYZ could affect this.

    """

    # Parameters

    # Few pre-calculations
    z_len = len(zmesh_w_v)
    x_len = len(xmesh_w_v)
    y_len = 1
    y_i = 0
    mesh_w_numOfPoints = z_len*x_len

    bend_start = bend_par_v[0]
    bend_end = bend_par_v[1]
    bend_radius = bend_par_v[2]

    #Declaring empty arrays for filling

        #Final bx and bz aproximated and back in warped coordinates
    by_1_warped_final_r = np.zeros(mesh_w_numOfPoints)
    bx_1_warped_final_r = np.zeros(mesh_w_numOfPoints)
    bz_1_warped_final_r = np.zeros(mesh_w_numOfPoints)

        #Final x an z of the bx and bz back in warped coordinates nonzero only inside the box, --> can be used for the box identification in warped coordinates otherwise not used
    x_1_warped_final_r = np.zeros(mesh_w_numOfPoints)
    z_1_warped_final_r = np.zeros(mesh_w_numOfPoints)

        # The mesh warped mesh in CAD coordinates:
    xx_CAD = np.zeros((x_len,y_len,z_len))
    zz_CAD = np.zeros((x_len,y_len,z_len))


    print("WarpingToCAD starting")
        # Transformation of the warped mesh to CAD coordinates
    for y_i in range(y_len):
        for x_i in range(x_len):
            for z_i in range(z_len):
                zz_CAD[x_i][y_i][z_i], xx_CAD[x_i][y_i][z_i] = WarpedToCAD_point(zmesh_w_v[z_i],xmesh_w_v[x_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius)
    print("WarpingToCAD completed")

    xx_CAD_r = xx_CAD.ravel()
    zz_CAD_r = zz_CAD.ravel()


    # Box cropping of the transformed warped mesh in lab - Finding the points that will be used for the aproximation (inside the bfield_box) 
    used_coord_0 = np.where(zz_CAD_r >= zmesh_box_CAD_v[0])
    used_coord = used_coord_0
    used_coord_1 = np.where(zz_CAD_r[used_coord] <= zmesh_box_CAD_v[-1])
    used_coord = (used_coord[0][used_coord_1],) 
    # print("used_coord: z  completed")
    used_coord_2 = np.where(xx_CAD_r[used_coord] <= xmesh_box_CAD_v[-1])
    used_coord = (used_coord[0][used_coord_2],) 
    used_coord_3 = np.where(xx_CAD_r[used_coord] >= xmesh_box_CAD_v[0])
    used_coord = (used_coord[0][used_coord_3],) 
    # print("used_coord: x  completed")

    print("the box: {} < z < {}".format(zmesh_box_CAD_v[-1],zmesh_box_CAD_v[0]))
    print("in CAD : {} < x < {}".format(xmesh_box_CAD_v[-1],xmesh_box_CAD_v[0]))
    print("used number of points: {}".format(len(used_coord[0])))

    # print("Original zz_CAD_r: {}".format(zz_lab_r))
    # print("Croped zz_lab_r: {}".format(zz_lab_r[used_coord]))
    # print("Original xx_lab_r: {}".format(xx_lab_r))
    # print("Croped xx_lab_r: {}".format(xx_lab_r[used_coord]))

    # Aproximating the B field from box mesh into the warped mesh points in lab coordinates and then transforming them back into warped coordinates 
    line_i = 0
    for i_warpedMesh in used_coord[0]:
        if line_i % 5000 == 0:
            print("{}/{} thousands points aproximated".format(int(line_i/1000),int(len(used_coord[0])/1000)))
        line_i += 1 
        bz_1_warped_final_r[i_warpedMesh], bx_1_warped_final_r[i_warpedMesh], by_1_warped_final_r[i_warpedMesh], \
        z_1_warped_final_r[i_warpedMesh], x_1_warped_final_r[i_warpedMesh] = \
        Aproximate_CAD_RZ( zmesh_box_CAD_v = zmesh_box_CAD_v,\
                    xmesh_box_CAD_v = xmesh_box_CAD_v,\
                    bz_1_vvv = bz_CAD_vvv,\
                    bx_1_vvv = bx_CAD_vvv,\
                    by_1_vvv = by_CAD_vvv,\
                    zz_CAD_r = zz_CAD_r[i_warpedMesh],\
                    xx_CAD_r = xx_CAD_r[i_warpedMesh],\
                    lab0inCAD_z = lab0inCAD_z ,\
                    lab0inCAD_x = lab0inCAD_x ,\
                    angle_labToCAD = angle_labToCAD ,\
                    bend_s = bend_start,\
                    bend_e = bend_end,\
                    bend_r = bend_radius,\
                    distanceTreshold = distanceTreshold,\
                    round_flag = round_flag)
    

    by_1_warped_final = by_1_warped_final_r.reshape(x_len, y_len, z_len)
    bx_1_warped_final = bx_1_warped_final_r.reshape(x_len, y_len, z_len)
    bz_1_warped_final = bz_1_warped_final_r.reshape(x_len, y_len, z_len)


    # Just testting plot of warped mesh and used cooridnates (inside the box in warped coordinates)
    # zz_w = np.array([[zmes_w for zmes_w in zmesh_w_v] for xmesh_w in xmesh_w_v])
    # xx_w = np.array([[xmesh_w for zmes_w in zmesh_w_v] for xmesh_w in xmesh_w_v])

    # xx_w_r = xx_w.ravel()
    # zz_w_r = zz_w.ravel()

    # plt.scatter(zz_w_r,xx_w_r, s = 1, color = "b", label = "Warped mesh")
    # plt.scatter(z_1_warped_final_r, x_1_warped_final_r, s = 1, color = "r", label = "Warped mesh")

    return bz_1_warped_final, bx_1_warped_final, by_1_warped_final


def MagneticFieldTransformation_boxPar_CAD_RZ(bfield_box_par_v, dz_box_CAD, dx_box_CAD, bz_CAD_vvv, bx_CAD_vvv, by_CAD_vvv, zmesh_w_v, xmesh_w_v, lab0inCAD_z, lab0inCAD_x, angle_labToCAD, bend_par_v, round_flag = True, distanceTreshold = 1e-10):
    """
    The bfield box must be rectangular and the bfield_box_par_v in format: 

    bfield_box_xcent_CAD = bfield_box_par_v[0] - x center at lowest z
    bfield_box_ycent_CAD = bfield_box_par_v[1] - y center at lowest z
    bfield_box_zs_CAD = bfield_box_par_v[2] - lowest z
    bfield_box_ze_CAD = bfield_box_par_v[3] - higest z
    bfield_box_XhalfOfwidth_CAD = bfield_box_par_v[4] - half of the width of side in x dimension ( so maybe technically hight and not width but wathever)
    bfield_box_YhalfOfwidth_CAD = bfield_box_par_v[5] - half of the width of side in y dimension ( so maybe technically hight and not width but wathever)

    dz_box_CAD = the increment in z direction of the box mesh in CADoratory coordinates 
    dx_box_CAD = the increment in x direction of the box mesh in CADoratory coordinates 
    bz_CAD_vvv = 3D array of the Bz in CADoratory coordinates (must correspond and sit on the z/xmesh_box_CAD_v)
    bx_CAD_vvv = 3D array of the Bx in CADoratory coordinates (must correspond and sit on the z/xmesh_box_CAD_v)
    by_CAD_vvv = 3D array of the By in CADoratory coordinates (must correspond and sit on the z/xmesh_box_CAD_v)
    zmesh_w_v = array of the z points of warped mesh in warped coordinates 
    xmesh_w_v = array of the x points of warped mesh in warped coordinates 
    bend_par_v = parameters of the bend, must be in format:

    bend_start = bend_par_v[0] - lowest z of the bend
    bend_end = bend_par_v[1] - - highest z of the bend
    bend_radius = bend_par_v[2] - radius of the bend

    round_flag = flag if to use distanceTreshold or exact coordinates for points to be delared identical and no aproximation is then done, for more info viz Aproximation function
    distanceTreshold = the distance treshold to declare two points as identical

    The idea is following: Get warped mesh in warped coordinates, transform it into CAD coordinates, aproximate the field in the CAD coordinates onto the warped mesh in CAD coordinates, 
    and then transform this field back into warped coordinates, and the x y z will create a good grid which is esential for applying the field
    The aproximation of the field is done in the CAD coord. instead in the warped coord.
    because of more clean and safe aproximation from grided data into "non-grid" instead the opposite.

    This function returns takes the bz and bx as 3D but returns bz_final and bx_final as a 2D (z and x dimensions) - the y dimension is ommited as RZ and XYZ could affect this.
    """
    bfield_box_xcent_CAD = bfield_box_par_v[0]
    bfield_box_zs_CAD = bfield_box_par_v[1]
    bfield_box_ze_CAD = bfield_box_par_v[2]
    bfield_box_XhalfOfwidth_CAD = bfield_box_par_v[3]
    print("bfield_box_par_v: {}".format(bfield_box_par_v))

    # Defining the bfield mesh in the LAB
    nz_box = int((bfield_box_ze_CAD-bfield_box_zs_CAD)/dz_box_CAD + 1)
    nx_box = int(bfield_box_XhalfOfwidth_CAD*2/dx_box_CAD + 1)

    zmesh_box_CAD_v = [bfield_box_zs_CAD + z_i*dz_box_CAD for z_i in range(nz_box)]
    xmesh_box_CAD_v = [bfield_box_xcent_CAD - bfield_box_XhalfOfwidth_CAD + x_i*dx_box_CAD for x_i in range(nx_box)]


    bz_1_warped_final, bx_1_warped_final, by_1_warped_final = MagneticFieldTransformation_boxMesh_CAD_RZ(zmesh_box_CAD_v = zmesh_box_CAD_v,\
                                                                                                        xmesh_box_CAD_v = xmesh_box_CAD_v,\
                                                                                                        bz_CAD_vvv = bz_CAD_vvv,\
                                                                                                        bx_CAD_vvv = bx_CAD_vvv,\
                                                                                                        by_CAD_vvv = by_CAD_vvv,\
                                                                                                        zmesh_w_v = zmesh_w_v,\
                                                                                                        xmesh_w_v = xmesh_w_v,\
                                                                                                        lab0inCAD_z = lab0inCAD_z ,\
                                                                                                        lab0inCAD_x = lab0inCAD_x ,\
                                                                                                        angle_labToCAD = angle_labToCAD ,\
                                                                                                        bend_par_v = bend_par_v,\
                                                                                                        round_flag = round_flag,\
                                                                                                        distanceTreshold = distanceTreshold)
    return bz_1_warped_final, bx_1_warped_final, by_1_warped_final


# 3D
# 3D of approximation using CAD coordinates
    # def Aproximate_CAD_3D(zz_CAD_r, xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v, xmesh_box_CAD_v, ymesh_box_CAD_v, bz_1_vvv, bx_1_vvv, by_1_vvv, lab0inCAD_z, lab0inCAD_x, angle_labToCAD, bend_s, bend_e, bend_r, distanceTreshold, round_flag = True):
    #     """
    #     zmesh_box_CAD_v = array of the z points of the box (the space coordiantes of bz/bx_1_vvv) in CAD coordinates 
    #     xmesh_box_CAD_v = array of the x points of the box (the space coordiantes of bz/bx_1_vvv) in CAD coordinates  
    #     ymesh_box_CAD_v = array of the y points of the box (the space coordiantes of bz/bx_1_vvv) in CAD coordinates  
    #     bz_1_vvv = 3D array of the Bz in CAD coordinates (must correspond and sit on the mesh_box_CAD_v)
    #     bx_1_vvv = 3D array of the Bx in CAD coordinates (must correspond and sit on the mesh_box_CAD_v)
    #     by_1_vvv = 3D array of the By in CAD coordinates (must correspond and sit on the mesh_box_CAD_v)
    #     zz_CAD_r = z coordinate of a point where the vector is to be aproximated
    #     xx_CAD_r = x coordinate of a point where the vector is to be aproximated
    #     yy_CAD_r = y coordinate of a point where the vector is to be aproximated
    #     bend_s = lowest z of the bend
    #     bend_e = highest z of the bend
    #     bend_r = radius of the bend
    #     round_flag = round_flag Carefull, this can have an effect on the code if the better precision for the coordinates is needed (finner mesh),
    #                  but removes some rounding effect stemming frin calculation process. If Off, the effect it removes should be negligible...
    #     distanceTreshold = The distance between points where no approximation is done

    #     This function takes the point (zz_CAD_r,xx_CAD_r, must be inside the mag. field mesh), and a magnetic field sitting on a mesh/grid.
    #     It aproximates (linearly) the vector of the magnetic field at that point (if the point is not identical with mesh-point, 4 vectors surounding the point are used for the aproximation).
    #     The aproximated vector and its coordinates are then transformed to warped coordinates and returned
    #     """

    #     by_1_CAD_ap = -100
    #     bx_1_CAD_ap = -100
    #     bz_1_CAD_ap = -100

    #     # print(yy_CAD_r)
    #     # print(ymesh_box_CAD_v)
    #     CAD_y_1_To_yy_CAD_ri_distance_a = np.array([abs(y_1 - yy_CAD_r) for y_1 in ymesh_box_CAD_v]) 
    #     CAD_z_1_To_zz_CAD_ri_distance_a = np.array([abs(z_1 - zz_CAD_r) for z_1 in zmesh_box_CAD_v])
    #     CAD_x_1_To_xx_CAD_ri_distance_a = np.array([abs(x_1 - xx_CAD_r) for x_1 in xmesh_box_CAD_v])
    #     pointAtZline_flag = min(CAD_z_1_To_zz_CAD_ri_distance_a) < distanceTreshold # The point is on the z-line -> Aproximation not needed in this direction 
    #     pointAtXline_flag = min(CAD_x_1_To_xx_CAD_ri_distance_a) < distanceTreshold # The point is on the x-line -> Aproximation not needed in this direction 
    #     pointAtYline_flag = min(CAD_y_1_To_yy_CAD_ri_distance_a) < distanceTreshold # The point is on the y-line -> Aproximation not needed in this direction Should be always true!
    #     if pointAtYline_flag == False:
    #         print("pointAtYline_flag == False -> should be True as there must not be any bend in y dimension!, ")
    #         print("CAD_y_1_To_yy_CAD_ri_distance_a = {}".format(CAD_y_1_To_yy_CAD_ri_distance_a))
    #         print("ymesh_box_CAD_v = {}".format(ymesh_box_CAD_v))
    #         print("yy_CAD_r = {}".format(yy_CAD_r))

    #     if  pointAtZline_flag and pointAtXline_flag: # The points overlap -> Aproximation not needed

    #         if round_flag:
    #             precise_z = np.where(CAD_z_1_To_zz_CAD_ri_distance_a<=distanceTreshold)
    #             precise_x = np.where(CAD_x_1_To_xx_CAD_ri_distance_a<=distanceTreshold)
    #             precise_y = np.where(CAD_y_1_To_yy_CAD_ri_distance_a<=distanceTreshold)
    #         else:
    #             precise_z = np.where(zmesh_box_CAD_v == zz_CAD_r)
    #             precise_x = np.where(xmesh_box_CAD_v == xx_CAD_r)
    #             precise_y = np.where(ymesh_box_CAD_v == yy_CAD_r)

    #         z_i = precise_z[0][0]
    #         x_i = precise_x[0][0]
    #         y_i = precise_y[0][0]
    #         bx_1_CAD_ap = bx_1_vvv[x_i][y_i][z_i]
    #         by_1_CAD_ap = by_1_vvv[x_i][y_i][z_i]
    #         bz_1_CAD_ap = bz_1_vvv[x_i][y_i][z_i]
            


    #     elif pointAtZline_flag: # The point is on the z-line -> Aproximation not needed in this direction
    #         if round_flag:
    #             precise_z = np.where(CAD_z_1_To_zz_CAD_ri_distance_a<=distanceTreshold)
    #             precise_y = np.where(CAD_y_1_To_yy_CAD_ri_distance_a<=distanceTreshold)
    #         else:
    #             precise_z = np.where(zmesh_box_CAD_v == zz_CAD_r)
    #             precise_y = np.where(ymesh_box_CAD_v == yy_CAD_r)
    #         first_greater_x = np.min(np.where(xmesh_box_CAD_v > xx_CAD_r)[0])
    #         z_i = precise_z[0][0]
    #         x_i = first_greater_x
    #         y_i = precise_y[0][0]
    #         weights = np.zeros(2)
    #         weights[0] = PPDistance(zz_CAD_r,xx_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i])
    #         weights[1] = PPDistance(zz_CAD_r,xx_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i-1])
    #         # Normalization of the weights:
    #         weights = [weight/sum(weights) for weight in weights]

    #         bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i-1][y_i][z_i]
    #         by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i-1][y_i][z_i]
    #         bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i-1][y_i][z_i]

    #     elif pointAtXline_flag: # The point is on the z-line -> Aproximation not needed in this direction

    #         first_greater_z = np.min(np.where(zmesh_box_CAD_v > zz_CAD_r)[0])
    #         if round_flag:
    #             precise_x = np.where(CAD_x_1_To_xx_CAD_ri_distance_a<=distanceTreshold)
    #             precise_y = np.where(CAD_y_1_To_yy_CAD_ri_distance_a<=distanceTreshold)
    #         else:
    #             precise_x = np.where(xmesh_box_CAD_v == xx_CAD_r)
    #             precise_y = np.where(ymesh_box_CAD_v == yy_CAD_r)


    #         z_i = first_greater_z
    #         x_i = precise_x[0][0]
    #         y_i = precise_y[0][0]

    #         weights = np.zeros(2)
    #         weights[0] = PPDistance(zz_CAD_r,xx_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i])
    #         weights[1] = PPDistance(zz_CAD_r,xx_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i])
    #         # Normalization of the weights:
    #         weights = [weight/sum(weights) for weight in weights]

    #         bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i][y_i][z_i-1]
    #         by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i][y_i][z_i-1]
    #         bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i][y_i][z_i-1]


    #     else:
    #         if round_flag:
    #             precise_y = np.where(CAD_y_1_To_yy_CAD_ri_distance_a<=distanceTreshold)
    #         else:
    #             precise_y = np.where(ymesh_box_CAD_v == yy_CAD_r)
    #         first_greater_z = np.min(np.where(zmesh_box_CAD_v > zz_CAD_r)[0])
    #         first_greater_x = np.min(np.where(xmesh_box_CAD_v > xx_CAD_r)[0])
    #         z_i = first_greater_z
    #         x_i = first_greater_x
    #         y_i = precise_y[0][0]
    #         weights = np.zeros(4)
    #         weights[0] = PPDistance(zz_CAD_r,xx_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i])
    #         weights[1] = PPDistance(zz_CAD_r,xx_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i-1])
    #         weights[2] = PPDistance(zz_CAD_r,xx_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i-1])
    #         weights[3] = PPDistance(zz_CAD_r,xx_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i])
    #         # Normalization of the weights:
    #         weights = [weight/sum(weights) for weight in weights]


    #         bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i-1][y_i][z_i] + weights[2]*bx_1_vvv[x_i-1][y_i][z_i-1] + weights[3]*bx_1_vvv[x_i][y_i][z_i-1]
    #         by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i-1][y_i][z_i] + weights[2]*by_1_vvv[x_i-1][y_i][z_i-1] + weights[3]*by_1_vvv[x_i][y_i][z_i-1]
    #         bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i-1][y_i][z_i] + weights[2]*bz_1_vvv[x_i-1][y_i][z_i-1] + weights[3]*bz_1_vvv[x_i][y_i][z_i-1]
        
    #     by_1_warped_final_r = by_1_CAD_ap
    #     y_1_warped_final_r = yy_CAD_r
    #     bz_1_warped_final_r, bx_1_warped_final_r, z_1_warped_final_r, x_1_warped_final_r = CADToWarped_vector(bz_1_CAD_ap,bx_1_CAD_ap,zz_CAD_r,xx_CAD_r,lab0inCAD_z, lab0inCAD_x, angle_labToCAD, bend_s,bend_e,bend_r)
    #     # print("by now : {} ".format(by_1_warped_final_r))
    #     # print("bx now : {} ".format(bx_1_warped_final_r))
    #     # print("bz now : {} ".format(bz_1_warped_final_r))
    #     # print("by orig: {} ".format(by_1_vvv[np.where(by_1_vvv != 0)]))

    #     return bz_1_warped_final_r, bx_1_warped_final_r, by_1_warped_final_r, z_1_warped_final_r, x_1_warped_final_r, y_1_warped_final_r

def Aproximate_CAD_3D(zz_CAD_r, xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v, xmesh_box_CAD_v, ymesh_box_CAD_v, bz_1_vvv, bx_1_vvv, by_1_vvv, lab0inCAD_z, lab0inCAD_x, angle_labToCAD, bend_s, bend_e, bend_r, distanceTreshold, round_flag = True):
    """
    zmesh_box_CAD_v = array of the z points of the box (the space coordiantes of bz/bx_1_vvv) in CAD coordinates 
    xmesh_box_CAD_v = array of the x points of the box (the space coordiantes of bz/bx_1_vvv) in CAD coordinates  
    ymesh_box_CAD_v = array of the y points of the box (the space coordiantes of bz/bx_1_vvv) in CAD coordinates  
    bz_1_vvv = 3D array of the Bz in CAD coordinates (must correspond and sit on the mesh_box_CAD_v)
    bx_1_vvv = 3D array of the Bx in CAD coordinates (must correspond and sit on the mesh_box_CAD_v)
    by_1_vvv = 3D array of the By in CAD coordinates (must correspond and sit on the mesh_box_CAD_v)
    zz_CAD_r = z coordinate of a point where the vector is to be aproximated
    xx_CAD_r = x coordinate of a point where the vector is to be aproximated
    yy_CAD_r = y coordinate of a point where the vector is to be aproximated
    bend_s = lowest z of the bend
    bend_e = highest z of the bend
    bend_r = radius of the bend
    round_flag = round_flag Carefull, this can have an effect on the code if the better precision for the coordinates is needed (finner mesh),
                 but removes some rounding effect stemming frin calculation process. If Off, the effect it removes should be negligible...
    distanceTreshold = The distance between points where no approximation is done

    This function takes the point (zz_CAD_r,xx_CAD_r, must be inside the mag. field mesh), and a magnetic field sitting on a mesh/grid.
    It aproximates (linearly) the vector of the magnetic field at that point (if the point is not identical with mesh-point, 4 vectors surounding the point are used for the aproximation).
    The aproximated vector and its coordinates are then transformed to warped coordinates and returned
    """

    by_1_CAD_ap = -100
    bx_1_CAD_ap = -100
    bz_1_CAD_ap = -100

    # print(yy_CAD_r)
    # print(ymesh_box_CAD_v)
    CAD_y_1_To_yy_CAD_ri_distance_a = np.array([abs(y_1 - yy_CAD_r) for y_1 in ymesh_box_CAD_v]) 
    CAD_z_1_To_zz_CAD_ri_distance_a = np.array([abs(z_1 - zz_CAD_r) for z_1 in zmesh_box_CAD_v])
    CAD_x_1_To_xx_CAD_ri_distance_a = np.array([abs(x_1 - xx_CAD_r) for x_1 in xmesh_box_CAD_v])
    pointAtZline_flag = min(CAD_z_1_To_zz_CAD_ri_distance_a) < distanceTreshold # The point is on the z-line -> Aproximation not needed in this direction 
    pointAtXline_flag = min(CAD_x_1_To_xx_CAD_ri_distance_a) < distanceTreshold # The point is on the x-line -> Aproximation not needed in this direction 
    pointAtYline_flag = min(CAD_y_1_To_yy_CAD_ri_distance_a) < distanceTreshold # The point is on the y-line -> Aproximation not needed in this direction Should be always true!
    # if pointAtYline_flag == False:
    #     print("pointAtYline_flag == False -> should be True as there must not be any bend in y dimension!, ")
    #     print("CAD_y_1_To_yy_CAD_ri_distance_a = {}".format(CAD_y_1_To_yy_CAD_ri_distance_a))
    #     print("ymesh_box_CAD_v = {}".format(ymesh_box_CAD_v))
    #     print("yy_CAD_r = {}".format(yy_CAD_r))
    if pointAtYline_flag:
        if  pointAtZline_flag and pointAtXline_flag: # The points overlap -> Aproximation not needed

            if round_flag:
                precise_z = np.where(CAD_z_1_To_zz_CAD_ri_distance_a<=distanceTreshold)
                precise_x = np.where(CAD_x_1_To_xx_CAD_ri_distance_a<=distanceTreshold)
                precise_y = np.where(CAD_y_1_To_yy_CAD_ri_distance_a<=distanceTreshold)
            else:
                precise_z = np.where(zmesh_box_CAD_v == zz_CAD_r)
                precise_x = np.where(xmesh_box_CAD_v == xx_CAD_r)
                precise_y = np.where(ymesh_box_CAD_v == yy_CAD_r)

            z_i = precise_z[0][0]
            x_i = precise_x[0][0]
            y_i = precise_y[0][0]
            bx_1_CAD_ap = bx_1_vvv[x_i][y_i][z_i]
            by_1_CAD_ap = by_1_vvv[x_i][y_i][z_i]
            bz_1_CAD_ap = bz_1_vvv[x_i][y_i][z_i]
            


        elif pointAtZline_flag: # The point is on the z-line -> Aproximation not needed in this direction
            if round_flag:
                precise_z = np.where(CAD_z_1_To_zz_CAD_ri_distance_a<=distanceTreshold)
                precise_y = np.where(CAD_y_1_To_yy_CAD_ri_distance_a<=distanceTreshold)
            else:
                precise_z = np.where(zmesh_box_CAD_v == zz_CAD_r)
                precise_y = np.where(ymesh_box_CAD_v == yy_CAD_r)
            first_greater_x = np.min(np.where(xmesh_box_CAD_v > xx_CAD_r)[0])
            z_i = precise_z[0][0]
            x_i = first_greater_x
            y_i = precise_y[0][0]
            weights = np.zeros(2)
            weights[0] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i], ymesh_box_CAD_v[y_i] )
            weights[1] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i-1], ymesh_box_CAD_v[y_i] )
            # Normalization of the weights:
            if 0 in weights: 
                print("There is 0 in weights something went wrong, how is not some flag true that should solve this...")
            weights = [1./weight for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            # weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            # weights[0], weights[1] = weights[1], weights[0]
            
            bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i-1][y_i][z_i]
            by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i-1][y_i][z_i]
            bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i-1][y_i][z_i]

        elif pointAtXline_flag: # The point is on the z-line -> Aproximation not needed in this direction

            first_greater_z = np.min(np.where(zmesh_box_CAD_v > zz_CAD_r)[0])
            if round_flag:
                precise_x = np.where(CAD_x_1_To_xx_CAD_ri_distance_a<=distanceTreshold)
                precise_y = np.where(CAD_y_1_To_yy_CAD_ri_distance_a<=distanceTreshold)
            else:
                precise_x = np.where(xmesh_box_CAD_v == xx_CAD_r)
                precise_y = np.where(ymesh_box_CAD_v == yy_CAD_r)


            z_i = first_greater_z
            x_i = precise_x[0][0]
            y_i = precise_y[0][0]

            weights = np.zeros(2)
            weights[0] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i], ymesh_box_CAD_v[y_i] )
            weights[1] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i], ymesh_box_CAD_v[y_i] )
            # Normalization of the weights:
            if 0 in weights: 
                print("There is 0 in weights something went wrong, how is not some flag true that should solve this...")
            weights = [1./weight for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            # weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            # weights[0], weights[1] = weights[1], weights[0]
            
            bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i][y_i][z_i-1]
            by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i][y_i][z_i-1]
            bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i][y_i][z_i-1]


        else:
            if round_flag:
                precise_y = np.where(CAD_y_1_To_yy_CAD_ri_distance_a<=distanceTreshold)
            else:
                precise_y = np.where(ymesh_box_CAD_v == yy_CAD_r)
            first_greater_z = np.min(np.where(zmesh_box_CAD_v > zz_CAD_r)[0])
            first_greater_x = np.min(np.where(xmesh_box_CAD_v > xx_CAD_r)[0])
            z_i = first_greater_z
            x_i = first_greater_x
            y_i = precise_y[0][0]
            weights = np.zeros(4)
            weights[0] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i], ymesh_box_CAD_v[y_i] )
            weights[1] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i], ymesh_box_CAD_v[y_i] )
            weights[2] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i-1], ymesh_box_CAD_v[y_i] )
            weights[3] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i-1], ymesh_box_CAD_v[y_i] )
            # Normalization of the weights:
            if 0 in weights: 
                print("There is 0 in weights something went wrong, how is not some flag true that should solve this...")
            weights = [1./weight for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            # weights[0],weights[1],weights[2],weights[3] = weights[3],weights[2],weights[1],weights[0]
            # weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            
            bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i][y_i][z_i-1] + weights[2]*bx_1_vvv[x_i-1][y_i][z_i] + weights[3]*bx_1_vvv[x_i-1][y_i][z_i-1]
            by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i][y_i][z_i-1] + weights[2]*by_1_vvv[x_i-1][y_i][z_i] + weights[3]*by_1_vvv[x_i-1][y_i][z_i-1]
            bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i][y_i][z_i-1] + weights[2]*bz_1_vvv[x_i-1][y_i][z_i] + weights[3]*bz_1_vvv[x_i-1][y_i][z_i-1]
            # print()

    else:
        if  pointAtZline_flag and pointAtXline_flag: # The points overlap -> Aproximation not needed

            if round_flag:
                precise_z = np.where(CAD_z_1_To_zz_CAD_ri_distance_a<=distanceTreshold)
                precise_x = np.where(CAD_x_1_To_xx_CAD_ri_distance_a<=distanceTreshold)
            else:
                precise_z = np.where(zmesh_box_CAD_v == zz_CAD_r)
                precise_x = np.where(xmesh_box_CAD_v == xx_CAD_r)
            first_greater_y = np.min(np.where(ymesh_box_CAD_v > yy_CAD_r)[0])

            z_i = precise_z[0][0]
            x_i = precise_x[0][0]
            y_i = first_greater_y

            weights = np.zeros(2)
            weights[0] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r,zmesh_box_CAD_v[z_i], xmesh_box_CAD_v[x_i], ymesh_box_CAD_v[y_i])
            weights[1] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r,zmesh_box_CAD_v[z_i], xmesh_box_CAD_v[x_i], ymesh_box_CAD_v[y_i-1])
            # Normalization of the weights:
            if 0 in weights: 
                print("There is 0 in weights something went wrong, how is not some flag true that should solve this...")
            weights = [1./weight for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            # weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            # weights[0], weights[1] = weights[1], weights[0]

            bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i][y_i-1][z_i]
            by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i][y_i-1][z_i]
            bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i][y_i-1][z_i]



        elif pointAtZline_flag: # The point is on the z-line -> Aproximation not needed in this direction
            if round_flag:
                precise_z = np.where(CAD_z_1_To_zz_CAD_ri_distance_a<=distanceTreshold)
            else:
                precise_z = np.where(zmesh_box_CAD_v == zz_CAD_r)
            first_greater_x = np.min(np.where(xmesh_box_CAD_v > xx_CAD_r)[0])
            first_greater_y = np.min(np.where(ymesh_box_CAD_v > yy_CAD_r)[0])


            z_i = precise_z[0][0]
            x_i = first_greater_x
            y_i = first_greater_y


            weights = np.zeros(4)
            weights[0] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i])
            weights[1] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i-1],ymesh_box_CAD_v[y_i])
            weights[2] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i-1])
            weights[3] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i-1],ymesh_box_CAD_v[y_i-1])
            # Normalization of the weights:
            if 0 in weights: 
                print("There is 0 in weights something went wrong, how is not some flag true that should solve this...")
            weights = [1./weight for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            # weights[0],weights[1],weights[2],weights[3] = weights[3],weights[2],weights[1],weights[0]
            # weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i-1][y_i][z_i] + weights[2]*bx_1_vvv[x_i][y_i-1][z_i] + weights[3]*bx_1_vvv[x_i-1][y_i-1][z_i]
            by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i-1][y_i][z_i] + weights[2]*by_1_vvv[x_i][y_i-1][z_i] + weights[3]*by_1_vvv[x_i-1][y_i-1][z_i]
            bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i-1][y_i][z_i] + weights[2]*bz_1_vvv[x_i][y_i-1][z_i] + weights[3]*bz_1_vvv[x_i-1][y_i-1][z_i]


        elif pointAtXline_flag: # The point is on the z-line -> Aproximation not needed in this direction

            first_greater_z = np.min(np.where(zmesh_box_CAD_v > zz_CAD_r)[0])
            first_greater_y = np.min(np.where(ymesh_box_CAD_v > yy_CAD_r)[0])
            if round_flag:
                precise_x = np.where(CAD_x_1_To_xx_CAD_ri_distance_a<=distanceTreshold)
            else:
                precise_x = np.where(xmesh_box_CAD_v == xx_CAD_r)


            z_i = first_greater_z
            x_i = precise_x[0][0]
            y_i = first_greater_y


            weights = np.zeros(4)
            weights[0] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i])
            weights[1] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i])
            weights[2] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i-1])
            weights[3] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i-1])
            # Normalization of the weights:
            if 0 in weights: 
                print("There is 0 in weights something went wrong, how is not some flag true that should solve this...")
            weights = [1./weight for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            # weights[0],weights[1],weights[2],weights[3] = weights[3],weights[2],weights[1],weights[0]
            # weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i][y_i][z_i-1] + weights[2]*bx_1_vvv[x_i][y_i-1][z_i] + weights[3]*bx_1_vvv[x_i][y_i-1][z_i-1]
            by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i][y_i][z_i-1] + weights[2]*by_1_vvv[x_i][y_i-1][z_i] + weights[3]*by_1_vvv[x_i][y_i-1][z_i-1]
            bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i][y_i][z_i-1] + weights[2]*bz_1_vvv[x_i][y_i-1][z_i] + weights[3]*bz_1_vvv[x_i][y_i-1][z_i-1]

        else:
            first_greater_z = np.min(np.where(zmesh_box_CAD_v > zz_CAD_r)[0])
            first_greater_x = np.min(np.where(xmesh_box_CAD_v > xx_CAD_r)[0])
            first_greater_y = np.min(np.where(ymesh_box_CAD_v > yy_CAD_r)[0])
            z_i = first_greater_z
            x_i = first_greater_x
            y_i = first_greater_y

            weights = np.zeros(8)
            weights[0] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i])
            weights[1] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i])
            weights[2] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i-1],ymesh_box_CAD_v[y_i])
            weights[3] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i-1],ymesh_box_CAD_v[y_i])
            weights[4] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i-1])
            weights[5] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i-1])
            weights[6] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i-1],ymesh_box_CAD_v[y_i-1])
            weights[7] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i-1],ymesh_box_CAD_v[y_i-1])
            # Normalization of the weights:
            if 0 in weights: 
                print("There is 0 in weights something went wrong, how is not some flag true that should solve this...")
            weights = [1./weight for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            # The weights are calculated as a distance from the diagonal corner therefore linearly decreasing, but it is not good approxiation...
            # weights[0],weights[1],weights[2],weights[3],weights[4],weights[5],weights[6],weights[7] = weights[7],weights[6],weights[5],weights[4],weights[3],weights[2],weights[1],weights[0]
            # weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i][y_i][z_i-1] + weights[2]*bx_1_vvv[x_i-1][y_i][z_i] + weights[3]*bx_1_vvv[x_i-1][y_i][z_i-1] + weights[4]*bx_1_vvv[x_i][y_i-1][z_i] + weights[5]*bx_1_vvv[x_i][y_i-1][z_i-1] + weights[6]*bx_1_vvv[x_i-1][y_i-1][z_i] + weights[7]*bx_1_vvv[x_i-1][y_i-1][z_i-1]
            by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i][y_i][z_i-1] + weights[2]*by_1_vvv[x_i-1][y_i][z_i] + weights[3]*by_1_vvv[x_i-1][y_i][z_i-1] + weights[4]*by_1_vvv[x_i][y_i-1][z_i] + weights[5]*by_1_vvv[x_i][y_i-1][z_i-1] + weights[6]*by_1_vvv[x_i-1][y_i-1][z_i] + weights[7]*by_1_vvv[x_i-1][y_i-1][z_i-1]
            bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i][y_i][z_i-1] + weights[2]*bz_1_vvv[x_i-1][y_i][z_i] + weights[3]*bz_1_vvv[x_i-1][y_i][z_i-1] + weights[4]*bz_1_vvv[x_i][y_i-1][z_i] + weights[5]*bz_1_vvv[x_i][y_i-1][z_i-1] + weights[6]*bz_1_vvv[x_i-1][y_i-1][z_i] + weights[7]*bz_1_vvv[x_i-1][y_i-1][z_i-1]
        
    if bx_1_CAD_ap <= -100 or by_1_CAD_ap <= -100 or bz_1_CAD_ap <= -100:
        print("Carefull the bx or by or bz was not calcualted...")
        print("bx_1_CAD_ap = {}".format(bx_1_CAD_ap))
        print("by_1_CAD_ap = {}".format(by_1_CAD_ap))
        print("bz_1_CAD_ap = {}".format(bz_1_CAD_ap))
        print("pointAtZline_flag = {}".format(pointAtZline_flag))
        print("pointAtXline_flag = {}".format(pointAtXline_flag))
        print("pointAtYline_flag = {}".format(pointAtYline_flag))
    by_1_warped_final_r = by_1_CAD_ap
    y_1_warped_final_r = yy_CAD_r
    bz_1_warped_final_r, bx_1_warped_final_r, z_1_warped_final_r, x_1_warped_final_r = CADToWarped_vector(bz_1_CAD_ap,bx_1_CAD_ap,zz_CAD_r,xx_CAD_r,lab0inCAD_z, lab0inCAD_x, angle_labToCAD, bend_s,bend_e,bend_r)
    # print("by now : {} ".format(by_1_warped_final_r))
    # print("bx now : {} ".format(bx_1_warped_final_r))
    # print("bz now : {} ".format(bz_1_warped_final_r))
    # print("by orig: {} ".format(by_1_vvv[np.where(by_1_vvv != 0)]))

    return bz_1_warped_final_r, bx_1_warped_final_r, by_1_warped_final_r, z_1_warped_final_r, x_1_warped_final_r, y_1_warped_final_r


def WarpedToCAD_Mesh1Din_3Dout(zmesh_w_v, xmesh_w_v, ymesh_w_v, lab0inCAD_z, lab0inCAD_x, angle_labToCAD, bend_par_v, LoadFromFile_flag = False, LoadFromFile_name = "None", SaveToFile_name = "WarpedMeshInCAD.h5py", timing = True):
    bend_start = bend_par_v[0]
    bend_end = bend_par_v[1]
    bend_radius = bend_par_v[2]

    z_len = len(zmesh_w_v)
    x_len = len(xmesh_w_v)
    y_len = len(ymesh_w_v)
    print(x_len)
    print(y_len)
    print(z_len)
    mesh_w_numOfPoints = z_len*x_len*y_len


    if LoadFromFile_flag and LoadFromFile_name != "None":
        file_backup = h5py.File(LoadFromFile_name,"r")
        zz_CAD = np.array(file_backup["zz_CAD"])
        yy_CAD = np.array(file_backup["yy_CAD"])
        xx_CAD = np.array(file_backup["xx_CAD"])
        file_backup.close()
    else:
        # The mesh warped mesh in CAD coordinates:
        yy_CAD = np.zeros((x_len,y_len,z_len))
        xx_CAD = np.zeros((x_len,y_len,z_len))
        zz_CAD = np.zeros((x_len,y_len,z_len))
        print("\nWarpingToCAD starting\n")
        if timing:
            pbar = tqdm(total = (x_len)*(y_len)*(z_len)) # Just for timing
            # Transformation of the warped mesh to CAD coordinates
        for y_i in range(y_len):
            for x_i in range(x_len):
                for z_i in range(z_len):
                    zz_CAD[x_i][y_i][z_i], xx_CAD[x_i][y_i][z_i] = WarpedToCAD_point(zmesh_w_v[z_i],xmesh_w_v[x_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius)
                    # print("{}, {} = WarpedToCAD({}, {}, {}, {}, {}, {}, {})".format(zz_CAD[x_i][y_i][z_i],xx_CAD[x_i][y_i][z_i],zmesh_w_v[z_i],xmesh_w_v[x_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius))
                # print("[{}][{}]/[{}][{}] x{} points converted".format(y_i,x_i,y_len,x_len,z_len))
                    if timing:
                        pbar.update(1)
                    # yy_lab[x_i][y_i][z_i] = ymesh_w_v[y_i]
        yy_lab = [[[ymesh_w_v[y_i] for z_i in range(z_len)] for y_i in range(y_len)] for x_i in range(x_len)]
        yy_CAD = np.array([[[ymesh_w_v[y_i] for z_i in range(z_len)] for y_i in range(y_len)] for x_i in range(x_len)]) # Sae as yy_lab and yy_warped

        print("\nWarpingToCAD completed\n")

        file_backup = h5py.File(SaveToFile_name,"w")
        zz_CAD_ds = file_backup.create_dataset("zz_CAD", data = zz_CAD)
        yy_CAD_ds = file_backup.create_dataset("yy_CAD", data = yy_CAD)
        xx_CAD_ds = file_backup.create_dataset("xx_CAD", data = xx_CAD)
        file_backup.close()
    return zz_CAD, yy_CAD, xx_CAD

def MagneticFieldTransformation_boxMesh_CAD_3D(zmesh_box_CAD_v, xmesh_box_CAD_v, ymesh_box_CAD_v, bz_CAD_vvv, bx_CAD_vvv, by_CAD_vvv, zmesh_w_v, xmesh_w_v, ymesh_w_v, lab0inCAD_z, lab0inCAD_x, angle_labToCAD, bend_par_v, round_flag = True, distanceTreshold = 1e-8, LoadFromFile_flag = False, LoadFromFile_name = "None", SaveToFile_name = "WarpedMeshInCAD.h5py"):
    """
    The box mesh must be rectangular,
    zmesh_box_CAD_v = array of the z points of the box (the space coordiantes of bz/bx_1_vvv) in CADoratory coordinates 
    xmesh_box_CAD_v = array of the x points of the box (the space coordiantes of bz/bx_1_vvv) in CADoratory coordinates 
    xmesh_box_CAD_v = array of the y points of the box (the space coordiantes of bz/bx_1_vvv) in CADoratory coordinates 
    bz_CAD_vvv = 3D array of the Bz in CADoratory coordinates (must correspond and sit on the mesh_box_CAD_v)
    bx_CAD_vvv = 3D array of the Bx in CADoratory coordinates (must correspond and sit on the mesh_box_CAD_v)
    by_CAD_vvv = 3D array of the By in CADoratory coordinates (must correspond and sit on the mesh_box_CAD_v)
    zmesh_w_v = array of the z points of warped mesh in warped coordinates 
    xmesh_w_v = array of the x points of warped mesh in warped coordinates 
    ymesh_w_v = array of the y points of warped mesh in warped coordinates 
    bend_par_v = parameters of the bend, must be in format:

    bend_start = bend_par_v[0] - lowest z of the bend
    bend_end = bend_par_v[1] - - highest z of the bend
    bend_radius = bend_par_v[2] - radius of the bend

    round_flag = flag if to use distanceTreshold or exact coordinates for points to be delared identical and no aproximation is then done, for more info viz Aproximation function
    distanceTreshold = the distance treshold to declare two points as identical



    The idea is following: Get warped mesh into CAD, aproximate the field in the CAD on the warped mesh in CAD, 
    and then transform this field back into warped coordinates, and the x y z will create a good grid which is esential.
    The aproximation of the field is done in the CAD coord. instead in the warped coord.
    because of more clean and safe aproximation from grided data into "non-grid" instead the opposite.

    This function returns takes the bz and bx as 3D but returns bz_final and bx_final as a 2D (z and x dimensions) - the y dimension is ommited as RZ and XYZ could affect this.

    """

    # Parameters

    # Few pre-calculations
    z_len = len(zmesh_w_v)
    x_len = len(xmesh_w_v)
    y_len = len(ymesh_w_v)
    mesh_w_numOfPoints = z_len*x_len*y_len

    bend_start = bend_par_v[0]
    bend_end = bend_par_v[1]
    bend_radius = bend_par_v[2]

    zz_CAD, yy_CAD, xx_CAD = WarpedToCAD_Mesh1Din_3Dout(zmesh_w_v, xmesh_w_v, ymesh_w_v, lab0inCAD_z, lab0inCAD_x, angle_labToCAD, bend_par_v, LoadFromFile_flag = LoadFromFile_flag, LoadFromFile_name = LoadFromFile_name, SaveToFile_name = SaveToFile_name)
    print("yy_CAD")
    print(yy_CAD[0,::,0])
    print("xx_CAD")
    print(xx_CAD[::,0,0])
    print("zz_CAD")
    print(zz_CAD[0,0,::])
    xx_CAD_r = xx_CAD.ravel()
    yy_CAD_r = yy_CAD.ravel()
    zz_CAD_r = zz_CAD.ravel()
    # print(zz_CAD)
    
    #Declaring empty arrays for filling

        #Final bx and bz aproximated and back in warped coordinates
    by_1_warped_final_r = np.zeros(mesh_w_numOfPoints)
    bx_1_warped_final_r = np.zeros(mesh_w_numOfPoints)
    bz_1_warped_final_r = np.zeros(mesh_w_numOfPoints)

        #Final x an z of the bx and bz back in warped coordinates nonzero only inside the box, --> can be used for the box identification in warped coordinates otherwise not used
    y_1_warped_final_r = np.zeros(mesh_w_numOfPoints)
    x_1_warped_final_r = np.zeros(mesh_w_numOfPoints)
    z_1_warped_final_r = np.zeros(mesh_w_numOfPoints)

    # print(zmesh_box_CAD_v)
    # Box cropping of the transformed warped mesh in lab - Finding the points that will be used for the aproximation (inside the bfield_box) 
    used_coord_0 = np.where(zz_CAD_r >= zmesh_box_CAD_v[0] - distanceTreshold)
    used_coord = used_coord_0
    used_coord_1 = np.where(zz_CAD_r[used_coord] <= zmesh_box_CAD_v[-1] + distanceTreshold)
    used_coord = (used_coord[0][used_coord_1],) 
    # print(used_coord)
    # print("used_coord: z  completed")
    used_coord_2 = np.where(xx_CAD_r[used_coord] <= xmesh_box_CAD_v[-1] + distanceTreshold)
    used_coord = (used_coord[0][used_coord_2],) 
    used_coord_3 = np.where(xx_CAD_r[used_coord] >= xmesh_box_CAD_v[0] - distanceTreshold)
    used_coord = (used_coord[0][used_coord_3],) 
    # print("used_coord: x  completed")
    used_coord_4 = np.where(yy_CAD_r[used_coord] <= ymesh_box_CAD_v[-1] + distanceTreshold)
    used_coord = (used_coord[0][used_coord_4],) 
    used_coord_5 = np.where(yy_CAD_r[used_coord] >= ymesh_box_CAD_v[0] - distanceTreshold)
    used_coord = (used_coord[0][used_coord_5],) 
    # print("used_coord: y  completed")
    print("used_coord: y ")
    print(yy_CAD_r[used_coord])
    print("used_coord: x ")
    print(xx_CAD_r[used_coord])
    print("used_coord: z ")
    print(zz_CAD_r[used_coord])

    # np.savetxt("tmp.txt",yy_CAD_r)

    print("the box: {} < z < {}".format(zmesh_box_CAD_v[-1],zmesh_box_CAD_v[0]))
    print("in CAD : {} < x < {}".format(xmesh_box_CAD_v[-1],xmesh_box_CAD_v[0]))
    print("coord. : {} < y < {}".format(ymesh_box_CAD_v[-1],ymesh_box_CAD_v[0]))
    print("used number of points: {}".format(len(used_coord[0])))

    # print("Original zz_CAD_r: {}".format(zz_lab_r))
    # print("Croped zz_lab_r: {}".format(zz_lab_r[used_coord]))
    # print("Original xx_lab_r: {}".format(xx_lab_r))
    # print("Croped xx_lab_r: {}".format(xx_lab_r[used_coord]))

    # Aproximating the B field from box mesh into the warped mesh points in lab coordinates and then transforming them back into warped coordinates 
    print("\nStarting the mapping of warped mesh in CAD and its transformation back to Warped coordinates \n")
    pbar = tqdm(total = len(used_coord[0]))
    # line_i = 0
    for i_warpedMesh in used_coord[0]:
        pbar.update(1)
        # if line_i % 5000 == 0:
            # print("{}/{} thousands points approximated".format(int(line_i/1000),int(len(used_coord[0])/1000)))
        # line_i += 1 
        bz_1_warped_final_r[i_warpedMesh], bx_1_warped_final_r[i_warpedMesh], by_1_warped_final_r[i_warpedMesh], \
        z_1_warped_final_r[i_warpedMesh], x_1_warped_final_r[i_warpedMesh], y_1_warped_final_r[i_warpedMesh] = \
        Aproximate_CAD_3D( zmesh_box_CAD_v = zmesh_box_CAD_v,\
                    xmesh_box_CAD_v = xmesh_box_CAD_v,\
                    ymesh_box_CAD_v = ymesh_box_CAD_v,\
                    bz_1_vvv = bz_CAD_vvv,\
                    bx_1_vvv = bx_CAD_vvv,\
                    by_1_vvv = by_CAD_vvv,\
                    zz_CAD_r = zz_CAD_r[i_warpedMesh],\
                    xx_CAD_r = xx_CAD_r[i_warpedMesh],\
                    yy_CAD_r = yy_CAD_r[i_warpedMesh],\
                    lab0inCAD_z = lab0inCAD_z ,\
                    lab0inCAD_x = lab0inCAD_x ,\
                    angle_labToCAD = angle_labToCAD ,\
                    bend_s = bend_start,\
                    bend_e = bend_end,\
                    bend_r = bend_radius,\
                    distanceTreshold = distanceTreshold,\
                    round_flag = round_flag)
    print("\nCompleted mapping of warped mesh in CAD and its transformation back to Warped coordinates \n")




    y_1_warped_final = y_1_warped_final_r.reshape(x_len, y_len, z_len)
    x_1_warped_final = x_1_warped_final_r.reshape(x_len, y_len, z_len)
    z_1_warped_final = z_1_warped_final_r.reshape(x_len, y_len, z_len)

    print("yy_warped")
    print(y_1_warped_final[0,::,0])
    print("xx_warped")
    print(x_1_warped_final[::,0,0])

        # Using map function -> does not seem to work faster
    # AproximatedValues = map(Aproximate_CAD_3D,  zz_CAD_r[used_coord],\
    #                                             xx_CAD_r[used_coord],\
    #                                             yy_CAD_r[used_coord],\
    #                                             it.repeat(zmesh_box_CAD_v,len(used_coord[0])),\
    #                                             it.repeat(xmesh_box_CAD_v,len(used_coord[0])),\
    #                                             it.repeat(ymesh_box_CAD_v,len(used_coord[0])),\
    #                                             it.repeat(bz_CAD_vvv,len(used_coord[0])),\
    #                                             it.repeat(bx_CAD_vvv,len(used_coord[0])),\
    #                                             it.repeat(by_CAD_vvv,len(used_coord[0])),\
    #                                             it.repeat(lab0inCAD_z,len(used_coord[0])),\
    #                                             it.repeat(lab0inCAD_x,len(used_coord[0])),\
    #                                             it.repeat(angle_labToCAD,len(used_coord[0])),\
    #                                             it.repeat(bend_start,len(used_coord[0])),\
    #                                             it.repeat(bend_end,len(used_coord[0])),\
    #                                             it.repeat(bend_radius,len(used_coord[0])),\
    #                                             it.repeat(distanceTreshold,len(used_coord[0])),\
    #                                             it.repeat(round_flag,len(used_coord[0])))
    # pbar.close()
    # bz_1_warped_final_r[used_coord] = np.array([AproximatedValues[i][0] for i in range(len(used_coord[0]))])
    # bx_1_warped_final_r[used_coord] = np.array([AproximatedValues[i][1] for i in range(len(used_coord[0]))])
    # by_1_warped_final_r[used_coord] = np.array([AproximatedValues[i][2] for i in range(len(used_coord[0]))])
    # z_1_warped_final_r[used_coord] = np.array([AproximatedValues[i][3] for i in range(len(used_coord[0]))])
    # x_1_warped_final_r[used_coord] = np.array([AproximatedValues[i][4] for i in range(len(used_coord[0]))])
    # y_1_warped_final_r[used_coord] = np.array([AproximatedValues[i][5] for i in range(len(used_coord[0]))])

    by_1_warped_final = by_1_warped_final_r.reshape(x_len, y_len, z_len)
    bx_1_warped_final = bx_1_warped_final_r.reshape(x_len, y_len, z_len)
    bz_1_warped_final = bz_1_warped_final_r.reshape(x_len, y_len, z_len)



    # Just testting plot of warped mesh and used cooridnates (inside the box in warped coordinates)
    # zz_w = np.array([[zmes_w for zmes_w in zmesh_w_v] for xmesh_w in xmesh_w_v])
    # xx_w = np.array([[xmesh_w for zmes_w in zmesh_w_v] for xmesh_w in xmesh_w_v])

    # xx_w_r = xx_w.ravel()
    # zz_w_r = zz_w.ravel()

    # plt.scatter(zz_w_r,xx_w_r, s = 1, color = "b", label = "Warped mesh")
    # plt.scatter(z_1_warped_final_r, x_1_warped_final_r, s = 1, color = "r", label = "Warped mesh")

    return bz_1_warped_final, bx_1_warped_final, by_1_warped_final


def MagneticFieldTransformation_boxPar_CAD_3D(bfield_box_par_v, dz_box_CAD, dx_box_CAD, dy_box_CAD, bz_CAD_vvv, bx_CAD_vvv, by_CAD_vvv, zmesh_w_v, xmesh_w_v, ymesh_w_v, lab0inCAD_z, lab0inCAD_x, angle_labToCAD, bend_par_v, round_flag = True, distanceTreshold = 1e-10, LoadFromFile_flag = False, LoadFromFile_name = "None", SaveToFile_name = "WarpedMeshInCAD.h5py"):
    """
    The bfield box must be rectangular and the bfield_box_par_v in format: 

    bfield_box_xcent_CAD = bfield_box_par_v[0] - x center at lowest z
    bfield_box_ycent_CAD = bfield_box_par_v[1] - y center at lowest z
    bfield_box_zs_CAD = bfield_box_par_v[2] - lowest z
    bfield_box_ze_CAD = bfield_box_par_v[3] - higest z
    bfield_box_XhalfOfwidth_CAD = bfield_box_par_v[4] - half of the width of side in x dimension ( so maybe technically hight and not width but wathever)
    bfield_box_YhalfOfwidth_CAD = bfield_box_par_v[5] - half of the width of side in y dimension ( so maybe technically hight and not width but wathever)

    dz_box_CAD = the increment in z direction of the box mesh in CADoratory coordinates 
    dx_box_CAD = the increment in x direction of the box mesh in CADoratory coordinates 
    bz_CAD_vvv = 3D array of the Bz in CADoratory coordinates (must correspond and sit on the z/xmesh_box_CAD_v)
    bx_CAD_vvv = 3D array of the Bx in CADoratory coordinates (must correspond and sit on the z/xmesh_box_CAD_v)
    zmesh_w_v = array of the z points of warped mesh in warped coordinates 
    xmesh_w_v = array of the x points of warped mesh in warped coordinates 
    bend_par_v = parameters of the bend, must be in format:

    bend_start = bend_par_v[0] - lowest z of the bend
    bend_end = bend_par_v[1] - - highest z of the bend
    bend_radius = bend_par_v[2] - radius of the bend

    round_flag = flag if to use distanceTreshold or exact coordinates for points to be delared identical and no aproximation is then done, for more info viz Aproximation function
    distanceTreshold = the distance treshold to declare two points as identical

    The idea is following: Get warped mesh in warped coordinates, transform it into CAD coordinates, aproximate the field in the CAD coordinates onto the warped mesh in CAD coordinates, 
    and then transform this field back into warped coordinates, and the x y z will create a good grid which is esential for applying the field
    The aproximation of the field is done in the CAD coord. instead in the warped coord.
    because of more clean and safe aproximation from grided data into "non-grid" instead the opposite.

    This function returns takes the bz and bx as 3D but returns bz_final and bx_final as a 2D (z and x dimensions) - the y dimension is ommited as RZ and XYZ could affect this.
    """
    bfield_box_xcent_CAD = bfield_box_par_v[0]
    bfield_box_ycent_CAD = bfield_box_par_v[1]
    bfield_box_zs_CAD = bfield_box_par_v[2]
    bfield_box_ze_CAD = bfield_box_par_v[3]
    bfield_box_XhalfOfwidth_CAD = bfield_box_par_v[4]
    bfield_box_YhalfOfwidth_CAD = bfield_box_par_v[5]
    print("bfield_box_par_v: {}".format(bfield_box_par_v))

    # Defining the bfield mesh in the LAB
    nz_box = int((bfield_box_ze_CAD-bfield_box_zs_CAD)/dz_box_CAD + 1)
    nx_box = int(bfield_box_XhalfOfwidth_CAD*2/dx_box_CAD + 1)
    ny_box = int(bfield_box_YhalfOfwidth_CAD*2/dy_box_CAD + 1)

    zmesh_box_CAD_v = [bfield_box_zs_CAD + z_i*dz_box_CAD for z_i in range(nz_box)]
    xmesh_box_CAD_v = [bfield_box_xcent_CAD - bfield_box_XhalfOfwidth_CAD + x_i*dx_box_CAD for x_i in range(nx_box)]
    ymesh_box_CAD_v = [bfield_box_ycent_CAD - bfield_box_YhalfOfwidth_CAD + y_i*dy_box_CAD for y_i in range(ny_box)]
    print("1513")
    print(zmesh_box_CAD_v)
    print(ymesh_box_CAD_v)
    print(xmesh_box_CAD_v)
    bz_1_warped_final, bx_1_warped_final, by_1_warped_final = MagneticFieldTransformation_boxMesh_CAD_3D(zmesh_box_CAD_v = zmesh_box_CAD_v,\
                                                                                                        xmesh_box_CAD_v = xmesh_box_CAD_v,\
                                                                                                        ymesh_box_CAD_v = ymesh_box_CAD_v,\
                                                                                                        bz_CAD_vvv = bz_CAD_vvv,\
                                                                                                        bx_CAD_vvv = bx_CAD_vvv,\
                                                                                                        by_CAD_vvv = by_CAD_vvv,\
                                                                                                        zmesh_w_v = zmesh_w_v,\
                                                                                                        xmesh_w_v = xmesh_w_v,\
                                                                                                        ymesh_w_v = ymesh_w_v,\
                                                                                                        lab0inCAD_z = lab0inCAD_z ,\
                                                                                                        lab0inCAD_x = lab0inCAD_x ,\
                                                                                                        angle_labToCAD = angle_labToCAD ,\
                                                                                                        bend_par_v = bend_par_v,\
                                                                                                        round_flag = round_flag,\
                                                                                                        distanceTreshold = distanceTreshold,\
                                                                                                        LoadFromFile_flag = LoadFromFile_flag,\
                                                                                                        LoadFromFile_name = LoadFromFile_name,\
                                                                                                        SaveToFile_name = SaveToFile_name,\
                                                                                                        )
    return bz_1_warped_final, bx_1_warped_final, by_1_warped_final


# CAD -> Lab 
def Aproximate_CADToLab_3D(zz_CAD_r, xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v, xmesh_box_CAD_v, ymesh_box_CAD_v, bz_1_vvv, bx_1_vvv, by_1_vvv, lab0inCAD_z, lab0inCAD_x, angle_labToCAD, distanceTreshold, round_flag = True):
    """
    zmesh_box_CAD_v = array of the z points of the box (the space coordiantes of bz/bx_1_vvv) in CAD coordinates 
    xmesh_box_CAD_v = array of the x points of the box (the space coordiantes of bz/bx_1_vvv) in CAD coordinates  
    ymesh_box_CAD_v = array of the y points of the box (the space coordiantes of bz/bx_1_vvv) in CAD coordinates  
    bz_1_vvv = 3D array of the Bz in CAD coordinates (must correspond and sit on the mesh_box_CAD_v)
    bx_1_vvv = 3D array of the Bx in CAD coordinates (must correspond and sit on the mesh_box_CAD_v)
    by_1_vvv = 3D array of the By in CAD coordinates (must correspond and sit on the mesh_box_CAD_v)
    zz_CAD_r = z coordinate of a point where the vector is to be aproximated
    xx_CAD_r = x coordinate of a point where the vector is to be aproximated
    yy_CAD_r = y coordinate of a point where the vector is to be aproximated
    bend_s = lowest z of the bend
    bend_e = highest z of the bend
    bend_r = radius of the bend
    round_flag = round_flag Carefull, this can have an effect on the code if the better precision for the coordinates is needed (finner mesh),
                 but removes some rounding effect stemming frin calculation process. If Off, the effect it removes should be negligible...
    distanceTreshold = The distance between points where no approximation is done

    This function takes the point (zz_CAD_r,xx_CAD_r, must be inside the mag. field mesh), and a magnetic field sitting on a mesh/grid.
    It aproximates (linearly) the vector of the magnetic field at that point (if the point is not identical with mesh-point, 4 vectors surounding the point are used for the aproximation).
    The aproximated vector and its coordinates are then transformed to lab coordinates and returned
    """

    by_1_CAD_ap = -100
    bx_1_CAD_ap = -100
    bz_1_CAD_ap = -100

    # print(yy_CAD_r)
    # print(ymesh_box_CAD_v)
    CAD_y_1_To_yy_CAD_ri_distance_a = np.array([abs(y_1 - yy_CAD_r) for y_1 in ymesh_box_CAD_v]) 
    CAD_z_1_To_zz_CAD_ri_distance_a = np.array([abs(z_1 - zz_CAD_r) for z_1 in zmesh_box_CAD_v])
    CAD_x_1_To_xx_CAD_ri_distance_a = np.array([abs(x_1 - xx_CAD_r) for x_1 in xmesh_box_CAD_v])
    pointAtZline_flag = min(CAD_z_1_To_zz_CAD_ri_distance_a) < distanceTreshold # The point is on the z-line -> Aproximation not needed in this direction 
    pointAtXline_flag = min(CAD_x_1_To_xx_CAD_ri_distance_a) < distanceTreshold # The point is on the x-line -> Aproximation not needed in this direction 
    pointAtYline_flag = min(CAD_y_1_To_yy_CAD_ri_distance_a) < distanceTreshold # The point is on the y-line -> Aproximation not needed in this direction Should be always true!
    # if pointAtYline_flag == False:
    #     print("pointAtYline_flag == False -> should be True as there must not be any bend in y dimension!, ")
    #     print("CAD_y_1_To_yy_CAD_ri_distance_a = {}".format(CAD_y_1_To_yy_CAD_ri_distance_a))
    #     print("ymesh_box_CAD_v = {}".format(ymesh_box_CAD_v))
    #     print("yy_CAD_r = {}".format(yy_CAD_r))
    if pointAtYline_flag:
        if  pointAtZline_flag and pointAtXline_flag: # The points overlap -> Aproximation not needed

            if round_flag:
                precise_z = np.where(CAD_z_1_To_zz_CAD_ri_distance_a<=distanceTreshold)
                precise_x = np.where(CAD_x_1_To_xx_CAD_ri_distance_a<=distanceTreshold)
                precise_y = np.where(CAD_y_1_To_yy_CAD_ri_distance_a<=distanceTreshold)
            else:
                precise_z = np.where(zmesh_box_CAD_v == zz_CAD_r)
                precise_x = np.where(xmesh_box_CAD_v == xx_CAD_r)
                precise_y = np.where(ymesh_box_CAD_v == yy_CAD_r)

            z_i = precise_z[0][0]
            x_i = precise_x[0][0]
            y_i = precise_y[0][0]
            bx_1_CAD_ap = bx_1_vvv[x_i][y_i][z_i]
            by_1_CAD_ap = by_1_vvv[x_i][y_i][z_i]
            bz_1_CAD_ap = bz_1_vvv[x_i][y_i][z_i]
            


        elif pointAtZline_flag: # The point is on the z-line -> Aproximation not needed in this direction
            if round_flag:
                precise_z = np.where(CAD_z_1_To_zz_CAD_ri_distance_a<=distanceTreshold)
                precise_y = np.where(CAD_y_1_To_yy_CAD_ri_distance_a<=distanceTreshold)
            else:
                precise_z = np.where(zmesh_box_CAD_v == zz_CAD_r)
                precise_y = np.where(ymesh_box_CAD_v == yy_CAD_r)
            first_greater_x = np.min(np.where(xmesh_box_CAD_v > xx_CAD_r)[0])
            z_i = precise_z[0][0]
            x_i = first_greater_x
            y_i = precise_y[0][0]
            weights = np.zeros(2)
            weights[0] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i], ymesh_box_CAD_v[y_i] )
            weights[1] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i-1], ymesh_box_CAD_v[y_i] )
            # Normalization of the weights:
            if 0 in weights: 
                print("There is 0 in weights something went wrong, how is not some flag true that should solve this...")
            weights = [1./weight for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            # weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            # weights[0], weights[1] = weights[1], weights[0]
            
            bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i-1][y_i][z_i]
            by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i-1][y_i][z_i]
            bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i-1][y_i][z_i]

        elif pointAtXline_flag: # The point is on the z-line -> Aproximation not needed in this direction

            first_greater_z = np.min(np.where(zmesh_box_CAD_v > zz_CAD_r)[0])
            if round_flag:
                precise_x = np.where(CAD_x_1_To_xx_CAD_ri_distance_a<=distanceTreshold)
                precise_y = np.where(CAD_y_1_To_yy_CAD_ri_distance_a<=distanceTreshold)
            else:
                precise_x = np.where(xmesh_box_CAD_v == xx_CAD_r)
                precise_y = np.where(ymesh_box_CAD_v == yy_CAD_r)


            z_i = first_greater_z
            x_i = precise_x[0][0]
            y_i = precise_y[0][0]

            weights = np.zeros(2)
            weights[0] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i], ymesh_box_CAD_v[y_i] )
            weights[1] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i], ymesh_box_CAD_v[y_i] )
            # Normalization of the weights:
            if 0 in weights: 
                print("There is 0 in weights something went wrong, how is not some flag true that should solve this...")
            weights = [1./weight for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            # weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            # weights[0], weights[1] = weights[1], weights[0]
            
            bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i][y_i][z_i-1]
            by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i][y_i][z_i-1]
            bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i][y_i][z_i-1]


        else:
            if round_flag:
                precise_y = np.where(CAD_y_1_To_yy_CAD_ri_distance_a<=distanceTreshold)
            else:
                precise_y = np.where(ymesh_box_CAD_v == yy_CAD_r)
            first_greater_z = np.min(np.where(zmesh_box_CAD_v > zz_CAD_r)[0])
            first_greater_x = np.min(np.where(xmesh_box_CAD_v > xx_CAD_r)[0])
            z_i = first_greater_z
            x_i = first_greater_x
            y_i = precise_y[0][0]
            weights = np.zeros(4)
            weights[0] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i], ymesh_box_CAD_v[y_i] )
            weights[1] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i], ymesh_box_CAD_v[y_i] )
            weights[2] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i-1], ymesh_box_CAD_v[y_i] )
            weights[3] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r, zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i-1], ymesh_box_CAD_v[y_i] )
            # Normalization of the weights:
            if 0 in weights: 
                print("There is 0 in weights something went wrong, how is not some flag true that should solve this...")
            weights = [1./weight for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            # weights[0],weights[1],weights[2],weights[3] = weights[3],weights[2],weights[1],weights[0]
            # weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            
            bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i][y_i][z_i-1] + weights[2]*bx_1_vvv[x_i-1][y_i][z_i] + weights[3]*bx_1_vvv[x_i-1][y_i][z_i-1]
            by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i][y_i][z_i-1] + weights[2]*by_1_vvv[x_i-1][y_i][z_i] + weights[3]*by_1_vvv[x_i-1][y_i][z_i-1]
            bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i][y_i][z_i-1] + weights[2]*bz_1_vvv[x_i-1][y_i][z_i] + weights[3]*bz_1_vvv[x_i-1][y_i][z_i-1]
            # print()

    else:
        if  pointAtZline_flag and pointAtXline_flag: # The points overlap -> Aproximation not needed

            if round_flag:
                precise_z = np.where(CAD_z_1_To_zz_CAD_ri_distance_a<=distanceTreshold)
                precise_x = np.where(CAD_x_1_To_xx_CAD_ri_distance_a<=distanceTreshold)
            else:
                precise_z = np.where(zmesh_box_CAD_v == zz_CAD_r)
                precise_x = np.where(xmesh_box_CAD_v == xx_CAD_r)
            first_greater_y = np.min(np.where(ymesh_box_CAD_v > yy_CAD_r)[0])

            z_i = precise_z[0][0]
            x_i = precise_x[0][0]
            y_i = first_greater_y

            weights = np.zeros(2)
            weights[0] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r,zmesh_box_CAD_v[z_i], xmesh_box_CAD_v[x_i], ymesh_box_CAD_v[y_i])
            weights[1] = PPDistance3D(zz_CAD_r,xx_CAD_r, yy_CAD_r,zmesh_box_CAD_v[z_i], xmesh_box_CAD_v[x_i], ymesh_box_CAD_v[y_i-1])
            # Normalization of the weights:
            if 0 in weights: 
                print("There is 0 in weights something went wrong, how is not some flag true that should solve this...")
            weights = [1./weight for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            # weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            # weights[0], weights[1] = weights[1], weights[0]

            bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i][y_i-1][z_i]
            by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i][y_i-1][z_i]
            bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i][y_i-1][z_i]



        elif pointAtZline_flag: # The point is on the z-line -> Aproximation not needed in this direction
            if round_flag:
                precise_z = np.where(CAD_z_1_To_zz_CAD_ri_distance_a<=distanceTreshold)
            else:
                precise_z = np.where(zmesh_box_CAD_v == zz_CAD_r)
            first_greater_x = np.min(np.where(xmesh_box_CAD_v > xx_CAD_r)[0])
            first_greater_y = np.min(np.where(ymesh_box_CAD_v > yy_CAD_r)[0])


            z_i = precise_z[0][0]
            x_i = first_greater_x
            y_i = first_greater_y


            weights = np.zeros(4)
            weights[0] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i])
            weights[1] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i-1],ymesh_box_CAD_v[y_i])
            weights[2] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i-1])
            weights[3] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i-1],ymesh_box_CAD_v[y_i-1])
            # Normalization of the weights:
            if 0 in weights: 
                print("There is 0 in weights something went wrong, how is not some flag true that should solve this...")
            weights = [1./weight for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            # weights[0],weights[1],weights[2],weights[3] = weights[3],weights[2],weights[1],weights[0]
            # weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i-1][y_i][z_i] + weights[2]*bx_1_vvv[x_i][y_i-1][z_i] + weights[3]*bx_1_vvv[x_i-1][y_i-1][z_i]
            by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i-1][y_i][z_i] + weights[2]*by_1_vvv[x_i][y_i-1][z_i] + weights[3]*by_1_vvv[x_i-1][y_i-1][z_i]
            bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i-1][y_i][z_i] + weights[2]*bz_1_vvv[x_i][y_i-1][z_i] + weights[3]*bz_1_vvv[x_i-1][y_i-1][z_i]


        elif pointAtXline_flag: # The point is on the z-line -> Aproximation not needed in this direction

            first_greater_z = np.min(np.where(zmesh_box_CAD_v > zz_CAD_r)[0])
            first_greater_y = np.min(np.where(ymesh_box_CAD_v > yy_CAD_r)[0])
            if round_flag:
                precise_x = np.where(CAD_x_1_To_xx_CAD_ri_distance_a<=distanceTreshold)
            else:
                precise_x = np.where(xmesh_box_CAD_v == xx_CAD_r)


            z_i = first_greater_z
            x_i = precise_x[0][0]
            y_i = first_greater_y


            weights = np.zeros(4)
            weights[0] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i])
            weights[1] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i])
            weights[2] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i-1])
            weights[3] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i-1])
            # Normalization of the weights:
            if 0 in weights: 
                print("There is 0 in weights something went wrong, how is not some flag true that should solve this...")
            weights = [1./weight for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            # weights[0],weights[1],weights[2],weights[3] = weights[3],weights[2],weights[1],weights[0]
            # weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i][y_i][z_i-1] + weights[2]*bx_1_vvv[x_i][y_i-1][z_i] + weights[3]*bx_1_vvv[x_i][y_i-1][z_i-1]
            by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i][y_i][z_i-1] + weights[2]*by_1_vvv[x_i][y_i-1][z_i] + weights[3]*by_1_vvv[x_i][y_i-1][z_i-1]
            bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i][y_i][z_i-1] + weights[2]*bz_1_vvv[x_i][y_i-1][z_i] + weights[3]*bz_1_vvv[x_i][y_i-1][z_i-1]

        else:
            first_greater_z = np.min(np.where(zmesh_box_CAD_v > zz_CAD_r)[0])
            first_greater_x = np.min(np.where(xmesh_box_CAD_v > xx_CAD_r)[0])
            first_greater_y = np.min(np.where(ymesh_box_CAD_v > yy_CAD_r)[0])
            z_i = first_greater_z
            x_i = first_greater_x
            y_i = first_greater_y

            weights = np.zeros(8)
            weights[0] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i])
            weights[1] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i])
            weights[2] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i-1],ymesh_box_CAD_v[y_i])
            weights[3] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i-1],ymesh_box_CAD_v[y_i])
            weights[4] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i-1])
            weights[5] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i],ymesh_box_CAD_v[y_i-1])
            weights[6] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i],xmesh_box_CAD_v[x_i-1],ymesh_box_CAD_v[y_i-1])
            weights[7] = PPDistance3D(zz_CAD_r,xx_CAD_r,yy_CAD_r,zmesh_box_CAD_v[z_i-1],xmesh_box_CAD_v[x_i-1],ymesh_box_CAD_v[y_i-1])
            # Normalization of the weights:
            if 0 in weights: 
                print("There is 0 in weights something went wrong, how is not some flag true that should solve this...")
            weights = [1./weight for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...
            weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            # The weights are calculated as a distance from the diagonal corner therefore linearly decreasing, but it is not good approxiation...
            # weights[0],weights[1],weights[2],weights[3],weights[4],weights[5],weights[6],weights[7] = weights[7],weights[6],weights[5],weights[4],weights[3],weights[2],weights[1],weights[0]
            # weights = [weight/sum(weights) for weight in weights] # Wrong does not work properly, 1/distance is needed, but 1/0 whould have to be taken care off propperly...

            bx_1_CAD_ap = weights[0]*bx_1_vvv[x_i][y_i][z_i] + weights[1]*bx_1_vvv[x_i][y_i][z_i-1] + weights[2]*bx_1_vvv[x_i-1][y_i][z_i] + weights[3]*bx_1_vvv[x_i-1][y_i][z_i-1] + weights[4]*bx_1_vvv[x_i][y_i-1][z_i] + weights[5]*bx_1_vvv[x_i][y_i-1][z_i-1] + weights[6]*bx_1_vvv[x_i-1][y_i-1][z_i] + weights[7]*bx_1_vvv[x_i-1][y_i-1][z_i-1]
            by_1_CAD_ap = weights[0]*by_1_vvv[x_i][y_i][z_i] + weights[1]*by_1_vvv[x_i][y_i][z_i-1] + weights[2]*by_1_vvv[x_i-1][y_i][z_i] + weights[3]*by_1_vvv[x_i-1][y_i][z_i-1] + weights[4]*by_1_vvv[x_i][y_i-1][z_i] + weights[5]*by_1_vvv[x_i][y_i-1][z_i-1] + weights[6]*by_1_vvv[x_i-1][y_i-1][z_i] + weights[7]*by_1_vvv[x_i-1][y_i-1][z_i-1]
            bz_1_CAD_ap = weights[0]*bz_1_vvv[x_i][y_i][z_i] + weights[1]*bz_1_vvv[x_i][y_i][z_i-1] + weights[2]*bz_1_vvv[x_i-1][y_i][z_i] + weights[3]*bz_1_vvv[x_i-1][y_i][z_i-1] + weights[4]*bz_1_vvv[x_i][y_i-1][z_i] + weights[5]*bz_1_vvv[x_i][y_i-1][z_i-1] + weights[6]*bz_1_vvv[x_i-1][y_i-1][z_i] + weights[7]*bz_1_vvv[x_i-1][y_i-1][z_i-1]
        
    if bx_1_CAD_ap <= -100 or by_1_CAD_ap <= -100 or bz_1_CAD_ap <= -100:
        print("Carefull the bx or by or bz was not calcualted...")
        print("bx_1_CAD_ap = {}".format(bx_1_CAD_ap))
        print("by_1_CAD_ap = {}".format(by_1_CAD_ap))
        print("bz_1_CAD_ap = {}".format(bz_1_CAD_ap))
        print("pointAtZline_flag = {}".format(pointAtZline_flag))
        print("pointAtXline_flag = {}".format(pointAtXline_flag))
        print("pointAtYline_flag = {}".format(pointAtYline_flag))
    by_1_lab_final_r = by_1_CAD_ap
    y_1_lab_final_r = yy_CAD_r
    bz_1_lab_final_r, bx_1_lab_final_r, z_1_lab_final_r, x_1_lab_final_r = CADToLab_vector(bz_1_CAD_ap,bx_1_CAD_ap,zz_CAD_r,xx_CAD_r,lab0inCAD_z, lab0inCAD_x, angle_labToCAD)
    # print("by now : {} ".format(by_1_lab_final_r))
    # print("bx now : {} ".format(bx_1_lab_final_r))
    # print("bz now : {} ".format(bz_1_lab_final_r))
    # print("by orig: {} ".format(by_1_vvv[np.where(by_1_vvv != 0)]))

    return bz_1_lab_final_r, bx_1_lab_final_r, by_1_lab_final_r, z_1_lab_final_r, x_1_lab_final_r, y_1_lab_final_r


def LabToCAD_Mesh1Din_3Dout(zmesh_lab_v, xmesh_lab_v, ymesh_lab_v, lab0inCAD_z, lab0inCAD_x, angle_labToCAD, LoadFromFile_flag = False, LoadFromFile_name = "None", SaveToFile_name = "LabMeshInCAD.h5py", timing = True):
    z_len = len(zmesh_lab_v)
    x_len = len(xmesh_lab_v)
    y_len = len(ymesh_lab_v)
    print(x_len)
    print(y_len)
    print(z_len)
    mesh_lab_numOfPoints = z_len*x_len*y_len


    if LoadFromFile_flag and LoadFromFile_name != "None":
        file_backup = h5py.File(LoadFromFile_name,"r")
        zz_CAD = np.array(file_backup["zz_CAD"])
        yy_CAD = np.array(file_backup["yy_CAD"])
        xx_CAD = np.array(file_backup["xx_CAD"])
        file_backup.close()
    else:
        # The mesh labarped mesh in CAD coordinates:
        yy_CAD = np.zeros((x_len,y_len,z_len))
        xx_CAD = np.zeros((x_len,y_len,z_len))
        zz_CAD = np.zeros((x_len,y_len,z_len))
        print("\nWarpingToCAD starting\n")
        if timing:
            pbar = tqdm(total = (x_len)*(y_len)*(z_len)) # Just for timing
            # Transformation of the labarped mesh to CAD coordinates
        for y_i in range(y_len):
            for x_i in range(x_len):
                for z_i in range(z_len):
                    zz_CAD[x_i][y_i][z_i], xx_CAD[x_i][y_i][z_i] = LabToCAD_point(zmesh_lab_v[z_i],xmesh_lab_v[x_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD)
                    # print("{}, {} = WarpedToCAD({}, {}, {}, {}, {}, {}, {})".format(zz_CAD[x_i][y_i][z_i],xx_CAD[x_i][y_i][z_i],zmesh_lab_v[z_i],xmesh_lab_v[x_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius))
                # print("[{}][{}]/[{}][{}] x{} points converted".format(y_i,x_i,y_len,x_len,z_len))
                    if timing:
                        pbar.update(1)
                    # yy_lab[x_i][y_i][z_i] = ymesh_lab_v[y_i]
        yy_lab = [[[ymesh_lab_v[y_i] for z_i in range(z_len)] for y_i in range(y_len)] for x_i in range(x_len)]
        yy_CAD = np.array([[[ymesh_lab_v[y_i] for z_i in range(z_len)] for y_i in range(y_len)] for x_i in range(x_len)]) # Sae as yy_lab and yy_labarped

        print("\nLabToCAD completed\n")

        file_backup = h5py.File(SaveToFile_name,"w")
        zz_CAD_ds = file_backup.create_dataset("zz_CAD", data = zz_CAD)
        yy_CAD_ds = file_backup.create_dataset("yy_CAD", data = yy_CAD)
        xx_CAD_ds = file_backup.create_dataset("xx_CAD", data = xx_CAD)
        file_backup.close()
    return zz_CAD, yy_CAD, xx_CAD

def MagneticFieldTransformation_boxMesh_CADLab_3D(zmesh_box_CAD_v, xmesh_box_CAD_v, ymesh_box_CAD_v, bz_CAD_vvv, bx_CAD_vvv, by_CAD_vvv, zmesh_lab_v, xmesh_lab_v, ymesh_lab_v, lab0inCAD_z, lab0inCAD_x, angle_labToCAD, round_flag = True, distanceTreshold = 1e-8, LoadFromFile_flag = False, LoadFromFile_name = "None", SaveToFile_name = "WarpedMeshInCAD.h5py"):
    """
    The box mesh must be rectangular,
    zmesh_box_CAD_v = array of the z points of the box (the space coordiantes of bz/bx_1_vvv) in CADoratory coordinates 
    xmesh_box_CAD_v = array of the x points of the box (the space coordiantes of bz/bx_1_vvv) in CADoratory coordinates 
    xmesh_box_CAD_v = array of the y points of the box (the space coordiantes of bz/bx_1_vvv) in CADoratory coordinates 
    bz_CAD_vvv = 3D array of the Bz in CADoratory coordinates (must correspond and sit on the mesh_box_CAD_v)
    bx_CAD_vvv = 3D array of the Bx in CADoratory coordinates (must correspond and sit on the mesh_box_CAD_v)
    by_CAD_vvv = 3D array of the By in CADoratory coordinates (must correspond and sit on the mesh_box_CAD_v)
    zmesh_lab_v = array of the z points of lab mesh in lab coordinates 
    xmesh_lab_v = array of the x points of lab mesh in lab coordinates 
    ymesh_lab_v = array of the y points of lab mesh in lab coordinates 
    bend_par_v = parameters of the bend, must be in format:

    bend_start = bend_par_v[0] - lowest z of the bend
    bend_end = bend_par_v[1] - - highest z of the bend
    bend_radius = bend_par_v[2] - radius of the bend

    round_flag = flag if to use distanceTreshold or exact coordinates for points to be delared identical and no aproximation is then done, for more info viz Aproximation function
    distanceTreshold = the distance treshold to declare two points as identical



    The idea is following: Get lab mesh into CAD, aproximate the field in the CAD on the lab mesh in CAD, 
    and then transform this field back into lab coordinates, and the x y z will create a good grid which is esential.
    The aproximation of the field is done in the CAD coord. instead in the lab coord.
    because of more clean and safe aproximation from grided data into "non-grid" instead the opposite.

    This function returns takes the bz and bx as 3D but returns bz_final and bx_final as a 2D (z and x dimensions) - the y dimension is ommited as RZ and XYZ could affect this.

    """

    # Parameters

    # Few pre-calculations
    z_len = len(zmesh_lab_v)
    x_len = len(xmesh_lab_v)
    y_len = len(ymesh_lab_v)
    mesh_lab_numOfPoints = z_len*x_len*y_len

    zz_CAD, yy_CAD, xx_CAD = LabToCAD_Mesh1Din_3Dout(zmesh_lab_v, xmesh_lab_v, ymesh_lab_v, lab0inCAD_z, lab0inCAD_x, angle_labToCAD, LoadFromFile_flag = LoadFromFile_flag, LoadFromFile_name = LoadFromFile_name, SaveToFile_name = SaveToFile_name)
    print("yy_CAD")
    print(yy_CAD[0,::,0])
    print("xx_CAD")
    print(xx_CAD[::,0,0])
    print("zz_CAD")
    print(zz_CAD[0,0,::])
    xx_CAD_r = xx_CAD.ravel()
    yy_CAD_r = yy_CAD.ravel()
    zz_CAD_r = zz_CAD.ravel()
    # print(zz_CAD)
    
    #Declaring empty arrays for filling

        #Final bx and bz aproximated and back in lab coordinates
    by_1_lab_final_r = np.zeros(mesh_lab_numOfPoints)
    bx_1_lab_final_r = np.zeros(mesh_lab_numOfPoints)
    bz_1_lab_final_r = np.zeros(mesh_lab_numOfPoints)

        #Final x an z of the bx and bz back in lab coordinates nonzero only inside the box, --> can be used for the box identification in lab coordinates otherlabise not used
    y_1_lab_final_r = np.zeros(mesh_lab_numOfPoints)
    x_1_lab_final_r = np.zeros(mesh_lab_numOfPoints)
    z_1_lab_final_r = np.zeros(mesh_lab_numOfPoints)

    # print(zmesh_box_CAD_v)
    # Box cropping of the transformed lab mesh in lab - Finding the points that will be used for the aproximation (inside the bfield_box) 
    used_coord_0 = np.where(zz_CAD_r >= zmesh_box_CAD_v[0] - distanceTreshold)
    used_coord = used_coord_0
    used_coord_1 = np.where(zz_CAD_r[used_coord] <= zmesh_box_CAD_v[-1] + distanceTreshold)
    used_coord = (used_coord[0][used_coord_1],) 
    # print(used_coord)
    # print("used_coord: z  completed")
    used_coord_2 = np.where(xx_CAD_r[used_coord] <= xmesh_box_CAD_v[-1] + distanceTreshold)
    used_coord = (used_coord[0][used_coord_2],) 
    used_coord_3 = np.where(xx_CAD_r[used_coord] >= xmesh_box_CAD_v[0] - distanceTreshold)
    used_coord = (used_coord[0][used_coord_3],) 
    # print("used_coord: x  completed")
    used_coord_4 = np.where(yy_CAD_r[used_coord] <= ymesh_box_CAD_v[-1] + distanceTreshold)
    used_coord = (used_coord[0][used_coord_4],) 
    used_coord_5 = np.where(yy_CAD_r[used_coord] >= ymesh_box_CAD_v[0] - distanceTreshold)
    used_coord = (used_coord[0][used_coord_5],) 
    # print("used_coord: y  completed")
    print("used_coord: y ")
    print(yy_CAD_r[used_coord])
    print("used_coord: x ")
    print(xx_CAD_r[used_coord])
    print("used_coord: z ")
    print(zz_CAD_r[used_coord])

    # np.savetxt("tmp.txt",yy_CAD_r)

    print("the box: {} < z < {}".format(zmesh_box_CAD_v[-1],zmesh_box_CAD_v[0]))
    print("in CAD : {} < x < {}".format(xmesh_box_CAD_v[-1],xmesh_box_CAD_v[0]))
    print("coord. : {} < y < {}".format(ymesh_box_CAD_v[-1],ymesh_box_CAD_v[0]))
    print("used number of points: {}".format(len(used_coord[0])))

    # print("Original zz_CAD_r: {}".format(zz_lab_r))
    # print("Croped zz_lab_r: {}".format(zz_lab_r[used_coord]))
    # print("Original xx_lab_r: {}".format(xx_lab_r))
    # print("Croped xx_lab_r: {}".format(xx_lab_r[used_coord]))

    # Aproximating the B field from box mesh into the lab mesh points in lab coordinates and then transforming them back into lab coordinates 
    print("\nStarting the mapping of lab mesh in CAD and its transformation back to Warped coordinates \n")
    pbar = tqdm(total = len(used_coord[0]))
    # line_i = 0
    for i_labMesh in used_coord[0]:
        pbar.update(1)
        # if line_i % 5000 == 0:
            # print("{}/{} thousands points approximated".format(int(line_i/1000),int(len(used_coord[0])/1000)))
        # line_i += 1 
        bz_1_lab_final_r[i_labMesh], bx_1_lab_final_r[i_labMesh], by_1_lab_final_r[i_labMesh], \
        z_1_lab_final_r[i_labMesh], x_1_lab_final_r[i_labMesh], y_1_lab_final_r[i_labMesh] = \
        Aproximate_CADToLab_3D( zmesh_box_CAD_v = zmesh_box_CAD_v,\
                    xmesh_box_CAD_v = xmesh_box_CAD_v,\
                    ymesh_box_CAD_v = ymesh_box_CAD_v,\
                    bz_1_vvv = bz_CAD_vvv,\
                    bx_1_vvv = bx_CAD_vvv,\
                    by_1_vvv = by_CAD_vvv,\
                    zz_CAD_r = zz_CAD_r[i_labMesh],\
                    xx_CAD_r = xx_CAD_r[i_labMesh],\
                    yy_CAD_r = yy_CAD_r[i_labMesh],\
                    lab0inCAD_z = lab0inCAD_z ,\
                    lab0inCAD_x = lab0inCAD_x ,\
                    angle_labToCAD = angle_labToCAD ,\
                    distanceTreshold = distanceTreshold,\
                    round_flag = round_flag)
    print("\nCompleted mapping of lab mesh in CAD and its transformation back to Warped coordinates \n")




    y_1_lab_final = y_1_lab_final_r.reshape(x_len, y_len, z_len)
    x_1_lab_final = x_1_lab_final_r.reshape(x_len, y_len, z_len)
    z_1_lab_final = z_1_lab_final_r.reshape(x_len, y_len, z_len)

    print("yy_lab")
    print(y_1_lab_final[0,::,0])
    print("xx_lab")
    print(x_1_lab_final[::,0,0])

        # Using map function -> does not seem to work faster
    # AproximatedValues = map(Aproximate_CAD_3D,  zz_CAD_r[used_coord],\
    #                                             xx_CAD_r[used_coord],\
    #                                             yy_CAD_r[used_coord],\
    #                                             it.repeat(zmesh_box_CAD_v,len(used_coord[0])),\
    #                                             it.repeat(xmesh_box_CAD_v,len(used_coord[0])),\
    #                                             it.repeat(ymesh_box_CAD_v,len(used_coord[0])),\
    #                                             it.repeat(bz_CAD_vvv,len(used_coord[0])),\
    #                                             it.repeat(bx_CAD_vvv,len(used_coord[0])),\
    #                                             it.repeat(by_CAD_vvv,len(used_coord[0])),\
    #                                             it.repeat(lab0inCAD_z,len(used_coord[0])),\
    #                                             it.repeat(lab0inCAD_x,len(used_coord[0])),\
    #                                             it.repeat(angle_labToCAD,len(used_coord[0])),\
    #                                             it.repeat(bend_start,len(used_coord[0])),\
    #                                             it.repeat(bend_end,len(used_coord[0])),\
    #                                             it.repeat(bend_radius,len(used_coord[0])),\
    #                                             it.repeat(distanceTreshold,len(used_coord[0])),\
    #                                             it.repeat(round_flag,len(used_coord[0])))
    # pbar.close()
    # bz_1_lab_final_r[used_coord] = np.array([AproximatedValues[i][0] for i in range(len(used_coord[0]))])
    # bx_1_lab_final_r[used_coord] = np.array([AproximatedValues[i][1] for i in range(len(used_coord[0]))])
    # by_1_lab_final_r[used_coord] = np.array([AproximatedValues[i][2] for i in range(len(used_coord[0]))])
    # z_1_lab_final_r[used_coord] = np.array([AproximatedValues[i][3] for i in range(len(used_coord[0]))])
    # x_1_lab_final_r[used_coord] = np.array([AproximatedValues[i][4] for i in range(len(used_coord[0]))])
    # y_1_lab_final_r[used_coord] = np.array([AproximatedValues[i][5] for i in range(len(used_coord[0]))])

    by_1_lab_final = by_1_lab_final_r.reshape(x_len, y_len, z_len)
    bx_1_lab_final = bx_1_lab_final_r.reshape(x_len, y_len, z_len)
    bz_1_lab_final = bz_1_lab_final_r.reshape(x_len, y_len, z_len)



    # Just testting plot of lab mesh and used cooridnates (inside the box in lab coordinates)
    # zz_w = np.array([[zmes_w for zmes_w in zmesh_w_v] for xmesh_w in xmesh_w_v])
    # xx_w = np.array([[xmesh_w for zmes_w in zmesh_w_v] for xmesh_w in xmesh_w_v])

    # xx_w_r = xx_w.ravel()
    # zz_w_r = zz_w.ravel()

    # plt.scatter(zz_w_r,xx_w_r, s = 1, color = "b", label = "Warped mesh")
    # plt.scatter(z_1_lab_final_r, x_1_lab_final_r, s = 1, color = "r", label = "Warped mesh")

    return bz_1_lab_final, bx_1_lab_final, by_1_lab_final


# def MagneticFieldTransformation_boxPar_CADLab_3D(bfield_box_par_v, dz_box_CAD, dx_box_CAD, dy_box_CAD, bz_CAD_vvv, bx_CAD_vvv, by_CAD_vvv, zmesh_w_v, xmesh_w_v, ymesh_w_v, lab0inCAD_z, lab0inCAD_x, angle_labToCAD, bend_par_v, round_flag = True, distanceTreshold = 1e-10, LoadFromFile_flag = False, LoadFromFile_name = "None", SaveToFile_name = "WarpedMeshInCAD.h5py"):
    #     """
    #     The bfield box must be rectangular and the bfield_box_par_v in format: 

    #     bfield_box_xcent_CAD = bfield_box_par_v[0] - x center at lowest z
    #     bfield_box_ycent_CAD = bfield_box_par_v[1] - y center at lowest z
    #     bfield_box_zs_CAD = bfield_box_par_v[2] - lowest z
    #     bfield_box_ze_CAD = bfield_box_par_v[3] - higest z
    #     bfield_box_XhalfOfwidth_CAD = bfield_box_par_v[4] - half of the width of side in x dimension ( so maybe technically hight and not width but wathever)
    #     bfield_box_YhalfOfwidth_CAD = bfield_box_par_v[5] - half of the width of side in y dimension ( so maybe technically hight and not width but wathever)

    #     dz_box_CAD = the increment in z direction of the box mesh in CADoratory coordinates 
    #     dx_box_CAD = the increment in x direction of the box mesh in CADoratory coordinates 
    #     bz_CAD_vvv = 3D array of the Bz in CADoratory coordinates (must correspond and sit on the z/xmesh_box_CAD_v)
    #     bx_CAD_vvv = 3D array of the Bx in CADoratory coordinates (must correspond and sit on the z/xmesh_box_CAD_v)
    #     zmesh_w_v = array of the z points of lab mesh in lab coordinates 
    #     xmesh_w_v = array of the x points of lab mesh in lab coordinates 
    #     bend_par_v = parameters of the bend, must be in format:

    #     bend_start = bend_par_v[0] - lowest z of the bend
    #     bend_end = bend_par_v[1] - - highest z of the bend
    #     bend_radius = bend_par_v[2] - radius of the bend

    #     round_flag = flag if to use distanceTreshold or exact coordinates for points to be delared identical and no aproximation is then done, for more info viz Aproximation function
    #     distanceTreshold = the distance treshold to declare two points as identical

    #     The idea is following: Get lab mesh in lab coordinates, transform it into CAD coordinates, aproximate the field in the CAD coordinates onto the lab mesh in CAD coordinates, 
    #     and then transform this field back into lab coordinates, and the x y z will create a good grid which is esential for applying the field
    #     The aproximation of the field is done in the CAD coord. instead in the lab coord.
    #     because of more clean and safe aproximation from grided data into "non-grid" instead the opposite.

    #     This function returns takes the bz and bx as 3D but returns bz_final and bx_final as a 2D (z and x dimensions) - the y dimension is ommited as RZ and XYZ could affect this.
    #     """
    #     bfield_box_xcent_CAD = bfield_box_par_v[0]
    #     bfield_box_ycent_CAD = bfield_box_par_v[1]
    #     bfield_box_zs_CAD = bfield_box_par_v[2]
    #     bfield_box_ze_CAD = bfield_box_par_v[3]
    #     bfield_box_XhalfOfwidth_CAD = bfield_box_par_v[4]
    #     bfield_box_YhalfOfwidth_CAD = bfield_box_par_v[5]
    #     print("bfield_box_par_v: {}".format(bfield_box_par_v))

    #     # Defining the bfield mesh in the LAB
    #     nz_box = int((bfield_box_ze_CAD-bfield_box_zs_CAD)/dz_box_CAD + 1)
    #     nx_box = int(bfield_box_XhalfOfwidth_CAD*2/dx_box_CAD + 1)
    #     ny_box = int(bfield_box_YhalfOfwidth_CAD*2/dy_box_CAD + 1)

    #     zmesh_box_CAD_v = [bfield_box_zs_CAD + z_i*dz_box_CAD for z_i in range(nz_box)]
    #     xmesh_box_CAD_v = [bfield_box_xcent_CAD - bfield_box_XhalfOfwidth_CAD + x_i*dx_box_CAD for x_i in range(nx_box)]
    #     ymesh_box_CAD_v = [bfield_box_ycent_CAD - bfield_box_YhalfOfwidth_CAD + y_i*dy_box_CAD for y_i in range(ny_box)]
    #     print("1513")
    #     print(zmesh_box_CAD_v)
    #     print(ymesh_box_CAD_v)
    #     print(xmesh_box_CAD_v)
    #     bz_1_lab_final, bx_1_lab_final, by_1_lab_final = MagneticFieldTransformation_boxMesh_CADLab(zmesh_box_CAD_v = zmesh_box_CAD_v,\
    #                                                                                                         xmesh_box_CAD_v = xmesh_box_CAD_v,\
    #                                                                                                         ymesh_box_CAD_v = ymesh_box_CAD_v,\
    #                                                                                                         bz_CAD_vvv = bz_CAD_vvv,\
    #                                                                                                         bx_CAD_vvv = bx_CAD_vvv,\
    #                                                                                                         by_CAD_vvv = by_CAD_vvv,\
    #                                                                                                         zmesh_w_v = zmesh_w_v,\
    #                                                                                                         xmesh_w_v = xmesh_w_v,\
    #                                                                                                         ymesh_w_v = ymesh_w_v,\
    #                                                                                                         lab0inCAD_z = lab0inCAD_z ,\
    #                                                                                                         lab0inCAD_x = lab0inCAD_x ,\
    #                                                                                                         angle_labToCAD = angle_labToCAD ,\
    #                                                                                                         bend_par_v = bend_par_v,\
    #                                                                                                         round_flag = round_flag,\
    #                                                                                                         distanceTreshold = distanceTreshold,\
    #                                                                                                         LoadFromFile_flag = LoadFromFile_flag,\
    #                                                                                                         LoadFromFile_name = LoadFromFile_name,\
    #                                                                                                         SaveToFile_name = SaveToFile_name,\
    #                                                                                                         )
    #     return bz_1_lab_final, bx_1_lab_final, by_1_lab_final

