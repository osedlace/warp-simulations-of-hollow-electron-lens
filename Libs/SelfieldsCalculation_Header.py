import numpy as np
import h5py 
from tqdm import tqdm
import itertools
import matplotlib.pyplot as plt
from tqdm import tqdm
from ParticleAndMesh_ClassHeader import Particle, Mesh_point
from CoordinationTransformationsHeader import WarpedToLab_point, WarpedToLab_vector


def PPDistance3D(Ax,Ay,Az,Bx,By,Bz):
    return np.sqrt((Ax-Bx)**2 + (Ay-By)**2 + (Az-Bz)**2)


# Calculating the Efield from particles
def ESelfield_ji(axis, mesh_point_j, particle_i, parbar):
    if parbar is not None:
        parbar.update(1)
    if axis == "x":
        return (mesh_point_j.x - particle_i.x)/PPDistance3D(mesh_point_j.x,mesh_point_j.y,mesh_point_j.z,particle_i.x,particle_i.y,particle_i.z)**3
    if axis == "y":
        return (mesh_point_j.y - particle_i.y)/PPDistance3D(mesh_point_j.x,mesh_point_j.y,mesh_point_j.z,particle_i.x,particle_i.y,particle_i.z)**3
    if axis == "z":
        return (mesh_point_j.z - particle_i.z)/PPDistance3D(mesh_point_j.x,mesh_point_j.y,mesh_point_j.z,particle_i.x,particle_i.y,particle_i.z)**3

def ESelfield_j_a(axis, mesh_point_j, particle_a, parbar ):
    return np.array([ESelfield_ji(axis = axis, mesh_point_j = mesh_point_j, particle_i = particle, parbar = parbar) for particle in particle_a])

def Set_ESelfield_j(mesh_point_j,particle_a, parbar, particleChargeTk = 1):
    mesh_point_j.Ex = particleChargeTk*sum(ESelfield_j_a(axis = "x", mesh_point_j = mesh_point_j, particle_a = particle_a,parbar = parbar))
    mesh_point_j.Ey = particleChargeTk*sum(ESelfield_j_a(axis = "y", mesh_point_j = mesh_point_j, particle_a = particle_a,parbar = parbar))
    mesh_point_j.Ez = particleChargeTk*sum(ESelfield_j_a(axis = "z", mesh_point_j = mesh_point_j, particle_a = particle_a,parbar = parbar))
    # mesh_point_j.Ex = 0 
    # mesh_point_j.Ey = 0 
    # mesh_point_j.Ez = 0
    # for particle in particle_a:
    #     mesh_point_j.Ex += particleChargeTk*ESelfield_ji(axis = "x", mesh_point_j = mesh_point_j, particle_i = particle)  
    #     mesh_point_j.Ey += particleChargeTk*ESelfield_ji(axis = "y", mesh_point_j = mesh_point_j, particle_i = particle)
    #     mesh_point_j.Ez += particleChargeTk*ESelfield_ji(axis = "z", mesh_point_j = mesh_point_j, particle_i = particle)


def Set_ESelfield(mesh_a,particle_a,particleChargeTk,parbar = None):
    for mesh_point_j in mesh_a:
        Set_ESelfield_j(mesh_point_j = mesh_point_j, particle_a = particle_a, particleChargeTk = particleChargeTk, parbar = parbar)
        # pbar.update(1)


# Analytical Solution
def EAnalyticalSolution_mag(mesh_point_j, overallChargeTk,radius = 1):
    if (mesh_point_j.x**2 + mesh_point_j.y**2 + mesh_point_j.z**2) < 1:
        return overallChargeTk*np.sqrt(mesh_point_j.x**2 + mesh_point_j.y**2 + mesh_point_j.z**2)/radius**3
    else:
        return overallChargeTk/(mesh_point_j.x**2 + mesh_point_j.y**2 + mesh_point_j.z**2)


def Set_EAnalytical_j(mesh_point_j,overallChargeTk):
    mesh_point_j_xyzsum = np.sqrt(mesh_point_j.x**2 + mesh_point_j.y**2 + mesh_point_j.z**2)

    # print("x,y,z               = ({},{},{})".format(mesh_point_j.x,mesh_point_j.y,mesh_point_j.z))
    # print("mesh_point_j_xyzsum = {}".format(mesh_point_j_xyzsum))
    if mesh_point_j_xyzsum != 0:
        EAnalyticalSolution_magnitude = EAnalyticalSolution_mag(mesh_point_j = mesh_point_j, overallChargeTk = overallChargeTk)
        mesh_point_j.Ex = mesh_point_j.x/mesh_point_j_xyzsum*EAnalyticalSolution_magnitude
        mesh_point_j.Ey = mesh_point_j.y/mesh_point_j_xyzsum*EAnalyticalSolution_magnitude
        mesh_point_j.Ez = mesh_point_j.z/mesh_point_j_xyzsum*EAnalyticalSolution_magnitude
    else:
        mesh_point_j.Ex = 0
        mesh_point_j.Ey = 0
        mesh_point_j.Ez = 0

def Set_EAnalytical(mesh_a,overallChargeTk):
    for mesh_point_j in mesh_a:
        Set_EAnalytical_j(mesh_point_j = mesh_point_j, overallChargeTk = overallChargeTk)
        # pbar.update(1)


def SaveMeshPointsToH5pyFile(fileName,mesh_a):

    # Read in the data in file and append the new data
    with h5py.File(fileName,"r") as file_in:
        x = np.append(file_in["x"],np.array([mesh.x for mesh in mesh_a]))
        y = np.append(file_in["y"],np.array([mesh.y for mesh in mesh_a]))
        z = np.append(file_in["z"],np.array([mesh.z for mesh in mesh_a]))
        Ex = np.append(file_in["Ex"],np.array([mesh.Ex for mesh in mesh_a]))
        Ey = np.append(file_in["Ey"],np.array([mesh.Ey for mesh in mesh_a]))
        Ez = np.append(file_in["Ez"],np.array([mesh.Ez for mesh in mesh_a]))
        # Bx = np.append(file_in["Bx"],np.array([mesh.Bx for mesh in mesh_a]))
        # By = np.append(file_in["By"],np.array([mesh.By for mesh in mesh_a]))
        # Bz = np.append(file_in["Bz"],np.array([mesh.Bz for mesh in mesh_a]))
    # Rewrite the file with new appended data
    with h5py.File(fileName,"w") as file_out:
        dx = file_out.create_dataset("x", data = x )
        dy = file_out.create_dataset("y", data = y )
        dz = file_out.create_dataset("z", data = z )
        dEx = file_out.create_dataset("Ex", data = Ex)
        dEy = file_out.create_dataset("Ey", data = Ey)
        dEz = file_out.create_dataset("Ez", data = Ez)
        # dBx = file_out.create_dataset("Bx", data = Bx)
        # dBy = file_out.create_dataset("By", data = By)
        # dBz = file_out.create_dataset("Bz", data = Bz)

def SaveBMeshPointsToH5pyFile(fileName,mesh_a):

    # Read in the data in file and append the new data
    with h5py.File(fileName,"r") as file_in:
        x = np.append(file_in["x"],np.array([mesh.x for mesh in mesh_a]))
        y = np.append(file_in["y"],np.array([mesh.y for mesh in mesh_a]))
        z = np.append(file_in["z"],np.array([mesh.z for mesh in mesh_a]))
        # Ex = np.append(file_in["Ex"],np.array([mesh.Ex for mesh in mesh_a]))
        # Ey = np.append(file_in["Ey"],np.array([mesh.Ey for mesh in mesh_a]))
        # Ez = np.append(file_in["Ez"],np.array([mesh.Ez for mesh in mesh_a]))
        Bx = np.append(file_in["Bx"],np.array([mesh.Bx for mesh in mesh_a]))
        By = np.append(file_in["By"],np.array([mesh.By for mesh in mesh_a]))
        Bz = np.append(file_in["Bz"],np.array([mesh.Bz for mesh in mesh_a]))
    # Rewrite the file with new appended data
    with h5py.File(fileName,"w") as file_out:
        dx = file_out.create_dataset("x", data = x )
        dy = file_out.create_dataset("y", data = y )
        dz = file_out.create_dataset("z", data = z )
        # dEx = file_out.create_dataset("Ex", data = Ex)
        # dEy = file_out.create_dataset("Ey", data = Ey)
        # dEz = file_out.create_dataset("Ez", data = Ez)
        dBx = file_out.create_dataset("Bx", data = Bx)
        dBy = file_out.create_dataset("By", data = By)
        dBz = file_out.create_dataset("Bz", data = Bz)


def LoadMeshPointsFromH5pyFile(fileName):
    with h5py.File(fileName,"r") as file_in:
        x = file_in["x"]
        y = file_in["y"]
        z = file_in["z"]
        Ex = file_in["Ex"]
        Ey = file_in["Ey"]
        Ez = file_in["Ez"]
        mesh_a = np.array([Mesh_point() for i in np.arange(len(x))])
        for i, mesh_point in enumerate(mesh_a):
            mesh_point.x = x[i]
            mesh_point.y = y[i]
            mesh_point.z = z[i]
            mesh_point.Ex = Ex[i]
            mesh_point.Ey = Ey[i]
            mesh_point.Ez = Ez[i]
        return mesh_a
        
def SelfE_CalculateAndSave(mesh_point_a,fileToSaveTheMeshPoints, fileUsed):
    path_DiagnosticsLog = "./Logs/DiagnosticsLog_{}.h5py".format(fileUsed)
    with h5py.File(path_DiagnosticsLog,"r") as f_log:
        #Saving the bend parameters
        bend_par = np.array(f_log["bend_par"])
    bend_start = bend_par[0]
    bend_end = bend_par[1]
    bend_radius = bend_par[2]

    FileName_ParticleData = "./ParticleData/ParticleData_{}.h5py".format(fileUsed)
    reduceTheArray = 2
    with h5py.File(FileName_ParticleData,"r") as fileIn:
        x_warped = np.array(fileIn["x"])[::reduceTheArray]
        y_warped = np.array(fileIn["y"])[::reduceTheArray]
        z_warped = np.array(fileIn["z"])[::reduceTheArray]
    numOfParticles = len(x_warped)

    # Creating the particles
    particle_a = np.array([Particle() for i in np.arange(numOfParticles)])

    #Calculating the lab coordinates and setting the particle properites:
    for i, particle in enumerate(particle_a):
        particle.z, particle.x = WarpedToLab_point(z_warped[i],x_warped[i],bend_start,bend_end,bend_radius)
        particle.y = y_warped[i]

    # Constants
    # -------------------
    e = 1.602e-19 # C
    k = 0.8988e10 # N*m^2*C^{-2}
    qOfParticle = 2.0089e4*10.*reduceTheArray
    ek = e*k*qOfParticle

    # Distribution of particles
    parbar = tqdm(total = len(mesh_point_a)*len(particle_a)*3) # Just for timing
    Set_ESelfield(mesh_a = mesh_point_a, particle_a = particle_a,particleChargeTk = ek, parbar = parbar )
    SaveMeshPointsToH5pyFile(fileToSaveTheMeshPoints,mesh_point_a)

def SelfE_LoadAndPlot(yposition,zposition,fileToLoadTheMeshPoints):
    mesh_a_Loaded = LoadMeshPointsFromH5pyFile(fileToLoadTheMeshPoints)
    x = np.array([mesh_point.x*1.e3 for mesh_point in mesh_a_Loaded])
    y = np.array([mesh_point.y for mesh_point in mesh_a_Loaded])
    z = np.array([mesh_point.z for mesh_point in mesh_a_Loaded])
    Ex = np.array([mesh_point.Ex*1.e-3 for mesh_point in mesh_a_Loaded])
    Ey = np.array([mesh_point.Ey for mesh_point in mesh_a_Loaded])
    Ez = np.array([mesh_point.Ez for mesh_point in mesh_a_Loaded])


    for mesh_point_j_loaded in mesh_a_Loaded:
        # if mesh_point_j_loaded.x**2 + mesh_point_j_loaded.y**2 + mesh_point_j_loaded.z**2 <= 1:
        print("x,y,z = ({},{},{}), Calculated Ex,Ey,Ez,|E| = ({},{},{},{}) ".format(mesh_point_j_loaded.x,mesh_point_j_loaded.y,mesh_point_j_loaded.z,mesh_point_j_loaded.Ex,mesh_point_j_loaded.Ey,mesh_point_j_loaded.Ez,np.sqrt(mesh_point_j_loaded.Ex**2 + mesh_point_j_loaded.Ey**2 + mesh_point_j_loaded.Ez**2)))

    # Filter in y
    xPlot_Filter_e0 = np.where(Ex != 0)
    xPlot_Filter = xPlot_Filter_e0

    xPlot_Filter_0 = np.where(y[xPlot_Filter] >= yposition-0.0001)
    xPlot_Filter = (xPlot_Filter[0][xPlot_Filter_0],)
    xPlot_Filter_1 = np.where(y[xPlot_Filter] <= yposition + 0.0001)
    xPlot_Filter = (xPlot_Filter[0][xPlot_Filter_1],) 

    # Filter in z
    xPlot_Filter_2 = np.where(z[xPlot_Filter] <= zposition+0.0001)
    xPlot_Filter = (xPlot_Filter[0][xPlot_Filter_2],) 
    xPlot_Filter_3 = np.where(z[xPlot_Filter] >= zposition-0.0001)
    xPlot_Filter = (xPlot_Filter[0][xPlot_Filter_3],) 
    print("positions of the filtered mesh points = {}".format(xPlot_Filter))
    

    plt.figure("Calculated Ey in x, z = {}, y = {}".format(z[xPlot_Filter][0],y[xPlot_Filter][0]), figsize=(8,6))
    plt.scatter(x[xPlot_Filter],Ex[xPlot_Filter])
    plt.xlabel("y [mm]")
    plt.ylabel("Ey[V/mm]")
    plt.xlim(-9,9)
    # plt.show()



# Calculating the Efield from particles
def BSelfield_ji(axis, mesh_point_j, particle_i, parbar):
    if parbar is not None:
        parbar.update(1)
    if axis == "x":
        return (particle_i.vy*(mesh_point_j.z - particle_i.z) - particle_i.vz*(mesh_point_j.y - particle_i.y))/PPDistance3D(mesh_point_j.x,mesh_point_j.y,mesh_point_j.z,particle_i.x,particle_i.y,particle_i.z)**3
    if axis == "y":
        return (particle_i.vz*(mesh_point_j.x - particle_i.x) - particle_i.vx*(mesh_point_j.z - particle_i.z))/PPDistance3D(mesh_point_j.x,mesh_point_j.y,mesh_point_j.z,particle_i.x,particle_i.y,particle_i.z)**3
    if axis == "z":
        return (particle_i.vx*(mesh_point_j.y - particle_i.y) - particle_i.vy*(mesh_point_j.x - particle_i.x))/PPDistance3D(mesh_point_j.x,mesh_point_j.y,mesh_point_j.z,particle_i.x,particle_i.y,particle_i.z)**3

def BSelfield_j_a(axis, mesh_point_j, particle_a, parbar ):
    return np.array([BSelfield_ji(axis = axis, mesh_point_j = mesh_point_j, particle_i = particle, parbar = parbar) for particle in particle_a])

def Set_BSelfield_j(mesh_point_j,particle_a, parbar, particleChargeTkDcSq = 1):
    mesh_point_j.Bx = particleChargeTkDcSq*sum(BSelfield_j_a(axis = "x", mesh_point_j = mesh_point_j, particle_a = particle_a,parbar = parbar))
    mesh_point_j.By = particleChargeTkDcSq*sum(BSelfield_j_a(axis = "y", mesh_point_j = mesh_point_j, particle_a = particle_a,parbar = parbar))
    mesh_point_j.Bz = particleChargeTkDcSq*sum(BSelfield_j_a(axis = "z", mesh_point_j = mesh_point_j, particle_a = particle_a,parbar = parbar))

def Set_BSelfield(mesh_a,particle_a,particleChargeTkDcSq,parbar = None):
    for mesh_point_j in mesh_a:
        Set_BSelfield_j(mesh_point_j = mesh_point_j, particle_a = particle_a, particleChargeTkDcSq = particleChargeTkDcSq, parbar = parbar)
        # pbar.update(1)


def SelfB_CalculateAndSave(mesh_point_a,fileToSaveTheMeshPoints, fileUsed):
    path_DiagnosticsLog = "./Logs/DiagnosticsLog_{}.h5py".format(fileUsed)
    with h5py.File(path_DiagnosticsLog,"r") as f_log:
        #Saving the bend parameters
        bend_par = np.array(f_log["bend_par"])
    bend_start = bend_par[0]
    bend_end = bend_par[1]
    bend_radius = bend_par[2]

    FileName_ParticleData = "./ParticleData/ParticleData_{}.h5py".format(fileUsed)
    reduceTheArray = 2
    with h5py.File(FileName_ParticleData,"r") as fileIn:
        x_warped = np.array(fileIn["x"])[::reduceTheArray]
        y_warped = np.array(fileIn["y"])[::reduceTheArray]
        z_warped = np.array(fileIn["z"])[::reduceTheArray]
        vx_warped = np.array(fileIn["vx"])[::reduceTheArray]
        vy_warped = np.array(fileIn["vy"])[::reduceTheArray]
        vz_warped = np.array(fileIn["vz"])[::reduceTheArray]
    numOfParticles = len(x_warped)

    # Creating the particles
    particle_a = np.array([Particle() for i in np.arange(numOfParticles)])

    #Calculating the lab coordinates and setting the particle properites:
    for i, particle in enumerate(particle_a):
        particle.vz, particle.vx, particle.z, particle.x = WarpedToLab_vector(vz_warped[i],vx_warped[i],z_warped[i],x_warped[i],bend_start,bend_end,bend_radius)
        particle.vy = vy_warped[i]
        particle.y = y_warped[i]

    # Constants
    # -------------------
    e = 1.602e-19 # C
    k = 0.8988e10 # N*m^2*C^{-2}
    c = 3.e8
    qOfParticle = 2.0089e4*10.*reduceTheArray
    ekc = e*k*qOfParticle/c**2

    # Distribution of particles
    parbar = tqdm(total = len(mesh_point_a)*len(particle_a)*3) # Just for timing
    Set_BSelfield(mesh_a = mesh_point_a, particle_a = particle_a,particleChargeTkDcSq = ekc, parbar = parbar )
    SaveBMeshPointsToH5pyFile(fileToSaveTheMeshPoints,mesh_point_a)


def SelfB_LoadAndPlot(yposition,zposition,fileToLoadTheMeshPoints):
    mesh_a_Loaded = LoadBMeshPointsFromH5pyFile(fileToLoadTheMeshPoints)
    x = np.array([mesh_point.x*1.e3 for mesh_point in mesh_a_Loaded])
    y = np.array([mesh_point.y for mesh_point in mesh_a_Loaded])
    z = np.array([mesh_point.z for mesh_point in mesh_a_Loaded])
    Bx = np.array([mesh_point.Bx for mesh_point in mesh_a_Loaded])
    By = np.array([mesh_point.By for mesh_point in mesh_a_Loaded])
    Bz = np.array([mesh_point.Bz for mesh_point in mesh_a_Loaded])


    for mesh_point_j_loaded in mesh_a_Loaded:
        # if mesh_point_j_loaded.x**2 + mesh_point_j_loaded.y**2 + mesh_point_j_loaded.z**2 <= 1:
        print("x,y,z = ({},{},{}), Calculated Bx,By,Bz,|B| = ({},{},{},{}) ".format(mesh_point_j_loaded.x,mesh_point_j_loaded.y,mesh_point_j_loaded.z,mesh_point_j_loaded.Bx,mesh_point_j_loaded.By,mesh_point_j_loaded.Bz,np.sqrt(mesh_point_j_loaded.Bx**2 + mesh_point_j_loaded.By**2 + mesh_point_j_loaded.Bz**2)))

    # Filter in y
    xPlot_Filter_0 = np.where(y >= yposition - 0.0001)
    xPlot_Filter = xPlot_Filter_0
    xPlot_Filter_1 = np.where(y[xPlot_Filter] <= yposition + 0.0001)
    xPlot_Filter = (xPlot_Filter[0][xPlot_Filter_1],) 

    # Filter in z
    xPlot_Filter_2 = np.where(z[xPlot_Filter] <= zposition+0.0001)
    xPlot_Filter = (xPlot_Filter[0][xPlot_Filter_2],) 
    xPlot_Filter_3 = np.where(z[xPlot_Filter] >= zposition-0.0001)
    xPlot_Filter = (xPlot_Filter[0][xPlot_Filter_3],) 
    print("positions of the filtered mesh points = {}".format(xPlot_Filter))
    

    plt.figure("Calculated Bx in x, z = {}, y = {}".format(z[xPlot_Filter][0],y[xPlot_Filter][0]), figsize=(8,6))
    plt.scatter(x[xPlot_Filter],By[xPlot_Filter])
    plt.xlabel("y [mm]")
    plt.ylabel("Bx[T]")
    plt.ylim(-0.0002,0.0002)
    plt.xlim(-9,9)
    plt.show()

def LoadBMeshPointsFromH5pyFile(fileName):
    with h5py.File(fileName,"r") as file_in:
        x = file_in["x"]
        y = file_in["y"]
        z = file_in["z"]
        Bx = file_in["Bx"]
        By = file_in["By"]
        Bz = file_in["Bz"]
        mesh_a = np.array([Mesh_point() for i in np.arange(len(x))])
        for i, mesh_point in enumerate(mesh_a):
            mesh_point.x = x[i]
            mesh_point.y = y[i]
            mesh_point.z = z[i]
            mesh_point.Bx = Bx[i]
            mesh_point.By = By[i]
            mesh_point.Bz = Bz[i]
        return mesh_a
        