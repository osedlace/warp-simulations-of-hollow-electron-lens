"""
The transformation of coordination functions
"""
import numpy as np
import math
import matplotlib.pyplot as plt
import h5py 
import itertools as it 
from tqdm import tqdm


def PPDistance(Ax,Ay,Bx,By):
    return np.sqrt((Ax-Bx)**2 + (Ay-By)**2)

def PPDistance3D(Ax,Ay,Az,Bx,By,Bz):
    return np.sqrt((Ax-Bx)**2 + (Ay-By)**2 + (Az-Bz)**2)

def LPDistance(A,B,C,x0,y0): 
    return abs(A*x0 + B*y0 + C)/np.sqrt(A**2 + B**2)

def LabToWarped_point(z_lab,x_lab,bend_s,bend_e,bend_r):
    """
    These functions work if the coordinates are in this matter:
    identical - polar - rotated linear.
    straight - bend - straight following the end of the bend
    Additionally the bend must be to the left (in lab view)
    For the bend to the right some changes are necessary ( pi*3/2 change, some signs, ...) 
    """
    z_w = -100
    x_w = -100

    bend_l = bend_e - bend_s # bend lenght

    # points at the bend-end line
    d_z = bend_s + bend_r*np.cos(bend_l/bend_r + np.pi*3./2.)  # The central point at the end of the bend
    d_x = bend_r*np.sin(bend_l/bend_r + np.pi*3./2.) + bend_r # The central point at the end of the bend
    point1_z = bend_s 
    point1_x = bend_r
    point2_x = d_x
    point2_z = d_z

    #   Line equation of the end of the bend
    A = point2_x - point1_x
    B = -point2_z + point1_z
    C = -point1_z*point2_x + point2_z*point1_x
    
    # Region without bend
    #--------------------
    if z_lab <= bend_s: 
        z_w = z_lab
        x_w = x_lab
    # Region of bend
    #--------------------
    elif A*z_lab + B*x_lab + C >= 0 and z_lab > bend_s : 
        r = PPDistance(bend_s,bend_r,z_lab,x_lab) # the "real radius" (distance between the center of the rotation of the bend and the point)
        z_w = bend_s + np.arctan((z_lab - bend_s)/(bend_r-x_lab))*bend_r # The angle, just given the dimension of distance = circumference of part-circle given by the angle
        x_w = bend_r - r # The x is the diference between the real radius and the bend radius
    # Region after the bend 
    #--------------------
    elif A*z_lab + B*x_lab + C < 0: 
        # d = LPDistance(A,B,C,z_lab,x_lab)
        c_z = (B*(B*z_lab-A*x_lab)-A*C)/(A**2 + B**2)   # Closes point at the bend-end line
        c_x = (A*(-B*z_lab+A*x_lab)-B*C)/(A**2 + B**2)   # Closes point at the bend-end line
        z_w = bend_e + PPDistance(c_z,c_x,z_lab,x_lab) # Distance from the line is the added z component after the bend
        if PPDistance(bend_s,bend_r,d_z,d_x) <= PPDistance(c_z,c_x,bend_s,bend_r): # Need to distinguish sign of the x component as the distance is without sign 
            x_w = -PPDistance(c_z,c_x,d_z,d_x) # The distance from the closest point at the bend-end line to the center of the bend is the x component after the bend
        else:
            x_w = PPDistance(c_z,c_x,d_z,d_x)
    else:
        print("Carefull z_w and x_w was not calculated!!")
    return z_w, x_w

def WarpedToLab_point(z_w,x_w,bend_s,bend_e,bend_r):
    z_lab = -100
    x_lab = -100
    # Region without bend
    #--------------------
    if z_w <= bend_s: 
        z_lab = z_w
        x_lab = x_w
    # Region of bend
    #--------------------
    elif bend_e >= z_w > bend_s: 
        z_lab = bend_s + (bend_r - x_w)*np.cos((z_w - bend_s)/bend_r + np.pi*3./2.) # linear plus polar coordinates, the pi*3/2 is there to shift the angle into 4. quadrant  
        x_lab = x_w + (bend_r - x_w)*np.sin((z_w - bend_s)/bend_r + np.pi*3./2.) + (bend_r - x_w)
    # Region after bend
    #--------------------
    elif bend_e < z_w: 
        z_lab = bend_s + (bend_r - x_w)*np.cos((bend_e - bend_s)/bend_r + np.pi*3./2.) + (z_w-bend_e)*np.cos((bend_e - bend_s)/bend_r) # linear plus polar plus rotated linear coordinates, the pi*3/2 at the end disapears as the angle is in the 1. quadrant 
        x_lab = x_w + (bend_r - x_w)*np.sin((bend_e - bend_s)/bend_r + np.pi*3./2.) + (bend_r - x_w) + (z_w-bend_e)*np.sin((bend_e - bend_s)/bend_r)
    else:
        print("Carefull z_w and x_w was not calculated!!")

    # print("(z_w,x_w)     = ({},{})".format(z_w,x_w))
    # print("(z_lab,x_lab) = ({},{})".format(z_lab,x_lab))
    # print("np.cos((z_w - bend_s)/bend_r + np.pi*3./2.) = ({})".format(np.cos((z_w - bend_s)/bend_r + np.pi*3./2.)))
    return z_lab, x_lab

def WarpedToCAD_point(z_w,x_w,lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_s,bend_e,bend_r):
    z_lab = -100
    x_lab = -100
    #From Warped to LAB
    z_lab, x_lab = WarpedToLab_point(z_w,x_w,bend_s,bend_e,bend_r)
    #From LAB to CAD
    z_CAD = z_lab*np.cos(angle_labToCAD) + x_lab*np.sin(angle_labToCAD) + lab0inCAD_z
    x_CAD = -z_lab*np.sin(angle_labToCAD) + x_lab*np.cos(angle_labToCAD) + lab0inCAD_x
    # z_CAD = (z_lab + lab0inCAD_z)*np.cos(angle_labToCAD) + (x_lab + lab0inCAD_x)*np.sin(angle_labToCAD) 
    # x_CAD = -(z_lab + lab0inCAD_z)*np.sin(angle_labToCAD) + (x_lab + lab0inCAD_x)*np.cos(angle_labToCAD)
    return z_CAD, x_CAD

def LabToCAD_point(z_lab,x_lab,lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_s,bend_e,bend_r):
    # z_lab = -100
    # x_lab = -100
    #From LAB to CAD
    # z_CAD = z_lab*np.cos(angle_labToCAD) + x_lab*np.sin(angle_labToCAD) + lab0inCAD_z
    # x_CAD = -z_lab*np.sin(angle_labToCAD) + x_lab*np.cos(angle_labToCAD) + lab0inCAD_x
    z_CAD = z_lab*np.cos(angle_labToCAD) + x_lab*np.sin(angle_labToCAD) + lab0inCAD_z
    x_CAD = -z_lab*np.sin(angle_labToCAD) + x_lab*np.cos(angle_labToCAD) + lab0inCAD_x
    return z_CAD, x_CAD

def CADToWarped_point(z_CAD,x_CAD,lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_s,bend_e,bend_r):
    z_lab = -100
    x_lab = -100
    #From CAD to LAB
    z_lab = (z_CAD - lab0inCAD_z)*np.cos(angle_labToCAD) - (x_CAD- lab0inCAD_x)*np.sin(angle_labToCAD) 
    x_lab = (z_CAD - lab0inCAD_z)*np.sin(angle_labToCAD) + (x_CAD- lab0inCAD_x)*np.cos(angle_labToCAD)
    # print("z_CAD,x_CAD -> z_lab, x_lab = {},{} -> {}, {}".format(z_CAD,x_CAD,z_lab, x_lab))
    # z_lab = z_CAD*np.cos(angle_labToCAD) - x_CAD*np.sin(angle_labToCAD) - lab0inCAD_z
    # x_lab = z_CAD*np.sin(angle_labToCAD) + x_CAD*np.cos(angle_labToCAD) - lab0inCAD_x
    #From LAB to Warped
    z_w, x_w = LabToWarped_point(z_lab,x_lab,bend_s,bend_e,bend_r)
    return z_w, x_w

def CADToLAB_point(z_CAD,x_CAD,lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_s,bend_e,bend_r):
    z_lab = -100
    x_lab = -100
    #From CAD to LAB
    z_lab = (z_CAD - lab0inCAD_z)*np.cos(angle_labToCAD) - (x_CAD - lab0inCAD_x)*np.sin(angle_labToCAD) 
    x_lab = (z_CAD - lab0inCAD_z)*np.sin(angle_labToCAD) + (x_CAD - lab0inCAD_x)*np.cos(angle_labToCAD)
    # print("z_CAD,x_CAD -> z_lab, x_lab = {},{} -> {}, {}".format(z_CAD,x_CAD,z_lab, x_lab))
    # z_lab = z_CAD*np.cos(angle_labToCAD) - x_CAD*np.sin(angle_labToCAD) - lab0inCAD_z
    # x_lab = z_CAD*np.sin(angle_labToCAD) + x_CAD*np.cos(angle_labToCAD) - lab0inCAD_x
    #From LAB to Warped
    return z_lab, x_lab

def LabToWarped_vector(m_z_lab,m_x_lab,z_lab,x_lab,bend_s,bend_e,bend_r):
    m_z_w = -100
    m_x_w = -100
    z_w = -100
    x_w = -100


    z_w, x_w = LabToWarped_point(z_lab,x_lab,bend_s,bend_e,bend_r)
    # print("z_lab = {},x_lab = {}".format(z_lab,x_lab))
    # print("z_w = {},x_w = {}".format(z_w,x_w))
    bend_l = bend_e - bend_s # bend lenght
    # points at the bend-end line
    d_z = bend_s + bend_r*np.cos(bend_l/bend_r + np.pi*3./2.)  # The central point at the end of the bend
    d_x = bend_r*np.sin(bend_l/bend_r + np.pi*3./2.) + bend_r # The central point at the end of the bend
    point1_z = bend_s 
    point1_x = bend_r
    point2_x = d_x
    point2_z = d_z

    #   Line equation of the end of the bend
    A = point2_x - point1_x
    B = -point2_z + point1_z
    C = -point1_z*point2_x + point2_z*point1_x
    
    # Region without bend
    #--------------------
    if z_lab <= bend_s: 
        m_z_w = m_z_lab
        m_x_w = m_x_lab
    # Region of bend
    #--------------------
    elif A*z_lab + B*x_lab + C >= 0 and z_lab > bend_s : 
        rotation_angle = (z_w - bend_s)/bend_r
        m_z_w = m_z_lab*np.cos(rotation_angle) + m_x_lab*np.sin(rotation_angle)
        m_x_w = -m_z_lab*np.sin(rotation_angle) + m_x_lab*np.cos(rotation_angle)
    # Region after the bend 
    #--------------------
    elif A*z_lab + B*x_lab + C < 0: 
        rotation_angle = (bend_e - bend_s)/bend_r
        m_z_w = m_z_lab*np.cos(rotation_angle) + m_x_lab*np.sin(rotation_angle)
        m_x_w = -m_z_lab*np.sin(rotation_angle) + m_x_lab*np.cos(rotation_angle)
    else:
        print("Carefull m_z_w and m_x_w was not calculated!!")
    return m_z_w, m_x_w, z_w, x_w

def CADToWarped_vector(m_z_CAD,m_x_CAD,z_CAD,x_CAD,lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_s,bend_e,bend_r):
    m_z_lab = -100
    m_x_lab = -100

    # CAD to LAB
    z_lab, x_lab = CADToLAB_point(z_CAD,x_CAD,lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_s,bend_e,bend_r)

    m_z_lab = m_z_CAD*np.cos(angle_labToCAD) - m_x_CAD*np.sin(angle_labToCAD)
    m_x_lab = m_z_CAD*np.sin(angle_labToCAD) + m_x_CAD*np.cos(angle_labToCAD)

    # LAB to Warped
    m_z_w, m_x_w, z_w, x_w = LabToWarped_vector(m_z_lab,m_x_lab,z_lab,x_lab,bend_s,bend_e,bend_r)
    return m_z_w, m_x_w, z_w, x_w

def CADToLab_vector(m_z_CAD,m_x_CAD,z_CAD,x_CAD,lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_s,bend_e,bend_r):
    m_z_lab = -100
    m_x_lab = -100

    # CAD to LAB
    z_lab, x_lab = CADToLAB_point(z_CAD,x_CAD,lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_s,bend_e,bend_r)

    m_z_lab = m_z_CAD*np.cos(angle_labToCAD) - m_x_CAD*np.sin(angle_labToCAD)
    m_x_lab = m_z_CAD*np.sin(angle_labToCAD) + m_x_CAD*np.cos(angle_labToCAD)

    # LAB to Warped
    return m_z_lab, m_x_lab, z_lab, x_lab

def WarpedToLab_vector(m_z_w,m_x_w,z_w,x_w,bend_s,bend_e,bend_r):
    # Somehow the angle after bend is not completly correct, there is small but visible deviation..., However this is used only in 1 visualization, no computation
    m_z_lab = -100
    m_x_lab = -100
    z_lab = -100
    x_lab = -100


    z_lab, x_lab = WarpedToLab_point(z_w,x_w,bend_s,bend_e,bend_r)
    # print("z_lab = {},x_lab = {}".format(z_lab,x_lab))
    # print("z_w = {},x_w = {}".format(z_w,x_w))
    bend_l = bend_e - bend_s # bend lenght
    # points at the bend-end line
    d_z = bend_s + bend_r*np.cos(bend_l/bend_r + np.pi*3./2.)  # The central point at the end of the bend
    d_x = bend_r*np.sin(bend_l/bend_r + np.pi*3./2.) + bend_r # The central point at the end of the bend
    point1_z = bend_s 
    point1_x = bend_r
    point2_x = d_x
    point2_z = d_z

    #   Line equation of the end of the bend
    A = point2_x - point1_x
    B = -point2_z + point1_z
    C = -point1_z*point2_x + point2_z*point1_x
    
    # Region without bend
    #--------------------
    if z_w <= bend_s: 
        m_z_lab = m_z_w
        m_x_lab = m_x_w
    # Region of bend
    #--------------------
    elif bend_s < z_w <= bend_e: 
        rotation_angle = (z_w - bend_s)/bend_r
        m_z_lab = m_z_w*np.cos(rotation_angle) - m_x_w*np.sin(rotation_angle)
        m_x_lab = m_z_w*np.sin(rotation_angle) + m_x_w*np.cos(rotation_angle)
    # Region after the bend 
    #--------------------
    elif bend_e < z_w: 
        rotation_angle = (bend_e - bend_s)/bend_r 
        m_z_lab = m_z_w*np.cos(rotation_angle) - m_x_w*np.sin(rotation_angle)
        m_x_lab = m_z_w*np.sin(rotation_angle) + m_x_w*np.cos(rotation_angle)
    else:
        print("Carefull m_z_w and m_x_w was not calculated!!")
    return m_z_lab, m_x_lab, z_lab, x_lab
