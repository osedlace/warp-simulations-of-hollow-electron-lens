
import numpy as np
import matplotlib.pyplot as plt
import h5py 
from CoordinationTransformationsHeader import *

def Size2d(x,y):
    return np.sqrt(x**2 + y**2)

def Size3d(x,y,z):
    return np.sqrt(x**2 + y**2 + z**2)

def CalculateDistance_a(x_a,y_a):
    r_a = np.zeros(len(x_a))
    for i in range(len(r_a)):
        r_a[i] = math.sqrt(x_a[i]**2 + y_a[i]**2)
    return r_a

def CalculateDistance2D(x,y):
    return math.sqrt(x**2 + y**2)


def CalculateEkinz_a(vz_a,m):
    ekin_a = np.zeros(len(vz_a))
    c = 3.*10**8
    for i in range(len(ekin_a)):
        ekin_a[i] = 1./2.*m*(vz_a[i]/c)**2
        if ekin_a[i] == 0:
            print("Possible rounding: {}/{} == {}".format(vz_a[i],c,vz_a[i]/c))
    return ekin_a

def CalculateAngularVelocity(x,y,vx,vy):
    r = CalculateDistance2D(x,y)
    vtheta = r*(vy*x - vx*y)/r**2
    return vtheta

def CalculateRadialVelocity(x,y,vx,vy):
    r = CalculateDistance2D(x,y)
    vr = (x*vx + y*vy)/r
    return vr

def CalculateAngularVelocity_a(x_a,y_a,vx_a,vy_a):
    vthetaMet_a = np.zeros(len(x_a))
    r_a = CalculateDistance_a(x_a,y_a)
    for i in range(len(x_a)):
        vthetaRad_a[i] = (vy_a[i]*x_a[i] - vx_a[i]*y_a[i])/r_a[i]**2
        vthetaMet_a[i] = r_a[i]*vthetaRad_a[i]
    return vthetaRad_a, vthetaMet_a

def CalculateRadialVelocity_a(x_a,y_a,vx_a,vy_a):
    vr_a = np.zeros(len(x_a))
    r_a = CalculateDistance_a(x_a,y_a)
    # vr_a = np.array([(x_a[i]*vx_a[i] + y_a[i]*vy_a[i])/r_a[i] for i in np.arange(len(x_a))])
    for i in np.arange(len(x_a)):
        vr_a[i] = (x_a[i]*vx_a[i] + y_a[i]*vy_a[i])/r_a[i]
    return vr_a


def PlotStd(data_x, data_y, title, xlabel, ylabel, xmin = None, xmax = None, ymin = None, ymax = None, figure = None, label = None, colors = None):
    if figure != None:
        plt.figure(figure)
    else:
        plt.figure()
        
    plt.scatter(x = data_x, y = data_y, s = 0.5, label = label, c = colors,  cmap=plt.cm.plasma,)
    if colors is not None: plt.colorbar().set_label("vz [m/s]")
    plt.xlim(xmin,xmax)
    plt.ylim(ymin,ymax)
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

def plotMesh(plotWarped_flag, plotLab_flag ,bend_par, depositoryName, fileId):
    bend_start = bend_par[0]
    bend_end = bend_par[1]
    bend_radius = bend_par[2]
    x_w = np.loadtxt("./Logs/meshX_{}_{}.log".format(depositoryName, fileId))
    z_w = np.loadtxt("./Logs/meshZ_{}_{}.log".format(depositoryName, fileId))

    x_len = len(x_w)
    z_len = len(z_w)

    # warped mesh
    xx_w = np.zeros((x_len,z_len))
    zz_w = np.zeros((x_len,z_len))

    for x_i in range(x_len):
        for z_i in range(z_len):
            zz_w[x_i][z_i] = z_w[z_i]
            xx_w[x_i][z_i] = x_w[x_i]


    xx_w_r = xx_w.ravel()
    zz_w_r = zz_w.ravel()

    # MESH PLOT - warped
    # ---------------------------------
    if plotWarped_flag:
        plt.figure("Warped")
        plt.scatter(zz_w_r,xx_w_r, s = 2, label = "Warped mesh")
    # ---------------------------------


    # LAB mesh
    xx_lab = np.zeros((x_len,z_len))
    zz_lab = np.zeros((x_len,z_len))

    for x_i in range(x_len):
        for z_i in range(z_len):
            zz_lab[x_i][z_i], xx_lab[x_i][z_i] = WarpedToLab_point(z_w[z_i],x_w[x_i],bend_start,bend_end,bend_radius)

    xx_lab_r = xx_lab.ravel()
    zz_lab_r = zz_lab.ravel()

    # MESH PLOT
    # ---------------------------------
    if plotLab_flag:
        plt.figure("Lab")
        plt.scatter(zz_lab_r,xx_lab_r, s = 2, label = "Lab mesh")
    # ---------------------------------

def plotMeshNew(plotWarped_flag, plotLab_flag , depositoryName, fileId, path_DiagnosticsLog = None):
    if path_DiagnosticsLog == None: path_DiagnosticsLog = "./Logs/DiagnosticsLog_{}_{}.h5py".format(depositoryName, fileId)
    with h5py.File(path_DiagnosticsLog,"r") as f_log:
        x_w = np.array(f_log["xmesh_w"])
        z_w = np.array(f_log["zmesh_w"])
        bend_par = np.array(f_log["bend_par"])
    
    bend_start = bend_par[0]
    bend_end = bend_par[1]
    bend_radius = bend_par[2]

    x_len = len(x_w)
    z_len = len(z_w)

    # warped mesh
    xx_w = np.zeros((x_len,z_len))
    zz_w = np.zeros((x_len,z_len))

    for x_i in range(x_len):
        for z_i in range(z_len):
            zz_w[x_i][z_i] = z_w[z_i]
            xx_w[x_i][z_i] = x_w[x_i]


    xx_w_r = xx_w.ravel()
    zz_w_r = zz_w.ravel()

    # MESH PLOT - warped
    # ---------------------------------
    if plotWarped_flag:
        plt.figure("Warped")
        plt.scatter(zz_w_r,xx_w_r, s = 2, label = "Warped mesh")
    # ---------------------------------


    # LAB mesh
    xx_lab = np.zeros((x_len,z_len))
    zz_lab = np.zeros((x_len,z_len))

    for x_i in range(x_len):
        for z_i in range(z_len):
            zz_lab[x_i][z_i], xx_lab[x_i][z_i] = WarpedToLab_point(z_w[z_i],x_w[x_i],bend_start,bend_end,bend_radius)

    xx_lab_r = xx_lab.ravel()
    zz_lab_r = zz_lab.ravel()

    # MESH PLOT
    # ---------------------------------
    if plotLab_flag:
        plt.figure("Lab")
        plt.scatter(zz_lab_r,xx_lab_r, s = 2, label = "Lab mesh")
    # ---------------------------------


def plotMeshNewCAD(plotWarped_flag, plotLab_flag, lab0inCAD_z,lab0inCAD_x,angle_labToCAD, depositoryName, fileId, path_DiagnosticsLog = None,g_bend_lab_len_alt = None):
    if path_DiagnosticsLog == None: path_DiagnosticsLog = "./Logs/DiagnosticsLog_{}_{}.h5py".format(depositoryName, fileId)
    with h5py.File(path_DiagnosticsLog,"r") as f_log:
        x_w = np.array(f_log["xmesh_w"])
        z_w = np.array(f_log["zmesh_w"])
        bend_par = np.array(f_log["bend_par"])
    
    bend_start = bend_par[0]
    bend_end = bend_par[1]
    bend_radius = bend_par[2]

    x_len = len(x_w)
    z_len = len(z_w)

    # warped mesh
    xx_w = np.zeros((x_len,z_len))
    zz_w = np.zeros((x_len,z_len))

    for x_i in range(x_len):
        for z_i in range(z_len):
            zz_w[x_i][z_i] = z_w[z_i]
            xx_w[x_i][z_i] = x_w[x_i]


    xx_w_r = xx_w.ravel()
    zz_w_r = zz_w.ravel()

    # MESH PLOT - warped
    # ---------------------------------
    if plotWarped_flag:
        plt.figure("Warped")
        if g_bend_lab_len_alt is not None:
            plt.scatter(zz_w_r,xx_w_r, s = 2, label = "bend lenght change: {}".format(g_bend_lab_len_alt))
        else:
            plt.scatter(zz_w_r,xx_w_r, s = 2, label = "Mesh")
    # ---------------------------------


    # LAB mesh
    xx_lab = np.zeros((x_len,z_len))
    zz_lab = np.zeros((x_len,z_len))

    for x_i in range(x_len):
        for z_i in range(z_len):
            zz_lab[x_i][z_i], xx_lab[x_i][z_i] = WarpedToLab_point(z_w[z_i],x_w[x_i],bend_start,bend_end,bend_radius)

    xx_lab_r = xx_lab.ravel()
    zz_lab_r = zz_lab.ravel()

    # MESH PLOT
    # ---------------------------------
    if plotLab_flag:
        plt.figure("Lab")
        if g_bend_lab_len_alt is not None:
            plt.scatter(zz_lab_r,xx_lab_r, s = 2, label = "bend lenght change: {}".format(g_bend_lab_len_alt))
        else:
            plt.scatter(zz_lab_r,xx_lab_r, s = 2, label = "Mesh")
    # ---------------------------------

        # CAD mesh
    xx_CAD = np.zeros((x_len,z_len))
    zz_CAD = np.zeros((x_len,z_len))

    for x_i in range(x_len):
        for z_i in range(z_len):
            zz_CAD[x_i][z_i], xx_CAD[x_i][z_i] = WarpedToCAD_point(z_w[z_i],x_w[x_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius)


    xx_CAD_r = xx_CAD.ravel()
    zz_CAD_r = zz_CAD.ravel()

    # MESH PLOT
    # ---------------------------------
    if plotLab_flag:
        plt.figure("CAD")
        if g_bend_lab_len_alt is not None:
            plt.scatter(zz_CAD_r,xx_CAD_r, s = 2, label = "bend lenght change: {}".format(g_bend_lab_len_alt))
        else:
            plt.scatter(zz_CAD_r,xx_CAD_r, s = 2, label = "Mesh")
    # ---------------------------------

def plotCenterline(plotWarped_flag, plotLab_flag, lab0inCAD_z,lab0inCAD_x,angle_labToCAD, depositoryName, fileId, path_DiagnosticsLog = None,g_bend_lab_len_alt = None):
    if path_DiagnosticsLog == None: path_DiagnosticsLog = "./Logs/DiagnosticsLog_{}_{}.h5py".format(depositoryName, fileId)
    with h5py.File(path_DiagnosticsLog,"r") as f_log:
        x_w = np.array(f_log["xmesh_w"])
        z_w = np.array(f_log["zmesh_w"])
        bend_par = np.array(f_log["bend_par"])
    x_w = np.array((x_w[len(x_w)/2],))

    bend_start = bend_par[0]
    bend_end = bend_par[1]
    bend_radius = bend_par[2]

    x_len = len(x_w)
    z_len = len(z_w)

    # warped mesh
    xx_w = np.zeros((x_len,z_len))
    zz_w = np.zeros((x_len,z_len))

    for x_i in range(x_len):
        for z_i in range(z_len):
            zz_w[x_i][z_i] = z_w[z_i]
            xx_w[x_i][z_i] = x_w[x_i]


    xx_w_r = xx_w.ravel()
    zz_w_r = zz_w.ravel()

    # MESH PLOT - warped
    # ---------------------------------
    if plotWarped_flag:
        plt.figure("Warped")
        if g_bend_lab_len_alt is not None:
            plt.scatter(zz_w_r,xx_w_r, s = 2, label = "bend lenght change: {}".format(g_bend_lab_len_alt))
        else:
            plt.scatter(zz_w_r,xx_w_r, s = 2, label = "Mesh")
    # ---------------------------------


    # LAB mesh
    xx_lab = np.zeros((x_len,z_len))
    zz_lab = np.zeros((x_len,z_len))

    for x_i in range(x_len):
        for z_i in range(z_len):
            zz_lab[x_i][z_i], xx_lab[x_i][z_i] = WarpedToLab_point(z_w[z_i],x_w[x_i],bend_start,bend_end,bend_radius)

    xx_lab_r = xx_lab.ravel()
    zz_lab_r = zz_lab.ravel()

    # MESH PLOT
    # ---------------------------------
    if plotLab_flag:
        plt.figure("Lab")
        if g_bend_lab_len_alt is not None:
            plt.scatter(zz_lab_r,xx_lab_r, s = 2, label = "bend lenght change: {}".format(g_bend_lab_len_alt))
        else:
            plt.scatter(zz_lab_r,xx_lab_r, s = 2, label = "Mesh")
    # ---------------------------------

        # CAD mesh
    xx_CAD = np.zeros((x_len,z_len))
    zz_CAD = np.zeros((x_len,z_len))

    for x_i in range(x_len):
        for z_i in range(z_len):
            zz_CAD[x_i][z_i], xx_CAD[x_i][z_i] = WarpedToCAD_point(z_w[z_i],x_w[x_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius)


    xx_CAD_r = xx_CAD.ravel()
    zz_CAD_r = zz_CAD.ravel()

    # MESH PLOT
    # ---------------------------------
    if plotLab_flag:
        plt.figure("CAD")
        if g_bend_lab_len_alt is not None:
            plt.scatter(zz_CAD_r,xx_CAD_r, s = 2, label = "bend lenght change: {}".format(g_bend_lab_len_alt))
        else:
            plt.scatter(zz_CAD_r,xx_CAD_r, s = 2, label = "Mesh")
    # ---------------------------------
def PlotPipe(pipeToPlot, plotWarped_flag, plotLab_flag, bend_par, slice_r_a, slice_zs_a, slice_ze_a, slice_xcent_a, pipe_num_OfPoints = 10, color = None):
    bend_start = bend_par[0]
    bend_end = bend_par[1]
    bend_radius = bend_par[2]

    pipeWarped_line1_z_a = np.array([np.linspace(slice_zs_a[i],slice_ze_a[i],pipe_num_OfPoints) for i in range(len(slice_zs_a))])
    pipeWarped_line1_x_a = np.array([np.linspace(slice_xcent_a[i] + slice_r_a[i],slice_xcent_a[i] + slice_r_a[i],pipe_num_OfPoints) for i in range(len(slice_zs_a))])
    pipeWarped_line2_z_a = np.array([np.linspace(slice_zs_a[i],slice_ze_a[i],pipe_num_OfPoints) for i in range(len(slice_zs_a))])
    pipeWarped_line2_x_a = np.array([np.linspace(slice_xcent_a[i] - slice_r_a[i],slice_xcent_a[i] - slice_r_a[i],pipe_num_OfPoints) for i in range(len(slice_zs_a))])
    
    # if plotWarped_flag:
    #     plt.figure("Warped")
    #     for i in range(len(pipeWarped_line1_z_a)):
    #         plt.scatter(pipeWarped_line1_z_a[i],pipeWarped_line1_x_a[i],s = 2, color = color)
    #         plt.scatter(pipeWarped_line2_z_a[i],pipeWarped_line2_x_a[i],s = 2, color = color)

    pipeLab_line1_z_a = np.array([np.zeros(pipe_num_OfPoints) for i in range(len(slice_zs_a))])
    pipeLab_line1_x_a = np.array([np.zeros(pipe_num_OfPoints) for i in range(len(slice_zs_a))])
    pipeLab_line2_z_a = np.array([np.zeros(pipe_num_OfPoints) for i in range(len(slice_zs_a))])
    pipeLab_line2_x_a = np.array([np.zeros(pipe_num_OfPoints) for i in range(len(slice_zs_a))])
    
    for slice_i in range(len(pipeWarped_line1_z_a)):
        for point_i in range(len(pipeWarped_line1_z_a[slice_i])):
            pipeLab_line1_z_a[slice_i][point_i] , pipeLab_line1_x_a[slice_i][point_i] = WarpedToLab_point(pipeWarped_line1_z_a[slice_i][point_i],pipeWarped_line1_x_a[slice_i][point_i],bend_start,bend_end,bend_radius)
            pipeLab_line2_z_a[slice_i][point_i] , pipeLab_line2_x_a[slice_i][point_i] = WarpedToLab_point(pipeWarped_line2_z_a[slice_i][point_i],pipeWarped_line2_x_a[slice_i][point_i],bend_start,bend_end,bend_radius)
    # if plotLab_flag:
    #     plt.figure("Lab")
    #     for i in range(len(pipeWarped_line1_z_a)):
    #         plt.scatter(pipeLab_line1_z_a[i],pipeLab_line1_x_a[i],s = 2, color = color)
    #         plt.scatter(pipeLab_line2_z_a[i],pipeLab_line2_x_a[i],s = 2, color = color)

    return pipeLab_line1_z_a, pipeLab_line1_x_a, pipeLab_line2_z_a, pipeLab_line2_x_a, pipeWarped_line1_z_a, pipeWarped_line1_x_a, pipeWarped_line2_z_a, pipeWarped_line2_x_a

def PlotPipeA(plotWarped_flag, plotLab_flag, bend_par, pipeA_r, pipeA_zs, pipeA_ze, pipeA_xs, pipeA_xe, pipe_num_OfPoints = 10, color = None):
    bend_start = bend_par[0]
    bend_end = bend_par[1]
    bend_radius = bend_par[2]

    pipeWarped_line1_z_a = np.linspace(pipeA_zs,pipeA_ze,pipe_num_OfPoints) 
    pipeWarped_line1_x_a = np.linspace(pipeA_xs + pipeA_r,pipeA_xe + pipeA_r,pipe_num_OfPoints) 
    pipeWarped_line2_z_a = np.linspace(pipeA_zs,pipeA_ze,pipe_num_OfPoints) 
    pipeWarped_line2_x_a = np.linspace(pipeA_xs - pipeA_r,pipeA_xe - pipeA_r,pipe_num_OfPoints) 



    # if plotWarped_flag:
    #     plt.figure("Warped")
    #     for i in range(len(pipeWarped_line1_z_a)):
    #         plt.scatter(pipeWarped_line1_z_a[i],pipeWarped_line1_x_a[i],s = 2, color = color)
    #         plt.scatter(pipeWarped_line2_z_a[i],pipeWarped_line2_x_a[i],s = 2, color = color)

    pipeLab_line1_z_a = np.zeros(pipe_num_OfPoints)
    pipeLab_line1_x_a = np.zeros(pipe_num_OfPoints)
    pipeLab_line2_z_a = np.zeros(pipe_num_OfPoints)
    pipeLab_line2_x_a = np.zeros(pipe_num_OfPoints)
    
    for point_i in range(len(pipeWarped_line1_z_a)):
        pipeLab_line1_z_a[point_i] , pipeLab_line1_x_a[point_i] = WarpedToLab_point(pipeWarped_line1_z_a[point_i],pipeWarped_line1_x_a[point_i],bend_start,bend_end,bend_radius)
        pipeLab_line2_z_a[point_i] , pipeLab_line2_x_a[point_i] = WarpedToLab_point(pipeWarped_line2_z_a[point_i],pipeWarped_line2_x_a[point_i],bend_start,bend_end,bend_radius)
    # if plotLab_flag:
    #     plt.figure("Lab")
    #     for i in range(len(pipeWarped_line1_z_a)):
    #         plt.scatter(pipeLab_line1_z_a[i],pipeLab_line1_x_a[i],s = 2, color = color)
    #         plt.scatter(pipeLab_line2_z_a[i],pipeLab_line2_x_a[i],s = 2, color = color)

    return pipeLab_line1_z_a, pipeLab_line1_x_a, pipeLab_line2_z_a, pipeLab_line2_x_a, pipeWarped_line1_z_a, pipeWarped_line1_x_a, pipeWarped_line2_z_a, pipeWarped_line2_x_a


def PlotPipes(plotWarped_flag, plotLab_flag, depositoryName,fileId, path_DiagnosticsLog = None, legacy = False):
    if path_DiagnosticsLog is not None :
        if legacy:
            with h5py.File(path_DiagnosticsLog,"r") as f_log:
                #Saving the mesh
                xmesh_w = np.array(f_log["xmesh_w"])
                zmesh_w = np.array(f_log["zmesh_w"])
                #Saving the bend parameters
                bend_par = np.array(f_log["bend_par"])
                #Saving the conductors in Warped for check
                sliceA_r_a = np.array(f_log["sliceA_r_a"])
                sliceA_zs_a = np.array(f_log["sliceA_zs_a"])
                sliceA_ze_a = np.array(f_log["sliceA_ze_a"])
                sliceA_xcent_a = np.array(f_log["sliceA_xcent_a"])
                sliceB_r_a = np.array(f_log["sliceB_r_a"])
                sliceB_zs_a = np.array(f_log["sliceB_zs_a"])
                sliceB_ze_a = np.array(f_log["sliceB_ze_a"])
                sliceB_xcent_a = np.array(f_log["sliceB_xcent_a"])
                gunToBend_pipe_z = np.array(f_log["gunToBend_pipe_z"])
                gunToBend_pipe_r = np.array(f_log["gunToBend_pipe_r"])
        else:
            with h5py.File(path_DiagnosticsLog,"r") as f_log:
                #Saving the mesh
                xmesh_w = np.array(f_log["xmesh_w"])
                zmesh_w = np.array(f_log["zmesh_w"])
                #Saving the bend parameters
                bend_par = np.array(f_log["bend_par"])
                #Saving the conductors in Warped for check

                pipeA_r = np.array(f_log["pipeA_r"])
                pipeA_zs = np.array(f_log["pipeA_zs"])
                pipeA_ze = np.array(f_log["pipeA_ze"])
                pipeA_xs = np.array(f_log["pipeA_xs"])
                pipeA_xe = np.array(f_log["pipeA_xe"])
                sliceB_r_a = np.array(f_log["sliceB_r_a"])
                sliceB_zs_a = np.array(f_log["sliceB_zs_a"])
                sliceB_ze_a = np.array(f_log["sliceB_ze_a"])
                sliceB_xcent_a = np.array(f_log["sliceB_xcent_a"])
                gunToBend_pipe_z = np.array(f_log["gunToBend_pipe_z"])[:-2]
                gunToBend_pipe_r = np.array(f_log["gunToBend_pipe_r"])[:-2]
    else:
        with h5py.File("./Logs/DiagnosticsLog_{}_{}.h5py".format(depositoryName, fileId),"r") as f_log:
            #Saving the mesh
            xmesh_w = np.array(f_log["xmesh_w"])
            zmesh_w = np.array(f_log["zmesh_w"])
            #Saving the bend parameters
            bend_par = np.array(f_log["bend_par"])
            #Saving the conductors in Warped for check
            sliceA_r_a = np.array(f_log["sliceA_r_a"])
            sliceA_zs_a = np.array(f_log["sliceA_zs_a"])
            sliceA_ze_a = np.array(f_log["sliceA_ze_a"])
            sliceA_xcent_a = np.array(f_log["sliceA_xcent_a"])
            sliceB_r_a = np.array(f_log["sliceB_r_a"])
            sliceB_zs_a = np.array(f_log["sliceB_zs_a"])
            sliceB_ze_a = np.array(f_log["sliceB_ze_a"])
            sliceB_xcent_a = np.array(f_log["sliceB_xcent_a"])
            gunToBend_pipe_z = np.array(f_log["gunToBend_pipe_z"])
            gunToBend_pipe_r = np.array(f_log["gunToBend_pipe_r"])


    # PlotPipe("A", plotWarped_flag, plotLab_flag, bend_par, depositoryName, fileId ,color = "black", pipe_num_OfPoints = 100)
    # PlotPipe("B", plotWarped_flag, plotLab_flag, bend_par, depositoryName, fileId ,color = "black")
    if legacy:
        pipeALab_line1_z_a, pipeALab_line1_x_a, pipeALab_line2_z_a, pipeALab_line2_x_a, pipeAWarped_line1_z_a, pipeAWarped_line1_x_a, pipeAWarped_line2_z_a, pipeAWarped_line2_x_a = \
        PlotPipe(pipeToPlot = "A",\
                 plotWarped_flag = plotWarped_flag,\
                 plotLab_flag = plotLab_flag,\
                 bend_par = bend_par,\
                 slice_r_a = sliceA_r_a,\
                 slice_zs_a = sliceA_zs_a,\
                 slice_ze_a = sliceA_ze_a,\
                 slice_xcent_a = sliceA_xcent_a,\
                 # pipe_num_OfPoints = 100,\
                 color = "black")
    else:
        pipeALab_line1_z_a, pipeALab_line1_x_a, pipeALab_line2_z_a, pipeALab_line2_x_a, pipeAWarped_line1_z_a, pipeAWarped_line1_x_a, pipeAWarped_line2_z_a, pipeAWarped_line2_x_a = \
        PlotPipeA(   plotWarped_flag = plotWarped_flag,\
                     plotLab_flag = plotLab_flag,\
                     bend_par = bend_par,\
                     pipeA_r = pipeA_r,\
                     pipeA_zs = pipeA_zs,\
                     pipeA_ze = pipeA_ze,\
                     pipeA_xs = pipeA_xs,\
                     pipeA_xe = pipeA_xe,\
                     pipe_num_OfPoints = 500,\
                     color = "black")
    pipeBLab_line1_z_a, pipeBLab_line1_x_a, pipeBLab_line2_z_a, pipeBLab_line2_x_a, pipeBWarped_line1_z_a, pipeBWarped_line1_x_a, pipeBWarped_line2_z_a, pipeBWarped_line2_x_a = \
    PlotPipe(pipeToPlot = "B",\
             plotWarped_flag = plotWarped_flag,\
             plotLab_flag = plotLab_flag,\
             bend_par = bend_par,\
             slice_r_a = sliceB_r_a,\
             slice_zs_a = sliceB_zs_a,\
             slice_ze_a = sliceB_ze_a,\
             slice_xcent_a = sliceB_xcent_a,\
             # pipe_num_OfPoints = 100,\
             color = "black")

    pipeALab_line2_mask = np.ones(len(pipeALab_line2_z_a), dtype = bool)
    for point_i in range(len(pipeALab_line2_z_a)):
        pipeBLab_line1_z_a_ravel = pipeBLab_line1_z_a.ravel()
        pipeBLab_line1_x_a_ravel = pipeBLab_line1_x_a.ravel()
        closes_point_i = np.argmin(np.array(\
        [PPDistance(pipeALab_line2_z_a[point_i],pipeALab_line2_x_a[point_i],pipeBLab_line1_z_a_ravel[point_i2],pipeBLab_line1_x_a_ravel[point_i2])\
        for point_i2 in range(len(pipeBLab_line1_z_a_ravel))]))
        closes_point_iu = np.unravel_index(closes_point_i,np.shape(pipeBLab_line1_z_a))
        if pipeALab_line2_z_a[point_i] - pipeBLab_line1_z_a[closes_point_iu] > 0:
            pipeALab_line2_mask[point_i] = False

    pipeBLab_line1_mask = np.ones((len(pipeBLab_line1_z_a),len(pipeBLab_line1_z_a[0])), dtype = bool)
    for slice_i in range(len(pipeBLab_line1_z_a)):
        for point_i in range(len(pipeBLab_line1_z_a[slice_i])):
            pipeALab_line1_z_a_ravel = pipeALab_line1_z_a.ravel()
            pipeALab_line1_x_a_ravel = pipeALab_line1_x_a.ravel()
            pipeALab_line2_z_a_ravel = pipeALab_line2_z_a.ravel()
            pipeALab_line2_x_a_ravel = pipeALab_line2_x_a.ravel()

            closes_point_i1 = np.argmin(np.array(\
            [PPDistance(pipeBLab_line1_z_a[slice_i][point_i],pipeBLab_line1_x_a[slice_i][point_i],pipeALab_line2_z_a_ravel[point_i2],pipeALab_line2_x_a_ravel[point_i2])\
            for point_i2 in range(len(pipeALab_line2_z_a_ravel))]))
            closes_point_iu1 = np.unravel_index(closes_point_i1,np.shape(pipeALab_line2_z_a))

            closes_point_i2 = np.argmin(np.array(\
            [PPDistance(pipeBLab_line1_z_a[slice_i][point_i],pipeBLab_line1_x_a[slice_i][point_i],pipeALab_line1_z_a_ravel[point_i2],pipeALab_line1_x_a_ravel[point_i2])\
            for point_i2 in range(len(pipeALab_line1_z_a_ravel))]))
            closes_point_iu2 = np.unravel_index(closes_point_i2,np.shape(pipeALab_line2_z_a))

            if pipeBLab_line1_x_a[slice_i][point_i] > pipeALab_line2_x_a[closes_point_iu1] and pipeBLab_line1_x_a[slice_i][point_i] < pipeALab_line1_x_a[closes_point_iu2]:
                pipeBLab_line1_mask[slice_i][point_i] = False

    color = "black"
    if plotWarped_flag:
        plt.figure("Warped")
        for i in range(len(pipeAWarped_line1_z_a)):
            plt.scatter(pipeAWarped_line1_z_a[i],pipeAWarped_line1_x_a[i],s = 2, color = color)
            plt.scatter(pipeAWarped_line2_z_a[i][pipeALab_line2_mask[i]],pipeAWarped_line2_x_a[i][pipeALab_line2_mask[i]],s = 2, color = color)

    if plotLab_flag:
        plt.figure("Lab")
        for i in range(len(pipeALab_line1_z_a)):
            plt.scatter(pipeALab_line1_z_a[i],pipeALab_line1_x_a[i],s = 2, color = color)
            plt.scatter(pipeALab_line2_z_a[i][pipeALab_line2_mask[i]],pipeALab_line2_x_a[i][pipeALab_line2_mask[i]],s = 2, color = color)

    if plotWarped_flag:
        plt.figure("Warped")
        for i in range(len(pipeBWarped_line1_z_a)):
            plt.scatter(pipeBWarped_line1_z_a[i][pipeBLab_line1_mask[i]],pipeBWarped_line1_x_a[i][pipeBLab_line1_mask[i]],s = 2, color = color)
            plt.scatter(pipeBWarped_line2_z_a[i],pipeBWarped_line2_x_a[i],s = 2, color = color)

    if plotLab_flag:
        plt.figure("Lab")
        for i in range(len(pipeBLab_line1_z_a)):
            plt.scatter(pipeBLab_line1_z_a[i][pipeBLab_line1_mask[i]],pipeBLab_line1_x_a[i][pipeBLab_line1_mask[i]],s = 2, color = color)
            plt.scatter(pipeBLab_line2_z_a[i],pipeBLab_line2_x_a[i],s = 2, color = color)


    gunToBendPipe_num_OfPoints = 100
    # f_conductorsLab = h5py.File("./Logs/warpedPipe{}_{}.h5py".format(depositoryName,filesNames_adic[files_used_a[0]]["id"]),"r")
    # gunToBend_pipe_z = np.array(f_conductorsLab["gunToBend_pipe_z"])
    # gunToBend_pipe_r = np.array(f_conductorsLab["gunToBend_pipe_r"])
    gunToBend_pipe_len = len(gunToBend_pipe_z)

    gunToBend_pipe_az = [np.linspace(gunToBend_pipe_z[i-1],gunToBend_pipe_z[i],gunToBendPipe_num_OfPoints) for i in range(1,gunToBend_pipe_len)]
    gunToBend_pipe_ax1 = [np.linspace(gunToBend_pipe_r[i-1],gunToBend_pipe_r[i],gunToBendPipe_num_OfPoints) for i in range(1,gunToBend_pipe_len)]
    gunToBend_pipe_ax2 = [np.linspace(-gunToBend_pipe_r[i-1],-gunToBend_pipe_r[i],gunToBendPipe_num_OfPoints) for i in range(1,gunToBend_pipe_len)]

    plt.figure("Warped")
    for i in range(gunToBend_pipe_len-1):
        plt.scatter(gunToBend_pipe_az[i],gunToBend_pipe_ax1[i],color = "black" ,s = 2)
        plt.scatter(gunToBend_pipe_az[i],gunToBend_pipe_ax2[i],color = "black" ,s = 2)
    plt.figure("Lab")
    for i in range(gunToBend_pipe_len-1):
        plt.scatter(gunToBend_pipe_az[i],gunToBend_pipe_ax1[i],color = "black" ,s = 2)
        plt.scatter(gunToBend_pipe_az[i],gunToBend_pipe_ax2[i],color = "black" ,s = 2)
    

def PlotPipesCAD(plotWarped_flag, plotLab_flag, lab0inCAD_z,lab0inCAD_x,angle_labToCAD, depositoryName,fileId, path_DiagnosticsLog = None, color = "black"):
    if path_DiagnosticsLog is not None:
        with h5py.File(path_DiagnosticsLog,"r") as f_log:
            #Saving the mesh
            xmesh_w = np.array(f_log["xmesh_w"])
            zmesh_w = np.array(f_log["zmesh_w"])
            #Saving the bend parameters
            bend_par = np.array(f_log["bend_par"])
            #Saving the conductors in Warped for check

            pipeA_r = np.array(f_log["pipeA_r"])
            pipeA_zs = np.array(f_log["pipeA_zs"])
            pipeA_ze = np.array(f_log["pipeA_ze"])
            pipeA_xs = np.array(f_log["pipeA_xs"])
            pipeA_xe = np.array(f_log["pipeA_xe"])
            sliceB_r_a = np.array(f_log["sliceB_r_a"])
            sliceB_zs_a = np.array(f_log["sliceB_zs_a"])
            sliceB_ze_a = np.array(f_log["sliceB_ze_a"])
            sliceB_xcent_a = np.array(f_log["sliceB_xcent_a"])
            gunToBend_pipe_z = np.array(f_log["gunToBend_pipe_z"])[:-2]
            gunToBend_pipe_r = np.array(f_log["gunToBend_pipe_r"])[:-2]

    else:
        with h5py.File("./Logs/DiagnosticsLog_{}_{}.h5py".format(depositoryName, fileId),"r") as f_log:
            #Saving the mesh
            xmesh_w = np.array(f_log["xmesh_w"])
            zmesh_w = np.array(f_log["zmesh_w"])
            #Saving the bend parameters
            bend_par = np.array(f_log["bend_par"])
            #Saving the conductors in Warped for check
            sliceA_r_a = np.array(f_log["sliceA_r_a"])
            sliceA_zs_a = np.array(f_log["sliceA_zs_a"])
            sliceA_ze_a = np.array(f_log["sliceA_ze_a"])
            sliceA_xcent_a = np.array(f_log["sliceA_xcent_a"])
            sliceB_r_a = np.array(f_log["sliceB_r_a"])
            sliceB_zs_a = np.array(f_log["sliceB_zs_a"])
            sliceB_ze_a = np.array(f_log["sliceB_ze_a"])
            sliceB_xcent_a = np.array(f_log["sliceB_xcent_a"])
            gunToBend_pipe_z = np.array(f_log["gunToBend_pipe_z"])
            gunToBend_pipe_r = np.array(f_log["gunToBend_pipe_r"])

    bend_start = bend_par[0]
    bend_end = bend_par[1]
    bend_radius = bend_par[2]
    # PlotPipe("A", plotWarped_flag, plotLab_flag, bend_par, depositoryName, fileId ,color = "black", pipe_num_OfPoints = 100)
    # PlotPipe("B", plotWarped_flag, plotLab_flag, bend_par, depositoryName, fileId ,color = "black")

    pipeALab_line1_z_a, pipeALab_line1_x_a, pipeALab_line2_z_a, pipeALab_line2_x_a, pipeAWarped_line1_z_a, pipeAWarped_line1_x_a, pipeAWarped_line2_z_a, pipeAWarped_line2_x_a = \
    PlotPipeA(   plotWarped_flag = plotWarped_flag,\
                 plotLab_flag = plotLab_flag,\
                 bend_par = bend_par,\
                 pipeA_r = pipeA_r,\
                 pipeA_zs = pipeA_zs,\
                 pipeA_ze = pipeA_ze,\
                 pipeA_xs = pipeA_xs,\
                 pipeA_xe = pipeA_xe,\
                 pipe_num_OfPoints = 500,\
                 color = None)
    pipeBLab_line1_z_a, pipeBLab_line1_x_a, pipeBLab_line2_z_a, pipeBLab_line2_x_a, pipeBWarped_line1_z_a, pipeBWarped_line1_x_a, pipeBWarped_line2_z_a, pipeBWarped_line2_x_a = \
    PlotPipe(pipeToPlot = "B",\
             plotWarped_flag = plotWarped_flag,\
             plotLab_flag = plotLab_flag,\
             bend_par = bend_par,\
             slice_r_a = sliceB_r_a,\
             slice_zs_a = sliceB_zs_a,\
             slice_ze_a = sliceB_ze_a,\
             slice_xcent_a = sliceB_xcent_a,\
             # pipe_num_OfPoints = 100,\
             color = "black")

    pipeALab_line2_mask = np.ones(len(pipeALab_line2_z_a), dtype = bool)
    for point_i in range(len(pipeALab_line2_z_a)):
        pipeBLab_line1_z_a_ravel = pipeBLab_line1_z_a.ravel()
        pipeBLab_line1_x_a_ravel = pipeBLab_line1_x_a.ravel()
        closes_point_i = np.argmin(np.array(\
        [PPDistance(pipeALab_line2_z_a[point_i],pipeALab_line2_x_a[point_i],pipeBLab_line1_z_a_ravel[point_i2],pipeBLab_line1_x_a_ravel[point_i2])\
        for point_i2 in range(len(pipeBLab_line1_z_a_ravel))]))
        closes_point_iu = np.unravel_index(closes_point_i,np.shape(pipeBLab_line1_z_a))
        if pipeALab_line2_z_a[point_i] - pipeBLab_line1_z_a[closes_point_iu] > 0:
            pipeALab_line2_mask[point_i] = False

    pipeALab_line1_mask = np.ones(len(pipeALab_line1_z_a), dtype = bool)
    for point_i in range(len(pipeALab_line1_z_a)):
        pipeBLab_line1_z_a_ravel = pipeBLab_line1_z_a.ravel()
        pipeBLab_line1_x_a_ravel = pipeBLab_line1_x_a.ravel()
        closes_point_i = np.argmin(np.array(\
        [PPDistance(pipeALab_line1_z_a[point_i],pipeALab_line1_x_a[point_i],pipeBLab_line1_z_a_ravel[point_i2],pipeBLab_line1_x_a_ravel[point_i2])\
        for point_i2 in range(len(pipeBLab_line1_z_a_ravel))]))
        closes_point_iu = np.unravel_index(closes_point_i,np.shape(pipeBLab_line1_z_a))
        if pipeALab_line1_z_a[point_i] - pipeBLab_line1_z_a[closes_point_iu] > 0:
            pipeALab_line1_mask[point_i] = False

    pipeBLab_line1_mask = np.ones((len(pipeBLab_line1_z_a),len(pipeBLab_line1_z_a[0])), dtype = bool)
    for slice_i in range(len(pipeBLab_line1_z_a)):
        for point_i in range(len(pipeBLab_line1_z_a[slice_i])):
            pipeALab_line1_z_a_ravel = pipeALab_line1_z_a.ravel()
            pipeALab_line1_x_a_ravel = pipeALab_line1_x_a.ravel()
            pipeALab_line2_z_a_ravel = pipeALab_line2_z_a.ravel()
            pipeALab_line2_x_a_ravel = pipeALab_line2_x_a.ravel()

            closes_point_i1 = np.argmin(np.array(\
            [PPDistance(pipeBLab_line1_z_a[slice_i][point_i],pipeBLab_line1_x_a[slice_i][point_i],pipeALab_line2_z_a_ravel[point_i2],pipeALab_line2_x_a_ravel[point_i2])\
            for point_i2 in range(len(pipeALab_line2_z_a_ravel))]))
            closes_point_iu1 = np.unravel_index(closes_point_i1,np.shape(pipeALab_line2_z_a))

            closes_point_i2 = np.argmin(np.array(\
            [PPDistance(pipeBLab_line1_z_a[slice_i][point_i],pipeBLab_line1_x_a[slice_i][point_i],pipeALab_line1_z_a_ravel[point_i2],pipeALab_line1_x_a_ravel[point_i2])\
            for point_i2 in range(len(pipeALab_line1_z_a_ravel))]))
            closes_point_iu2 = np.unravel_index(closes_point_i2,np.shape(pipeALab_line2_z_a))

            if pipeBLab_line1_x_a[slice_i][point_i] > pipeALab_line2_x_a[closes_point_iu1] and pipeBLab_line1_x_a[slice_i][point_i] < pipeALab_line1_x_a[closes_point_iu2]:
                pipeBLab_line1_mask[slice_i][point_i] = False

    pipeACAD_line1_z_a = np.zeros(len(pipeAWarped_line1_z_a))
    pipeACAD_line1_x_a = np.zeros(len(pipeAWarped_line1_x_a))
    pipeACAD_line2_z_a = np.zeros(len(pipeAWarped_line2_z_a))
    pipeACAD_line2_x_a = np.zeros(len(pipeAWarped_line2_x_a))
    for point_i in np.arange(len(pipeACAD_line1_z_a)):
        pipeACAD_line1_z_a[point_i], pipeACAD_line1_x_a[point_i] = WarpedToCAD_point(pipeAWarped_line1_z_a[point_i],pipeAWarped_line1_x_a[point_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius)
        pipeACAD_line2_z_a[point_i], pipeACAD_line2_x_a[point_i] = WarpedToCAD_point(pipeAWarped_line2_z_a[point_i],pipeAWarped_line2_x_a[point_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius)

    pipeBCAD_line1_z_a = np.zeros((len(pipeBWarped_line1_z_a),len(pipeBWarped_line1_z_a[0])))
    pipeBCAD_line1_x_a = np.zeros((len(pipeBWarped_line1_x_a),len(pipeBWarped_line1_x_a[0])))
    pipeBCAD_line2_z_a = np.zeros((len(pipeBWarped_line2_z_a),len(pipeBWarped_line2_z_a[0])))
    pipeBCAD_line2_x_a = np.zeros((len(pipeBWarped_line2_x_a),len(pipeBWarped_line2_x_a[0])))
    for slice_i in np.arange(len(pipeBCAD_line1_z_a)):
        for point_i in np.arange(len(pipeBCAD_line1_z_a[slice_i])):
            pipeBCAD_line1_z_a[slice_i][point_i], pipeBCAD_line1_x_a[slice_i][point_i] = WarpedToCAD_point(pipeBWarped_line1_z_a[slice_i][point_i],pipeBWarped_line1_x_a[slice_i][point_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius)
            pipeBCAD_line2_z_a[slice_i][point_i], pipeBCAD_line2_x_a[slice_i][point_i] = WarpedToCAD_point(pipeBWarped_line2_z_a[slice_i][point_i],pipeBWarped_line2_x_a[slice_i][point_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius)


    if plotWarped_flag:
        plt.figure("Warped")
        for i in range(len(pipeAWarped_line1_z_a)):
            plt.scatter(pipeAWarped_line1_z_a[i][pipeALab_line1_mask[i]],pipeAWarped_line1_x_a[i][pipeALab_line1_mask[i]],s = 2, color = color)
            plt.scatter(pipeAWarped_line2_z_a[i][pipeALab_line2_mask[i]],pipeAWarped_line2_x_a[i][pipeALab_line2_mask[i]],s = 2, color = "black")

    if plotLab_flag:
        plt.figure("Lab")
        plt.scatter(pipeALab_line1_z_a[pipeALab_line1_mask],pipeALab_line1_x_a[pipeALab_line1_mask],s = 2, color = color)
        plt.scatter(pipeALab_line2_z_a[pipeALab_line2_mask],pipeALab_line2_x_a[pipeALab_line2_mask],s = 2, color = "black")
    # for i in range(len(pipeALab_line1_z_a)):
    #         plt.scatter(pipeALab_line1_z_a[i][pipeALab_line1_mask[i]],pipeALab_line1_x_a[i][pipeALab_line1_mask[i]],s = 2, color = color,  label = "Pipe")
    #         plt.scatter(pipeALab_line2_z_a[i][pipeALab_line2_mask[i]],pipeALab_line2_x_a[i][pipeALab_line2_mask[i]],s = 2, color = color)
    
    if plotWarped_flag:
        plt.figure("CAD")
        plt.scatter(pipeACAD_line1_z_a[pipeALab_line1_mask],pipeACAD_line1_x_a[pipeALab_line1_mask],s = 2, color = color)
        plt.scatter(pipeACAD_line2_z_a[pipeALab_line2_mask],pipeACAD_line2_x_a[pipeALab_line2_mask],s = 2, color = color)



    if plotWarped_flag:
        plt.figure("Warped")
        for i in range(len(pipeBWarped_line1_z_a)):
            plt.scatter(pipeBWarped_line1_z_a[i][pipeBLab_line1_mask[i]],pipeBWarped_line1_x_a[i][pipeBLab_line1_mask[i]],s = 2, color = "black")
            plt.scatter(pipeBWarped_line2_z_a[i],pipeBWarped_line2_x_a[i],s = 2, color = "black")

    if plotLab_flag:
        plt.figure("Lab")
        for i in range(len(pipeBLab_line1_z_a)):
            plt.scatter(pipeBLab_line1_z_a[i][pipeBLab_line1_mask[i]],pipeBLab_line1_x_a[i][pipeBLab_line1_mask[i]],s = 2, color = "black")
            plt.scatter(pipeBLab_line2_z_a[i],pipeBLab_line2_x_a[i],s = 2, color = "black")
    
    if plotWarped_flag:
        plt.figure("CAD")
        for i in range(len(pipeBCAD_line1_z_a)):
            plt.scatter(pipeBCAD_line1_z_a[i][pipeBLab_line1_mask[i]],pipeBCAD_line1_x_a[i][pipeBLab_line1_mask[i]],s = 2, color = "black")
            plt.scatter(pipeBCAD_line2_z_a[i],pipeBCAD_line2_x_a[i],s = 2, color = "black")


    gunToBendPipe_num_OfPoints = 1000
    # f_conductorsLab = h5py.File("./Logs/warpedPipe{}_{}.h5py".format(depositoryName,filesNames_adic[files_used_a[0]]["id"]),"r")
    # gunToBend_pipe_z = np.array(f_conductorsLab["gunToBend_pipe_z"])
    # gunToBend_pipe_r = np.array(f_conductorsLab["gunToBend_pipe_r"])
    gunToBend_pipe_len = len(gunToBend_pipe_z)

    gunToBend_pipe_az1 = [np.linspace(gunToBend_pipe_z[i-1],gunToBend_pipe_z[i],gunToBendPipe_num_OfPoints) for i in range(1,gunToBend_pipe_len)]
    gunToBend_pipe_az2 = [np.linspace(gunToBend_pipe_z[i-1],gunToBend_pipe_z[i],gunToBendPipe_num_OfPoints) for i in range(1,gunToBend_pipe_len)]
    gunToBend_pipe_az = [np.linspace(gunToBend_pipe_z[i-1],gunToBend_pipe_z[i],gunToBendPipe_num_OfPoints) for i in range(1,gunToBend_pipe_len)]
    gunToBend_pipe_ax1 = [np.linspace(gunToBend_pipe_r[i-1],gunToBend_pipe_r[i],gunToBendPipe_num_OfPoints) for i in range(1,gunToBend_pipe_len)]
    gunToBend_pipe_ax2 = [np.linspace(-gunToBend_pipe_r[i-1],-gunToBend_pipe_r[i],gunToBendPipe_num_OfPoints) for i in range(1,gunToBend_pipe_len)]

    gunToBend_pipeCAD_az1 = np.zeros(np.shape(gunToBend_pipe_az1))
    gunToBend_pipeCAD_az2 = np.zeros(np.shape(gunToBend_pipe_az2))
    gunToBend_pipeCAD_ax1 = np.zeros(np.shape(gunToBend_pipe_ax1))
    gunToBend_pipeCAD_ax2 = np.zeros(np.shape(gunToBend_pipe_ax2))

    for slice_i in np.arange(len(gunToBend_pipeCAD_az1)):
        for point_i in np.arange(len(gunToBend_pipeCAD_az1[slice_i])):
            gunToBend_pipeCAD_az1[slice_i][point_i], gunToBend_pipeCAD_ax1[slice_i][point_i] = WarpedToCAD_point(gunToBend_pipe_az1[slice_i][point_i],gunToBend_pipe_ax1[slice_i][point_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius)
            gunToBend_pipeCAD_az2[slice_i][point_i], gunToBend_pipeCAD_ax2[slice_i][point_i] = WarpedToCAD_point(gunToBend_pipe_az2[slice_i][point_i],gunToBend_pipe_ax2[slice_i][point_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius)


    plt.figure("Warped")
    for i in range(gunToBend_pipe_len-1):
        plt.scatter(gunToBend_pipe_az[i],gunToBend_pipe_ax1[i],color = "black" ,s = 2)
        plt.scatter(gunToBend_pipe_az[i],gunToBend_pipe_ax2[i],color = "black" ,s = 2)

    plt.figure("Lab")
    for i in range(gunToBend_pipe_len-1):
        plt.scatter(gunToBend_pipe_az[i],gunToBend_pipe_ax1[i],color = "black" ,s = 2)
        plt.scatter(gunToBend_pipe_az[i],gunToBend_pipe_ax2[i],color = "black" ,s = 2)

    plt.figure("CAD")
    for i in range(gunToBend_pipe_len-1):
        plt.scatter(gunToBend_pipeCAD_az1[i],gunToBend_pipeCAD_ax1[i],color = "black" ,s = 2)
        plt.scatter(gunToBend_pipeCAD_az2[i],gunToBend_pipeCAD_ax2[i],color = "black" ,s = 2)

        

def PlotPipesNewCAD(plotWarped_flag, plotLab_flag, lab0inCAD_z,lab0inCAD_x,angle_labToCAD, depositoryName,fileId, path_DiagnosticsLog = None, color = "black"):
    with h5py.File(path_DiagnosticsLog,"r") as f_log:
        #Saving the mesh
        xmesh_w = np.array(f_log["xmesh_w"])
        zmesh_w = np.array(f_log["zmesh_w"])
        #Saving the bend parameters
        bend_par = np.array(f_log["bend_par"])
        #Saving the conductors in Warped for check

        sliceA_r_a = np.array(f_log["sliceA_r_a"])
        sliceA_zs_a = np.array(f_log["sliceA_zs_a"])
        sliceA_ze_a = np.array(f_log["sliceA_ze_a"])
        sliceA_xcent_a = np.array(f_log["sliceA_xcent_a"])

        sliceB_r_a = np.array(f_log["sliceB_r_a"])
        sliceB_zs_a = np.array(f_log["sliceB_zs_a"])
        sliceB_ze_a = np.array(f_log["sliceB_ze_a"])
        sliceB_xcent_a = np.array(f_log["sliceB_xcent_a"])
        gunToBend_pipe_z = np.array(f_log["gunToBend_pipe_z"])[:-2]
        gunToBend_pipe_r = np.array(f_log["gunToBend_pipe_r"])[:-2]

    bend_start = bend_par[0]
    bend_end = bend_par[1]
    bend_radius = bend_par[2]
    # PlotPipe("A", plotWarped_flag, plotLab_flag, bend_par, depositoryName, fileId ,color = "black", pipe_num_OfPoints = 100)
    # PlotPipe("B", plotWarped_flag, plotLab_flag, bend_par, depositoryName, fileId ,color = "black")

    pipeALab_line1_z_a, pipeALab_line1_x_a, pipeALab_line2_z_a, pipeALab_line2_x_a, pipeAWarped_line1_z_a, pipeAWarped_line1_x_a, pipeAWarped_line2_z_a, pipeAWarped_line2_x_a = \
    PlotPipe(pipeToPlot = "A",\
             plotWarped_flag = plotWarped_flag,\
             plotLab_flag = plotLab_flag,\
             bend_par = bend_par,\
             slice_r_a = sliceA_r_a,\
             slice_zs_a = sliceA_zs_a,\
             slice_ze_a = sliceA_ze_a,\
             slice_xcent_a = sliceA_xcent_a,\
             # pipe_num_OfPoints = 100,\
             color = "black")

    pipeBLab_line1_z_a, pipeBLab_line1_x_a, pipeBLab_line2_z_a, pipeBLab_line2_x_a, pipeBWarped_line1_z_a, pipeBWarped_line1_x_a, pipeBWarped_line2_z_a, pipeBWarped_line2_x_a = \
    PlotPipe(pipeToPlot = "B",\
             plotWarped_flag = plotWarped_flag,\
             plotLab_flag = plotLab_flag,\
             bend_par = bend_par,\
             slice_r_a = sliceB_r_a,\
             slice_zs_a = sliceB_zs_a,\
             slice_ze_a = sliceB_ze_a,\
             slice_xcent_a = sliceB_xcent_a,\
             # pipe_num_OfPoints = 100,\
             color = "black")

    pipeALab_line2_mask = np.ones((len(pipeALab_line2_z_a),len(pipeALab_line2_z_a[0])), dtype = bool)
    for slice_i in range(len(pipeALab_line2_z_a)):
        for point_i in range(len(pipeALab_line2_z_a[slice_i])):
            pipeBLab_line1_z_a_ravel = pipeBLab_line1_z_a.ravel()
            pipeBLab_line1_x_a_ravel = pipeBLab_line1_x_a.ravel()
            closes_point_i = np.argmin(np.array(\
            [PPDistance(pipeALab_line2_z_a[slice_i][point_i],pipeALab_line2_x_a[slice_i][point_i],pipeBLab_line1_z_a_ravel[point_i2],pipeBLab_line1_x_a_ravel[point_i2])\
            for point_i2 in range(len(pipeBLab_line1_z_a_ravel))]))
            closes_point_iu = np.unravel_index(closes_point_i,np.shape(pipeBLab_line1_z_a))
            if pipeALab_line2_z_a[slice_i][point_i] - pipeBLab_line1_z_a[closes_point_iu] > 0 or pipeALab_line1_z_a[slice_i][point_i] > -gunToBend_pipe_r[-1]:
                pipeALab_line2_mask[slice_i][point_i] = False

    pipeALab_line1_mask = np.ones((len(pipeALab_line1_z_a),len(pipeALab_line1_z_a[0])), dtype = bool)
    for slice_i in range(len(pipeALab_line1_z_a)):
        for point_i in range(len(pipeALab_line1_z_a[slice_i])):
            pipeBLab_line1_z_a_ravel = pipeBLab_line1_z_a.ravel()
            pipeBLab_line1_x_a_ravel = pipeBLab_line1_x_a.ravel()
            closes_point_i = np.argmin(np.array(\
            [PPDistance(pipeALab_line1_z_a[slice_i][point_i],pipeALab_line1_x_a[slice_i][point_i],pipeBLab_line1_z_a_ravel[point_i2],pipeBLab_line1_x_a_ravel[point_i2])\
            for point_i2 in range(len(pipeBLab_line1_z_a_ravel))]))
            closes_point_iu = np.unravel_index(closes_point_i,np.shape(pipeBLab_line1_z_a))
            if pipeALab_line1_z_a[slice_i][point_i] - pipeBLab_line1_z_a[closes_point_iu] > 0 :
                pipeALab_line1_mask[slice_i][point_i] = False

    pipeBLab_line1_mask = np.ones((len(pipeBLab_line1_z_a),len(pipeBLab_line1_z_a[0])), dtype = bool)
    for slice_i in range(len(pipeBLab_line1_z_a)):
        for point_i in range(len(pipeBLab_line1_z_a[slice_i])):
            pipeALab_line1_z_a_ravel = pipeALab_line1_z_a.ravel()
            pipeALab_line1_x_a_ravel = pipeALab_line1_x_a.ravel()
            pipeALab_line2_z_a_ravel = pipeALab_line2_z_a.ravel()
            pipeALab_line2_x_a_ravel = pipeALab_line2_x_a.ravel()

            closes_point_i1 = np.argmin(np.array(\
            [PPDistance(pipeBLab_line1_z_a[slice_i][point_i],pipeBLab_line1_x_a[slice_i][point_i],pipeALab_line2_z_a_ravel[point_i2],pipeALab_line2_x_a_ravel[point_i2])\
            for point_i2 in range(len(pipeALab_line2_z_a_ravel))]))
            closes_point_iu1 = np.unravel_index(closes_point_i1,np.shape(pipeALab_line2_z_a))

            closes_point_i2 = np.argmin(np.array(\
            [PPDistance(pipeBLab_line1_z_a[slice_i][point_i],pipeBLab_line1_x_a[slice_i][point_i],pipeALab_line1_z_a_ravel[point_i2],pipeALab_line1_x_a_ravel[point_i2])\
            for point_i2 in range(len(pipeALab_line1_z_a_ravel))]))
            closes_point_iu2 = np.unravel_index(closes_point_i2,np.shape(pipeALab_line2_z_a))

            if (pipeBLab_line1_x_a[slice_i][point_i] > pipeALab_line2_x_a[closes_point_iu1] or pipeBLab_line1_x_a[slice_i][point_i] > -gunToBend_pipe_r[-1]) and pipeBLab_line1_x_a[slice_i][point_i] < pipeALab_line1_x_a[closes_point_iu2]:
                pipeBLab_line1_mask[slice_i][point_i] = False

    pipeACAD_line1_z_a = np.zeros((len(pipeAWarped_line1_z_a),len(pipeAWarped_line1_z_a[0])))
    pipeACAD_line1_x_a = np.zeros((len(pipeAWarped_line1_x_a),len(pipeAWarped_line1_x_a[0])))
    pipeACAD_line2_z_a = np.zeros((len(pipeAWarped_line2_z_a),len(pipeAWarped_line2_z_a[0])))
    pipeACAD_line2_x_a = np.zeros((len(pipeAWarped_line2_x_a),len(pipeAWarped_line2_x_a[0])))
    for slice_i in np.arange(len(pipeACAD_line1_z_a)):
        for point_i in np.arange(len(pipeACAD_line1_z_a[slice_i])):
            pipeACAD_line1_z_a[slice_i][point_i], pipeACAD_line1_x_a[slice_i][point_i] = WarpedToCAD_point(pipeAWarped_line1_z_a[slice_i][point_i],pipeAWarped_line1_x_a[slice_i][point_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius)
            pipeACAD_line2_z_a[slice_i][point_i], pipeACAD_line2_x_a[slice_i][point_i] = WarpedToCAD_point(pipeAWarped_line2_z_a[slice_i][point_i],pipeAWarped_line2_x_a[slice_i][point_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius)


    pipeBCAD_line1_z_a = np.zeros((len(pipeBWarped_line1_z_a),len(pipeBWarped_line1_z_a[0])))
    pipeBCAD_line1_x_a = np.zeros((len(pipeBWarped_line1_x_a),len(pipeBWarped_line1_x_a[0])))
    pipeBCAD_line2_z_a = np.zeros((len(pipeBWarped_line2_z_a),len(pipeBWarped_line2_z_a[0])))
    pipeBCAD_line2_x_a = np.zeros((len(pipeBWarped_line2_x_a),len(pipeBWarped_line2_x_a[0])))
    for slice_i in np.arange(len(pipeBCAD_line1_z_a)):
        for point_i in np.arange(len(pipeBCAD_line1_z_a[slice_i])):
            pipeBCAD_line1_z_a[slice_i][point_i], pipeBCAD_line1_x_a[slice_i][point_i] = WarpedToCAD_point(pipeBWarped_line1_z_a[slice_i][point_i],pipeBWarped_line1_x_a[slice_i][point_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius)
            pipeBCAD_line2_z_a[slice_i][point_i], pipeBCAD_line2_x_a[slice_i][point_i] = WarpedToCAD_point(pipeBWarped_line2_z_a[slice_i][point_i],pipeBWarped_line2_x_a[slice_i][point_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius)


    if plotWarped_flag:
        plt.figure("Warped")
        for i in range(len(pipeAWarped_line1_z_a)):
            plt.scatter(pipeAWarped_line1_z_a[i][pipeALab_line1_mask[i]],pipeAWarped_line1_x_a[i][pipeALab_line1_mask[i]],s = 2, color = color)
            plt.scatter(pipeAWarped_line2_z_a[i][pipeALab_line2_mask[i]],pipeAWarped_line2_x_a[i][pipeALab_line2_mask[i]],s = 2, color = "black")

    if plotLab_flag:
        plt.figure("Lab")
        for i in range(len(pipeALab_line1_z_a)):
            plt.scatter(pipeALab_line1_z_a[i][pipeALab_line1_mask[i]],pipeALab_line1_x_a[i][pipeALab_line1_mask[i]],s = 2, color = color)
            plt.scatter(pipeALab_line2_z_a[i][pipeALab_line2_mask[i]],pipeALab_line2_x_a[i][pipeALab_line2_mask[i]],s = 2, color = "black")
    # for i in range(len(pipeALab_line1_z_a)):
    #         plt.scatter(pipeALab_line1_z_a[i][pipeALab_line1_mask[i]],pipeALab_line1_x_a[i][pipeALab_line1_mask[i]],s = 2, color = color,  label = "Pipe")
    #         plt.scatter(pipeALab_line2_z_a[i][pipeALab_line2_mask[i]],pipeALab_line2_x_a[i][pipeALab_line2_mask[i]],s = 2, color = color)
    
    if plotWarped_flag:
        plt.figure("CAD")
        for i in range(len(pipeACAD_line1_z_a)):
            plt.scatter(pipeACAD_line1_z_a[i][pipeALab_line1_mask[i]],pipeACAD_line1_x_a[i][pipeALab_line1_mask[i]],s = 2, color = color)
            plt.scatter(pipeACAD_line2_z_a[i][pipeALab_line2_mask[i]],pipeACAD_line2_x_a[i][pipeALab_line2_mask[i]],s = 2, color = color)

    if plotWarped_flag:
        plt.figure("Warped")
        for i in range(len(pipeBWarped_line1_z_a)):
            plt.scatter(pipeBWarped_line1_z_a[i][pipeBLab_line1_mask[i]],pipeBWarped_line1_x_a[i][pipeBLab_line1_mask[i]],s = 2, color = "black")
            plt.scatter(pipeBWarped_line2_z_a[i],pipeBWarped_line2_x_a[i],s = 2, color = "black")

    if plotLab_flag:
        plt.figure("Lab")
        for i in range(len(pipeBLab_line1_z_a)):
            plt.scatter(pipeBLab_line1_z_a[i][pipeBLab_line1_mask[i]],pipeBLab_line1_x_a[i][pipeBLab_line1_mask[i]],s = 2, color = "black")
            plt.scatter(pipeBLab_line2_z_a[i],pipeBLab_line2_x_a[i],s = 2, color = "black")
    
    if plotWarped_flag:
        plt.figure("CAD")
        for i in range(len(pipeBCAD_line1_z_a)):
            plt.scatter(pipeBCAD_line1_z_a[i][pipeBLab_line1_mask[i]],pipeBCAD_line1_x_a[i][pipeBLab_line1_mask[i]],s = 2, color = "black")
            plt.scatter(pipeBCAD_line2_z_a[i],pipeBCAD_line2_x_a[i],s = 2, color = "black")


    gunToBendPipe_num_OfPoints = 300
    # f_conductorsLab = h5py.File("./Logs/warpedPipe{}_{}.h5py".format(depositoryName,filesNames_adic[files_used_a[0]]["id"]),"r")
    # gunToBend_pipe_z = np.array(f_conductorsLab["gunToBend_pipe_z"])
    # gunToBend_pipe_r = np.array(f_conductorsLab["gunToBend_pipe_r"])
    gunToBend_pipe_len = len(gunToBend_pipe_z)

    gunToBend_pipe_az1 = [np.linspace(gunToBend_pipe_z[i-1],gunToBend_pipe_z[i],gunToBendPipe_num_OfPoints) for i in range(1,gunToBend_pipe_len)]
    gunToBend_pipe_az2 = [np.linspace(gunToBend_pipe_z[i-1],gunToBend_pipe_z[i],gunToBendPipe_num_OfPoints) for i in range(1,gunToBend_pipe_len)]
    gunToBend_pipe_az  = [np.linspace(gunToBend_pipe_z[i-1],gunToBend_pipe_z[i],gunToBendPipe_num_OfPoints) for i in range(1,gunToBend_pipe_len)]
    gunToBend_pipe_ax1 = [np.linspace(gunToBend_pipe_r[i-1],gunToBend_pipe_r[i],gunToBendPipe_num_OfPoints) for i in range(1,gunToBend_pipe_len)]
    gunToBend_pipe_ax2 = [np.linspace(-gunToBend_pipe_r[i-1],-gunToBend_pipe_r[i],gunToBendPipe_num_OfPoints) for i in range(1,gunToBend_pipe_len)]

    gunToBend_pipeCAD_az1 = np.zeros(np.shape(gunToBend_pipe_az1))
    gunToBend_pipeCAD_az2 = np.zeros(np.shape(gunToBend_pipe_az2))
    gunToBend_pipeCAD_ax1 = np.zeros(np.shape(gunToBend_pipe_ax1))
    gunToBend_pipeCAD_ax2 = np.zeros(np.shape(gunToBend_pipe_ax2))

    for slice_i in np.arange(len(gunToBend_pipeCAD_az1)):
        for point_i in np.arange(len(gunToBend_pipeCAD_az1[slice_i])):
            gunToBend_pipeCAD_az1[slice_i][point_i], gunToBend_pipeCAD_ax1[slice_i][point_i] = WarpedToCAD_point(gunToBend_pipe_az1[slice_i][point_i],gunToBend_pipe_ax1[slice_i][point_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius)
            gunToBend_pipeCAD_az2[slice_i][point_i], gunToBend_pipeCAD_ax2[slice_i][point_i] = WarpedToCAD_point(gunToBend_pipe_az2[slice_i][point_i],gunToBend_pipe_ax2[slice_i][point_i],lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_start,bend_end,bend_radius)


    plt.figure("Warped")
    for i in range(gunToBend_pipe_len-1):
        plt.scatter(gunToBend_pipe_az[i],gunToBend_pipe_ax1[i],color = "black" ,s = 2)
        plt.scatter(gunToBend_pipe_az[i],gunToBend_pipe_ax2[i],color = "black" ,s = 2)

    plt.figure("Lab")
    for i in range(gunToBend_pipe_len-1):
        plt.scatter(gunToBend_pipe_az[i],gunToBend_pipe_ax1[i],color = "black" ,s = 2)
        plt.scatter(gunToBend_pipe_az[i],gunToBend_pipe_ax2[i],color = "black" ,s = 2)

    plt.figure("CAD")
    for i in range(gunToBend_pipe_len-1):
        plt.scatter(gunToBend_pipeCAD_az1[i],gunToBend_pipeCAD_ax1[i],color = "black" ,s = 2)
        plt.scatter(gunToBend_pipeCAD_az2[i],gunToBend_pipeCAD_ax2[i],color = "black" ,s = 2)

        
def PlotParticleData(plotWarped_flag, plotLab_flag,bend_par, filesNames_adic, filesNames_a, files_used_a):
    bend_start = bend_par[0]
    bend_end = bend_par[1]
    bend_radius = bend_par[2]
        #Creating the file array
    filesIn_af = [h5py.File(filesName_s,"r") for filesName_s in filesNames_a]


    reduceTheArray = 10
    x_dic_a = [np.array(fileIn_f["x"])[::reduceTheArray] for fileIn_f in filesIn_af]
    y_dic_a = [np.array(fileIn_f["y"])[::reduceTheArray] for fileIn_f in filesIn_af]
    z_dic_a = [np.array(fileIn_f["z"])[::reduceTheArray] for fileIn_f in filesIn_af]
    Ekin_dic_a = [np.array(fileIn_f["KinEn"])[::reduceTheArray] for fileIn_f in filesIn_af]
    for fileToPlot in range(len(files_used_a)):
        # Ekin_colors = Ekin_dic_a[fileToPlot]/sum(Ekin_dic_a[fileToPlot])
        color_tresholdB = 3800.
        Ekin_colors = Ekin_dic_a[fileToPlot]
        Ekin_colors[np.where(Ekin_colors<color_tresholdB)] = color_tresholdB
        print("Plotting {}/{}".format(fileToPlot+1,len(files_used_a)))
        # Warped
        if plotWarped_flag:
            PlotStd(z_dic_a[fileToPlot], x_dic_a[fileToPlot], title = "z vs x", label = "File used: {}".format(filesNames_adic[files_used_a[fileToPlot]]["id"]), colors = Ekin_colors, xlabel = "z [m]", ylabel = "x [m]", figure = "Warped")
            PlotStd(z_dic_a[fileToPlot], y_dic_a[fileToPlot], title = "z vs y", label = "File used: {}".format(filesNames_adic[files_used_a[fileToPlot]]["id"]), colors = Ekin_colors, xlabel = "z [m]", ylabel = "x [m]", figure = "Warped_zy")
            # plt.figure("Warped")
            # plt.colorbar().
            
        # Lab
        z_lab_a = np.zeros(len(z_dic_a[fileToPlot]))
        x_lab_a = np.zeros(len(x_dic_a[fileToPlot]))
        for i in range(len(z_lab_a)):
            z_lab_a[i], x_lab_a[i] = WarpedToLab_point(z_dic_a[fileToPlot][i],x_dic_a[fileToPlot][i],bend_start,bend_end,bend_radius)
        if plotLab_flag:
            PlotStd(z_lab_a, x_lab_a, title = "z vs x", label = "File used: {}".format(filesNames_adic[files_used_a[fileToPlot]]["id"]),  colors = Ekin_colors, xlabel = "z [m]", ylabel = "x [m]", figure = "Lab")
            PlotStd(z_lab_a, y_dic_a[fileToPlot], title = "z vs y", label = "File used: {}".format(filesNames_adic[files_used_a[fileToPlot]]["id"]),  colors = Ekin_colors, xlabel = "z [m]", ylabel = "x [m]", figure = "Lab_zy")
            # plt.figure("Lab")
            # plt.colorbar().set_label("eV")
#Creating a histogram of a eKin distribution at 100dt and 
def PlotNormHistKineticEnergy(eKin_aa, filesToPlot_a, dt, filesNames_dic):
    plt.figure()
    for i in range(len(filesToPlot_a)): 
        plt.hist(eKin_aa[i], density = True, histtype='step', stacked=True, fill=False, bins = 100,label = "File: {}".format(filesToPlot_a[i]))
        # plt.hist(eKin_aa[i], density = True, histtype='step', stacked=True, fill=False, label = "{} cells/cm, {} p/st".format(filesNames_dic[filesToPlot_a[i]]["cPcm"],filesNames_dic[filesToPlot_a[i]]["pPdt"]))
    plt.title("Normalised Kinetic Energy, step {}".format(dt))
    plt.ylabel("N_norm [~]")
    plt.xlabel("Ek [eV]")
    plt.legend()
    # plt.ylim(0,np.amax([filesNames_dic[file]["pPdt"]*filesNames_dic[file]["steps"]/5. for file in files_used_a]))
    # plt.ylim(0,0.006)
    # plt.xlim(9000,12000)
    # plt.savefig("Plots/NoPipe/H_KinEn_{}CellsPerCm_{}ParPerStep.png".format(cells_per_cm, inject_NumParticlesPerStep))

def PlotMagneticFieldSimOutput(rz_flag, plotWarped_flag, depositoryName, fileId, path_DiagnosticsLog = None):
    if path_DiagnosticsLog is not None:
        with h5py.File(path_DiagnosticsLog,"r") as f_log:
            #Saving the mesh
            xmesh_w = np.array(f_log["xmesh_w"])
            zmesh_w = np.array(f_log["zmesh_w"])
            #Saving the bend parameters
            bend_par = np.array(f_log["bend_par"])
            #Saving applied magnetic field in Warped for check
            bx_warped_sim = np.array(f_log["magFieldWarped_x"])
            by_warped_sim = np.array(f_log["magFieldWarped_y"])
            bz_warped_sim = np.array(f_log["magFieldWarped_z"])
    else:
        with h5py.File("./Logs/DiagnosticsLog_{}_{}.h5py".format(depositoryName, fileId),"r") as f_log:
            #Saving the mesh
            xmesh_w = np.array(f_log["xmesh_w"])
            zmesh_w = np.array(f_log["zmesh_w"])
            #Saving the bend parameters
            bend_par = np.array(f_log["bend_par"])
            #Saving applied magnetic field in Warped for check
            bx_warped_sim = np.array(f_log["magFieldWarped_x"])
            by_warped_sim = np.array(f_log["magFieldWarped_y"])
            bz_warped_sim = np.array(f_log["magFieldWarped_z"])
    nx_sim,ny_sim,nz_sim = np.shape(bx_warped_sim)
    print("np.shape(bx_warped_sim) = {}".format(np.shape(bx_warped_sim)))

    if rz_flag:
        # The applied file is only from x = 0 (or -xs to xs) to xmax
        xmesh_w_half = xmesh_w[int((np.shape(xmesh_w)[0]-1.)/2.):]
        y_i_sim = 0
        meshgridWarped_z,meshgridWarped_x = np.meshgrid(zmesh_w,xmesh_w_half)
    else:
        y_i_sim = ny_sim/2 # The center of y diemnsion
        meshgridWarped_z,meshgridWarped_x = np.meshgrid(zmesh_w,xmesh_w)
        x_i_sim = nx_sim/2
        by_warped_2d_y = np.array([[by_warped_sim[x_i_sim][i_y][i_z] for i_z in range(nz_sim)] for i_y in range(ny_sim)])
        bz_warped_2d_y = np.array([[bz_warped_sim[x_i_sim][i_y][i_z] for i_z in range(nz_sim)] for i_y in range(ny_sim)])

    bx_warped_2d = np.array([[bx_warped_sim[i_x][y_i_sim][i_z] for i_z in range(nz_sim)] for i_x in range(nx_sim)])
    by_warped_2d = np.array([[by_warped_sim[i_x][y_i_sim][i_z] for i_z in range(nz_sim)] for i_x in range(nx_sim)])
    bz_warped_2d = np.array([[bz_warped_sim[i_x][y_i_sim][i_z] for i_z in range(nz_sim)] for i_x in range(nx_sim)])

    if plotWarped_flag:
        plt.figure("Warped")
        plt.streamplot(meshgridWarped_z,meshgridWarped_x,bz_warped_2d,bx_warped_2d,color = "red", linewidth=1, cmap=plt.cm.inferno, density=2, arrowstyle='->', arrowsize=1.5)
    #     plt.figure("Warped_zy")
    #     plt.streamplot(meshgridWarped_z,meshgridWarped_x,bz_warped_2d_y,by_warped_2d_y,color = "red", linewidth=1, cmap=plt.cm.inferno, density=2, arrowstyle='->', arrowsize=1.5)
    #     plt.figure("by_zy")
    #     plt.contourf(meshgridWarped_z,meshgridWarped_x,by_warped_2d_y)
    #     plt.figure("bz_zy")
    #     plt.contourf(meshgridWarped_z,meshgridWarped_x,bz_warped_2d_y)
    #     plt.colorbar()

    for y_i in range(0,ny_sim,10):
        bx_warped_2d_y = np.array([[bx_warped_sim[i_x][y_i][i_z] for i_z in range(nz_sim)] for i_x in range(nx_sim)])
        by_warped_2d_y = np.array([[by_warped_sim[i_x][y_i][i_z] for i_z in range(nz_sim)] for i_x in range(nx_sim)])
        bz_warped_2d_y = np.array([[bz_warped_sim[i_x][y_i][i_z] for i_z in range(nz_sim)] for i_x in range(nx_sim)])
        plt.figure("warped_bz_y={}".format(xmesh_w[y_i]))
        plt.contourf(meshgridWarped_z,meshgridWarped_x,bz_warped_2d_y,50)
        plt.colorbar()

        plt.figure("warped_bx_y={}".format(xmesh_w[y_i]))
        plt.contourf(meshgridWarped_z,meshgridWarped_x,bx_warped_2d_y,50)
        plt.colorbar()

        plt.figure("warped_by_y={}".format(xmesh_w[y_i]))
        plt.contourf(meshgridWarped_z,meshgridWarped_x,by_warped_2d_y,50)
        plt.colorbar()

def PlotMagneticFieldSimInput(plotWarped_flag, depositoryName, fileId, fName_BfieldFinal,path_DiagnosticsLog = None):
    with h5py.File(fName_BfieldFinal,"r") as bfile:
        bx_warped_final_3d = np.array(bfile["bx_warped_final_3d"])
        by_warped_final_3d = np.array(bfile["by_warped_final_3d"])
        bz_warped_final_3d = np.array(bfile["bz_warped_final_3d"])
    nx_calc,ny_calc,nz_calc = np.shape(bx_warped_final_3d)
    print("np.shape(bx_warped_final_3d) = {}".format(np.shape(bx_warped_final_3d)))
    fName_BfieldFinal_rect = "../Bfield_files/FlatTopMode/BFieldFinalLab_Zfull_4cpcm.h5py"
    with h5py.File(fName_BfieldFinal_rect,"r") as bfile:
        bx_Lab_final_3d = np.array(bfile["bx_Lab_final_3d"])
        by_Lab_final_3d = np.array(bfile["by_Lab_final_3d"])
        bz_Lab_final_3d = np.array(bfile["bz_Lab_final_3d"])
    nx_lab,ny_lab,nz_lab = np.shape(bx_Lab_final_3d)

    if path_DiagnosticsLog is not None:
        with h5py.File(path_DiagnosticsLog,"r") as f_log:
            #Saving the mesh
            xmesh_w = np.array(f_log["xmesh_w"])
            zmesh_w = np.array(f_log["zmesh_w"])
    else:
        with h5py.File("./Logs/DiagnosticsLog_{}_{}.h5py".format(depositoryName, fileId),"r") as f_log:
            #Saving the mesh
            xmesh_w = np.array(f_log["xmesh_w"])
            zmesh_w = np.array(f_log["zmesh_w"])

    meshgridWarped_z,meshgridWarped_x = np.meshgrid(zmesh_w,xmesh_w)
    meshgridWarped_yx,meshgridWarped_xy = np.meshgrid(xmesh_w,xmesh_w)
    
    path_DiagnosticsLog_rect = "../Run5_rect/Logs/DiagnosticsLog_127.h5py"
    with h5py.File(path_DiagnosticsLog_rect,"r") as f_log:
            #Saving the mesh
            xmesh_lab = np.array(f_log["xmesh_lab"])
            zmesh_lab = np.array(f_log["zmesh_lab"])
    meshgridLab_z,meshgridLab_x = np.meshgrid(zmesh_lab,xmesh_lab)
    

    y_i_clac = ny_calc/2 # y_i slice
    # print("y_slice = {}".format(y_w[y_i]))

    bx_warped_2d = np.array([[bx_warped_final_3d[i_x][y_i_clac][i_z] for i_z in range(nz_calc)] for i_x in range(nx_calc)])
    by_warped_2d = np.array([[by_warped_final_3d[i_x][y_i_clac][i_z] for i_z in range(nz_calc)] for i_x in range(nx_calc)])
    bz_warped_2d = np.array([[bz_warped_final_3d[i_x][y_i_clac][i_z] for i_z in range(nz_calc)] for i_x in range(nx_calc)])

    # plt.figure("warped_prop_bz")
    # plt.contourf(meshgridWarped_z,meshgridWarped_x,bz_warped_2d,50)
    # plt.colorbar()

    print(np.shape(bx_warped_2d))
    print(np.shape(bz_warped_2d))
    print(np.shape(zmesh_w))
    print(np.shape(xmesh_w))
    if plotWarped_flag:
        plt.figure("Warped")
        plt.streamplot(meshgridWarped_z,meshgridWarped_x,bz_warped_2d,bx_warped_2d,color = "red", linewidth=1, cmap=plt.cm.inferno, density=2, arrowstyle='->', arrowsize=1.5)
        # plt.figure("xy")
        # plt.streamplot(meshgridWarped_yx,meshgridWarped_xy,by_warped_final_3d[::,::,0],bx_warped_final_3d[::,::,0],color = "red", linewidth=1, cmap=plt.cm.inferno, density=2, arrowstyle='->', arrowsize=1.5)
        plt.figure("Lab")
        plt.streamplot(meshgridLab_z,meshgridLab_x,bz_Lab_final_3d[::,int(ny_lab/2),::],bx_Lab_final_3d[::,int(ny_lab/2),::],color = "red", linewidth=1, cmap=plt.cm.inferno, density=2, arrowstyle='->', arrowsize=1.5)


    # for y_i in range(0,ny_calc,10):
    #     bx_warped_2d_y = np.array([[bx_warped_final_3d[i_x][y_i][i_z] for i_z in range(nz_calc)] for i_x in range(nx_calc)])
    #     by_warped_2d_y = np.array([[by_warped_final_3d[i_x][y_i][i_z] for i_z in range(nz_calc)] for i_x in range(nx_calc)])
    #     bz_warped_2d_y = np.array([[bz_warped_final_3d[i_x][y_i][i_z] for i_z in range(nz_calc)] for i_x in range(nx_calc)])
    #     plt.figure("warped_bz_y={}".format(xmesh_w[y_i]))
    #     plt.contourf(meshgridWarped_z,meshgridWarped_x,bz_warped_2d_y,50)
    #     plt.colorbar()

    #     plt.figure("warped_bx_y={}".format(xmesh_w[y_i]))
    #     plt.contourf(meshgridWarped_z,meshgridWarped_x,bx_warped_2d_y,50)
    #     plt.colorbar()

    #     plt.figure("warped_by_y={}".format(y_i))
    #     plt.contourf(meshgridWarped_z,meshgridWarped_x,by_warped_2d_y,50)
    #     plt.colorbar()

def plotMagneticFieldCADOriginal(fName_BfieldCADReshaped):
    with h5py.File(fName_BfieldCADReshaped,"r") as Bfile_in:
        print(Bfile_in.keys())
        x_CAD = np.array(Bfile_in["x_CAD"])
        y_CAD = np.array(Bfile_in["y_CAD"])
        z_CAD = np.array(Bfile_in["z_CAD"])
        bx_CAD = np.array(Bfile_in["bx_CAD"])
        by_CAD = np.array(Bfile_in["by_CAD"])
        bz_CAD = np.array(Bfile_in["bz_CAD"])
    nx_CAD,ny_CAD,nz_CAD = np.shape(bx_CAD)
    print("np.shape(bx_CAD) = {}".format(np.shape(bx_CAD)))
    print("np.shape(x_CAD) = {}".format(np.shape(x_CAD)))

    meshgridCAD_yx,meshgridCAD_xy = np.meshgrid(y_CAD,x_CAD)
    meshgridCAD_zx,meshgridCAD_xz = np.meshgrid(z_CAD,x_CAD)
    meshgridCAD_zy,meshgridCAD_yz = np.meshgrid(z_CAD,y_CAD)
    y_i = len(y_CAD)/2 # y_i slice
    print("y_slice = {}".format(y_CAD[y_i]))
    print(np.shape(bz_CAD))
    bx_CAD_2d_y = np.array([[bx_CAD[i_x][y_i][i_z] for i_z in range(nz_CAD)] for i_x in range(nx_CAD)])
    by_CAD_2d_y = np.array([[by_CAD[i_x][y_i][i_z] for i_z in range(nz_CAD)] for i_x in range(nx_CAD)])
    bz_CAD_2d_y = np.array([[bz_CAD[i_x][y_i][i_z] for i_z in range(nz_CAD)] for i_x in range(nx_CAD)])

    plt.figure("CAD_bz_y")
    plt.contourf(meshgridCAD_zx,meshgridCAD_xz,bz_CAD_2d_y,50)
    plt.colorbar()

    plt.figure("CAD_bx_y")
    plt.contourf(meshgridCAD_zx,meshgridCAD_xz,bx_CAD_2d_y,50)
    plt.colorbar()

    plt.figure("CAD_by_y")
    plt.contourf(meshgridCAD_zx,meshgridCAD_xz,by_CAD_2d_y,50)
    plt.colorbar()

    # for y_i in range(0,len(y_CAD),10):
    #     bx_CAD_2d_y = np.array([[bx_CAD[i_x][y_i][i_z] for i_z in range(nz_CAD)] for i_x in range(nx_CAD)])
    #     by_CAD_2d_y = np.array([[by_CAD[i_x][y_i][i_z] for i_z in range(nz_CAD)] for i_x in range(nx_CAD)])
    #     bz_CAD_2d_y = np.array([[bz_CAD[i_x][y_i][i_z] for i_z in range(nz_CAD)] for i_x in range(nx_CAD)])
    #     plt.figure("CAD_bz_y={}".format(y_CAD[y_i]))
    #     plt.contourf(meshgridCAD_zx,meshgridCAD_xz,bz_CAD_2d_y,50)
    #     plt.colorbar()

    #     plt.figure("CAD_bx_y={}".format(y_CAD[y_i]))
    #     plt.contourf(meshgridCAD_zx,meshgridCAD_xz,bx_CAD_2d_y,50)
    #     plt.colorbar()

    #     plt.figure("CAD_by_y={}".format(y_CAD[y_i]))
    #     plt.contourf(meshgridCAD_zx,meshgridCAD_xz,by_CAD_2d_y,50)
    #     plt.colorbar()

    # plt.figure("CAD_bzx")
    # plt.streamplot(meshgridCAD_zx,meshgridCAD_xz,bz_CAD_2d_y,bx_CAD_2d_y, color = "red", linewidth=1, cmap=plt.cm.inferno, density=2, arrowstyle='->', arrowsize=1.5)

    # plt.figure("CAD_bzy")
    # plt.streamplot(meshgridCAD_zx,meshgridCAD_xz,bz_CAD_2d_y,by_CAD_2d_y, color = "red", linewidth=1, cmap=plt.cm.inferno, density=2, arrowstyle='->', arrowsize=1.5)

    # x_i = len(x_CAD)/2 # y_i slice
    # print("x_slice = {}".format(x_CAD[x_i]))
    # print(np.shape(bz_CAD))
    # bx_CAD_2d_x = np.array([[bx_CAD[x_i][i_y][i_z] for i_z in range(nz_CAD)] for i_y in range(ny_CAD)])
    # by_CAD_2d_x = np.array([[by_CAD[x_i][i_y][i_z] for i_z in range(nz_CAD)] for i_y in range(ny_CAD)])
    # bz_CAD_2d_x = np.array([[bz_CAD[x_i][i_y][i_z] for i_z in range(nz_CAD)] for i_y in range(ny_CAD)])

    # z_i = 450 # y_i slice
    # print("z_slice = {}".format(z_CAD[z_i]))
    # print(np.shape(bz_CAD))
    # bx_CAD_2d_z = np.array([[bx_CAD[i_x][i_y][z_i] for i_y in range(ny_CAD)] for i_x in range(nx_CAD)])
    # by_CAD_2d_z = np.array([[by_CAD[i_x][i_y][z_i] for i_y in range(ny_CAD)] for i_x in range(nx_CAD)])
    # bz_CAD_2d_z = np.array([[bz_CAD[i_x][i_y][z_i] for i_y in range(ny_CAD)] for i_x in range(nx_CAD)])


    # plt.figure("CAD_bx_z")
    # plt.contourf(meshgridCAD_xy,meshgridCAD_yx,bx_CAD_2d_z,50)
    # plt.colorbar()

    # plt.figure("CAD_by_z")
    # plt.contourf(meshgridCAD_xy,meshgridCAD_yx,by_CAD_2d_z,50)
    # plt.colorbar()

    # plt.figure("CAD_bz_z")
    # plt.contourf(meshgridCAD_xy,meshgridCAD_yx,bz_CAD_2d_z,50)
    # plt.colorbar()

    # plt.figure("CAD_bz_x")
    # plt.contourf(meshgridCAD_zy,meshgridCAD_yz,bz_CAD_2d_x,50)
    # plt.colorbar()

    # plt.figure("CAD_bx_x")
    # plt.contourf(meshgridCAD_zy,meshgridCAD_yz,bx_CAD_2d_x,50)
    # plt.colorbar()


    # plt.figure("CAD_bzx_xhalf")
    # plt.streamplot(meshgridCAD_zy,meshgridCAD_yz,bz_CAD_2d_x,bx_CAD_2d_x, color = "red", linewidth=1, cmap=plt.cm.inferno, density=2, arrowstyle='->', arrowsize=1.5)

    # plt.figure("CAD_bzy_xhalf")
    # plt.streamplot(meshgridCAD_zy,meshgridCAD_yz,bz_CAD_2d_x,by_CAD_2d_x, color = "red", linewidth=1, cmap=plt.cm.inferno, density=2, arrowstyle='->', arrowsize=1.5)

    # plt.quiver(meshgridCAD_z[::5,::5],meshgridCAD_x[::5,::5],bz_CAD_2d[::5,::5],by_CAD_2d[::5,::5], units = "width")

def JoinParticleDataFiles(name_FilesToJoin, name_FileToSave):

    x = np.array([])
    y = np.array([])
    z = np.array([])
    vx = np.array([])
    vy = np.array([])
    vz = np.array([])
    kinEn = np.array([])

    for name_FileToJoin in name_FilesToJoin:
        with h5py.File(name_FileToJoin,"r") as f_partdata:
            x = np.append(x,np.array(f_partdata["x"]))
            y = np.append(y,np.array(f_partdata["y"]))
            z = np.append(z,np.array(f_partdata["z"]))
            vx = np.append(vx,np.array(f_partdata["vx"]))
            vy = np.append(vy,np.array(f_partdata["vy"]))
            vz = np.append(vz,np.array(f_partdata["vz"]))
            kinEn = np.append(kinEn,np.array(f_partdata["KinEn"]))
            print(np.array(f_partdata["x"]))

    # filesToJoin = [h5py.File(name_FileToJoin,"r") for name_FileToJoin in name_FilesToJoin]
    # for processId1 in filesToJoin:
    #     for processId2 in filesToJoin:
    #         if processId1 != processId2:
    #             if (np.array(processId1["x"]) == np.array(processId2["x"])).all():
    #                 print("wierd, x[{}] == x[{}]".format(processId1,processId2))
    #             if (np.array(processId1["y"]) == np.array(processId2["y"])).all():
    #                 print("wierd, y[{}] == y[{}]".format(processId1,processId2))
    #             if (np.array(processId1["z"]) == np.array(processId2["z"])).all():
    #                 print("wierd, z[{}] == z[{}]".format(processId1,processId2))
    #             if (np.array(processId1["vx"]) == np.array(processId2["vx"])).all():
    #                 print("wierd, vx[{}] == vx[{}]".format(processId1,processId2))
    #             if (np.array(processId1["vx"]) == np.array(processId2["vx"])).all():
    #                 print("wierd, vx[{}] == vx[{}]".format(processId1,processId2))
    #             if (np.array(processId1["vz"]) == np.array(processId2["vz"])).all():
    #                 print("wierd, vz[{}] == vz[{}]".format(processId1,processId2))
    #             if (np.array(processId1["KinEn"]) == np.array(processId2["KinEn"])).all():
    #                 print("wierd, kinEn[{}] == KinEn[{}]".format(processId1,processId2))

    with h5py.File(name_FileToSave,"w") as f_partdata:
        dx_ds = f_partdata.create_dataset("x", data = x.ravel())
        dy_ds = f_partdata.create_dataset("y", data = y.ravel())
        dz_ds = f_partdata.create_dataset("z", data = z.ravel())
        dvx_ds = f_partdata.create_dataset("vx", data = vx.ravel())
        dvy_ds = f_partdata.create_dataset("vy", data = vy.ravel())
        dvz_ds = f_partdata.create_dataset("vz", data = vz.ravel())
        dkinEn_ds = f_partdata.create_dataset("KinEn", data = kinEn.ravel())

