import numpy as np
import matplotlib.pyplot as plt
from ParticleAndMesh_ClassHeader import Particle, Mesh_point
from Plot_diagnostics_Header import CalculateAngularVelocity, CalculateRadialVelocity
from CoordinationTransformationsHeader import WarpedToCAD_point
from SelfieldsCalculation_Header import PPDistance3D
# Should in the calculations of the current density be the charge weighted?


def CalculateRho(mesh3D,particles_a,dx,dy = 0,dz = 0,q=1,parbar = None):
    if dz==0: dz = dx
    if dy==0: dy = dx
    dxdydz=dx*dy*dz
    x = np.array([particle.x for particle in particles_a]) 
    y = np.array([particle.y for particle in particles_a])
    z = np.array([particle.z for particle in particles_a])
    nz,ny,nx = np.shape(mesh3D)

    for i_z in np.arange(nz):
        zFilterUpp = np.where(z<=mesh3D[i_z,0,0].z+dz)
        zFilter = zFilterUpp
        zFilterDown = np.where(z[zFilter] >= mesh3D[i_z,0,0].z-dz)
        zFilter = (zFilter[0][zFilterDown],)
        mean_x_warp = np.mean([closeParticle1.x for closeParticle1 in particles_a[zFilter]])

        for i_y in np.arange(ny):
            yFilter = zFilter
            yFilterUpp = np.where(y[yFilter]<=mesh3D[i_z,i_y,0].y+dy)
            yFilter = (yFilter[0][yFilterUpp],)
            yFilterDown = np.where(y[yFilter] >= mesh3D[i_z,i_y,0].y-dy)
            yFilter = (yFilter[0][yFilterDown],)
            for i_x in np.arange(nx):
                xFilter = yFilter
                mesh3D[i_z,i_y,i_x].x = mesh3D[i_z,i_y,i_x].x + mean_x_warp 
                if parbar is not None:
                    parbar.update(1)
                xFilterUpp = np.where(x[xFilter]<=mesh3D[i_z,i_y,i_x].x+dx)
                xFilter = (xFilter[0][xFilterUpp],)
                xFilterDown = np.where(x[xFilter] >= mesh3D[i_z,i_y,i_x].x-dx)
                xFilter = (xFilter[0][xFilterDown],)
                # filtering particles closeby


                # linear weighting
                for closeParticle in particles_a[xFilter]:
                    mesh3D[i_z,i_y,i_x].Rho += q*(dx - abs(closeParticle.x-mesh3D[i_z,i_y,i_x].x))*(dy - abs(closeParticle.y-mesh3D[i_z,i_y,i_x].y))*(dz - abs(closeParticle.z-mesh3D[i_z,i_y,i_x].z))/dxdydz 
                mesh3D[i_z,i_y,i_x].Rho = mesh3D[i_z,i_y,i_x].Rho/dxdydz # m
        # print("x,y,z = ({},{},{}) => Rho,Jx,Jy,Jz = ({},{},{})".format(mesh_point.x,mesh_point.y,mesh_point.z,mesh_point.Rho,mesh_point.Jx,mesh_point.Jy,mesh_point.Jz))

def CalculateRhoJ(mesh_a,particles_a,dx,dy = 0,dz = 0,q=1,parbar = None):
    if dz==0: dz = dx
    if dy==0: dy = dx
    dxdydz=dx*dy*dz
    x = np.array([particle.x for particle in particles_a])
    y = np.array([particle.y for particle in particles_a])
    z = np.array([particle.z for particle in particles_a])
    for mesh_point in mesh_a:
        if parbar is not None:
            parbar.update(1)
        # filtering particles closeby
        xFilterUpp = np.where(x<=mesh_point.x+dx)
        Filter = xFilterUpp
        xFilterDown = np.where(x[Filter] >= mesh_point.x-dx)
        Filter = (Filter[0][xFilterDown],)

        yFilterUpp = np.where(y[Filter]<=mesh_point.y+dy)
        Filter = (Filter[0][yFilterUpp],)
        yFilterDown = np.where(y[Filter] >= mesh_point.y-dy)
        Filter = (Filter[0][yFilterDown],)

        zFilterUpp = np.where(z[Filter]<=mesh_point.z+dz)
        Filter = (Filter[0][zFilterUpp],)
        zFilterDown = np.where(z[Filter] >= mesh_point.z-dz)
        Filter = (Filter[0][zFilterDown],)
        # linear weighting
        for closeParticle in particles_a[Filter]:
            mesh_point.Rho += q*(dx - abs(closeParticle.x-mesh_point.x))*(dy - abs(closeParticle.y-mesh_point.y))*(dz - abs(closeParticle.z-mesh_point.z))/dxdydz 
            mesh_point.Jx += q*closeParticle.vx*(dx - abs(closeParticle.x-mesh_point.x))*(dy - abs(closeParticle.y-mesh_point.y))*(dz - abs(closeParticle.z-mesh_point.z))/dxdydz 
            mesh_point.Jy += q*closeParticle.vy*(dx - abs(closeParticle.x-mesh_point.x))*(dy - abs(closeParticle.y-mesh_point.y))*(dz - abs(closeParticle.z-mesh_point.z))/dxdydz 
            mesh_point.Jz += q*closeParticle.vz*(dx - abs(closeParticle.x-mesh_point.x))*(dy - abs(closeParticle.y-mesh_point.y))*(dz - abs(closeParticle.z-mesh_point.z))/dxdydz 
        mesh_point.Rho = mesh_point.Rho/dxdydz # m
        mesh_point.Jx = mesh_point.Jx/dxdydz # m
        mesh_point.Jy = mesh_point.Jy/dxdydz # m
        mesh_point.Jz = mesh_point.Jz/dxdydz # m
        # print("x,y,z = ({},{},{}) => Rho,Jx,Jy,Jz = ({},{},{})".format(mesh_point.x,mesh_point.y,mesh_point.z,mesh_point.Rho,mesh_point.Jx,mesh_point.Jy,mesh_point.Jz))

def CalculateI(mesh_a,particles_a,dz,q=1,parbar = None):
    x = np.array([particle.x for particle in particles_a])
    y = np.array([particle.y for particle in particles_a])
    z = np.array([particle.z for particle in particles_a])
    for mesh_point in mesh_a:
        if parbar is not None:
            parbar.update(1)
        # filtering particles closeby
        zFilterUpp = np.where(z<=mesh_point.z+dz/2.)
        Filter = zFilterUpp
        zFilterDown = np.where(z[Filter] >= mesh_point.z-dz/2.)
        Filter = (Filter[0][zFilterDown],)
        numOfCloseParticles = len(z[Filter])

        for closeParticle in particles_a[Filter]:
            mesh_point.mean_vx += closeParticle.vx
            mesh_point.mean_vy += closeParticle.vy
            mesh_point.mean_vz += closeParticle.vz

        if numOfCloseParticles != 0:
            mesh_point.mean_vx = mesh_point.mean_vx/numOfCloseParticles
            mesh_point.mean_vy = mesh_point.mean_vy/numOfCloseParticles
            mesh_point.mean_vz = mesh_point.mean_vz/numOfCloseParticles
        else:
            mesh_point.mean_vx = 0
            mesh_point.mean_vy = 0
            mesh_point.mean_vz = 0
        mesh_point.qPm = q*numOfCloseParticles/dz

        mesh_point.Ix = mesh_point.mean_vx*mesh_point.qPm # m
        mesh_point.Iy = mesh_point.mean_vy*mesh_point.qPm # m
        mesh_point.Iz = mesh_point.mean_vz*mesh_point.qPm # m
        # print("x,y,z = ({},{},{}) => Rho,Ix,Iy,Iz = ({},{},{},{})".format(mesh_point.x,mesh_point.y,mesh_point.z,mesh_point.Rho,mesh_point.Ix,mesh_point.Iy,mesh_point.Iz))

def CalculateIinf(mesh_a,particles_a,dz,q=1,parbar = None):
    x = np.array([particle.x for particle in particles_a])
    y = np.array([particle.y for particle in particles_a])
    z = np.array([particle.z for particle in particles_a])
    for mesh_point in mesh_a:
        if parbar is not None:
            parbar.update(1)
        # filtering particles closeby
        zFilterUpp = np.where(z<=mesh_point.z+dz/10.)
        Filter = zFilterUpp
        zFilterDown = np.where(z[Filter] >= mesh_point.z-dz/10.)
        Filter = (Filter[0][zFilterDown],)
        numOfCloseParticles = len(z[Filter])

        for closeParticle in particles_a[Filter]:
            mesh_point.mean_vx += closeParticle.vx
            mesh_point.mean_vy += closeParticle.vy
            mesh_point.mean_vz += closeParticle.vz

        if numOfCloseParticles != 0:
            mesh_point.mean_vx = mesh_point.mean_vx/numOfCloseParticles
            mesh_point.mean_vy = mesh_point.mean_vy/numOfCloseParticles
            mesh_point.mean_vz = mesh_point.mean_vz/numOfCloseParticles
        else:
            mesh_point.mean_vx = 0
            mesh_point.mean_vy = 0
            mesh_point.mean_vz = 0
        mesh_point.qPm = q*numOfCloseParticles/(dz/5.)

        mesh_point.Ix = mesh_point.mean_vx*mesh_point.qPm # m
        mesh_point.Iy = mesh_point.mean_vy*mesh_point.qPm # m
        mesh_point.Iz = mesh_point.mean_vz*mesh_point.qPm # m
        # print("x,y,z = ({},{},{}) => Rho,Ix,Iy,Iz = ({},{},{},{})".format(mesh_point.x,mesh_point.y,mesh_point.z,mesh_point.Rho,mesh_point.Ix,mesh_point.Iy,mesh_point.Iz))

def CalculateIalt(mesh_a,particles_a,dz,q=1,parbar = None):
    x = np.array([particle.x for particle in particles_a])
    y = np.array([particle.y for particle in particles_a])
    z = np.array([particle.z for particle in particles_a])
    for mesh_point in mesh_a:
        if parbar is not None:
            parbar.update(1)
        # filtering particles closeby
        zFilterUpp = np.where(z<=mesh_point.z+dz/2.)
        Filter = zFilterUpp
        zFilterDown = np.where(z[Filter] >= mesh_point.z-dz/2.)
        Filter = (Filter[0][zFilterDown],)

        for closeParticle in particles_a[Filter]:
            mesh_point.vx_av += closeParticle.vx
            mesh_point.vy_av += closeParticle.vy
            mesh_point.vz_av += closeParticle.vz
        mesh_point.Ix = q*vx_av/dz # m
        mesh_point.Iy = q*vy_av/dz # m
        mesh_point.Iz = q*vz_av/dz # m
def CalculateT(mesh_a,particles_a,dz,q=1,ke0=1,mOk=1,parbar = None):
    x = np.array([particle.x for particle in particles_a])
    z = np.array([particle.z for particle in particles_a])
    for mesh_point in mesh_a:
        if parbar is not None:
            parbar.update(1)
        # filtering particles closeby
        zFilterUpp = np.where(z<=mesh_point.z+dz/2.)
        Filter = zFilterUpp
        zFilterDown = np.where(z[Filter] >= mesh_point.z-dz/2.)
        Filter = (Filter[0][zFilterDown],)
        vx_av,vy_av,vz_av,v_av,vtheta_av,vr_av,vm_av = 0,0,0,0,0,0,0
        vx_sq,vy_sq,vz_sq,v_sq,vtheta_sq,vr_sq,vm_sq = 0,0,0,0,0,0,0
        numOfCloseParticles = len(particles_a[Filter])
        for closeParticle in particles_a[Filter]:
            vtheta      = CalculateAngularVelocity(closeParticle.x,closeParticle.y,closeParticle.vx,closeParticle.vy)
            vr          = CalculateRadialVelocity(closeParticle.x,closeParticle.y,closeParticle.vx,closeParticle.vy)

            vx_av     += closeParticle.vx
            vy_av     += closeParticle.vy
            vz_av     += closeParticle.vz

            vtheta_av += vtheta
            vr_av     += vr
            # vm_av     += vr+vz

            vx_sq += (closeParticle.vx)**2
            vy_sq += (closeParticle.vy)**2
            vz_sq += (closeParticle.vz)**2

            vtheta_sq += vtheta**2
            vr_sq     += vr**2
            vm_sq     += vr**2+closeParticle.vz**2

            v_sq += (closeParticle.vx**2+closeParticle.vy**2+closeParticle.vz**2)

        # print("vz_sq/numOfCloseParticles,(vz_av/numOfCloseParticles)**2 = {},{}".format(vz_sq/numOfCloseParticles,(vz_av/numOfCloseParticles)**2))
        # print("vx_sq/numOfCloseParticles,(vx_av/numOfCloseParticles)**2 = {},{}".format(vx_sq/numOfCloseParticles,(vx_av/numOfCloseParticles)**2))
        # print("vy_sq/numOfCloseParticles,(vy_av/numOfCloseParticles)**2 = {},{}".format(vy_sq/numOfCloseParticles,(vy_av/numOfCloseParticles)**2))
        # print("vy_av/numOfCloseParticles = {}".format(vy_av/numOfCloseParticles))
        # print("vy_sq/numOfCloseParticlesn - (vy_av/numOfCloseParticles)**2 = {}".format(vy_sq/numOfCloseParticles - (vy_av/numOfCloseParticles)**2))
        # print("((vx_av/numOfCloseParticles)**2 + (vy_av/numOfCloseParticles)**2 )**1/2 = {}".format(((vx_av/numOfCloseParticles)**2 + (vy_av/numOfCloseParticles)**2)**(1./2.)))
        # print("vt_sq/numOfCloseParticles - ((vx_av/numOfCloseParticles)**2 + (vy_av/numOfCloseParticles)**2 ) = {}".format(vt_sq/numOfCloseParticles - ((vx_av/numOfCloseParticles)**2 + (vy_av/numOfCloseParticles)**2 )))
        # print("vx_sq/numOfCloseParticlesn - (vx_av/numOfCloseParticles)**2 = {}".format(vx_sq/numOfCloseParticles - (vx_av/numOfCloseParticles)**2))
        # print("vx_sq/numOfCloseParticlesn - (vx_av/numOfCloseParticles)**2 = {}".format(vx_sq/numOfCloseParticles - (vx_av/numOfCloseParticles)**2))
        # mesh_point.Tt = mOk*(vt_sq/numOfCloseParticles - ((vx_av/numOfCloseParticles)**2 + (vy_av/numOfCloseParticles)**2 ))
        mesh_point.T = mOk*(v_sq/numOfCloseParticles - ((vx_av/numOfCloseParticles)**2 + (vy_av/numOfCloseParticles)**2 + (vz_av/numOfCloseParticles)**2))
        mesh_point.Tz = mOk*(vz_sq/numOfCloseParticles - (vz_av/numOfCloseParticles)**2) # K
        mesh_point.Ty = mOk*(vy_sq/numOfCloseParticles - (vy_av/numOfCloseParticles)**2) # K
        mesh_point.Tx = mOk*(vx_sq/numOfCloseParticles - (vx_av/numOfCloseParticles)**2) # K
        mesh_point.Ttheta = mOk*(vtheta_sq/numOfCloseParticles - (vtheta_av/numOfCloseParticles)**2) # K
        mesh_point.Tr = mOk*(vr_sq/numOfCloseParticles - (vr_av/numOfCloseParticles)**2) # K
        mesh_point.Tm = mOk*(vm_sq/numOfCloseParticles - ((vr_av/numOfCloseParticles)**2+(vm_av/numOfCloseParticles)**2)) # K
        n = numOfCloseParticles/(dz*max(x)**2*np.pi*0.71973564) #0.71973564 is the 1-outer/inner radius ratio
        mesh_point.Lambda = (mesh_point.T*ke0/(q**2*n))**(1./2.)
        # print("\nn,T = ({},{})".format(n,mesh_point.T))
        # print("x,y,z = ({},{},{}) => Rho,Ix,Iy,Iz = ({},{},{},{})".format(mesh_point.x,mesh_point.y,mesh_point.z,mesh_point.Rho,mesh_point.Ix,mesh_point.Iy,mesh_point.Iz))

def CalculateTApprox(mesh_a,particles_a,dz,q=1,ke0=1,tConst=1,parbar = None):
    x = np.array([particle.x for particle in particles_a])
    z = np.array([particle.z for particle in particles_a])
    n_av = 0
    for mesh_point in mesh_a:
        if parbar is not None:
            parbar.update(1)
        # filtering particles closeby
        zFilterUpp = np.where(z<=mesh_point.z+dz/2.)
        Filter = zFilterUpp
        zFilterDown = np.where(z[Filter] >= mesh_point.z-dz/2.)
        Filter = (Filter[0][zFilterDown],)

        numOfCloseParticles = len(particles_a[Filter])

        n = numOfCloseParticles/(dz*max(x)**2*np.pi*0.71973564) #0.71973564 is the 1-inner/outer radius ratio
        # mesh_point.LambdaApprox = (tConst*ke0/(q**2*n))**(1./2.)
        # w = 2.0089e5
        w = 3.0448e5
        k = 1.38e-23
        e = 1.602e-19
        T = 1173.15
        print(n*w)
        mesh_point.LambdaApprox = (T*k/(4*np.pi*n*w*e**2))**(1./2.)
        # print(mesh_point.LambdaApprox)
        # n_av += n
    # print(n_av/len(mesh_a))
        # print("\nn,T = ({},{})".format(n,mesh_point.T))
        # print("x,y,z = ({},{},{}) => Rho,Ix,Iy,Iz = ({},{},{},{})".format(mesh_point.x,mesh_point.y,mesh_point.z,mesh_point.Rho,mesh_point.Ix,mesh_point.Iy,mesh_point.Iz))

def CalculateOmegaL(mesh_a,particles_a,dz,parbar = None):
    z = np.array([particle.z for particle in particles_a])
    for mesh_point in mesh_a:
        if parbar is not None:
            parbar.update(1)
        # filtering particles closeby
        zFilterUpp = np.where(z<=mesh_point.z+dz/2.)
        Filter = zFilterUpp
        zFilterDown = np.where(z[Filter] >= mesh_point.z-dz/2.)
        Filter = (Filter[0][zFilterDown],)

        numOfCloseParticles = len(particles_a[Filter])
        omegaL_av = 0 
        for closeParticle in particles_a[Filter]:
            vtheta      = CalculateAngularVelocity(closeParticle.x,closeParticle.y,closeParticle.vx,closeParticle.vy)
            omegaL_av  += abs(vtheta/(2.*np.pi*(closeParticle.x**2+closeParticle.y**2)**(1./2.)))
            # omegaL_av  += vtheta/(2.*np.pi*(closeParticle.x**2+closeParticle.y**2)**(1./2.))
        mesh_point.OmegaL = omegaL_av/numOfCloseParticles

def PlotVdistr(mesh_a,particles_a,dz,parbar = None):
    z = np.array([particle.z for particle in particles_a])
    for mesh_point in mesh_a:
        if parbar is not None:
            parbar.update(1)
        # filtering particles closeby
        zFilterUpp = np.where(z<=mesh_point.z+dz/2.)
        Filter = zFilterUpp
        zFilterDown = np.where(z[Filter] >= mesh_point.z-dz/2.)
        Filter = (Filter[0][zFilterDown],)

        v_sq = np.array([np.sqrt(closeParticle.vx**2+closeParticle.vy**2+closeParticle.vz**2) for closeParticle in particles_a[Filter]])
        plt.figure("Velocity distributions, z = {}".format(mesh_point.z))
        plt.hist(v_sq,20,label = "{} < z < {}".format(mesh_point.z-dz/2.,mesh_point.z+dz/2.))
        plt.xlabel("|v| [m/s]")
    # plt.legend()

def CalculateMeanXCAD(mesh_a,particles_a,dz,lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_s,bend_e,bend_r, parbar = None):
    z = np.array([particle.z for particle in particles_a])
    for mesh_point in mesh_a:
        if parbar is not None:
            parbar.update(1)
        # filtering particles closeby
        zFilterUpp = np.where(z<=mesh_point.z+dz/2.)
        Filter = zFilterUpp
        zFilterDown = np.where(z[Filter] >= mesh_point.z-dz/2.)
        Filter = (Filter[0][zFilterDown],)

        mean_x_warp = np.mean([closeParticle.x for closeParticle in particles_a[Filter]])
        mean_z_warp = np.mean([closeParticle.z for closeParticle in particles_a[Filter]])
        mesh_point.mean_z, mesh_point.mean_x = WarpedToCAD_point(mean_z_warp,mean_x_warp,lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_s,bend_e,bend_r)
        mesh_point.mean_y = np.mean([closeParticle.y for closeParticle in particles_a[Filter]])

def CalculateMeanXWarp(mesh_a,particles_a,dz, parbar = None):
    z = np.array([particle.z for particle in particles_a])
    for mesh_point in mesh_a:
        if parbar is not None:
            parbar.update(1)
        # filtering particles closeby
        zFilterUpp = np.where(z<=mesh_point.z+dz/2.)
        Filter = zFilterUpp
        zFilterDown = np.where(z[Filter] >= mesh_point.z-dz/2.)
        Filter = (Filter[0][zFilterDown],)

        mesh_point.mean_x = np.mean([closeParticle.x for closeParticle in particles_a[Filter]])
        mesh_point.mean_y = np.mean([closeParticle.y for closeParticle in particles_a[Filter]])
        mesh_point.mean_z = np.mean([closeParticle.z for closeParticle in particles_a[Filter]])


def PlotAxisymetry(mesh_a,particles_a,dz,parbar = None):
    z = np.array([particle.z for particle in particles_a])
    x = np.array([particle.x for particle in particles_a])
    # z = np.array([particle.z for particle in particles_a])
    for mesh_point in mesh_a:
        if parbar is not None:
            parbar.update(1)
        # filtering particles closeby
        zFilterUpp = np.where(z<=mesh_point.z+dz/2.)
        Filter = zFilterUpp
        zFilterDown = np.where(z[Filter] >= mesh_point.z-dz/2.)
        Filter = (Filter[0][zFilterDown],)

        mean_x_warp = np.mean([closeParticle.x for closeParticle in particles_a[Filter]])
        mean_y_warp = np.mean([closeParticle.y for closeParticle in particles_a[Filter]])
        mesh_point.mean_x = mean_x_warp
        mesh_point.mean_y = mean_y_warp
        alpha = np.array([np.arctan2((closeParticle.x-mean_x_warp),(closeParticle.y-mean_y_warp)) for closeParticle in particles_a[Filter]])
        # print(list(alpha))
        numberOfBins = 10
        # numberOfParticles = np.zeros(numberOfBins)
        # for i_angle in np.arange(numberOfBins):
        #     alphaFilterDown = np.where(alpha >= 2.*np.pi*i_angle/numberOfBins)
        #     alphaFilterup = np.where(alpha[alphaFilterDown] <= 2.*np.pi*(i_angle+1)/numberOfBins)
        #     alphaFilter = (alphaFilterDown[0][alphaFilterup],)
        #     numberOfParticles[i_angle] = len(alpha[alphaFilter])
        # print(numberOfParticles)
        weights = np.array([1./float(len(alpha)) for i in np.arange(len(alpha))])
        plt.figure("Particle azimuthal angle distribution at z = {} +- {}".format(mesh_point.z, dz/2.))
        plt.hist(map(lambda x: x*180./np.pi,alpha),bins = numberOfBins, weights = weights, histtype = 'step')
        plt.xlabel("angle [degree]")
        # mean_z_warp = np.mean([closeParticle.z for closeParticle in particles_a[Filter]])




        # mesh_point.mean_z, mesh_point.mean_x = WarpedToCAD_point(mean_z_warp,mean_x_warp,lab0inCAD_z,lab0inCAD_x,angle_labToCAD,bend_s,bend_e,bend_r)


def CalculateMeanEzWarp(mesh_a,particles_a,dz, parbar = None):
    z = np.array([particle.z for particle in particles_a])
    for mesh_point in mesh_a:
        if parbar is not None:
            parbar.update(1)
        # filtering particles closeby
        zFilterUpp = np.where(z<=mesh_point.z+dz/2.)
        Filter = zFilterUpp
        zFilterDown = np.where(z[Filter] >= mesh_point.z-dz/2.)
        Filter = (Filter[0][zFilterDown],)
        c = 3.*10**8
        m = 512
        mesh_point.mean_Ekin_z = np.mean([1./2.*m*(closeParticle.vz/c)**2 for closeParticle in particles_a[Filter]])

def CalculateSelfEfield(mesh_a,particles_a,dz, ekw = 1, parbar = None):
    if parbar is not None:
        parbar.total = len(mesh_a)*3

    z = np.array([particle.z for particle in particles_a])
    for mesh_point in mesh_a:
        zFilterUpp = np.where(z<=mesh_point.z+dz/2.)
        Filter = zFilterUpp
        zFilterDown = np.where(z[Filter] >= mesh_point.z-dz/2.)
        Filter = (Filter[0][zFilterDown],)
        # filtering particles closeby
        mesh_point.mean_x = np.mean([particle.x for particle in particles_a[Filter]])
        mesh_point.mean_y = np.mean([particle.y for particle in particles_a[Filter]])

        mesh_point.Ex = ekw*np.sum([(mesh_point.mean_x - particle.x)/PPDistance3D(mesh_point.mean_x,mesh_point.mean_y,mesh_point.z,particle.x,particle.y,particle.z)**3 for particle in particles_a])
        if parbar is not None:
            parbar.update(1)

        mesh_point.Ey = ekw*np.sum([(mesh_point.mean_y - particle.y)/PPDistance3D(mesh_point.mean_x,mesh_point.mean_y,mesh_point.z,particle.x,particle.y,particle.z)**3 for particle in particles_a])
        if parbar is not None:
            parbar.update(1)

        mesh_point.Ez = ekw*np.sum([(mesh_point.z - particle.z)/PPDistance3D(mesh_point.mean_x,mesh_point.mean_y,mesh_point.z,particle.x,particle.y,particle.z)**3 for particle in particles_a])
        if parbar is not None:
            parbar.update(1)
